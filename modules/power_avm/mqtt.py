import os
import sys
import json
import time

try:
    import paho.mqtt.client as mqtt
except:
    os.popen(sys.executable + ' -m pip install paho-mqtt --upgrade').read()
    import paho.mqtt.client as mqtt

import power_avm.db as db

sl = None

def init(_sl):
    global sl, client
    sl = _sl
    ip = sl["cfg"]["const"]["mqtt"]
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect(ip, 1883, 60)
    client.loop_start()

def on_connect(client, userdata, flags, rc):
    if rc == 0:
       client.subscribe("sl/avm/+/p")
       client.subscribe("sl/avm/+/etot")

def on_message(client, userdata, msg):
    topic = "/".join(msg.topic.split("/")[2:])
    db.add(topic, json.loads(msg.payload.decode())["val"])

def stop():
    global client
    if client:
        client.disconnect()
        time.sleep(1)
        client.loop_stop()

