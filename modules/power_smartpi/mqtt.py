import os
import sys
import json
import time

try:
    import paho.mqtt.client as mqtt
except:
    os.popen(sys.executable + ' -m pip install paho-mqtt --upgrade').read()
    import paho.mqtt.client as mqtt

import power_smartpi.db as db

sl = None

def init(_sl):
    global sl, client
    sl = _sl
    ip = sl["cfg"]["const"]["mqtt"]["host"]
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect(ip, 1883, 60)
    client.loop_start()

def on_connect(client, userdata, flags, rc):
    if rc == 0:
       client.subscribe("smartpi/status/+")

def on_message(client, userdata, msg):
    topic = msg.topic.split("/")[2]
    if topic in db.values:
        db.values[topic]["value"] = db.values[topic]["value"] + json.loads(msg.payload.decode())
        db.values[topic]["count"] = db.values[topic]["count"] + 1
    else:
        db.values[topic] = {"count": 1, "value" : json.loads(msg.payload.decode())}

def stop():
    global client
    if client:
        client.disconnect()
        time.sleep(1)
        client.loop_stop()

