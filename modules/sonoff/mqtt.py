import os
import sys
import json
import time

try:
    import paho.mqtt.client as mqtt
except:
    os.popen(sys.executable + ' -m pip install paho-mqtt --upgrade').read()
    import paho.mqtt.client as mqtt

import sonoff.mqtt_master as mqtt_master
import sonoff.db as db

sl = None
client = None
ip = ""

def init(_sl):
    global sl, client
    sl = _sl
    ip = sl["cfg"]["const"]["mqtt"]["host"]
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect(ip, 1883, 60)
    client.loop_start()

def on_connect(client, userdata, flags, rc):
    if rc == 0:
       client.subscribe("tele/+/SENSOR")

def on_message(client, userdata, msg):
    topic = msg.topic.split("/")
    if "SENSOR" == topic[2]:
        _topic = topic[1]
        for mac in db.device:
            if db.device[mac]['name'] == _topic:
                if mac in db.friendly:
                    _topic = db.friendly[mac]
        payload = json.loads(msg.payload.decode())["ENERGY"]
        for entry in payload:
            if entry not in ["TotalStartTime", "Period", "ApparentPower", "ReactivePower"]:
                mqtt_master.publish(_topic+'/'+entry.lower(), payload[entry])

def stop():
    global client
    if client:
        client.disconnect()
        time.sleep(1)
        client.loop_stop()
