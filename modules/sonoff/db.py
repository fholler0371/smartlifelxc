import os
import sys
import json
import copy
from threading import Thread

try:
    import requests
except:
    os.popen(sys.executable + ' -m pip install requests --upgrade').read()
    import requests

device = {}
friendly = {}
sl = None
db = ""
dbf = ""

def init(_sl):
    global sl, db, device, dbf, friendly
    sl = _sl
    db = sl["cfg"]["dbpath"] + sl["cfg"]["name"] + "_scan.json"
    if os.path.isfile(db):
        f = open(db, "r")
        device = json.loads(f.read())
        f.close()
    dbf = sl["cfg"]["dbpath"] + sl["cfg"]["name"] + "_friendly.json"
    if os.path.isfile(dbf):
        f = open(dbf, "r")
        friendly = json.loads(f.read())
        f.close()

def set_friendly(data):
    global friendly, device, dbf
    friendly[data["mac"]] = data["friendly"]
    if device[data["mac"]]['name'] == data["friendly"]:
        del friendly[data["mac"]]
    f = open(dbf, "w")
    f.write(json.dumps(friendly))
    f.close()
    return []

def get_data():
    global device, friendly
    out = []
    for mac in device:
        d = copy.copy(device[mac])
        d['ip'] = '<a href="http://'+d['ip']+'" target="sonoff">'+d['ip']+'</a>'
        d['friendly'] = d['name']
        if mac in friendly:
            d['friendly'] = friendly[mac]
        out.append(d)
    out.sort(key=lambda x: x['name'].upper(), reverse=False)
    return out

def save():
    global db, device
    f = open(db, "w")
    f.write(json.dumps(device))
    f.close()

class ping(Thread):
    def __init__(self, mac, host):
        Thread.__init__(self)
        self.host = host
        self.mac = mac

    def run(self):
        if 0 == os.system("ping -c 1 -w2 " + self.host + " > /dev/null 2>&1"):
            ping_result(self.mac, 1)
        else:
            ping_result(self.mac, 0)

def ping_result(mac, mode):
    global device
    device[mac]['online'] = mode
    if mode == 1:
        try:
            url = "http://"+device[mac]['ip']+"/cm?cmnd=TelePeriod"
            r = requests.get(url)
            device[mac]['teleperiod'] = r.json()['TelePeriod']
        except:
            pass
        try:
            url = "http://"+device[mac]['ip']+"/cm?cmnd=LedState"
            r = requests.get(url)
            device[mac]['ledstate'] = r.json()['LedState']
        except:
            pass
        try:
            url = "http://"+device[mac]['ip']+"/cm?cmnd=PowerOnState"
            r = requests.get(url)
            device[mac]['poweronstate'] = r.json()['PowerOnState']
        except:
            pass
        try:
            url = "http://"+device[mac]['ip']+"/cm?cmnd=SetOption21"
            r = requests.get(url)
            device[mac]['powerreport'] = r.json()['SetOption21']
        except:
            pass
        try:
            url = "http://"+device[mac]['ip']+"/cm?cmnd=NtpServer1"
            r = requests.get(url)
            device[mac]['ntp'] = r.json()['NtpServer1']
        except:
            pass

def add(data):
    global device
    mac = data['mac']
    if mac in device:
        device[mac]['ip'] = data['ip']
        device[mac]['typ'] = data['typ']
        device[mac]['name'] = data['name']
        device[mac]['software'] = data['software']
        device[mac]['online'] = -1
    else:
        device[mac] = data
        device[mac]['online'] = -1
        device[mac]['teleperiod'] = -1
        device[mac]['ledstate'] = -1
        device[mac]['poweronstate'] = -1
        device[mac]['powerreport'] = 'Ox'
        device[mac]['ntp'] = ''
    th = ping(mac, device[mac]['ip'])
    th.start()
