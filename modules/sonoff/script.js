define (['module', 'jqxdata', 'jqxgrid', 'jqxgrid_selection', 'jqxgrid_sort', 'jqxgrid_edit', 'jqxbutton'], function(module) {
  var mod = {
    init: function() {
      html = '<div style="width:100%; height:100%; padding:5px;" id="sonoff"><div id="sonoff_grid"></div></div>'
      $('#client_area').html(html)
      $(window).off('resize', window.module.sonoff.resize)
      $(window).on('resize', window.sonoff.resize)
      window.module.sonoff.resize()
      window.module.sonoff.get()
    },
    get : function() {
      window.smcall({'client': 'sonoff', 'cmd':'get_data', 'data': {}}, function(data) {
        var source = {
          localdata: data.data,
          datatype: "array",
          datafields: [
            { name: 'name', type: 'string'},
            { name: 'friendly', type: 'string'},
            { name: 'typ', type: 'string'},
            { name: 'ip', type: 'string'},
            { name: 'mac', type: 'string'},
            { name: 'ntp', type: 'string'},
            { name: 'software', type: 'string'},
            { name: 'teleperiod', type: 'integer'},
            { name: 'ledstate', type: 'integer'},
            { name: 'online', type: 'integer'},
            { name: 'poweronstate', type: 'integer'},
            { name: 'powerreport', type: 'string'}
          ]
        }
        var dataAdapter = new $.jqx.dataAdapter(source, {
          downloadComplete: function (data, status, xhr) { },
          loadComplete: function (data) { },
          loadError: function (xhr, status, error) { }
        })
        $("#sonoff_grid").jqxGrid({
          width: '100%',
          height: '100%',
          source: dataAdapter,
          sortable: true,
          showtoolbar: true,
          rendertoolbar: function (statusbar) {
            var container = $("<div style='overflow: hidden; position: relative; margin: 5px;'></div>")
            var scanButton = $("<div style='float: left; margin-left: 5px;'><span style='margin-left: 4px; position: relative; top: -3px;'>Scannen</span></div>")
            var reloadButton = $("<div style='float: left; margin-left: 5px;'><span style='margin-left: 4px; position: relative; top: -3px;'>Neuladen</span></div>")
            container.append(scanButton)
            container.append(reloadButton)
            statusbar.append(container)
            scanButton.jqxButton({width: 65, height: 20 })
            scanButton.click(function() {
              window.smcall({'client': 'sonoff', 'cmd':'scan', 'data': {}}, function(data) {
                setTimeout(window.module.sonoff.get, 5000)
              })
            })
            reloadButton.jqxButton({width: 65, height: 20 })
            reloadButton.click(function() {
              window.module.sonoff.get()
            })
          },
          editable: true,
          columns: [
            { text: 'Bezeichnung', datafield: 'name', width:175, editable: false},
            { text: 'lokaler Name', datafield: 'friendly'},
            { text: 'Typ', datafield: 'typ', width: 125, editable: false},
            { text: 'IP-Adresse', datafield: 'ip', width: 125, editable: false},
            { text: 'MAC-Adresse', datafield: 'mac', width: 125, editable: false},
            { text: 'Zeit-Quelle', datafield: 'ntp', width: 175, editable: false},
            { text: 'Software', datafield: 'software', width: 100, editable: false},
            { text: 'Sendeinterval', datafield: 'teleperiod', width: 125, cellsalign: 'right', editable: false},
            { text: 'LED', datafield: 'ledstate', width: 75, cellsalign: 'right', editable: false},
            { text: 'Online', datafield: 'online', width: 75, cellsalign: 'center', editable: false},
            { text: 'Energie senden', datafield: 'powerreport', width: 125, cellsalign: 'center', editable: false},
            { text: 'Startmode', datafield: 'poweronstate', width: 100, cellsalign: 'right', editable: false}
          ]
        })
        $("#sonoff_grid").parent().css('padding', 0).css('overflow', 'hidden')
        $("#sonoff_grid").off('cellendedit')
        $("#sonoff_grid").on('cellendedit', function (event) {
          var friendly = event.args.value,
              mac = event.args.row.mac
          window.smcall({'client': 'sonoff', 'cmd':'set_friendly', 'data': {"mac": mac, "friendly": friendly}}, function(data) {})
        })
      })
    },
    stop : function() {
    },
    resize: function() {
      $('#client_area').children().height($('#client_area').height())
    }
  }
  mod['init_data'] = window.module_const[module.id]
  window.module.sonoff = mod
  return mod
})


