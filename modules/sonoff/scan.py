import os
import sys
import json
import time
from threading import Timer

try:
    import paho.mqtt.client as mqtt
except:
    os.popen(sys.executable + ' -m pip install paho-mqtt --upgrade').read()
    import paho.mqtt.client as mqtt

import sonoff.db as db

sl = None
ip = ""
client = None
cb = None

def init(_sl, _cb):
    global sl, ip, cb
    sl = _sl
    cb = _cb
    ip = sl["cfg"]["const"]["mqtt"]["host"]

def scan():
    global ip, client
    Timer(10, scan_stop).start()
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect(ip, 1883, 60)
    client.loop_start()

def scan_stop():
    global client
    client.disconnect()
    time.sleep(1)
    client.loop_stop()
    db.save()

def on_connect(client, userdata, flags, rc):
    if rc == 0:
       client.subscribe("tasmota/discovery/+/config")

def on_message(client, userdata, msg):
    global cb
    data = json.loads(msg.payload.decode())
    ip = data["ip"]
    typ = data["md"]
    topic = data["t"]
    sw = data["sw"]
    mac = data["mac"]
    cb({'ip': ip, 'typ': typ, 'name': topic, 'software': sw, 'mac': mac})
