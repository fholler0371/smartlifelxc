import sonoff.scan as scan
import sonoff.db as db
import sonoff.mqtt as mqtt
import sonoff.mqtt_master as mqtt_master
import sonoff.web as web

sl = None
data = {}

def init(_sl):
    global sl
    sl = _sl
    db.init(sl)
    mqtt.init(sl)
    mqtt_master.init(sl)
    scan.init(sl, scan_data)
    scan.scan()
    web.init(sl)

def scan_data(data):
    db.add(data)

def run():
    pass

def stop():
    mqtt.stop()
    mqtt_master.stop()
    web.stop()
