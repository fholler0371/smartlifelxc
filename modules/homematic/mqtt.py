import os
import sys
import json
import time

try:
    import paho.mqtt.client as mqtt
except:
    os.popen(sys.executable + ' -m pip install paho-mqtt --upgrade').read()
    import paho.mqtt.client as mqtt

import homematic.devices as devices
import homematic.mqtt_master as mqtt_master

sl = None
client = None

def init(_sl):
    global sl, client
    sl = _sl
    ip = sl["cfg"]["const"]["mqtt"]
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect(ip, 1883, 60)
    client.loop_start()

def on_connect(client, userdata, flags, rc):
    if rc == 0:
       client.subscribe("hm/status/#")

def on_message(client, userdata, msg):
    topic = msg.topic.split("/")
    try:
        payload = json.loads(msg.payload.decode())
        devices.add(payload["device"], payload["deviceType"])
        retain = True
        if 'press' in topic[-1:][0].lower():
            retain = False
        name = devices.devices[payload["device"]]['friendly']
        dev = devices.typen[payload["deviceType"]]['friendly'] 
        if name != payload["device"] and dev != payload["deviceType"]:
            topic = dev+'/'+ name +'/'+topic[-1:][0].lower()
            val = payload['val']
            mqtt_master.publish(topic, val, retain)
    except:
        pass

def stop():
    global client
    if client:
        client.disconnect()
        time.sleep(1)
        client.loop_stop()
