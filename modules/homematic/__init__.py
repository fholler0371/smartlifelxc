import homematic.mqtt as mqtt
import homematic.mqtt_master as mqtt_master
import homematic.devices as devices
import homematic.web as web

sl = None

def init(_sl):
    global sl
    sl = _sl
    devices.init(sl)
    mqtt_master.init(sl)
    mqtt.init(sl)
    web.init(sl)

def run():
    print('run')

def stop():
    web.stop()
    mqtt.stop()
    mqtt_master.stop()
    print('stop')
