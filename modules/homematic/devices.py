import os
import json
import copy

sl = None
devices = {}
typen = {}
db_devices = ''
db_typen = ''

def init(_sl):
    global sl, db_devices, devices, db_typen, typen
    sl = _sl
    db_devices = db = sl["cfg"]["dbpath"] + sl["cfg"]["name"] + "_devices.json"
    if os.path.isfile(db_devices):
        f = open(db_devices, "r")
        devices = json.loads(f.read())
        f.close()
    db_typen = db = sl["cfg"]["dbpath"] + sl["cfg"]["name"] + "_typen.json"
    if os.path.isfile(db_typen):
        f = open(db_typen, "r")
        typen = json.loads(f.read())
        f.close()

def get_types():
    global typen
    out = []
    for dev in typen:
        out.append(typen[dev])
    out.sort(key=lambda x: x['friendly'].upper(), reverse=False)
    return out

def set_types(data):
    global typen, db_typen
    if data['id'] in typen:
        typen[data['id']]['friendly'] = data['friendly']
        f = open(db_typen, "w")
        f.write(json.dumps(typen))
        f.close()
    return {}

def get_devices():
    global devices, typen
    out = []
    for dev in devices:
        out.append(copy.copy(devices[dev]))
        out[len(out)-1]['type'] = typen[out[len(out)-1]['type']]['friendly']
    out.sort(key=lambda x: x['friendly'].upper(), reverse=False)
    return out

def set_devices(data):
    global devices, db_devices
    if data['id'] in devices:
        devices[data['id']]['friendly'] = data['friendly']
        f = open(db_devices, "w")
        f.write(json.dumps(devices))
        f.close()
    return {}

def add(id, type):
    global devices, db_devices, typen, db_typen
    if not (id in devices):
        devices[id] = {'id': id, 'type': type, 'friendly': id}
        f = open(db_devices, "w")
        f.write(json.dumps(devices))
        f.close()
    if not (type in typen):
        typen[type] = {'type': type, 'friendly': type}
        f = open(db_typen, "w")
        f.write(json.dumps(typen))
        f.close()
