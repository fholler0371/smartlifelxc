define (['module', 'jqxtabs', 'jqxdata', 'jqxgrid', 'jqxgrid_selection', 'jqxgrid_sort', 'jqxgrid_edit'], function(module) {
  var mod = {
    init : function() {
      html = '<div style="width:100%; height:100%;"><div id="homematic_tab"><ul><li>Geräte</li><li>Typen</li></ul>'
      html += '<div><div id="homematic_devices" style="width:100%; height:100%;"></div></div>'
      html += '<div><div id="homematic_typen"></div></div>'
      $('#client_area').html(html)
      $(window).off('resize', window.module.homematic.resize)
      $(window).on('resize', window.module.homematic.resize)
      window.module.homematic.resize()
      $('#homematic_tab').jqxTabs({ width: '100%', height: '100%', position: 'top'})
      $('#homematic_tab').on('selected', function(ev) {
        var selectedTab = ev.args.item
        window.module.homematic.call_tab(selectedTab)
      })
      window.module.homematic.call_tab(0)
    },
    call_tab: function(id) {
      if (id == 0) {
        window.smcall({'client': 'homematic', 'cmd':'get_devices', 'data': {}}, function(data) {
          var source = {
            localdata: data.data,
            datatype: "array",
            datafields: [
              { name: 'id', type: 'string'},
              { name: 'type', type: 'string'},
              { name: 'friendly', type: 'string'}
            ]
          }
          var dataAdapter = new $.jqx.dataAdapter(source, {
            downloadComplete: function (data, status, xhr) { },
            loadComplete: function (data) { },
            loadError: function (xhr, status, error) { }
          })
          $("#homematic_devices").jqxGrid({
            width: '100%',
            height: '100%',
            source: dataAdapter,
            sortable: true,
            editable: true,
            columns: [
              { text: 'ID', datafield: 'id', width:200, editable: false},
              { text: 'Typ', datafield: 'type', width:200, editable: false},
              { text: 'lokaler Name', datafield: 'friendly'}
            ]
          })
          $("#homematic_devices").parent().css('padding', 0).css('overflow', 'hidden')
          $("#homematic_devices").off('cellendedit')
          $("#homematic_devices").on('cellendedit', function (event) {
            var friendly = event.args.value,
                id = event.args.row.id
            window.smcall({'client': 'homematic', 'cmd':'set_devices', 'data': {"id": id, "friendly": friendly}}, function(data) {})
          })
        })
      }
      if (id == 1) {
        window.smcall({'client': 'homematic', 'cmd':'get_types', 'data': {}}, function(data) {
          var source = {
            localdata: data.data,
            datatype: "array",
            datafields: [
              { name: 'type', type: 'string'},
              { name: 'friendly', type: 'string'}
            ]
          }
          var dataAdapter = new $.jqx.dataAdapter(source, {
            downloadComplete: function (data, status, xhr) { },
            loadComplete: function (data) { },
            loadError: function (xhr, status, error) { }
          })
          $("#homematic_typen").jqxGrid({
            width: '100%',
            height: '100%',
            source: dataAdapter,
            sortable: true,
            editable: true,
            columns: [
              { text: 'Typ', datafield: 'type', width:200, editable: false},
              { text: 'lokaler Name', datafield: 'friendly'}
            ]
          })
          $("#homematic_typen").parent().css('padding', 0).css('overflow', 'hidden')
          $("#homematic_typen").off('cellendedit')
          $("#homematic_typen").on('cellendedit', function (event) {
            var friendly = event.args.value,
                typ = event.args.row.type
            window.smcall({'client': 'homematic', 'cmd':'set_types', 'data': {"id": typ, "friendly": friendly}}, function(data) {})
          })
        })
      }


      console.log(id)
    },
    stop : function() {
    },
    resize: function() {
      $('#client_area').children().height($('#client_area').height())
    }
  }
  mod['init_data'] = window.module_const[module.id]
  window.module.homematic = mod
  return mod
})
