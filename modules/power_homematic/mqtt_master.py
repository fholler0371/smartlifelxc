import os
import sys
import time
import json

try:
    import paho.mqtt.client as mqtt
except:
    os.popen(sys.executable + ' -m pip install paho-mqtt --upgrade').read()
    import paho.mqtt.client as mqtt

sl = None
client = None

def init(_sl):
    global sl, host_name, ip, client
    sl = _sl
    host_name = sl["cfg"]["name"]
    ip = os.popen("hostname -I | awk '{print $1}'").read().split("\n")[0]
    client = mqtt.Client()
    client.connect("10.0.0.1", 1883, 60) #ip form bridge settings
    client.on_connect = on_connect
    client.on_disconnect = on_disconnect
    client.loop_start()

def on_connect(client, userdata, flags, rc):
    global sl, host_name, ip
    t = int(time.time())
    if rc == 0:
        client.will_set("sl/" + host_name + "/status/_status/online", payload=False, retain=True)
        client.publish("sl/" + host_name + "/status/_status/online", payload=True, retain=True)
        publish("status/_status/ip", ip)
        publish("status/_status/state", True)
    else:
        sl["log"].error("Keine Verbindung zum MQTT-Server "+str(rc))

def on_disconnect(client, userdata, rc):
    global log
    if rc != 0:
        client.reconnect()

def publish(topic, payload):
    global client, host_name
    t = int(time.time())
    try:
        if "val" in payload:
            isDict = True
        else:
            isDict = False
    except:
        isDict = False
    if not isDict:
        payload = {"val": payload}
    t = int(time.time())
    if not ("ts" in payload):
        payload["ts"] = t
    if not ("lc" in payload):
        payload["lc"] = payload["ts"]
    client.publish("sl/" + host_name + "/" + topic, payload=json.dumps(payload), retain=True)

def stop():
    global client
    if client:
        client.disconnect()
        time.sleep(1)
        client.loop_stop()

