import power_homematic.mqtt as mqtt
import power_homematic.mqtt_master as mqtt_master
import power_homematic.db as db
import power_homematic.web as web

sl = None

def init(_sl):
    global sl
    sl = _sl
    print('init')
    web.init(sl)
    db.init(sl)
    mqtt_master.init(sl)
    mqtt.init(sl)

def run():
    print('run')


def stop():
    print('stop')
    web.stop()
    db.stop()
    mqtt.stop()
    mqtt_master.stop()
