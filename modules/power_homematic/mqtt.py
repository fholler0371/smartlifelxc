import os
import sys
import json
import time

try:
    import paho.mqtt.client as mqtt
except:
    os.popen(sys.executable + ' -m pip install paho-mqtt --upgrade').read()
    import paho.mqtt.client as mqtt

import power_homematic.db as db

sl = None

def init(_sl):
    global sl, client
    sl = _sl
    ip = sl["cfg"]["const"]["mqtt"]
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect(ip, 1883, 60)
    client.loop_start()

def on_connect(client, userdata, flags, rc):
    if rc == 0:
       client.subscribe("sl/homematic/plug/+/current")
       client.subscribe("sl/homematic/plug/+/frequency")
       client.subscribe("sl/homematic/plug/+/power")
       client.subscribe("sl/homematic/plug/+/voltage")
       client.subscribe("sl/homematic/plug/+/energy_counter")

def on_message(client, userdata, msg):
    topic = "/".join(msg.topic.split("/")[3:]).replace("current", "i").replace("frequency", "f").replace("power", "p").replace("voltage", "v")
    topic = topic.replace("energy_counter", "etot")
    val = json.loads(msg.payload.decode())["val"]
    if topic.endswith('/i'):
        val = val/1000
    db.add(topic, val)

def stop():
    global client
    if client:
        client.disconnect()
        time.sleep(1)
        client.loop_stop()


