import timer.astro as astro

sl = None

def init(_sl):
    global sl
    sl = _sl

def get_cards():
   return [{"mod": "timer", "card": "sun", "name": "Sonne"},
           {"mod": "timer", "card": "moon", "name": "Mond"}]

def get_card(data):
   out = {}
   if (data["id"] == 'all'):
       out['sunrise'] = astro.sunrise_str
       out['sunrise_post'] = astro.sunrise_post_str
       out['sunset_pre'] = astro.sunset_pre_str
       out['sunset'] = astro.sunset_str
       out['moon'] = int(astro.moon/28*100)
       out['moon_state'] = "Neumond"
       if astro.moon > 7:
           out['moon_state'] = "erstes Viertel"
       if astro.moon > 14:
           out['moon_state'] = "Vollmond"
       if astro.moon > 21:
           out['moon_state'] = "letztes Viertel"
   return out
