import time
from threading import Timer

import timer.mqtt as mqtt
import timer.astro as astro
import timer.web as web

sl = None
min5 = 0
min15 = 0
min60 = 0
h3 = 0
h6 = 0
th = None

def init(_sl):
    global sl, min5, min15, min60, h3, h6
    sl = _sl
    t = int(time.time()/300)
    min5 = t
    t = int(t/3)
    min15 = t
    t = int(t/4)
    min60 = t
    t = int(t/3)
    h3 = t
    t = int(t/2)
    h6 = t
    mqtt.init(sl)
    astro.init(sl)
    web.init(sl)

def run():
    check_timer()

def stop():
    global th
    if th:
        th.cancel()
    astro.stop()
    web.stop()

def check_timer():
    global min5, min15, min60, h3, h6, th
    t = int(time.time()/300)
    if min5 != t:
        min5 = t
        mqtt.publish("status/5min", 1)
    w = (t+1) * 300 - time.time()
    th = Timer(w, check_timer)
    th.start()
    t = int(t/3)
    if min15 != t:
        min15 = t
        mqtt.publish("status/15min", 1)
    t = int(t/4)
    if min60 != t:
        min60 = t
        mqtt.publish("status/1h", 1)
        astro.calc()
    t = int(t/3)
    if h3 != t:
        h3 = t
        mqtt.publish("status/3h", 1)
    t = int(t/2)
    if h6 != t:
        h6 = t
        mqtt.publish("status/6h", 1)
