define(['module', 'packery'], function(module, Packery) {
  var timer = {
    t_update_1: null,
    t_update_2: null,
    wdata : { },
    wdata_last : 0,
    wdata_call : 0,
    init : function() {
      window.module.timer.sub = window.module.timer.init_data.p1
      var sub = window.module.timer.sub
      if (sub == "app") {
        html = '<div id="timer_cards" style="width:100%; height:100%; margin: 10px;" class="jqx-widget-content-custom-scheme">'
        $('#client_area').html(html)
      }
      window.module.timer.call_tab(0)
    },
    call_tab: function(id) {
      if (id == 0) {
        window.smcall({'client': 'timer.app', 'cmd':'get_cards', 'data': {}}, function(data) {
          window.module.timer.cards = data.data
          window.module.timer.cards_check_mod = 10
          window.module.timer.create_cards()
        })
      }
    },
    create_cards : function() {
      var ok = 1
      var cards = window.module.timer.cards
      var l = cards.length
      for (var i=0; i<l; i++) {
         if (!(typeof(window.module[cards[i].mod].get_card) === 'function')) {
           console.log("Modul not found: "+cards[i].mod)
           ok = 0
         }
      }
      if (ok == 1) {
        for (var i=0; i<l; i++) {
          window.module[cards[i].mod].get_card(cards[i], $("#timer_cards"))
          require( [ 'jquery-bridget/jquery-bridget' ], function( jQueryBridget ) {
            jQueryBridget( 'packery', Packery, $ );
            var grid = $('#timer_cards').packery({
              itemSelector: '.card',
              gutter: 10
            })
          })
        }
      } else {
        console.log("recheck")
      }
    },
    check_data : function() {
      var now = new Date()
      var t = Math.round(now.getTime() / 1000)
      if (t - window.module.timer.wdata_last > 120) {
        if (t - window.module.timer.wdata_call > 10) {
          window.module.timer.wdata_call = t
          window.smcall({'client': 'timer.app', 'cmd':'get_card', 'data': {'id': 'all'}}, function(data) {
            window.module.timer.wdata_last = t
            window.module.timer.wdata = data.data
          })
        }
      }
    },
    get_card : function(data, div) {
      if (data["card"] == "sun") {
        if (div.find(".cards_timer_1").length == 0) {
          var html = '<div class="cards_timer_1 card"><span></span></div>'
          div.append(html)
        } else {
          console.log(data)
        }
        window.module.timer.update_1()
      }
      if (data["card"] == "moon") {
        if (div.find(".cards_timer_2").length == 0) {
          var html = '<div class="cards_timer_2 card"><span></span></div>'
          div.append(html)
        } else {
          console.log(data)
        }
        window.module.timer.update_2()
      }
    },
    update_1 : function() {
      clearTimeout(window.module.timer.t_update_1)
      window.module.timer.check_data()
      var ele = $($("body").find('.cards_timer_1')[0])
      var old = ele.data('last')
      var data = window.module.timer.wdata
      if (!(data.sunrise == undefined)) {
        if (!(old == data)) {
          ele.data('last', data)
          var d = data
          html = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">Sonne</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 25px;">Sonnenaufgang:</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 45px;">Sonnenuntergang:</div>'
          html += '<div style=" position: absolute; right: 5px; top: 25px;">'+d.sunrise+' ('+d.sunrise_post+')</div>'
          html += '<div style=" position: absolute; right: 5px; top: 45px;">'+d.sunset+' ('+d.sunset_pre+')</div>'
          $("body").find('.cards_timer_1').html(html)
        }
        if ($("body").find('.cards_timer_1').length > 0) {
          window.module.timer.t_update_1 = setTimeout(window.module.timer.update_1, 10000)
        }
      } else {
        window.module.timer.t_update_1 = setTimeout(window.module.timer.update_1, 250)
      }
    },
    update_2 : function() {
      clearTimeout(window.module.timer.t_update_2)
      window.module.timer.check_data()
      var ele = $($("body").find('.cards_timer_2')[0])
      var old = ele.data('last')
      var data = window.module.timer.wdata
      if (!(data.moon == undefined)) {
        if (!(old == data)) {
          ele.data('last', data)
          var d = data
          html = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">Mond</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 25px;">Status:</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 45px;">Prozent:</div>'
          html += '<div style=" position: absolute; right: 5px; top: 25px;">'+d.moon_state+'</div>'
          html += '<div style=" position: absolute; right: 5px; top: 45px;">'+d.moon+' %</div>'
          $("body").find('.cards_timer_2').html(html)
        }
        if ($("body").find('.cards_timer_2').length > 0) {
          window.module.timer.t_update_2 = setTimeout(window.module.timer.update_2, 10000)
        }
      } else {
        window.module.timer.t_update_2 = setTimeout(window.module.timer.update_2, 250)
      }
    },
    stop : function() {
    },
    resize: function() {
      $('#client_area').children().height($('#client_area').height())
    }
  }
  timer['init_data'] = window.module_const[module.id]
  window.module.timer = timer
  return timer
})
