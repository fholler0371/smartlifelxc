from functools import partial
from http.server import HTTPServer, BaseHTTPRequestHandler
from socketserver import ThreadingMixIn
from threading import Thread
import json
import os

#import weather.remote as remote
import timer.cards as cards

############################################

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
     """Handle requests in a separate thread."""

class webserverHandler(BaseHTTPRequestHandler):
    def __init__(self, sl, app, *args, **kwargs):
        self.sl = sl
        self.app = app
        super().__init__(*args, **kwargs)
    def log_message(self, format, *args):
        self.sl["log"].debug("\n    %s - - [%s] %s" % (self.address_string(), self.log_date_time_string(),format%args))
    def do_POST(self):
        if self.path == "/data":
            try:
                content_len = int(self.headers['Content-Length'])
                data = json.loads(self.rfile.read(content_len).decode())
                data = json.dumps(self.app(data))
                self.send_response(200)
                self.send_header('Content-Type', 'application/json')
                self.send_header('Content-Length', str(len(data)))
                self.end_headers()
                self.wfile.write(data.encode())
            except:
                self.send_response(501)
                self.send_header('Content-Type', 'text')
                self.end_headers()
                self.wfile.write("Error 501".encode())
        elif self.path == "/rights":
            data = json.dumps(["timer.app"])
            self.send_response(200)
            self.send_header('Content-Type', 'application/json')
            self.send_header('Content-Length', str(len(data)))
            self.end_headers()
            self.wfile.write(data.encode())
        elif self.path == "/menu":
            content_len = int(self.headers['Content-Length'])
            data = json.loads(self.rfile.read(content_len).decode())
            if 'timer.app' in data:
                data = json.dumps([{'label': 'Zeitgeber', 'mod': 'timer', 'p1':'app'}])
            self.send_response(200)
            self.send_header('Content-Type', 'application/json')
            self.send_header('Content-Length', str(len(data)))
            self.end_headers()
            self.wfile.write(data.encode())
        elif self.path == "/script":
            f = open("home/.gitlab/smartlifelxc/modules/timer/script.js", "rb")
            text = f.read()
            self.send_response(200)
            self.send_header('Content-Type', 'application/javascript')
            self.send_header('Content-Length', str(len(text)))
            self.end_headers()
            self.wfile.write(text)
        else:
            self.send_response(404)
            self.send_header('Content-Type', 'text')
            self.end_headers()
            self.wfile.write("Not found 404".encode())


##############################################

def app(data):
    if data["client"] in ["timer.app"]:
        if data["cmd"] == "get_cards":
            return cards.get_cards()
        elif data["cmd"] == "get_card":
            return cards.get_card(data["data"])
    return []

##############################################

class loopThread(Thread):
    def __init__(self, server):
        Thread.__init__(self)
        self.server = server

    def run(self):
        self.server.serve_forever()

##############################################

def init(_sl):
    global sl, server, th
    sl = _sl
    handler = partial(webserverHandler, sl, app)
    server = ThreadedHTTPServer(('0.0.0.0', 80), handler)
    th = loopThread(server)
    th.start()

def stop():
    global server
    server.shutdown()

