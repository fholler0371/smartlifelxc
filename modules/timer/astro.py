import os
import sys
import time
from datetime import timedelta, datetime as dt
from threading import Timer

import timer.mqtt as mqtt

try:
    import astral
    from astral.sun import sun, golden_hour, blue_hour
    from astral.moon import phase
except:
    os.popen(sys.executable + ' -m pip install astral --upgrade').read()
    import astral
    from astral.sun import sun, golden_hour, blue_hour

sl = None
city = None
sun_data = None
tzinfo = ""
sunrise_str = ""
sunrise_th = None
sunrise_post_str = ""
sunrise_post_th = None
sunset_pre_str = ""
sunset_pre_th = None
sunset_str = ""
sunset_th = None
sunstate = ""
moon = 0

def init(_sl):
    global sl, city, tzinfo
    sl = _sl
    data = sl["cfg"]["const"]
    tzinfo = sl["cfg"]["const"]["tzinfo"]
    city = astral.LocationInfo(name=data["city"], region=data["country"], timezone=tzinfo, latitude=data["latitude"], longitude=data["longitude"])
    calc()


def calc():
    global sl, city, tzinfo, sunrise_str, sunrise_post_str, sunset_pre_str, sunset_str, sunstate, moon
    global sunrise_th, sunrise_post_th, sunset_pre_th, sunset_th
    t = int(time.time())
    now = dt.today()
    sunrise = astral.sun.sunrise(city.observer, date=now, tzinfo=tzinfo)
    if int(now.timestamp()) > int(sunrise.timestamp()):
        sunstate = "day"
        sunrise = astral.sun.sunrise(city.observer, date=(now + timedelta(days= 1)), tzinfo=tzinfo)
    sunrise_str = sunrise.strftime("%H:%M")
    mqtt.publish("status/sunrise/text", sunrise_str)
    mqtt.publish("status/sunrise/utc", int(sunrise.timestamp()))
    sunrise_post = golden_hour(city.observer, tzinfo=tzinfo, direction=astral.SunDirection.RISING, date=now)[1]
    if int(now.timestamp()) > int(sunrise_post.timestamp()):
        sunstate = "night"
        sunrise_post = golden_hour(city.observer, tzinfo=tzinfo, direction=astral.SunDirection.RISING, date=(now + timedelta(days= 1)))[1]
    sunrise_post_str = sunrise_post.strftime("%H:%M")
    mqtt.publish("status/sunrise_post/text", sunrise_post_str)
    mqtt.publish("status/sunrise_post/utc", int(sunrise_post.timestamp()))
    sunset_pre = golden_hour(city.observer, tzinfo=tzinfo, direction=astral.SunDirection.SETTING, date=now)[0]
    if int(now.timestamp()) > int(sunset_pre.timestamp()):
        sunset_pre = golden_hour(city.observer, tzinfo=tzinfo, direction=astral.SunDirection.SETTING, date=(now + timedelta(days= 1)))[0]
    sunset_pre_str = sunset_pre.strftime("%H:%M")
    mqtt.publish("status/sunset_pre/text", sunset_pre_str)
    mqtt.publish("status/sunset_pre/utc", int(sunset_pre.timestamp()))
    sunset = astral.sun.sunset(city.observer, date=now, tzinfo=tzinfo)
    if int(now.timestamp()) > int(sunset.timestamp()):
        sunset = astral.sun.sunset(city.observer, date=(now + timedelta(days= 1)), tzinfo=tzinfo)
    sunset_str = sunset.strftime("%H:%M")
    mqtt.publish("status/sunset/text", sunset_str)
    mqtt.publish("status/sunset/utc", int(sunset.timestamp()))
    mqtt.publish("status/sun/state", sunstate)

    moon = round(phase(), 2)
    mqtt.publish("status/moon/state", moon)

    if sunrise_th:
        sunrise_th.cancel()
    sunrise_th = Timer(int(sunrise.timestamp()-t), sunrise_ev)
    sunrise_th.start()
    if sunrise_post_th:
        sunrise_post_th.cancel()
    sunrise_post_th = Timer(int(sunrise_post.timestamp()-t), sunrise_post_ev)
    sunrise_post_th.start()
    if sunset_pre_th:
        sunset_pre_th.cancel()
    sunset_pre_th = Timer(int(sunset_pre.timestamp()-t), sunset_pre_ev)
    sunset_pre_th.start()
    if sunset_th:
        sunset_th.cancel()
    sunset_th = Timer(int(sunset.timestamp()-t), sunset_ev)
    sunset_th.start()

def sunrise_ev():
    mqtt.publish("status/sunrise/event", 1, False)

def sunrise_post_ev():
    mqtt.publish("status/sunrise_post/event", 1, False)

def sunset_pre_ev():
    mqtt.publish("status/sunset_pre/event", 1, False)

def sunset_ev():
    mqtt.publish("status/sunset/event", 1, False)

def stop():
    global sunrise_th, sunrise_post_th, sunset_pre_th, sunset_th
    if sunrise_th:
        sunrise_th.cancel()
    if sunrise_post_th:
        sunrise_post_th.cancel()
    if sunset_pre_th:
        sunset_pre_th.cancel()
    if sunset_th:
        sunset_th.cancel()

