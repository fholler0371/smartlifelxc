import json

import wifiscan_base.scan as scan
import wifiscan_base.db as db
import wifiscan_base.mqtt as mqtt

sl = {}

def init(_sl):
    global sl
    sl = _sl
    sl["log"].info("init modul")
    try:
        dev = sl["cfg"]["device"]["ttyUSB"]
    except Exception as e:
       sl["log"].error(repr(e))
       sl["running"] = False
    try:
        scantime = sl["cfg"]["const"]["scantime"]
    except Exception as e:
       sl["log"].error(repr(e))
       sl["running"] = False
    if sl["running"]:
        mqtt.init(sl["log"], sl["cfg"]["name"])
        db.init(sl["log"], sl["cfg"]['dbpath'], sl["cfg"]["name"])
        scan.init(sl["log"], dev, scantime, scan_ap_data, scan_station_data)

def run():
    global sl
    sl["log"].info("run")
    scan.start()

def stop():
    global sl
    sl["log"].info("stop")
    scan.stop()
    db.stop()
    mqtt.stop()
    
def scan_ap_data(data):
    sl["log"].debug("scan_ap_data")
    db.aps_in(data)

def scan_station_data(data):
    sl["log"].debug("scan_station_data")
    pass
    db.stations_in(data)
