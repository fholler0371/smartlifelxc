import os
import sys
import time
import json
from threading import Thread 

try:
    import serial
except:
    os.popen(sys.executable + ' -m pip install pyserial --upgrade').read()
    import serial

log = None
th = None
dev = ""
scantime = 15
ap_cb = None
station_cb = None

class scanner(Thread):
    def __init__(self):
        Thread.__init__(self)
        global log
        self.running = True
        log.info("thread init")
        
    def run(self):
        global dev, scantime, ap_cb, station_cb
        log.info("thread run")
        while self.running:
            ser = serial.Serial(dev, 115200)
            ser.write('stopap\n'.encode())
            time.sleep(0.5)
            ser.write(('scan -t '+str(scantime)+'\n').encode())
            time.sleep(scantime+3)
            ser.read(ser.in_waiting)
            ser.write('print /scan.json\n'.encode())
            time.sleep(0.5)
            res = ser.read(ser.in_waiting).decode().split('\r\n')
            jdata = ""
            for rec in res:
                if rec.startswith("{"):
                    jdata = rec
            if jdata != "":
                data = json.loads(jdata)
            else:
                data = {}
            if "aps" in data and ap_cb:
                ap_cb(data["aps"])
            if "stations" in data and station_cb:
                station_cb(data["stations"])
            ser.close()
        log.info("thread stop")

def init(_log, _dev, _scantime, _ap_cb, _station_cb):
    global log, dev, scantime, ap_cb, station_cb
    log = _log
    log.info("init")
    dev = _dev
    scantime = _scantime
    ap_cb = _ap_cb
    station_cb = _station_cb
    
def start():
    global log, th
    if log == None:
        return
    log.info("run")
    th = scanner()
    th.start()

def stop():
    global log, th
    if log == None:
        return
    log.info("stop")
    if th:
        th.running = False
