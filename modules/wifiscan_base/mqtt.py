import os
import sys
import time
import json

try:
    import paho.mqtt.client as mqtt
except:
    os.popen(sys.executable + ' -m pip install paho-mqtt --upgrade').read()
    import paho.mqtt.client as mqtt

log = None
host_name = ""
client = None
ip = ""

def init(_log, _host_name):
    global log, host, client, ip, host_name
    log = _log
    log.debug("init")
    host_name = _host_name
    ip = os.popen("hostname -I | awk '{print $1}'").read().split("\n")[0]
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_disconnect = on_disconnect
    client.connect("10.0.0.1", 1883, 60) #ip form bridge settings
    client.loop_start()
    
def stop():
    global log, client, host_name
    log.debug("stop")
    client.publish("sl/" + host_name + "/status/_status/online", payload=False, retain=True)
    publish("status/_status/state", False)
    time.sleep(0.5)
    client.loop_stop()
    
def on_connect(client, userdata, flags, rc):
    global log, host_name, ip
    t = int(time.time())
    if rc == 0:
        client.will_set("sl/" + host_name + "/status/_status/online", payload=False, retain=True)
        client.publish("sl/" + host_name + "/status/_status/online", payload=True, retain=True)
        publish("status/_status/ip", ip)
        publish("status/_status/state", True)
    else:
        log.error("Keine Verbindung zum MQTT-Server "+str(rc))
        
def on_disconnect(client, userdata, rc):
    global log
    if rc != 0:
        client.reconnect()
        
        
def publish(topic, payload):
    global client, hostname
    t = int(time.time())
    try:
        if "val" in payload:
            isDict = True
        else:
            isDict = False
    except:
        isDict = False
    if not isDict:
        payload = {"val": payload}
    t = int(time.time())
    if not ("ts" in payload):
        payload["ts"] = t
    if not ("lc" in payload):
        payload["lc"] = payload["ts"]
    client.publish("sl/" + host_name + "/" + topic, payload=json.dumps(payload), retain=True)
