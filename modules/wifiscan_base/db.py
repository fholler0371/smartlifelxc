import json
import os
import time

from threading import Timer 

import wifiscan_base.mqtt as mqtt

save_interval = 6*60*60 # 6 Stunden

log = None
mac_db = []
aps = {}
aps_last = []
stations = {}
db_path = ""
host_name = ""
th_timer = None

def init(_log, _db_path, _host_name):
    global log, mac_db, db_path, host_name, save_interval, th_timer
    log = _log
    db_path = _db_path
    host_name = _host_name
    log.debug("init")
    mac_db_name = "/".join(os.path.abspath(__file__).split("/")[:-1])+'/macaddress.io-db.json'
    if os.path.isfile(mac_db_name):
        try:
            f = open(mac_db_name, "r")
            lines = f.read().split("\n")
            for line in lines:
                if line.startswith("{"):
                    mac_db.append(json.loads(line))
            f.close()
        except Exception as e:
            log.warning("Mac Datei konnte nicht geladen werden.")
    th_timer = Timer(save_interval, runTimer)
    th_timer.start()
            
def aps_in(data):
    global log, aps, aps_last
    log.debug("aps_in")
    aps_last = data
    for entry in data:
        is_new = False
        ssid = entry[0]
        channel = entry[2]
        rssi = entry[3]
        key = entry[4]
        mac = entry[5]
        vendor = entry[6]
        seen = int(time.time())
        if not(mac in aps):
            is_new = True
            aps[mac] = {"ssid": ssid, "channel": channel, "rssi": rssi, "key": key, "mac": mac, "vendor": vendor, "ven_in_db": -1, "last": 0, "seen": seen}
            log.debug("new ap: "+mac+", "+ssid)
        else:
            aps[mac]["rssi"] = rssi
            aps[mac]["channel"] = channel
            aps[mac]["key"] = key
            aps[mac]["seen"] = seen
    for mac in aps:
        if aps[mac]["ven_in_db"] == -1:
            vendor = get_mac_from_db(mac)
            if len(vendor) > 0:
                aps[mac]["vendor"] = vendor
                aps[mac]["ven_in_db"] = 1
            else:
                aps[mac]["ven_in_db"] = 0
    for mac in aps:
        ap = aps[mac]
        if aps[mac]["last"] == 0:
            mqtt.publish("status/ap/rec", {"val": {"ssid": ap["ssid"], "rssi": ap["rssi"], "key": ap["key"],
                                                   "mac": ap["mac"], "vendor": ap["vendor"]}, "ts": seen, "lc": seen})
            log.debug("new ap: "+mac+", "+aps[mac]["ssid"])
            aps[mac]["last"] = seen
        if aps[mac]["last"] < time.time()-300 and aps[mac]["seen"] > time.time()-900:
            mqtt.publish("status/ap/rec", {"val": {"ssid": ap["ssid"], "rssi": ap["rssi"], "key": ap["key"],
                                                   "mac": ap["mac"], "vendor": ap["vendor"]}, "ts": seen, "lc": ap["seen"]})
            aps[mac]["last"] = int(time.time())
    
def stations_in(data):
    global log, stations, aps_last, aps
    log.debug("stations_in")
    for station in data:
        mac = station[0]
        channel = station[1]
        vendor = station[3]
        packets = station[4]
        ap_id = station[5]
        time_text = station[6]
        ap = aps_last[ap_id][5]
        if time_text == "<1sec":
            time_seen = int(time.time())
        elif time_text == "<1min":
            time_seen = int(time.time()-60)
        else:
            log.warning("Unbekannte Zeitangabe: "+time_text)
            time_seen = int(time.time())
        if not(mac in stations):
            stations[mac] = {"mac": mac, "vendor": vendor, "ven_in_db": -1, "ap": ap, "last": 0, "seen": time_seen}
        else: 
            stations[mac]["ap"] = ap
            stations[mac]["seen"] = time_seen
    for mac in stations:
        if stations[mac]["ven_in_db"] == -1:
            vendor = get_mac_from_db(mac)
            if len(vendor) > 0:
                stations[mac]["vendor"] = vendor
                stations[mac]["ven_in_db"] = 1
            else:
                stations[mac]["ven_in_db"] = 0
    for mac in stations:
        station = stations[mac]
        ap_name = station["ap"]
        if ap_name in aps:
            ap_name = aps[ap_name]["ssid"]
        if stations[mac]["last"] == 0:
            mqtt.publish("status/station/rec", {"val": {"mac": station["mac"], "vendor": station["vendor"], "ap": ap_name},
                                                        "ts": int(time.time()), "lc": station["seen"]})
            log.debug("new station: "+mac+", "+stations[mac]["ap"])
            stations[mac]["last"] = int(time.time())
        if station["last"] < time.time()-15 and station["seen"] > time.time()-600:
            mqtt.publish("status/station/rec", {"val": {"mac": station["mac"], "vendor": station["vendor"], "ap": ap_name},
                                                        "ts": int(time.time()), "lc": station["seen"]})
            stations[mac]["last"] = int(time.time())
            
def stop():
    global log, th_timer
    log.debug("stop")
    if th_timer:
        th_timer.cancel()
    save_data()

def save_data():
    global log, host_name, stations, aps
    log.debug("save")
    name = db_path + host_name + "_stations.json"
    f = open(name, "w")
    f.write(json.dumps(stations, sort_keys=True, indent=2))
    f.close()
    name = db_path + host_name + "_aps.json"
    f = open(name, "w")
    f.write(json.dumps(aps, sort_keys=True, indent=2))
    f.close()

def get_mac_from_db(mac):
    global log, mac_db
    log.debug(mac) 
    for entry in mac_db:
        if mac.upper().startswith(entry["oui"]):
            return entry["companyName"]
    return ""

def runTimer():
    global th_timer, save_interval
    th_timer = Timer(save_interval, runTimer)
    th_timer.start()
    save_data()
