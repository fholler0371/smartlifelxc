define(['module', 'packery', 'jqxtabs', 'jqxdata', 'jqxgrid_selection', 'jqxgrid_edit', 'jqxgrid_sort', 'jqxdropdownlist'], function(module, Packery) {
  var power = {
    t_power_energy : null,
    t_power_samrtpi : null,
    t_power_diff : null,
    t_power_sonoff_m : null,
    t_power_sonoff_p : null,
    t_power_homematic_m : null,
    t_power_homematic_p : null,
    t_power_avm_m : null,
    t_power_avm_p : null,
    wdata : {
      energy: {}, smartpi: {}
    },
    wdata_last : 0,
    wdata_call : 0,
    init : function() {
      window.module.power.sub = window.module.power.init_data.p1
      var sub = window.module.power.sub
      if (sub == "app") {
        html = '<div id="power_cards" style="width:100%; height:100%; margin: 10px;" class="jqx-widget-content-custom-scheme">'
        $('#client_area').html(html)
      } else {
        html = '<div style="width:100%; height:100%;"><div id="power_tab"><ul><li>Karten</li><li>Werte</li><li>Geräte</li></ul>'
        html += '<div><div id="power_cards" style="width:100%; height:100%;"></div></div>'
        html += '<div><div id="power_table"></div></div>'
        html += '<div><div id="power_devices"></div></div>'
        $('#client_area').html(html+'</div>')
        $(window).off('resize', window.module.power.resize)
        $(window).on('resize', window.module.power.resize)
        window.module.power.resize()
        $('#power_tab').jqxTabs({ width: '100%', height: '100%', position: 'top'})
        $('#power_tab').off('selected')
        $('#power_tab').on('selected', function(ev) {
          var selectedTab = ev.args.item
          window.module.power.call_tab(selectedTab)
        })
      }
      window.module.power.call_tab(0)
    },
    check_data : function() {
      var now = new Date()
      var t = Math.round(now.getTime() / 1000)
      if (t - window.module.power.wdata_last > 120) {
        if (t - window.module.power.wdata_call > 10) {
          window.module.power.wdata_call = t
          window.smcall({'client': 'power.app', 'cmd':'get_card', 'data': {'id': 'all'}}, function(data) {
            window.module.power.wdata_last = t
            window.module.power.wdata = data.data
          })
        }
      }
    },
    update_smartpi: function() {
      clearTimeout(window.module.power.t_update_smartpi)
      window.module.power.check_data()
      var ele = $($("body").find('.cards_power_smartpi')[0])
      var old = ele.data('last')
      var data = window.module.power.wdata.smartpi
      if (data.u1 > -1) {
        if (!(old == data)) {
          ele.data('last', data)
          var d = data
          html = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">Messungen</div>'
          html += '<img src="/lib/img/energy.png" style="position: relative; top: 25px;">'
          html += '<table style=" position: absolute; right: 5px; float: right; top: 35px;"><td></td><td>L1</td><td>L2</td><td>L3</td><td>N</td></tr>'
          html += '<tr><td>U:</td><td align="right"><span'
          if (d.u1_chart.length > 0) {
            html += ' class="cards_chart"  data-chart="power.app:'+d.u1_chart+'"'
          }
          html += '>'+d.u1.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' V</span></td><td align="right"><span'
          if (d.u2_chart.length > 0) {
            html += ' class="cards_chart"  data-chart="power.app:'+d.u2_chart+'"'
          }
          html += '>'+d.u2.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' V</span></td><td align="right"><span'
          if (d.u3_chart.length > 0) {
            html += ' class="cards_chart"  data-chart="power.app:'+d.u3_chart+'"'
          }
          html += '>'+d.u3.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' V</span></td><td></td></tr>'
          html += '<tr><td>I:</td><td align="right"><span'
          if (d.i1_chart.length > 0) {
            html += ' class="cards_chart"  data-chart="power.app:'+d.i1_chart+'"'
          }
          html += '>'+d.i1.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2})+' A</span></td><td align="right"><span'
          if (d.i2_chart.length > 0) {
            html += ' class="cards_chart"  data-chart="power.app:'+d.i2_chart+'"'
          }
          html += '>'+d.i2.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2})+' A</span></td><td align="right"><span'
          if (d.i3_chart.length > 0) {
            html += ' class="cards_chart"  data-chart="power.app:'+d.i3_chart+'"'
          }
          html += '>'+d.i3.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2})+' A</span></td><td align="right"><span'
          if (d.i4_chart.length > 0) {
            html += ' class="cards_chart"  data-chart="power.app:'+d.i4_chart+'"'
          }
          html += '>'+d.i4.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2})+' A</span></td></tr>'
          html += '<tr><td>P:</td><td align="right" style="font-weight: bold;"><span'
          if (d.p1_chart.length > 0) {
            html += ' class="cards_chart"  data-chart="power.app:'+d.p1_chart+'"'
          }
          html += '>'+d.p1.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' W</span></td><td align="right" style="font-weight: bold;"><span'
          if (d.p2_chart.length > 0) {
            html += ' class="cards_chart"  data-chart="power.app:'+d.p2_chart+'"'
          }
          html += '>'+d.p2.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' W</span></td><td align="right" style="font-weight: bold;"><span'
          if (d.p3_chart.length > 0) {
            html += ' class="cards_chart"  data-chart="power.app:'+d.p3_chart+'"'
          }
          html += '>'+d.p3.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' W</span></td><td></td></tr>'
          html += '<tr><td>f:</td><td align="right"><span'
          if (d.f1_chart.length > 0) {
            html += ' class="cards_chart"  data-chart="power.app:'+d.f1_chart+'"'
          }
          html += '>'+d.f1.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2})+' Hz</span></td><td align="right"><span'
          if (d.f2_chart.length > 0) {
            html += ' class="cards_chart"  data-chart="power.app:'+d.f2_chart+'"'
          }
          html += '>'+d.f2.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2})+' Hz</span></td><td align="right"><span'
          if (d.f3_chart.length > 0) {
            html += ' class="cards_chart"  data-chart="power.app:'+d.f3_chart+'"'
          }
          html += '>'+d.f3.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2})+' Hz</span></td><td></td></tr>'
          html += '<tr><td>&phi;:</td><td align="right"><span'
          if (d.c1_chart.length > 0) {
            html += ' class="cards_chart"  data-chart="power.app:'+d.c1_chart+'"'
          }
          html += '>'+d.c1.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2})+'</span></td><td align="right"><span'
          if (d.c2_chart.length > 0) {
            html += ' class="cards_chart"  data-chart="power.app:'+d.c2_chart+'"'
          }
          html += '>'+d.c2.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2})+'</span></td><td align="right"><span'
          if (d.c3_chart.length > 0) {
            html += ' class="cards_chart"  data-chart="power.app:'+d.c3_chart+'"'
          }
          html += '>'+d.c3.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2})+'</span></td><td></td></tr>'
          html += '</table>'
          $("body").find('.cards_power_smartpi').html(html)
        }
        window.module.power.t_update_smartpi = setTimeout(window.module.power.update_smartpi, 1000)
      } else {
        window.module.power.t_update_smartpi = setTimeout(window.module.power.update_smartpi, 250)
      }
    },
    update_energy: function() {
      clearTimeout(window.module.power.t_update_energy)
      window.module.power.check_data()
      var ele = $($("body").find('.cards_power_energy')[0])
      var old = ele.data('last')
      var data = window.module.power.wdata.energy
      if (data.total > 0) {
        if (!(old == data)) {
          ele.data('last', data)
          var d = data
          html = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">Energie</div>'
          html += '<img src="/lib/img/energy.png" style="position: relative; top: 25px;">'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 35px;"><span  style="font-size: 150%; font-weight: 600;">'
          html += d.total.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kW/h</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 60px;"><span'
          if (d.power_chart.length > 0) {
            html += ' class="cards_chart"  data-chart="power.app:'+d.power_chart+'"'
          }
          html += '>'+d.power.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2})+' W</span></div>'
          html += '<table style=" position: absolute; right: 5px; float: right; top: 77px;">'
          html += '<tr><td>Tag</td><td align="right"><span style="font-weight: 600;">'
          html += d.day0.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td><td align="right"><span>'
          html += d.day1.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td></tr>'
          html += '<tr><td>Woche</td><td align="right"><span style="font-weight: 600;">'
          html += d.week0.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td><td align="right"><span>'
          html += d.week1.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td></tr>'
          html += '<tr><td>Monat</td><td align="right"><span style="font-weight: 600;">'
          html += d.month0.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td><td align="right"><span>'
          html += d.month1.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td></tr>'
          html += '<tr><td>Jahr</td><td align="right"><span style="font-weight: 600;">'
          html += d.year0.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td><td align="right"><span>'
          html += d.year1.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td></tr>'
          html += '</table>'
          $("body").find('.cards_power_energy').html(html)
        }
        window.module.power.t_update_energy = setTimeout(window.module.power.update_energy, 10000)
      } else {
        window.module.power.t_update_energy = setTimeout(window.module.power.update_energy, 250)
      }
    },
    update_diff: function() {
      clearTimeout(window.module.power.t_update_diff)
      window.module.power.check_data()
      var ele = $($("body").find('.cards_power_diff')[0])
      var old = ele.data('last')
      var data = window.module.power.wdata.diff
      if (data != undefined) {
        if (!(old == data)) {
          ele.data('last', data)
          var d = data
          html = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">Differenz</div>'
          html += '<img src="/lib/img/energy.png" style="position: relative; top: 12px; height: 48px; width: 48px">'
          html += '<table style=" position: absolute; right: 5px; float: right; top: 25px;">'
          html += '<tr><td>P1</td><td>P2</td><td>P3</td></tr>'
          html += '<tr><td align="right">'
          html += d.p1.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' W</td><td align="right">'
          html += d.p2.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' W</td><td align="right">'
          html += d.p3.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' W</td>'
          html += '<tr></table>'
          $("body").find('.cards_power_diff').html(html)
        }
        window.module.power.t_update_diff = setTimeout(window.module.power.update_diff, 10000)
      } else {
        window.module.power.t_update_diff = setTimeout(window.module.power.update_diff, 250)
      }
    },
    update_sonoff_m: function() {
      clearTimeout(window.module.power.t_update_sonoff_m)
      window.module.power.check_data()
      var eles = $("body").find('.cards_power_sonoff_m'),
          l = eles.length
      var ok = true
      for (var i=0; i<l; i++) {
        var ele = $(eles[i]),
            old = ele.data('last'),
            card = ele.data('card').split("_")
        card.shift()
        card = card.join("_")
        var d = window.module.power.wdata[card]
        if (d != undefined) {
          if (!(old == d)) {
            ele.data('last', d)
            html = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">'+d.label+'</div>'
            html += '<img src="/lib/img/tasmota.png" style="position: relative; top: 15px; left: 10px">'
            html += '<table style=" position: absolute; right: 5px; float: right; top: 25px;"><tr><td><b>P:</b></td><td align="right"><b><span'
            if (d.p_chart.length > 0) {
              html += ' class="cards_chart"  data-chart="power.app:'+d.p_chart+'"'
            }
            html += '>'+d.p.toLocaleString('de-DE', {minimumFractionDigits: 0, maximumFractionDigits: 0})+' W</span></b></td><td>&phi;:</td><td align="right"><span'
            if (d.cos_chart.length > 0) {
              html += ' class="cards_chart"  data-chart="power.app:'+d.cos_chart+'"'
            }
            html += '>'+d.cos.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2})+'</td></tr><tr><td>U:</td><td align="right"><span'
            if (d.u_chart.length > 0) {
              html += ' class="cards_chart"  data-chart="power.app:'+d.u_chart+'"'
            }
            html += '>'+d.u.toLocaleString('de-DE', {minimumFractionDigits: 0, maximumFractionDigits: 0})+' V</td><td>I:</td><td align="right"><span'
            if (d.i_chart.length > 0) {
              html += ' class="cards_chart"  data-chart="power.app:'+d.i_chart+'"'
            }
            html += '>'+d.i.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2})+' A</td></tr>'
            html += '</table>'
            ele.html(html)
          }
        } else {
          ok = false
        }
      }
      if (ok) {
        window.module.power.t_update_sonoff_m = setTimeout(window.module.power.update_sonoff_m, 10000)
      } else {
        window.module.power.t_update_sonoff_m = setTimeout(window.module.power.update_sonoff_m, 250)
      }
    },
    update_sonoff_p: function() {
      clearTimeout(window.module.power.t_update_sonoff_p)
      window.module.power.check_data()
      var eles = $("body").find('.cards_power_sonoff_p'),
          l = eles.length
      var ok = true
      for (var i=0; i<l; i++) {
        var ele = $(eles[i]),
            old = ele.data('last'),
            card = ele.data('card').split("_")
        card.shift()
        card = card.join("_")
        var d = window.module.power.wdata[card]
        if (d != undefined) {
          if (!(old == d)) {
            ele.data('last', d)
            html = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">'+d.label+'</div>'
            html += '<img src="/lib/img/tasmota.png" style="position: relative; top: 30px; left: 10px">'
            html += '<div style=" position: absolute; right: 5px; float: right; top: 35px;"><span  style="font-size: 150%; font-weight: 600;"'
            if (d.p_chart.length > 0) {
              html += ' class="cards_chart"  data-chart="power.app:'+d.p_chart+'"'
            }
            html += '>' + d.p.toLocaleString('de-DE', {minimumFractionDigits: 0, maximumFractionDigits: 0})+' W</span></div>'
            html += '<table style=" position: absolute; right: 5px; float: right; top: 65px;">'
            html += '<tr><td>Tag</td><td align="right"><span style="font-weight: 600;">'
            html += d.day0.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td><td align="right"><span>'
            html += d.day1.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td></tr>'
            html += '<tr><td>Woche</td><td align="right"><span style="font-weight: 600;">'
            html += d.week0.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td><td align="right"><span>'
            html += d.week1.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td></tr>'
            html += '<tr><td>Monat</td><td align="right"><span style="font-weight: 600;">'
            html += d.month0.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td><td align="right"><span>'
            html += d.month1.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td></tr>'
            html += '<tr><td>Jahr</td><td align="right"><span style="font-weight: 600;">'
            html += d.year0.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td><td align="right"><span>'
            html += d.year1.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td></tr>'
            html += '</table>'
            ele.html(html)
          }
        } else {
          ok = false
        }
      }
      if (ok) {
        window.module.power.t_update_sonoff_p = setTimeout(window.module.power.update_sonoff_p, 10000)
      } else {
        window.module.power.t_update_sonoff_p = setTimeout(window.module.power.update_sonoff_p, 250)
      }
    },
    update_homematic_m: function() {
      clearTimeout(window.module.power.t_update_homematic_m)
      window.module.power.check_data()
      var eles = $("body").find('.cards_power_homematic_m'),
          l = eles.length
      var ok = true
      for (var i=0; i<l; i++) {
        var ele = $(eles[i]),
            old = ele.data('last'),
            card = ele.data('card').split("_")
        card.shift()
        card = card.join("_")
        var d = window.module.power.wdata[card]
        if (d != undefined) {
          if (!(old == d)) {
            ele.data('last', d)
            html = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">'+d.label+'</div>'
            html += '<img src="/lib/img/hm_plug.png" style="position: relative; top: 15px; left: 10px">'
            html += '<table style=" position: absolute; right: 5px; float: right; top: 25px;"><tr><td><b>P:</b></td><td align="right"><b><span'
            if (d.p_chart.length > 0) {
              html += ' class="cards_chart"  data-chart="power.app:'+d.p_chart+'"'
            }
            html += '>'+d.p.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' W</span></b></td><td>f:</td><td align="right"><span'
            if (d.f_chart.length > 0) {
              html += ' class="cards_chart"  data-chart="power.app:'+d.f_chart+'"'
            }
            html += '>'+d.f.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2})+' Hz</td></tr><tr><td>U:</td><td align="right"><span'
            if (d.u_chart.length > 0) {
              html += ' class="cards_chart"  data-chart="power.app:'+d.u_chart+'"'
            }
            html += '>'+d.u.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' V</td><td>I:</td><td align="right"><span'
            if (d.i_chart.length > 0) {
              html += ' class="cards_chart"  data-chart="power.app:'+d.i_chart+'"'
            }
            html += '>'+d.i.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2})+' A</td></tr>'
            html += '</table>'
            ele.html(html)
          }
        } else {
          ok = false
        }
      }
      if (ok) {
        window.module.power.t_update_homematic_m = setTimeout(window.module.power.update_homematic_m, 10000)
      } else {
        window.module.power.t_update_homematic_m = setTimeout(window.module.power.update_homematic_m, 250)
      }
    },
    update_homematic_p: function() {
      clearTimeout(window.module.power.t_update_homematic_p)
      window.module.power.check_data()
      var eles = $("body").find('.cards_power_homematic_p'),
          l = eles.length
      var ok = true
      for (var i=0; i<l; i++) {
        var ele = $(eles[i]),
            old = ele.data('last'),
            card = ele.data('card').split("_")
        card.shift()
        card = card.join("_")
        var d = window.module.power.wdata[card]
        if (d != undefined) {
          if (!(old == d)) {
            ele.data('last', d)
            html = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">'+d.label+'</div>'
            html += '<img src="/lib/img/hm_plug.png" style="position: relative; top: 30px; left: 10px">'
            html += '<div style=" position: absolute; right: 5px; float: right; top: 35px;"><span  style="font-size: 150%; font-weight: 600;"'
            if (d.p_chart.length > 0) {
              html += ' class="cards_chart"  data-chart="power.app:'+d.p_chart+'"'
            }
            html += '>' + d.p.toLocaleString('de-DE', {minimumFractionDigits: 0, maximumFractionDigits: 0})+' W</span></div>'
            html += '<table style=" position: absolute; right: 5px; float: right; top: 65px;">'
            html += '<tr><td>Tag</td><td align="right"><span style="font-weight: 600;">'
            html += d.day0.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td><td align="right"><span>'
            html += d.day1.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td></tr>'
            html += '<tr><td>Woche</td><td align="right"><span style="font-weight: 600;">'
            html += d.week0.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td><td align="right"><span>'
            html += d.week1.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td></tr>'
            html += '<tr><td>Monat</td><td align="right"><span style="font-weight: 600;">'
            html += d.month0.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td><td align="right"><span>'
            html += d.month1.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td></tr>'
            html += '<tr><td>Jahr</td><td align="right"><span style="font-weight: 600;">'
            html += d.year0.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td><td align="right"><span>'
            html += d.year1.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td></tr>'
            html += '</table>'
            ele.html(html)
          }
        } else {
          ok = false
        }
      }
      if (ok) {
        window.module.power.t_update_homematic_p = setTimeout(window.module.power.update_homematic_p, 10000)
      } else {
        window.module.power.t_update_homematic_p = setTimeout(window.module.power.update_homematic_p, 250)
      }
    },
    update_avm_m: function() {
      clearTimeout(window.module.power.t_update_avm_m)
      window.module.power.check_data()
      var eles = $("body").find('.cards_power_avm_m'),
          l = eles.length
      var ok = true
      for (var i=0; i<l; i++) {
        var ele = $(eles[i]),
            old = ele.data('last'),
            card = ele.data('card').split("_")
        card.shift()
        card = card.join("_")
        var d = window.module.power.wdata[card]
        if (d != undefined) {
          if (!(old == d)) {
            ele.data('last', d)
            html = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">'+d.label+'</div>'
            html += '<img src="/lib/img/fritzpowerline.svg" style="position: relative; top: 15px; left: 10px; height: 45px">'
            html += '<table style=" position: absolute; right: 5px; float: right; top: 35px;"><tr><td><b>P:</b></td><td align="right"><b><span'
            if (d.p_chart.length > 0) {
              html += ' class="cards_chart"  data-chart="power.app:'+d.p_chart+'"'
            }
            html += '>'+d.p.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' W</span></b></td></tr>'
            html += '</table>'
            ele.html(html)
          }
        } else {
          ok = false
        }
      }
      if (ok) {
        window.module.power.t_update_avm_m = setTimeout(window.module.power.update_avm_m, 10000)
      } else {
        window.module.power.t_update_avm_m = setTimeout(window.module.power.update_avm_m, 250)
      }
    },
    update_avm_p: function() {
      clearTimeout(window.module.power.t_update_avm_p)
      window.module.power.check_data()
      var eles = $("body").find('.cards_power_avm_p'),
          l = eles.length
      var ok = true
      for (var i=0; i<l; i++) {
        var ele = $(eles[i]),
            old = ele.data('last'),
            card = ele.data('card').split("_")
        card.shift()
        card = card.join("_")
        var d = window.module.power.wdata[card]
        if (d != undefined) {
          if (!(old == d)) {
            ele.data('last', d)
            html = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">'+d.label+'</div>'
            html += '<img src="/lib/img/fritzpowerline.svg" style="position: relative; top: 30px; left: 10px; height: 45px">'
            html += '<div style=" position: absolute; right: 5px; float: right; top: 35px;"><span  style="font-size: 150%; font-weight: 600;"'
            if (d.p_chart.length > 0) {
              html += ' class="cards_chart"  data-chart="power.app:'+d.p_chart+'"'
            }
            html += '>' + d.p.toLocaleString('de-DE', {minimumFractionDigits: 0, maximumFractionDigits: 0})+' W</span></div>'
            html += '<table style=" position: absolute; right: 5px; float: right; top: 65px;">'
            html += '<tr><td>Tag</td><td align="right"><span style="font-weight: 600;">'
            html += d.day0.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td><td align="right"><span>'
            html += d.day1.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td></tr>'
            html += '<tr><td>Woche</td><td align="right"><span style="font-weight: 600;">'
            html += d.week0.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td><td align="right"><span>'
            html += d.week1.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td></tr>'
            html += '<tr><td>Monat</td><td align="right"><span style="font-weight: 600;">'
            html += d.month0.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td><td align="right"><span>'
            html += d.month1.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td></tr>'
            html += '<tr><td>Jahr</td><td align="right"><span style="font-weight: 600;">'
            html += d.year0.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td><td align="right"><span>'
            html += d.year1.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' kWh</span></td></tr>'
            html += '</table>'
            ele.html(html)
          }
        } else {
          ok = false
        }
      }
      if (ok) {
        window.module.power.t_update_avm_p = setTimeout(window.module.power.update_avm_p, 10000)
      } else {
        window.module.power.t_update_avm_p = setTimeout(window.module.power.update_avm_p, 250)
      }
    },
    get_card : function(data, div) {
      if (data["card"] == "power_energy") {
        if (div.find(".cards_power_energy").length == 0) {
          var html = '<div class="cards_power_energy card card_h2"><span></span></div>'
          div.append(html)
        } else {
          console.log(data)
        }
        window.module.power.update_energy()
      } else if (data["card"] == "power_smartpi") {
        if (div.find(".cards_power_smartpi").length == 0) {
          var html = '<div class="cards_power_smartpi card card_h2"><span></span></div>'
          div.append(html)
        } else {
          console.log(data)
        }
        window.module.power.update_smartpi()
      } else if (data["card"] == "power_diff") {
        if (div.find(".cards_power_diff").length == 0) {
          var html = '<div class="cards_power_diff card"><span></span></div>'
          div.append(html)
        } else {
          console.log(data)
        }
        window.module.power.update_diff()
      } else if (data["card"].startsWith("power_sonoff_m")) {
        if (div.find(".cards_"+data.card).length == 0) {
          var html = $('<div class="cards_power_sonoff_m cards_'+data.card+' card"><span></span></div>')
          html.data('card', data.card)
          div.append(html)
        }
        window.module.power.update_sonoff_m()
      } else if (data["card"].startsWith("power_sonoff_p")) {
        if (div.find(".cards_"+data.card).length == 0) {
          var html = $('<div class="cards_power_sonoff_p cards_'+data.card+' card card_h2"><span></span></div>')
          html.data('card', data.card)
          div.append(html)
        }
        window.module.power.update_sonoff_p()
      } else if (data["card"].startsWith("power_homematic_m")) {
        if (div.find(".cards_"+data.card).length == 0) {
          var html = $('<div class="cards_power_homematic_m cards_'+data.card+' card"><span></span></div>')
          html.data('card', data.card)
          div.append(html)
        }
        window.module.power.update_homematic_m()
      } else if (data["card"].startsWith("power_homematic_p")) {
        if (div.find(".cards_"+data.card).length == 0) {
          var html = $('<div class="cards_power_homematic_p cards_'+data.card+' card card_h2"><span></span></div>')
          html.data('card', data.card)
          div.append(html)
        }
        window.module.power.update_homematic_p()
      } else if (data["card"].startsWith("power_avm_m")) {
        if (div.find(".cards_"+data.card).length == 0) {
          var html = $('<div class="cards_power_avm_m cards_'+data.card+' card"><span></span></div>')
          html.data('card', data.card)
          div.append(html)
        }
        window.module.power.update_avm_m()
      } else if (data["card"].startsWith("power_avm_p")) {
        if (div.find(".cards_"+data.card).length == 0) {
          var html = $('<div class="cards_power_avm_p cards_'+data.card+' card card_h2"><span></span></div>')
          html.data('card', data.card)
          div.append(html)
        }
        window.module.power.update_avm_p()
      }
    },
    create_cards: function() {
      var ok = 1
      var cards = window.module.power.cards
      var l = cards.length
      for (var i=0; i<l; i++) {
         if (!(typeof(window.module[cards[i].mod].get_card) === 'function')) {
           console.log("Modul not found: "+cards[i].mod)
           ok = 0
         }
      }
      if (ok == 1) {
        for (var i=0; i<l; i++) {
          window.module[cards[i].mod].get_card(cards[i], $("#power_cards"))
          require( [ 'jquery-bridget/jquery-bridget' ], function( jQueryBridget ) {
            jQueryBridget( 'packery', Packery, $ );
            var grid = $('#power_cards').packery({
              itemSelector: '.card',
              gutter: 10
            })
          })
        }
      } else {
        console.log("recheck")
      }
    },
    call_tab: function(id) {
      if (id == 0) {
        window.smcall({'client': 'power.app', 'cmd':'get_cards', 'data': {}}, function(data) {
          window.module.power.cards = data.data
          window.module.power.cards_check_mod = 10
          window.module.power.create_cards()
        })
      }
      if (id == 1) {
        window.smcall({'client': 'power.full', 'cmd':'get_sensors', 'data': {}}, function(data) {
          var source = {
            localdata: data.data,
            datatype: "array",
            datafields: [
              { name: 'sensor', type: 'string'},
              { name: 'value', type: 'float'}
            ]
          }
          var dataAdapter = new $.jqx.dataAdapter(source, {
            downloadComplete: function (data, status, xhr) { },
            loadComplete: function (data) { },
            loadError: function (xhr, status, error) { }
          })
          var col = [
              { text: 'Sensor', datafield: 'sensor', editable: false},
              { text: 'Wert', datafield: 'value', width: 200, cellsalign: 'right', cellsformat: 'f2'}
            ]
          $("#power_table").jqxGrid({
            width: '100%',
            height: '100%',
            source: dataAdapter,
            sortable: true,
            columns: col
          })
          $("#power_table").parent().css('padding', 0).css('overflow', 'hidden')
        })
      }
      if (id == 2) {
        window.smcall({'client': 'power.full', 'cmd':'get_devices', 'data': {}}, function(data) {
          var source = {
            localdata: data.data,
            datatype: "array",
            datafields: [
              { name: 'channel', type: 'string'},
              { name: 'device', type: 'string'},
              { name: 'label', type: 'string'},
              { name: 'phase', type: 'integer'}
            ]
          }
          var dataAdapter = new $.jqx.dataAdapter(source, {
            downloadComplete: function (data, status, xhr) { },
            loadComplete: function (data) { },
            loadError: function (xhr, status, error) { }
          })
          var col = [
              { text: 'Typ', datafield: 'channel', width: 200, editable: false},
              { text: 'Gerät', datafield: 'device', width: 200, editable: false},
              { text: 'Bezeichnung', datafield: 'label'},
              { text: 'Phase', datafield: 'phase', columntype: 'dropdownlist', width: 200, cellsalign: 'center',
                createeditor: function (row, column, editor) {
                  var list = [
                    {value: 0},
                    {value: 1},
                    {value: 2},
                    {value: 3}
                  ]
                  editor.jqxDropDownList({ autoDropDownHeight: true, source: list, displayMember: 'value', valueMember: 'value' })
                }
              }
          ]
          $("#power_devices").jqxGrid({
            width: '100%',
            height: '100%',
            source: dataAdapter,
            sortable: true,
            editable: true,
            columns: col
          })
          $("#power_devices").parent().css('padding', 0).css('overflow', 'hidden')
          $("#power_devices").off('cellendedit')
          $("#power_devices").on('cellendedit', function (event) {
            var value = event.args.value,
                field = event.args.datafield,
                channel = event.args.row.channel,
                device = event.args.row.device
            if (value != "") {
              window.smcall({'client': 'power.full', 'cmd':'set_devices', 'data': {field: field, value: value, channel: channel, device: device}}, function(data) {})
            }
          })
        })
      }
    },
    stop : function() {
    },
    resize: function() {
      $('#client_area').children().height($('#client_area').height())
    }
  }
  power['init_data'] = window.module_const[module.id]
  window.module.power = power
  return power
})
