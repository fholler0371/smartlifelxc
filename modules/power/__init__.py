import power.mqtt_smartpi as mqtt_smartpi
import power.mqtt_sonoff as mqtt_sonoff
import power.mqtt_homematic as mqtt_homematic
import power.mqtt_avm as mqtt_avm
import power.mqtt_master as mqtt_master
import power.db as db
import power.db_devices as db_devices
import power.web as web
import power.cards as cards

sl = None

def init(_sl):
    global sl
    sl = _sl
    db_devices.init(sl)
    db.init(sl)
    web.init(sl)
    mqtt_master.init(sl)
    mqtt_smartpi.init(sl, callback)
    mqtt_sonoff.init(sl, callback)
    mqtt_homematic.init(sl, callback)
    mqtt_avm.init(sl, callback)
    cards.init(sl)

def callback(topic, value):
    db.add(topic, value)

def run():
    print("run")

def stop():
    mqtt_smartpi.stop()
    mqtt_sonoff.stop()
    mqtt_homematic.stop()
    mqtt_avm.stop()
    mqtt_master.stop()
    db.stop()
    web.stop()
    print("stop")
