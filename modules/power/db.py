import os
import json
import time
import sqlite3
from threading import Timer

import power.db_devices as dev

sl = None

values = {}
db_name = ""
db_sql = ""
th = None
last_day = 0
th_diff = None

def init(_sl):
    global sl, db_name, values, th, db_sql, th_diff
    sl = _sl
    db_name = sl["cfg"]["dbpath"] + sl["cfg"]["name"] + ".json"
    db_sql = sl["cfg"]["dbpath"] + sl["cfg"]["name"] + ".sqlite"
    if os.path.isfile(db_name):
        f = open(db_name, "r")
        values = json.loads(f.read())
        f.close()
    th = Timer(3600*6, run_th)
    th.start()
    th_diff = Timer(time.time()-int(time.time()/60)*60, diff)
    th_diff.start()

def diff():
    global values, th_diff
    th_diff = Timer(time.time()-int(time.time()/60)*60, diff)
    th_diff.start()
    i = 1
    while i < 4:
        if "main/p"+str(i) in values:
            val = values["main/p"+str(i)]
            for device in dev.devices:
                if device["phase"] == i:
                    d = device["device"]+"/p"
                    if d in values:
                        val -= values[d]
#                    else:
#                        print("No", d)
            val = float(("%.2f" % round(val, 2)))
            add("diff/p"+str(i), val)
        i += 1

def db_val(topic):
    global db_sql
    t = int(time.time()/86400)
    out = {}
    con = sqlite3.connect(db_sql)
    cur = con.cursor()
    out["day0"] = 0
    out["week0"] = 0
    out["month0"] = 0
    out["year0"] = 0
    out["day1"] = 0
    out["week1"] = 0
    out["month1"] = 0
    out["year1"] = 0
    sql = "select sum(value) from history where time>'"+str(((t-1)*86400)-1)+"' and topic='"+topic+"'"
    cur.execute(sql)
    row = cur.fetchone()
    if row[0] != None:
        out["day0"] = row[0]
    sql = "select sum(value) from history where time>'"+str(((t-7)*86400)-1)+"' and topic='"+topic+"'"
    cur.execute(sql)
    row = cur.fetchone()
    if row[0] != None:
        out["week0"] = row[0]
    sql = "select sum(value) from history where time>'"+str(((t-31)*86400)-1)+"' and topic='"+topic+"'"
    cur.execute(sql)
    row = cur.fetchone()
    if row[0] != None:
        out["month0"] = row[0]
    sql = "select sum(value) from history where time>'"+str(((t-365)*86400)-1)+"' and topic='"+topic+"'"
    cur.execute(sql)
    row = cur.fetchone()
    if row[0] != None:
        out["year0"] = row[0]
    sql = "select sum(value) from history where time>'"+str(((t-2)*86400)-1)+"' and time<'"+str(((t-1)*86400))+"' and topic='"+topic+"'"
    cur.execute(sql)
    row = cur.fetchone()
    if row[0] != None:
        out["day1"] = row[0]
    sql = "select sum(value) from history where time>'"+str(((t-14)*86400)-1)+"' and time<'"+str(((t-7)*86400))+"' and topic='"+topic+"'"
    cur.execute(sql)
    row = cur.fetchone()
    if row[0] != None:
        out["week1"] = row[0]
    sql = "select sum(value) from history where time>'"+str(((t-62)*86400)-1)+"' and time<'"+str(((t-31)*86400))+"' and topic='"+topic+"'"
    cur.execute(sql)
    row = cur.fetchone()
    if row[0] != None:
        out["month1"] = row[0]
    sql = "select sum(value) from history where time>'"+str(((t-730)*86400)-1)+"' and time<'"+str(((t-365)*86400))+"' and topic='"+topic+"'"
    cur.execute(sql)
    row = cur.fetchone()
    if row[0] != None:
        out["year1"] = row[0]
    con.close()
    return out

def web_get():
    global values
    out = []
    for entry in values:
        out.append({"sensor": entry, "value": values[entry]})
    out.sort(key=lambda x: x["sensor"].upper(), reverse=False)
    return out

def stop():
    save()

def add(topic, value):
    global values, last_day, db_sql
    values[topic] = value
    if topic == "main/ecday":
        t = (int(time.time()/86400)-1)*86400
        v = round(value/1000, 3)
        if True:
            con = sqlite3.connect(db_sql)
            cur = con.cursor()
            sql = "CREATE TABLE IF NOT EXISTS history (topic string, time integer, value float)"
            cur.execute(sql)
            con.commit()
            try:
                sql = "CREATE UNIQUE INDEX time ON history (time ASC, topic)"
                cur.execute(sql)
            except:
                pass
            try:
                sql = "insert into history (topic, time, value) values ('main', '"+str(t)+"', '"+str(v)+"')"
                cur.execute(sql)
            except:
                pass
            con.commit()
            con.close()
    elif topic.endswith('pday'):
        channel = topic.split('/')[0]
        t = (int(time.time()/86400)-1)*86400
        if True:
            con = sqlite3.connect(db_sql)
            cur = con.cursor()
            sql = "CREATE TABLE IF NOT EXISTS history (topic string, time integer, value float)"
            cur.execute(sql)
            con.commit()
            try:
                sql = "CREATE UNIQUE INDEX time ON history (time ASC, topic)"
                cur.execute(sql)
            except:
                pass
            try:
                sql = "insert into history (topic, time, value) values ('"+channel+"', '"+str(t)+"', '"+str(value)+"')"
                cur.execute(sql)
            except:
                pass
            con.commit()
            con.close()

def run_th():
    save()

def save():
    global db_name, values, th, th_diff
    if th:
        th.cancel()
    if th_diff:
        th_diff.cancel()
    f = open(db_name, "w")
    f.write(json.dumps(values))
    f.close()
