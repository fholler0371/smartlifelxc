import os
import json

sl = None
devices = []
db_name = ""

def init(_sl):
    global sl, db_name, devices
    sl = _sl
    db_name = sl["cfg"]["dbpath"] + sl["cfg"]["name"] + "_devices.json"
    if os.path.isfile(db_name):
        f = open(db_name, "r")
        devices = json.loads(f.read())
        f.close()


def add(device, channel):
    global devices, db_name
    found = False
    for dev in devices:
       if dev["device"] == device and dev["channel"] == channel:
         found = True
    if not found:
       devices.append({"device": device, "channel": channel, "label": device, "phase": 0})
       f = open(db_name, "w")
       f.write(json.dumps(devices))
       f.close()

def web_get():
    global devices
    devices.sort(key=lambda x: x["label"].upper(), reverse=False)
    return devices

def web_set(data):
    global devices, db_name
    l = len(devices)
    i = 0
    while i<l:
       if devices[i]["device"] == data["device"] and devices[i]["channel"] == data["channel"]:
           devices[i][data["field"]] = data["value"]
       i += 1
    f = open(db_name, "w")
    f.write(json.dumps(devices))
    f.close()
