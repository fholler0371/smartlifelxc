import json
import copy
from urllib import request

import power.db as db
import power.db_devices as db_devices

sl = None

def init(_sl):
    global sl
    sl = _sl

def get_cards():
   out = [{"mod": "power", "card": "power_energy", "name": "Energie"},
          {"mod": "power", "card": "power_smartpi", "name": "Energie SmartPi"},
          {"mod": "power", "card": "power_diff", "name": "Differenz SmartPi"}]
   db_devices.devices.sort(key=lambda x: x["label"].upper(), reverse=False)
   for device in db_devices.devices:
       if device["channel"] == "sonoff":
           out.append({"mod": "power", "card": "power_sonoff_m_"+device["device"], "name": device["label"]+" (L"+str(device["phase"])+")"})
   for device in db_devices.devices:
       if device["channel"] == "sonoff":
           out.append({"mod": "power", "card": "power_sonoff_p_"+device["device"], "name": device["label"]+" Power (L"+str(device["phase"])+")"})
   for device in db_devices.devices:
       if device["channel"] == "homematic":
           out.append({"mod": "power", "card": "power_homematic_m_"+device["device"], "name": device["label"]+" (L"+str(device["phase"])+")"})
   for device in db_devices.devices:
       if device["channel"] == "homematic":
           out.append({"mod": "power", "card": "power_homematic_p_"+device["device"], "name": device["label"]+" Power (L"+str(device["phase"])+")"})
   for device in db_devices.devices:
       if device["channel"] == "avm":
           out.append({"mod": "power", "card": "power_avm_m_"+device["device"], "name": device["label"]+" (L"+str(device["phase"])+")"})
   for device in db_devices.devices:
       if device["channel"] == "avm":
           out.append({"mod": "power", "card": "power_avm_p_"+device["device"], "name": device["label"]+" Power (L"+str(device["phase"])+")"})
   return out

def get_card(data):
    out = {"energy": {}, "smartpi": {}}
    out["energy"] = db.db_val('main')
    out["energy"]["total"] = 0
    out["energy"]["power"] = 0
    out["energy"]["power_chart"] = ""
    if "main/ectot" in db.values:
        out["energy"]["total"] = db.values["main/ectot"]/1000
    if "main/ptot" in db.values:
        out["energy"]["power"] = db.values["main/ptot"]
        out["energy"]["power_chart"] = "main/ptot:W"
    out["smartpi"]["u1"] = 0
    out["smartpi"]["u1_chart"] = ""
    out["smartpi"]["u2"] = 0
    out["smartpi"]["u2_chart"] = ""
    out["smartpi"]["u3"] = 0
    out["smartpi"]["u3_chart"] = ""
    out["smartpi"]["i1"] = 0
    out["smartpi"]["i1_chart"] = ""
    out["smartpi"]["i2"] = 0
    out["smartpi"]["i2_chart"] = ""
    out["smartpi"]["i3"] = 0
    out["smartpi"]["i3_chart"] = ""
    out["smartpi"]["i4"] = 0
    out["smartpi"]["i4_chart"] = ""
    out["smartpi"]["f1"] = 0
    out["smartpi"]["f1_chart"] = ""
    out["smartpi"]["f2"] = 0
    out["smartpi"]["f2_chart"] = ""
    out["smartpi"]["f3"] = 0
    out["smartpi"]["f3_chart"] = ""
    out["smartpi"]["p1"] = 0
    out["smartpi"]["p1_chart"] = ""
    out["smartpi"]["p2"] = 0
    out["smartpi"]["p2_chart"] = ""
    out["smartpi"]["p3"] = 0
    out["smartpi"]["p3_chart"] = ""
    out["smartpi"]["c1"] = 0
    out["smartpi"]["c1_chart"] = ""
    out["smartpi"]["c2"] = 0
    out["smartpi"]["c1_chart"] = ""
    out["smartpi"]["c3"] = 0
    out["smartpi"]["c1_chart"] = ""
    if "main/v1" in db.values:
        out["smartpi"]["u1"] = db.values["main/v1"]
        out["smartpi"]["u1_chart"] = "main/v1:W"
    if "main/v2" in db.values:
        out["smartpi"]["u2"] = db.values["main/v2"]
        out["smartpi"]["u2_chart"] = "main/v2:W"
    if "main/v3" in db.values:
        out["smartpi"]["u3"] = db.values["main/v3"]
        out["smartpi"]["u3_chart"] = "main/v3:W"
    if "main/i1" in db.values:
        out["smartpi"]["i1"] = db.values["main/i1"]
        out["smartpi"]["i1_chart"] = "main/i1:W"
    if "main/i2" in db.values:
        out["smartpi"]["i2"] = db.values["main/i2"]
        out["smartpi"]["i2_chart"] = "main/i2:W"
    if "main/i3" in db.values:
        out["smartpi"]["i3"] = db.values["main/i3"]
        out["smartpi"]["i3_chart"] = "main/i3:W"
    if "main/i4" in db.values:
        out["smartpi"]["i4"] = db.values["main/i4"]
        out["smartpi"]["i4_chart"] = "main/i4:W"
    if "main/f1" in db.values:
        out["smartpi"]["f1"] = db.values["main/f1"]
        out["smartpi"]["f1_chart"] = "main/f1:W"
    if "main/f2" in db.values:
        out["smartpi"]["f2"] = db.values["main/f2"]
        out["smartpi"]["f2_chart"] = "main/f2:W"
    if "main/f3" in db.values:
        out["smartpi"]["f3"] = db.values["main/f3"]
        out["smartpi"]["f3_chart"] = "main/f3:W"
    if "main/p1" in db.values:
        out["smartpi"]["p1"] = db.values["main/p1"]
        out["smartpi"]["p1_chart"] = "main/p1:W"
    if "main/p2" in db.values:
        out["smartpi"]["p2"] = db.values["main/p2"]
        out["smartpi"]["p2_chart"] = "main/p2:W"
    if "main/p3" in db.values:
        out["smartpi"]["p3"] = db.values["main/p3"]
        out["smartpi"]["p3_chart"] = "main/p3:W"
    if "main/cos1" in db.values:
        out["smartpi"]["c1"] = db.values["main/cos1"]
        out["smartpi"]["c1_chart"] = "main/cos1:W"
    if "main/cos2" in db.values:
        out["smartpi"]["c2"] = db.values["main/cos2"]
        out["smartpi"]["c2_chart"] = "main/cos2:W"
    if "main/cos3" in db.values:
        out["smartpi"]["c3"] = db.values["main/cos3"]
        out["smartpi"]["c3_chart"] = "main/cos3:W"
    out["diff"] = {"p1": 0, "p2": 0, "p3": 0}
    if "diff/p1" in db.values:
        out["diff"]["p1"] = db.values["diff/p1"]
    if "diff/p2" in db.values:
        out["diff"]["p2"] = db.values["diff/p2"]
    if "diff/p3" in db.values:
        out["diff"]["p3"] = db.values["diff/p3"]
    for device in db_devices.devices:
        if device["channel"] == "sonoff":
            d = device["device"]
            out["sonoff_m_"+d] = {'label': device["label"]+" (L"+str(device["phase"])+")", "p": 0, "cos": 0, "u": 0, "i": 0, "p_chart": "", "cos_chart": "",
                                  'u_chart': "", 'i_chart': ""}
            if d+'/p' in db.values:
                out["sonoff_m_"+d]["p"] = db.values[d+'/p']
                out["sonoff_m_"+d]["p_chart"] = "sonoff/"+d+"/p:W"
            if d+'/cos' in db.values:
                out["sonoff_m_"+d]["cos"] = db.values[d+'/cos']
                out["sonoff_m_"+d]["cos_chart"] = "sonoff/"+d+"/cos:W"
            if d+'/v' in db.values:
                out["sonoff_m_"+d]["u"] = db.values[d+'/v']
                out["sonoff_m_"+d]["u_chart"] = "sonoff/"+d+"/v:W"
            if d+'/i' in db.values:
                out["sonoff_m_"+d]["i"] = db.values[d+'/i']
                out["sonoff_m_"+d]["i_chart"] = "sonoff/"+d+"/i:W"
            out["sonoff_p_"+d] = db.db_val(d)
            out["sonoff_p_"+d]["label"] = "Energie: " + device["label"]+" (L"+str(device["phase"])+")"
            out["sonoff_p_"+d]["p"] = 0
            out["sonoff_p_"+d]["p_chart"] = ""
            if d+'/p' in db.values:
                out["sonoff_p_"+d]["p"] = db.values[d+'/p']
                out["sonoff_p_"+d]["p_chart"] = "sonoff/"+d+"/p:W"
    for device in db_devices.devices:
        if device["channel"] == "homematic":
            d = device["device"]
            out["homematic_m_"+d] = {'label': device["label"]+" (L"+str(device["phase"])+")", "p": 0, "f": 0, "u": 0, "i": 0, "p_chart": "", "f_chart": "",
                                  'u_chart': "", 'i_chart': ""}
            if d+'/p' in db.values:
                out["homematic_m_"+d]["p"] = db.values[d+'/p']
                out["homematic_m_"+d]["p_chart"] = "homematic/"+d+"/p:W"
            if d+'/f' in db.values:
                out["homematic_m_"+d]["f"] = db.values[d+'/f']
                out["homematic_m_"+d]["f_chart"] = "homematic/"+d+"/f:W"
            if d+'/v' in db.values:
                out["homematic_m_"+d]["u"] = db.values[d+'/v']
                out["homematic_m_"+d]["u_chart"] = "homematic/"+d+"/v:W"
            if d+'/i' in db.values:
                out["homematic_m_"+d]["i"] = db.values[d+'/i']
                out["homematic_m_"+d]["i_chart"] = "homematic/"+d+"/i:W"
            out["homematic_p_"+d] = db.db_val(d)
            out["homematic_p_"+d]["label"] = "Energie: " + device["label"]+" (L"+str(device["phase"])+")"
            out["homematic_p_"+d]["p"] = 0
            out["homematic_p_"+d]["p_chart"] = ""
            if d+'/p' in db.values:
                out["homematic_p_"+d]["p"] = db.values[d+'/p']
                out["homematic_p_"+d]["p_chart"] = "homematic/"+d+"/p:W"
    for device in db_devices.devices:
        if device["channel"] == "avm":
            d = device["device"]
            out["avm_m_"+d] = {'label': device["label"]+" (L"+str(device["phase"])+")", "p": 0, "p_chart": ""}
            if d+'/p' in db.values:
                out["avm_m_"+d]["p"] = db.values[d+'/p']
                out["avm_m_"+d]["p_chart"] = "avm/"+d+"/p:W"
            out["avm_p_"+d] = db.db_val(d)
            out["avm_p_"+d]["label"] = "Energie: " + device["label"]+" (L"+str(device["phase"])+")"
            out["avm_p_"+d]["p"] = 0
            out["avm_p_"+d]["p_chart"] = ""
            if d+'/p' in db.values:
                out["avm_p_"+d]["p"] = db.values[d+'/p']
                out["avm_p_"+d]["p_chart"] = "avm/"+d+"/p:W"
    return out

def get_chart(data):
    global sl
    if data["sensor"].startswith("main/"):
        url = "http://"+sl["cfg"]["const"]["smartpi"]+":9999/power_smartpi/data"
        send_data = {'client': 'power.app', 'cmd': 'get_chart', 'data': data}
        try:
            resp = request.urlopen(url, data=json.dumps(send_data).encode())
            if resp.code == 200:
                data = json.loads(resp.read().decode())
        except:
            print("Error get Data")
    elif data["sensor"].startswith("sonoff/"):
        sdata = copy.copy(data)
        sdata["sensor"] = "/".join(data["sensor"].split("/")[1:])
        url = "http://"+sl["cfg"]["const"]["sonoff"]+":9999/power_sonoff/data"
        send_data = {'client': 'power.app', 'cmd': 'get_chart', 'data': sdata}
        try:
            resp = request.urlopen(url, data=json.dumps(send_data).encode())
            if resp.code == 200:
                data = json.loads(resp.read().decode())
        except:
            print("Error get Data")
    elif data["sensor"].startswith("homematic/"):
        sdata = copy.copy(data)
        sdata["sensor"] = "/".join(data["sensor"].split("/")[1:])
        url = "http://"+sl["cfg"]["const"]["homematic"]+":9999/power_homematic/data"
        send_data = {'client': 'power.app', 'cmd': 'get_chart', 'data': sdata}
        try:
            resp = request.urlopen(url, data=json.dumps(send_data).encode())
            if resp.code == 200:
                data = json.loads(resp.read().decode())
        except:
            print("Error get Data")
    elif data["sensor"].startswith("avm/"):
        sdata = copy.copy(data)
        sdata["sensor"] = "/".join(data["sensor"].split("/")[1:])
        url = "http://"+sl["cfg"]["const"]["avm"]+":9999/power_avm/data"
        send_data = {'client': 'power.app', 'cmd': 'get_chart', 'data': sdata}
        try:
            resp = request.urlopen(url, data=json.dumps(send_data).encode())
            if resp.code == 200:
                data = json.loads(resp.read().decode())
        except:
            print("Error get Data")
    return data
