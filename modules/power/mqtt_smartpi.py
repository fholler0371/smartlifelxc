import os
import sys
import json
import time

try:
    import paho.mqtt.client as mqtt
except:
    os.popen(sys.executable + ' -m pip install paho-mqtt --upgrade').read()
    import paho.mqtt.client as mqtt

import power_smartpi.db as db
import power_smartpi

sl = None
cb = None

def init(_sl, _cb):
    global sl, client, cb
    sl = _sl
    cb = _cb
    ip = sl["cfg"]["const"]["smartpi"]
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_disconnect = on_disconnect
    client.on_message = on_message
    client.connect(ip, 1883, 60)
    client.loop_start()

def on_connect(client, userdata, flags, rc):
    if rc == 0:
       client.subscribe("sl/power_smartpi/+")

def on_disconnect(client, userdata, rc):
    if rc != 0:
       power_smartpi.stop()

def on_message(client, userdata, msg):
    global cb
    cb("main/"+"/".join(msg.topic.split("/")[2:]), json.loads(msg.payload.decode())["val"])

def stop():
    global client
    if client:
        client.disconnect()
        time.sleep(1)
        client.loop_stop()

