import os
import sys
import json
import time

try:
    import paho.mqtt.client as mqtt
except:
    os.popen(sys.executable + ' -m pip install paho-mqtt --upgrade').read()
    import paho.mqtt.client as mqtt

import power.db_devices as db_devices

sl = None
cb = None
devices = []

def init(_sl, _cb):
    global sl, client, cb
    sl = _sl
    cb = _cb
    ip = sl["cfg"]["const"]["avm"]
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_disconnect = on_disconnect
    client.on_message = on_message
    client.connect(ip, 1883, 60)
    client.loop_start()

def on_connect(client, userdata, flags, rc):
    if rc == 0:
       client.subscribe("sl/power_avm/+/+")

def on_disconnect(client, userdata, rc):
    if rc != 0:
       power_smartpi.stop()

def on_message(client, userdata, msg):
    global cb, devices
    cb("/".join(msg.topic.split("/")[2:]), json.loads(msg.payload.decode())["val"])
    db_devices.add(msg.topic.split("/")[2], "avm")

def stop():
    global client
    if client:
        client.disconnect()
        time.sleep(1)
        client.loop_stop()

