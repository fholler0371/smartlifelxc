import sys
import os
import time
from threading import Timer

try:
    from fritzconnection import FritzConnection
except:
    os.popen(sys.executable + ' -m pip install fritzconnection --upgrade').read()
    from fritzconnection import FritzConnection

sl = None
th = None
cb = None

def init(_sl, _cb):
    global sl, th, cb
    sl = _sl
    cb = _cb
    t = time.time()
    t = 60*(int(t/60)+1)-t
    th = Timer(t, loop)
    th.start()

def loop():
    global sl, th, cb
    t = time.time()
    t = 60*(int(t/60)+1)-t
    th = Timer(t, loop)
    th.start()
    con = sl["cfg"]["const"]["fritzbox"]
    try:
        fc = FritzConnection(address=con["ip"], password=con["password"], user=con["user"])
    except:
        fc = None
    i = 0
    if fc:
        error = False
        while not error:
            try:
                res = fc.call_action('X_AVM-DE_Homeauto1', 'GetGenericDeviceInfos', NewIndex=i)
                id = res['NewAIN'].replace(' ', '_')
                power = res['NewMultimeterPower']/100
                energy = res['NewMultimeterEnergy']/1000
                temperature = res['NewTemperatureCelsius']/10
                status = res['NewSwitchState'] == 'ON'
                cb(id + '/p', power)
                cb(id + '/etot', energy)
                cb(id + '/temperature', temperature)
                cb(id + '/state', status)
                i += 1
            except IndexError:
                error = True

def stop():
    global th
    if th:
        th.cancel()
