define (['module', 'jqxtabs', 'jqxdata', 'jqxgrid', 'jqxgrid_selection', 'jqxgrid_sort', 'jqxgrid_edit'], function(module) {
  var mod = {
    init : function() {
      html = '<div style="width:100%; height:100%;"><div id="avm_tab"><ul><li>Werte</li><li>Typen</li></ul>'
      html += '<div><div id="avm_values" style="width:100%; height:100%;"></div></div>'
      html += '<div><div id="avm_typen"></div></div>'
      $('#client_area').html(html)
      $(window).off('resize', window.module.avm.resize)
      $(window).on('resize', window.module.avm.resize)
      window.module.avm.resize()
      $('#avm_tab').jqxTabs({ width: '100%', height: '100%', position: 'top'})
      $('#avm_tab').on('selected', function(ev) {
        var selectedTab = ev.args.item
        window.module.avm.call_tab(selectedTab)
      })
      window.module.avm.call_tab(0)
    },
    call_tab: function(id) {
      if (id == 0) {
        window.smcall({'client': 'avm', 'cmd':'get_values', 'data': {}}, function(data) {
          var source = {
            localdata: data.data,
            datatype: "array",
            datafields: [
              { name: 'name', type: 'string'},
              { name: 'value', type: 'string'}
            ]
          }
          var dataAdapter = new $.jqx.dataAdapter(source, {
            downloadComplete: function (data, status, xhr) { },
            loadComplete: function (data) { },
            loadError: function (xhr, status, error) { }
          })
          $("#avm_values").jqxGrid({
            width: '100%',
            height: '100%',
            source: dataAdapter,
            sortable: true,
            columns: [
              { text: 'Sensor', datafield: 'name'},
              { text: 'Wert', datafield: 'value', width:200, cellsalign: 'right'}
            ]
          })
          $("#avm_values").parent().css('padding', 0).css('overflow', 'hidden')
        })
      }
      if (id == 1) {
        window.smcall({'client': 'avm', 'cmd':'get_devices', 'data': {}}, function(data) {
          var source = {
            localdata: data.data,
            datatype: "array",
            datafields: [
              { name: 'name', type: 'string'},
              { name: 'friendly', type: 'string'}
            ]
          }
          var dataAdapter = new $.jqx.dataAdapter(source, {
            downloadComplete: function (data, status, xhr) { },
            loadComplete: function (data) { },
            loadError: function (xhr, status, error) { }
          })
          $("#avm_typen").jqxGrid({
            width: '100%',
            height: '100%',
            source: dataAdapter,
            sortable: true,
            editable: true,
            columns: [
              { text: 'Gerät', datafield: 'name', width:200, editable: false},
              { text: 'lokaler Name', datafield: 'friendly'}
            ]
          })
          $("#avm_typen").parent().css('padding', 0).css('overflow', 'hidden')
          $("#avm_typen").off('cellendedit')
          $("#avm_typen").on('cellendedit', function (event) {
            var friendly = event.args.value,
                name = event.args.row.name
            window.smcall({'client': 'avm', 'cmd':'set_device', 'data': {"name": name, "friendly": friendly}}, function(data) {})
          })
        })
      }
    },
    stop : function() {
    },
    resize: function() {
      $('#client_area').children().height($('#client_area').height())
    }
  }
  mod['init_data'] = window.module_const[module.id]
  window.module.avm = mod
  return mod
})
