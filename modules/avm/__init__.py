import avm.fritzbox as fb
import avm.devices as devices
import avm.mqtt_master as mqtt
import avm.web as web

sl = None

def init(_sl):
    global sl
    sl = _sl
    print("init")
    mqtt.init(sl)
    web.init(sl)
    devices.init(sl)
    fb.init(sl, new_data)

def new_data(topic, payload):
    devices.add_device(topic.split('/')[0])
    devices.add_value(topic, payload)

def run():
    print("run")

def stop():
    print("stop")
    fb.stop()
    web.stop()
    devices.stop()
    mqtt.stop()
