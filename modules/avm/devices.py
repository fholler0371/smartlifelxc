import os
import json
import copy
from threading import Timer

import avm.mqtt_master as mqtt

sl = None

devices = {}
db_devices = ''
db_values = ''
values = {}
th = None
running = True

def init(_sl):
    global sl, db_devices, th, values, db_values, devices
    sl = _sl
    db_devices = sl["cfg"]["dbpath"] + sl["cfg"]["name"] + "_devices.json"
    if os.path.isfile(db_devices):
        f = open(db_devices, "r")
        devices = json.loads(f.read())
        f.close()
    db_values = sl["cfg"]["dbpath"] + sl["cfg"]["name"] + "_values.json"
    if os.path.isfile(db_values):
        f = open(db_values, "r")
        values = json.loads(f.read())
        f.close()
    th = Timer(3600, loop)
    th.start()

def add_device(name):
    global devices, db_devices
    if not ( name in devices ):
        devices[name] = {'name': name, 'friendly': name}
        f = open(db_devices, "w")
        f.write(json.dumps(devices))
        f.close()

def add_value(topic, payload):
    global values, devices
    values[topic] = payload
    _topic = topic.split('/')
    if _topic[0] in devices and devices[_topic[0]]['friendly'] != _topic[0]:
        _topic = devices[_topic[0]]['friendly'] + "/" + _topic[1]
        mqtt.publish(_topic, payload)

def get_devices():
    global devices
    out = []
    for dev in devices:
        out.append(copy.copy(devices[dev]))
    out.sort(key=lambda x: x['friendly'].upper(), reverse=False)
    return out

def set_device(data):
    global devices
    if data['name'] in devices:
        devices[data['name']]['friendly'] = data['friendly']
        f = open(db_devices, "w")
        f.write(json.dumps(devices))
        f.close()

def get_values():
    global values, devices
    out = []
    for name in values:
        topic = name
        if topic.split('/')[0] in devices:
            topic = devices[topic.split('/')[0]]['friendly'] + '/' + topic.split('/')[1]
        out.append({'name': topic, 'value': values[name]})
    out.sort(key=lambda x: x['name'].upper(), reverse=False)
    return out

def loop():
    global th, running
    if running:
        th = Timer(3600, loop)
        th.start()
    save()

def save():
    global values, db_values
    f = open(db_values, "w")
    f.write(json.dumps(values))
    f.close()

def stop():
    global th, running
    running = False
    if th:
        th.cancel()
    save()
