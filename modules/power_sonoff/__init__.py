import power_sonoff.mqtt as mqtt
import power_sonoff.mqtt_master as mqtt_master
import power_sonoff.db as db
import power_sonoff.web as web

sl = None

def init(_sl):
    global sl
    sl = _sl
    db.init(sl)
    mqtt_master.init(sl)
    mqtt.init(sl)
    web.init(sl)
    print("init")

def run():
    print("run")

def stop():
    mqtt.stop()
    mqtt_master.stop()
    db.stop()
    web.stop()
    print("stop")
