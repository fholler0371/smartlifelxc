import time
import sqlite3
import os
import sys
from threading import Timer
from datetime import datetime

import power_sonoff.mqtt_master as mqtt

try:
    from dateutil import tz
except:
    os.popen(sys.executable + ' -m pip install python-dateutil --upgrade').read()
    from dateutil import tz

values = {}
sl = None
last = 0
th = None
db_1day = ""
running = True
db_1week = ""
week_last = 0
db_month = ""
month_last = 0
year_last = 0

def init(_sl):
    global sl, last, th, db_1day, db_1week, week_last, db_month, month_last, year_last
    sl = _sl
    last = int(time.time()/60)
    next = (last+1)*60+1-time.time()
    th = Timer(next, loop)
    th.start()
    db_1day = sl["cfg"]["dbpath"] + sl["cfg"]["name"] + "_one_day.sqlite"
    con = sqlite3.connect(db_1day)
    cur = con.cursor()
    sql = "CREATE TABLE IF NOT EXISTS history (time integer, topic text, value float)"
    cur.execute(sql)
    con.commit()
    con.close()
    db_1week = sl["cfg"]["dbpath"] + sl["cfg"]["name"] + "_one_week.sqlite"
    con = sqlite3.connect(db_1week)
    cur = con.cursor()
    sql = "CREATE TABLE IF NOT EXISTS history (time integer, topic text, value float, value_min float, value_max float)"
    cur.execute(sql)
    con.commit()
    con.close()
    week_last = 300*int(time.time()/300)
    db_month = sl["cfg"]["dbpath"] + sl["cfg"]["name"] + "_"
    month_last = 3600*int(time.time()/3600)
    year_last = 3600*24*int(time.time()/3600/24)

def get_chart(data):
    global db_1day, db_1week, db_month
    out = {"title": "", "buttons": "D:W:M:Y:A", "data": [], "unit": "", "pre": 2}
    if "/p" in data["sensor"]:
        out["title"] = "Leistung [W]"
        out["unit"] = "W"
        out["pre"] = 0
        out["label"] = "Leistung"
    elif "/v" in data["sensor"]:
        out["title"] = "Spannung [V]"
        out["unit"] = "V"
        out["pre"] = 1
        out["label"] = "Spannung"
    elif "/i" in data["sensor"]:
        out["title"] = "Strom [A]"
        out["unit"] = "A"
        out["pre"] = 1
        out["label"] = "Strom"
    elif "/cos" in data["sensor"]:
        out["title"] = "Leistungsfaktor"
        out["unit"] = "Degree"
        out["pre"] = 2
        out["label"] = "Leistungsfaktor"
    if data["mode"] == "D":
        con = sqlite3.connect(db_1day)
        cur = con.cursor()
        sql = "SELECT time, value FROM history WHERE topic='%s' ORDER BY time"
        cur.execute(sql % data["sensor"])
        rows = cur.fetchall()
        con.close()
        for row in rows:
            out["data"].append({"time": row[0], "value": row[1]})
    elif data["mode"] == "W":
        con = sqlite3.connect(db_1week)
        cur = con.cursor()
        sql = "SELECT time, value, value_min, value_max FROM history WHERE topic='%s' ORDER BY time"
        cur.execute(sql % data["sensor"])
        rows = cur.fetchall()
        con.close()
        for row in rows:
            out["data"].append({"time": row[0], "value": row[1], "min": row[2], "max": row[3]})
    elif data["mode"] == "M":
        t = int(time.time())
        tstart = t - 31*24*60*60
        d = datetime.utcfromtimestamp(tstart)
        from_zone = tz.gettz('UTC')
        to_zone = tz.gettz('Europe/Berlin')
        d = d.replace(tzinfo=from_zone)
        d = d.astimezone(to_zone)
        fname = db_month+d.strftime("%y%m")+".sqlite"
        if os.path.isfile(fname):
            con = sqlite3.connect(fname)
            cur = con.cursor()
            sql = "SELECT time, value, value_min, value_max FROM history WHERE topic='%s' and time>'"+str(tstart)+"' ORDER BY time"
            cur.execute(sql % data["sensor"])
            rows = cur.fetchall()
            con.close()
            for row in rows:
                out["data"].append({"time": row[0], "value": row[1], "min": row[2], "max": row[3]})
        d = datetime.utcfromtimestamp(t)
        from_zone = tz.gettz('UTC')
        to_zone = tz.gettz('Europe/Berlin')
        d = d.replace(tzinfo=from_zone)
        d = d.astimezone(to_zone)
        fname = db_month+d.strftime("%y%m")+".sqlite"
        if os.path.isfile(fname):
            con = sqlite3.connect(fname)
            cur = con.cursor()
            sql = "SELECT time, value, value_min, value_max FROM history WHERE topic='%s' and time>'"+str(tstart)+"' ORDER BY time"
            cur.execute(sql % data["sensor"])
            rows = cur.fetchall()
            con.close()
            for row in rows:
                out["data"].append({"time": row[0], "value": row[1], "min": row[2], "max": row[3]})
    elif data["mode"] == "Y":
        t = int(time.time())
        tstart = t - 365*24*60*60
        d = datetime.utcfromtimestamp(tstart)
        from_zone = tz.gettz('UTC')
        to_zone = tz.gettz('Europe/Berlin')
        d = d.replace(tzinfo=from_zone)
        d = d.astimezone(to_zone)
        fname = db_month+d.strftime("%y")+".sqlite"
        if os.path.isfile(fname):
            con = sqlite3.connect(fname)
            cur = con.cursor()
            sql = "SELECT time, value, value_min, value_max FROM history WHERE topic='%s' and time>'"+str(tstart)+"' ORDER BY time"
            cur.execute(sql % data["sensor"])
            rows = cur.fetchall()
            con.close()
            for row in rows:
                out["data"].append({"time": row[0], "value": row[1], "min": row[2], "max": row[3]})
        d = datetime.utcfromtimestamp(t)
        from_zone = tz.gettz('UTC')
        to_zone = tz.gettz('Europe/Berlin')
        d = d.replace(tzinfo=from_zone)
        d = d.astimezone(to_zone)
        fname = db_month+d.strftime("%y")+".sqlite"
        if os.path.isfile(fname):
            con = sqlite3.connect(fname)
            cur = con.cursor()
            sql = "SELECT time, value, value_min, value_max FROM history WHERE topic='%s' and time>'"+str(tstart)+"' ORDER BY time"
            cur.execute(sql % data["sensor"])
            rows = cur.fetchall()
            con.close()
            for row in rows:
                out["data"].append({"time": row[0], "value": row[1], "min": row[2], "max": row[3]})
    elif data["mode"] == "A":
        t = int(time.time())
        d = datetime.utcfromtimestamp(t)
        from_zone = tz.gettz('UTC')
        to_zone = tz.gettz('Europe/Berlin')
        d = d.replace(tzinfo=from_zone)
        d = d.astimezone(to_zone)
        yend = int(d.strftime("%y"))
        ystart = yend
        fname = db_month+('0'+str(ystart))[-2:]+".sqlite"
        while os.path.isfile(fname):
            ystart -= 1
            fname = db_month+('0'+str(ystart))[-2:]+".sqlite"
        ystart += 1
        yend += 1
        while ystart < yend:
            fname = db_month+('0'+str(ystart))[-2:]+".sqlite"
            con = sqlite3.connect(fname)
            cur = con.cursor()
            sql = "SELECT time, value, value_min, value_max FROM history WHERE topic='%s' ORDER BY time"
            cur.execute(sql % data["sensor"])
            rows = cur.fetchall()
            con.close()
            for row in rows:
                out["data"].append({"time": row[0], "value": row[1], "min": row[2], "max": row[3]})
            ystart += 1
    else:
        print(data)
    out["buttons"] = out["buttons"].replace(data["mode"], data["mode"]+"!")
    return out


def loop():
    global last, running, th, values, db_1day, db_1week, week_last, month_last, db_month, year_last
    t = int(time.time())
    last = int(time.time()/60)
    next = (last+1)*60+1-time.time()
    if running:
        th = Timer(next, loop)
        th.start()
    day = sqlite3.connect(db_1day)
    cur = day.cursor()
    sql = "delete from history where time < "+str(t-86400)
    cur.execute(sql)
    day.commit()
    for entry in values:
        if entry.find("pday") == -1 :
            sql = "insert into history (time, topic, value) values ('"+str(t)+"', '"+entry.lower()+"', '"+str(values[entry])+"')"
            cur.execute(sql)
    day.commit()
    week = sqlite3.connect(db_1week)
    if week_last != 300*int(t/300):
        week_last = 300*int(t/300)
        wcur = week.cursor()
        sql = "delete from history where time < "+str(t-86400*7)
        wcur.execute(sql)
        week.commit()
        sql = "select avg(value), min(value), max(value), topic from history where time > '"+str(t-301)+"' group by topic"
        cur.execute(sql)
        for row in cur.fetchall():
            sql = "insert into history (time, topic, value, value_min, value_max) values ('"+str(t)+"', '"+row[3]+"', '"+str(row[0])
            sql += "', '"+str(row[1])+"', '"+str(row[2])+"')"
            wcur.execute(sql)
        week.commit()
        d = datetime.utcfromtimestamp(t)
        from_zone = tz.gettz('UTC')
        to_zone = tz.gettz('Europe/Berlin')
        d = d.replace(tzinfo=from_zone)
        d = d.astimezone(to_zone)
        if month_last != 3600*int(t/3600):
            month_last = 3600*int(t/3600)
            month = sqlite3.connect(db_month+d.strftime("%y%m")+".sqlite")
            mcur = month.cursor()
            sql = "CREATE TABLE IF NOT EXISTS history (time integer, topic text, value float, value_min float, value_max float)"
            mcur.execute(sql)
            month.commit()
            sql = "select avg(value), min(value_min), max(value_max), topic from history where time > '"+str(t-3601)+"' group by topic"
            wcur.execute(sql)
            for row in wcur.fetchall():
                sql = "insert into history (time, topic, value, value_min, value_max) values ('"+str(t)+"', '"+row[3]+"', '"+str(row[0])
                sql += "', '"+str(row[1])+"', '"+str(row[2])+"')"
                mcur.execute(sql)
            month.commit()
            month.close()
        if year_last != 24*3600*int(t/24/3600):
            year_last = 24*3600*int(t/24/3600)
            year = sqlite3.connect(db_month+d.strftime("%y")+".sqlite")
            ycur = year.cursor()
            sql = "CREATE TABLE IF NOT EXISTS history (time integer, topic text, value float, value_min float, value_max float)"
            ycur.execute(sql)
            year.commit()
            sql = "select avg(value), min(value_min), max(value_max), topic from history where time > '"+str(t-86401)+"' group by topic"
            wcur.execute(sql)
            for row in wcur.fetchall():
                sql = "insert into history (time, topic, value, value_min, value_max) values ('"+str(t)+"', '"+row[3]+"', '"+str(row[0])
                sql += "', '"+str(row[1])+"', '"+str(row[2])+"')"
                ycur.execute(sql)
            year.commit()
            year.close()
    week.close()
    day.close()

def add(topic, value):
    global values
    values[topic] = value
    mqtt.publish(topic, value)

def stop():
    global running, th
    running = False
    if th:
        th.cancel()
