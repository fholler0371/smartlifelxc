import os
import wifiscan_master.mqtt_client as mqtt_client
import wifiscan_master.ap_database as ap_database
import wifiscan_master.station_database as station_database
import wifiscan_master.mqtt_heartbeat as mqtt_heartbeat
import wifiscan_master.web as web
import wifiscan_master.db as db
import wifiscan_master.cards as cards

sl = {}
base_clients = []

def init(_sl):
    global sl, base_clients
    sl = _sl
    sl["log"].info("init modul")
    mqtt_heartbeat.init(sl)
    cards.init(sl)
    db.init(sl)
    web.init(sl)
    ap_database.init(sl)
    station_database.init(sl)
    if "base" in sl["cfg"]["const"]:
        for base in sl["cfg"]["const"]["base"]:
            base_clients.append({"ip": base["mqtt"], "client": None})

def run():
    global sl, base_clients
    sl["log"].info("start")
    l = len(base_clients)
    x = 0
    while x < l:
      client = base_clients[x]
      base_clients[x]["client"] = mqtt_client.base(client["ip"], sl["log"], ap_data, station_data)
      x += 1

def stop():
    l = len(base_clients)
    x = 0
    while x < l:
      client = base_clients[x]
      if client["client"]:
          client["client"].stop()
      x += 1
    db.stop()
    web.stop()
    mqtt_heartbeat.stop()

def ap_data(client, msg):
    ap_database.add(client, msg)
    mqtt_heartbeat.counter = mqtt_heartbeat.counter + 1

def station_data(client, msg):
    global test_count
    station_database.add(client, msg)
    mqtt_heartbeat.counter = mqtt_heartbeat.counter + 1
