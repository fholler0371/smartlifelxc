define(['module', 'packery', 'jqxtabs', 'jqxdata', 'jqxgrid', 'jqxgrid_selection', 'jqxgrid_edit', 'jqxcheckbox', 'jqxgrid_sort', 'jqxbutton'], 
    function(module, Packery) {
  var wifiscan_master = {
    t_update : null,
    wdata_last : 0,
    wdata_call : 0,
    init : function() {
      window.module.wifiscan_master.sub = window.module.wifiscan_master.init_data.p1
      if ("app" == window.module.wifiscan_master.sub) {
        html = '<div style="width:100%; height:100%; padding:5px;" id="wifiscan_cards"></div>'
        $('#client_area').html(html)
        $(window).off('resize', window.module.wifiscan_master.resize)
        $(window).on('resize', window.module.wifiscan_master.resize)
        window.module.wifiscan_master.resize()
      } else {
        html = '<div style="width:100%; height:100%;"><div id="wifiscan_tab"><ul><li>Karten</li><li>Geräte</li><li>Access Points</li><li>Karten-Manager</li></ul>'
        html += '<div><div id="wifiscan_cards"></div></div>'
        html += '<div><div id="wifiscan_station"></div></div>'
        html += '<div><div id="wifiscan_ap"></div></div>'
        html += '<div><div id="wifiscan_card_manager"></div></div>'
        $('#client_area').html(html+'</div>')
        $(window).off('resize', window.module.wifiscan_master.resize)
        $(window).on('resize', window.module.wifiscan_master.resize)
        window.module.wifiscan_master.resize()
        $('#wifiscan_tab').jqxTabs({ width: '100%', height: '100%', position: 'top'})
        $('#wifiscan_tab').off('selected')
        $('#wifiscan_tab').on('selected', function(ev) {
          var selectedTab = ev.args.item
          window.module.wifiscan_master.call_tab(selectedTab)
        })
      }
      window.module.wifiscan_master.call_tab(0)
    },
    call_tab : function(id) {
      if (id == 0) {
        window.smcall({'client': 'wifiscan_master.app', 'cmd':'get_cards', 'data': {}}, function(data) {
          window.module.wifiscan_master.cards = data.data
          window.module.wifiscan_master.cards_check_mod = 10
          window.module.wifiscan_master.create_cards()
        })
      }
      if (id == 1) {
        if ("full" == window.module.wifiscan_master.sub) {
          var mode = "write"
        } else {
          var mode = "read"
        }
        window.smcall({'client': 'wifiscan_master.' + mode, 'cmd':'get_station', 'data': {}}, function(data) {
          var len = data.data.length
          var now = new Date
          for ( let i = 0; i<len; i++) {
            var d = now.getTime()/1000 - data.data[i].lc
            d = d / 60
            if (d < 60) {
              data.data[i].last = Math.floor(d) + ' min'
            } else {
              var m = Math.floor(d) % 60
              if (m < 10) {
                data.data[i].last = ":0" + m
              } else {
                data.data[i].last = ":" + m
              }
              d = Math.floor(d / 60)
              var h = d % 24
              if (h < 10) {
                data.data[i].last = '0' + h + data.data[i].last
              } else {
                data.data[i].last = h + data.data[i].last
              }
              d = Math.floor(d / 24)
              if (d > 0) {
                data.data[i].last = d + "d " + data.data[i].last
              }
            }
          }
          var source = {
            localdata: data.data,
            datatype: "array",
            datafields: [
              { name: 'mac', type: 'string'},
              { name: 'vendor', type: 'string'},
              { name: 'ap', type: 'string'},
              { name: 'rssi', type: 'integer'},
              { name: 'activ', type: 'boolean'},
              { name: 'last', type: 'string'},
              { name: 'send', type: 'boolean'}
            ]
          }
          var dataAdapter = new $.jqx.dataAdapter(source, {
            downloadComplete: function (data, status, xhr) { },
            loadComplete: function (data) { },
            loadError: function (xhr, status, error) { }
          })
          var col = [
              { text: 'MAC-Adresse', datafield: 'mac', width: 150, editable: false},
              { text: 'Hersteller', datafield: 'vendor', editable: false},
              { text: 'Access-Point', datafield: 'ap', width: 200, editable: false},
              { text: 'Zuletzt', datafield: 'last', width: 150, cellsalign: 'right', editable: false},
              { text: 'Aktiv', datafield: 'activ', width: 75, cellsalign: 'center', columntype: 'checkbox', editable: false}
            ]
          if ("full" == window.module.wifiscan_master.sub) {
              col.push({ text: 'Senden', datafield: 'send', width: 75, cellsalign: 'center', columntype: 'checkbox'})
          }
          $("#wifiscan_station").jqxGrid({
            width: '100%',
            height: '100%',
            source: dataAdapter,
            sortable: true,
            columns: col,
            editable: ("full" == window.module.wifiscan_master.sub)
          })
          $("#wifiscan_station").parent().css('padding', 0).css('overflow', 'hidden')
          $("#wifiscan_station").on('cellendedit', function (event) {
            var send = event.args.row.send,
                mac = event.args.row.mac
            window.smcall({'client': 'wifiscan_master.write', 'cmd':'set_send_station', 'data': {"mac": mac, "value": send}}, function(data) {})
          });
        })
      }
      if (id == 2) {
        window.smcall({'client': 'wifiscan_master.read', 'cmd':'get_ap', 'data': {}}, function(data) {
          var len = data.data.length
          var now = new Date
          for ( let i = 0; i<len; i++) {
            var d = now.getTime()/1000 - data.data[i].lc
            d = d / 60
            if (d < 60) {
              data.data[i].last = Math.floor(d) + ' min'
            } else {
              var m = Math.floor(d) % 60
              if (m < 10) {
                data.data[i].last = ":0" + m
              } else {
                data.data[i].last = ":" + m
              }
              d = Math.floor(d / 60)
              var h = d % 24
              if (h < 10) {
                data.data[i].last = '0' + h + data.data[i].last
              } else {
                data.data[i].last = h + data.data[i].last
              }
              d = Math.floor(d / 24)
              if (d > 0) {
                data.data[i].last = d + "d " + data.data[i].last
              }
            }
          }
          var source = {
            localdata: data.data,
            datatype: "array",
            datafields: [
              { name: 'ssid', type: 'string' },
              { name: 'mac', type: 'string'},
              { name: 'vendor', type: 'string'},
              { name: 'key', type: 'string'},
              { name: 'rssi', type: 'integer'},
              { name: 'lc', type: 'integer'},
              { name: 'last', type: 'string'}
            ]
          }
          var dataAdapter = new $.jqx.dataAdapter(source, {
            downloadComplete: function (data, status, xhr) { },
            loadComplete: function (data) { },
            loadError: function (xhr, status, error) { }
          })
          $("#wifiscan_ap").jqxGrid({
            width: '100%',
            height: '100%',
            source: dataAdapter,
            sortable: true,
            columns: [
              { text: 'SSID', datafield: 'ssid', width: 250},
              { text: 'MAC-Adresse', datafield: 'mac', width: 150},
              { text: 'Hersteller', datafield: 'vendor'},
              { text: 'Mode', datafield: 'key', width: 75},
              { text: 'RSSI', datafield: 'rssi', width: 75, cellsalign: 'right'},
              { text: 'Zuletzt', datafield: 'last', width: 150, cellsalign: 'right'}
            ]
          })
          $("#wifiscan_ap").parent().css('padding', 0).css('overflow', 'hidden')
        })
      }
      if (id == 3) {
        window.smcall({'client': 'wifiscan_master.write', 'cmd':'get_filter', 'data': {}}, function(data) {
          var source = {
            localdata: data.data,
            datatype: "array",
            datafields: [
              { name: 'id', type: 'integer' },
              { name: 'rule', type: 'string'},
              { name: 'is_ap', type: 'boolean'},
              { name: 'name', type: 'string'}
            ]
          }
          var dataAdapter = new $.jqx.dataAdapter(source, {
            downloadComplete: function (data, status, xhr) { },
            loadComplete: function (data) { },
            loadError: function (xhr, status, error) { }
          })
          $("#wifiscan_card_manager").jqxGrid({
            width: '100%',
            height: '100%',
            source: dataAdapter,
            sortable: true,
            showtoolbar: true,
            editable: true,
            rendertoolbar: function (statusbar) {
              var container = $("<div style='overflow: hidden; position: relative; margin: 5px;'></div>")
              var addButton = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;' src='lib/img/add.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Neu</span></div>");
              var deleteButton = $("<div style='float: left; margin-left: 5px;'><img style='position: relative; margin-top: 2px;' src='lib/img/close.png'/><span style='margin-left: 4px; position: relative; top: -3px;'>Entfernen</span></div>");
              container.append(addButton);
              container.append(deleteButton);
              statusbar.append(container);
              addButton.jqxButton({  width: 60, height: 20 })
              deleteButton.jqxButton({  width: 85, height: 20 })
              addButton.click(function (event) {
                window.smcall({'client': 'wifiscan_master.write', 'cmd':'new_filter', 'data': {}}, function(data) {
                  $("#wifiscan_card_manager").jqxGrid('addrow', null, data.data)
                })
              })
              deleteButton.click(function (event) {
                var selectedrowindex = $("#wifiscan_card_manager").jqxGrid('getselectedrowindex')
                if (selectedrowindex > -1) {
                  var row = $("#wifiscan_card_manager").jqxGrid('getrowdatabyid', selectedrowindex)
                  window.smcall({'client': 'wifiscan_master.write', 'cmd':'delete_filter', 'data': {'id': row.id, 'index': selectedrowindex}}, function(data) {
                    $("#wifiscan_card_manager").jqxGrid('deleterow', data.data);
                  })
                }
              })
            },
            columns: [
              { text: 'id', datafield: 'id', 'hidden': true},
              { text: 'Filter', datafield: 'rule', width: 200},
              { text: 'Bezeichnung', datafield: 'name'},
              { text: 'Access-Point', datafield: 'is_ap', width: 150, cellsalign: 'center', columntype: 'checkbox'}
            ]
          })
          $("#wifiscan_card_manager").off('cellendedit')
          $("#wifiscan_card_manager").on('cellendedit', function (event) {
            var rule = event.args.row.rule,
                id = event.args.row.id,
                is_ap = event.args.row.is_ap,
                name = event.args.row.name
                window.smcall({'client': 'wifiscan_master.write', 'cmd':'edit_filter', 'data': {'id': id, 'rule': rule, 'is_ap': is_ap, 'name': name}}, function(data) {})
          });
          console.log(dataAdapter)
          $("#wifiscan_card_manager").parent().css('padding', 0).css('overflow', 'hidden')
        })
      }
    },
    create_cards : function() {
      var ok = 1
      var cards = window.module.wifiscan_master.cards
      var l = cards.length
      for (var i=0; i<l; i++) {
         if (!(typeof(window.module[cards[i].mod].get_card) === 'function')) {
           console.log("Modul not found: "+cards[i].mod)
           ok = 0
         }
      }
      if (ok == 1) {
        for (var i=0; i<l; i++) {
          window.module[cards[i].mod].get_card(cards[i], $("#wifiscan_cards"))
          require( [ 'jquery-bridget/jquery-bridget' ], function( jQueryBridget ) {
            jQueryBridget( 'packery', Packery, $ );
            var grid = $('#wifiscan_cards').packery({
              itemSelector: '.card',
              gutter: 10
            })
          })
        }
      } else {
        console.log("recheck")
      }
    },
    get_card : function(data, div) {
      if (div.find(".cards_wifiscan_master_"+data.card).length == 0) {
        var html = $('<div class="cards_wifiscan_master cards_wifiscan_master_'+data.card+' card"><span></span></div>')
        html.data('card', data.card)
        div.append(html)
      }
      window.module.wifiscan_master.update()
    },
    check_data: function() {
      var now = new Date()
      var t = Math.round(now.getTime() / 1000)
      if (t - window.module.wifiscan_master.wdata_last > 600) {
        if (t - window.module.wifiscan_master.wdata_call > 10) {
          window.module.wifiscan_master.wdata_call = t
          window.smcall({'client': 'wifiscan_master.app', 'cmd':'get_card', 'data': {'id': 'all'}}, function(data) {
            window.module.wifiscan_master.wdata_last = t
            window.module.wifiscan_master.wdata = data.data
          })
        }
      }
    },
    update : function() {
      var cards =  $("#wifiscan_cards").find(".cards_wifiscan_master")
      clearTimeout(window.module.wifiscan_master.t_update)
      window.module.wifiscan_master.check_data()
      var len = cards.length
      var tout = 10000
      var now = new Date()
      var t = Math.round(now.getTime() / 1000)
      for (var i = 0; i<len; i++) {
         var ele = $(cards[i]),
             c_id = ele.data("card")
         if (window.module.wifiscan_master.wdata != undefined) {
           if (window.module.wifiscan_master.wdata[c_id] != undefined) {
             var old = ele.data('last')
             var data = window.module.wifiscan_master.wdata[c_id]
             if (!(old == data)) {
               ele.data('last', data)
               html = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">'+data.caption+'</div>'
               if (c_id.startsWith("ap")) {
                 html += '<img src="/lib/img/wifi-ap.png" style="position: relative; top: 5px; left: 10px;">'
                 html += '<div  style="position: absolute; right: 5px; top: 20px;">'
                 html += data.rssi.toLocaleString('de-DE', {maximumFractionDigits: 0})
                 html += ' db</div>'
                 html += '<div  style="position: absolute; right: 5px; top: 38px;">'
                 html += data.activ +' von '+data.all+' Clients aktiv</div>'
               } else {
                 if (t - data.lc > 300) {
                   html += '<img src="/lib/img/wifi-off.png" style="position: relative; top: 5px; left: 10px;">'
                 } else {
                   html += '<img src="/lib/img/wifi-on.png" style="position: relative; top: 5px; left: 10px;">'
                 }
               }
               var diff = Math.round((t - data.lc) / 60)
               tstr = diff + " Minuten"
               if (diff > 60) {
                 diff = Math.round(diff/60)
                 tstr = diff + " Stunden"
                 if (diff > 24) {
                   diff = Math.round(diff/24)
                   tstr = diff + " Tage"
                 }
               }
               html += '<div  style="position: absolute; right: 5px; top: 56px;">'
               html += 'Zuletzt vor '+tstr+'</div>'
               ele.html(html)
             }
           }
         } else {
           tout = 250
         }
      }
      window.module.wifiscan_master.t_update = setTimeout(window.module.wifiscan_master.update, tout)
    },
    stop : function() {
    },
    resize: function() {
      $('#client_area').children().height($('#client_area').height())
    }
  }
  wifiscan_master['init_data'] = window.module_const[module.id]
  window.module.wifiscan_master = wifiscan_master
  return wifiscan_master
})

