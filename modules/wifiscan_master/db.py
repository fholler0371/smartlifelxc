import json
import os
from threading import Timer
import wifiscan_master.ap_database as ap_database
import wifiscan_master.station_database as station_database

save_interval = 6*60*60 # 6 Stunden
sl = None
timer = None

def init(_sl):
    global sl, timer, save_interval
    sl = _sl
    base = sl["cfg"]["dbpath"] + sl["cfg"]["name"]
    if os.path.isfile(base + "_ap.json"):
        f = open(base + "_ap.json", "r")
        ap_database.data = json.loads(f.read())
        f.close()
    if os.path.isfile(base + "_ap_rssi.json"):
        f = open(base + "_ap_rssi.json", "r")
        ap_database.rssi_data = json.loads(f.read())
        f.close()
    if os.path.isfile(base + "_station.json"):
        f = open(base + "_station.json", "r")
        station_database.data = json.loads(f.read())
        f.close()
    if os.path.isfile(base + "_station_activ.json"):
        f = open(base + "_station_activ.json", "r")
        station_database.activ_data = json.loads(f.read())
        f.close()
    if os.path.isfile(base + "_station_mqtt.json"):
        f = open(base + "_station_mqtt.json", "r")
        station_database.data_mqtt = json.loads(f.read())
        f.close()
    timer = Timer(save_interval, runTimer)
    timer.start()

def save():
    global sl
    base = sl["cfg"]["dbpath"] + sl["cfg"]["name"]
    f = open(base + "_ap.json", "w")
    f.write(json.dumps(ap_database.data))
    f.close()
    f = open(base + "_ap_rssi.json", "w")
    f.write(json.dumps(ap_database.rssi_data))
    f.close()
    f = open(base + "_station.json", "w")
    f.write(json.dumps(station_database.data))
    f.close()
    f = open(base + "_station_activ.json", "w")
    f.write(json.dumps(station_database.activ_data))
    f.close()
    f = open(base + "_station_mqtt.json", "w")
    f.write(json.dumps(station_database.data_mqtt))
    f.close()

def runTimer():
    global timer, save_interval
    timer = Timer(save_interval, runTimer)
    timer.start()
    save()

def stop():
    global timer
    if timer:
        timer.cancel()
    save()

