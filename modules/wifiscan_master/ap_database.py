import time

data = {}
rssi_data = {}
sl = None

def init(_sl):
    global sl
    sl = _sl

def add(ip, rec):
    global data, rssi_data, sl
    t = int(time.time())
    mac = rec["val"]["mac"]
    rssi = rec["val"]["rssi"]
    lc = rec["lc"]
    if not(mac in data):
        sl["log"].debug("new AP "+mac)
    data[mac] = rec["val"]
    data[mac]["lc"] = lc
    del data[mac]["rssi"]
    try:
        if mac in rssi_data:
            for _lc in rssi_data[mac]:
                if t - int(_lc) > 3600:
                    del rssi_data[mac][_lc]
            if lc in rssi_data[mac]:
                del rssi_data[mac][lc]
        else:
            rssi_data[mac] = {}
    except:
        pass
    rssi_data[mac][str(lc)] = rssi
    _rssi = -100
    for _lc in rssi_data[mac]:
        if rssi_data[mac][_lc] > _rssi:
           _rssi = rssi_data[mac][_lc]
    data[mac]["rssi"] = _rssi
    try:
        for rec in data:
            _lc = data[rec]["lc"]
            if t - _lc > 2592000: #Monat
                del data[rec]
    except:
        pass

def web_get():
    global data
    out = []
    for rec in data:
        out.append(data[rec])
    out.sort(key=lambda x: x["ssid"].upper(), reverse=False)
    return out
