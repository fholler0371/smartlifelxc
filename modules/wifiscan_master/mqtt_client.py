import time
import json

try:
    import paho.mqtt.client as mqtt
except:
    os.popen(sys.executable + ' -m pip install paho-mqtt --upgrade').read()
    import paho.mqtt.client as mqtt

class base:
    def __init__(self, ip, log, cb_ap, cb_station):
        self.ip = ip
        self.log = log
        self.cb_ap = cb_ap
        self.cb_station = cb_station
        self.connected = False
        self.client = mqtt.Client(userdata=self)
        self.client.on_connect = self.on_connect
        self.client.on_disconnect = self.on_disconnect
        self.client.on_message = self.on_message
        self.client.connect(ip, 1883, 60)
        self.client.loop_start()
        self.running = True

    def on_connect(self, client, userdata, flags, rc):
        if rc == 0:
            userdata.connected = True
            client.subscribe("sl/wifiscan_base/status/ap/+")
            client.subscribe("sl/wifiscan_base/status/station/+")
        userdata.log.info("connect to " + userdata.ip)

    def on_disconnect(self, client, userdata, rc):
        userdata.connected = False
        if rc != 0 and userdata.running:
            client.reconnect()
        userdata.log.info("disconnect from " + userdata.ip)

    def on_message(self, client, userdata, msg):
        if msg.topic.find("/ap/") > 0:
            userdata.cb_ap(userdata.ip, json.loads(msg.payload))
        elif msg.topic.find("/station/") > 0:
            userdata.cb_station(userdata.ip, json.loads(msg.payload))

    def stop(self):
        self.running = False
        if self.connected:
            self.client.disconnect()
            time.sleep(0.5)
            self.client.loop_stop()
