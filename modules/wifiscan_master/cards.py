import os
import json
import re
import time
import wifiscan_master.ap_database as ap
import wifiscan_master.station_database as station

sl = None

filter = {"last": 0, "data":{}}
file_name = ""

def init(_sl):
    global sl, file_name, filter
    sl = _sl
    file_name = sl["cfg"]["dbpath"] + sl["cfg"]["name"] + "_filter.json"
    if os.path.isfile(file_name):
        f = open(file_name, "r")
        filter = json.loads(f.read())
        f.close()

def get_card():
    global filter
    out = {}
    t = int(time.time())
    for entry in filter["data"]:
        fdata = filter["data"][entry]
        if fdata["is_ap"]:
            card_id = "ap_"+str(fdata["id"])
            aps = []
            caption = fdata["name"]
            rssi = 0
            lc = 0
            activ = 0
            all = 0
            stations = []
            rule = fdata["rule"].replace("*", ".+").replace("?", ".")
            for ap_entry in ap.data:
                match = re.search(rule, ap.data[ap_entry]['ssid'])
                if match:
                    if (0 == match.start()) and (len(ap.data[ap_entry]['ssid']) == match.end()):
                        aps.append(ap.data[ap_entry])
                match = re.search(rule, ap.data[ap_entry]['mac'])
                if match:
                    if (0 == match.start()) and (len(ap.data[ap_entry]['mac']) == match.end()):
                        aps.append(ap.data[ap_entry])
                match = re.search(rule, ap.data[ap_entry]['vendor'])
                if match:
                    if (0 == match.start()) and (len(ap.data[ap_entry]['vendor']) == match.end()):
                        aps.append(ap.data[ap_entry])
            if len(aps) > 0:
                for entry in aps:
                    if lc < entry["lc"]:
                        lc = entry["lc"]
                        rssi = entry["rssi"]
                    for sentry in station.data:
                        if entry["ssid"] == station.data[sentry]["ap"]:
                            if not(sentry in stations):
                                all += 1
                                if t-600 < station.data[sentry]["lc"]:
                                    activ += 1
                                stations.append(sentry)
            out[card_id] = {"caption": "AP: "+caption, "lc": lc, "rssi": rssi, "activ": activ, "all": all}
        else:
            sts = []
            card_id = "st_"+str(fdata["id"])
            rule = fdata["rule"].replace("*", ".+").replace("?", ".")
            for st_entry in station.data:
                match = re.search(rule, station.data[st_entry]['mac'])
                if match:
                    if (0 == match.start()) and (len(station.data[st_entry]['mac']) == match.end()):
                        sts.append(station.data[st_entry])
                match = re.search(rule, station.data[st_entry]['vendor'])
                if match:
                    if (0 == match.start()) and (len(station.data[st_entry]['vendor']) == match.end()):
                        sts.append(station.data[st_entry])
            if len(sts) > 0:
                out[card_id] = {"caption": fdata["name"], "lc": sts[0]["lc"]}
    return out

def get_cards():
    global filter
    out = []
    for entry in filter["data"]:
        fdata = filter["data"][entry]
        if fdata["is_ap"]:
            rule = fdata["rule"].replace("*", ".+").replace("?", ".")
            found = False
            for ap_entry in ap.data:
                match = re.search(rule, ap.data[ap_entry]['ssid'])
                if match:
                    if (0 == match.start()) and (len(ap.data[ap_entry]['ssid']) == match.end()):
                        found = True
                match = re.search(rule, ap.data[ap_entry]['mac'])
                if match:
                    if (0 == match.start()) and (len(ap.data[ap_entry]['mac']) == match.end()):
                        found = True
                match = re.search(rule, ap.data[ap_entry]['vendor'])
                if match:
                    if (0 == match.start()) and (len(ap.data[ap_entry]['vendor']) == match.end()):
                        found = True
            if found:
                out.append({"mod": "wifiscan_master", "card": "ap_"+str(fdata["id"]), "name": "AP ("+fdata["name"]+")"})
        else:
            rule = fdata["rule"].replace("*", ".+").replace("?", ".")
            found = False
            for st_entry in station.data:
                match = re.search(rule, station.data[st_entry]["mac"])
                if match:
                    if (0 == match.start()) and (len(station.data[st_entry]['mac']) == match.end()):
                        found = True
                match = re.search(rule, station.data[st_entry]["vendor"])
                if match:
                    if (0 == match.start()) and (len(station.data[st_entry]['vendor']) == match.end()):
                        found = True
            if found:
                out.append({"mod": "wifiscan_master", "card": "st_"+str(fdata["id"]), "name": "WiFi ("+fdata["name"]+")"})
    return out

def save():
    global filter, file_name
    f = open(file_name, "w")
    f.write(json.dumps(filter))
    f.close()

def get_filter():
    global filter
    out = []
    for entry in filter["data"]:
        out.append(filter["data"][entry])
    return out

def new_filter():
    global filter
    id = filter["last"] + 1
    filter["last"] = id
    filter["data"][str(id)] = {"id": id, "rule": "", "is_ap": False, "name": ""}
    save()
    return filter["data"][str(id)]

def del_filter(data):
    global filter
    if str(data["id"]) in filter["data"]:
       del filter["data"][str(data["id"])]
       save()
    return data["index"]

def edit_filter(data):
    global filter
    if str(data["id"]) in filter["data"]:
       filter["data"][str(data["id"])]["rule"] = data["rule"]
       filter["data"][str(data["id"])]["is_ap"] = data["is_ap"]
       filter["data"][str(data["id"])]["name"] = data["name"]
       save()
    return []
