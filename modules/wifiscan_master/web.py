from functools import partial
from http.server import HTTPServer, BaseHTTPRequestHandler
from socketserver import ThreadingMixIn
from threading import Thread
import json
import os

import wifiscan_master.ap_database as ap_database
import wifiscan_master.station_database as station_database
import wifiscan_master.cards as cards

sl = None
server = None
th = None

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
     """Handle requests in a separate thread."""

class webserverHandler(BaseHTTPRequestHandler):
    def __init__(self, sl, app, *args, **kwargs):
        self.sl = sl
        self.app = app
        super().__init__(*args, **kwargs)
    def log_message(self, format, *args):
        self.sl["log"].debug("\n    %s - - [%s] %s" % (self.address_string(), self.log_date_time_string(),format%args))
    def do_POST(self):
        if self.path == "/data":
            try:
                content_len = int(self.headers['Content-Length'])
                data = json.loads(self.rfile.read(content_len).decode())
                data = json.dumps(self.app(data))
                self.send_response(200)
                self.send_header('Content-Type', 'application/json')
                self.send_header('Content-Length', str(len(data)))
                self.end_headers()
                self.wfile.write(data.encode())
            except:
                self.send_response(501)
                self.send_header('Content-Type', 'text')
                self.end_headers()
                self.wfile.write("Error 501".encode())
        elif self.path == "/rights":
            data = json.dumps(['wifiscan_master.read', 'wifiscan_master.write', 'wifiscan_master.app'])
            self.send_response(200)
            self.send_header('Content-Type', 'application/json')
            self.send_header('Content-Length', str(len(data)))
            self.end_headers()
            self.wfile.write(data.encode())
        elif self.path == "/menu":
            content_len = int(self.headers['Content-Length'])
            data = json.loads(self.rfile.read(content_len).decode())
            if 'wifiscan_master.write' in data:
                data = json.dumps([{'label': 'WiFi-Scan', 'mod': 'wifiscan_master', 'p1':'full'}])
            elif 'wifiscan_master.read' in data:
                data = json.dumps([{'label': 'WiFi-Scan', 'mod': 'wifiscan_master', 'p1':'read'}])
            elif 'wifiscan_master.app' in data:
                data = json.dumps([{'label': 'WiFi-Scan', 'mod': 'wifiscan_master', 'p1':'app'}])
            self.send_response(200)
            self.send_header('Content-Type', 'application/json')
            self.send_header('Content-Length', str(len(data)))
            self.end_headers()
            self.wfile.write(data.encode())
        elif self.path == "/script":
            f = open("home/.gitlab/smartlifelxc/modules/wifiscan_master/script.js", "rb")
            text = f.read()
            self.send_response(200)
            self.send_header('Content-Type', 'application/javascript')
            self.send_header('Content-Length', str(len(text)))
            self.end_headers()
            self.wfile.write(text)
        else:
            self.send_response(404)
            self.send_header('Content-Type', 'text')
            self.end_headers()
            self.wfile.write("Not found 404".encode())

class loopThread(Thread):
    def __init__(self, server):
        Thread.__init__(self)
        self.server = server
    def run(self):
        self.server.serve_forever()

def app(data):
    if data["client"] in ["wifiscan_master.read", "wifiscan_master.write", "wifiscan_master.app"]:
       if data["cmd"] == "get_cards":
            return cards.get_cards()
       elif data["cmd"] == "get_card":
            return cards.get_card()
    if data["client"] in ["wifiscan_master.read", "wifiscan_master.write"]:
        if data["cmd"] == "get_ap":
            return ap_database.web_get()
        elif data["cmd"] == "get_station":
            return station_database.web_get(data["client"] in "wifiscan_master.write")
    if data["client"] == "wifiscan_master.write":
        if data["cmd"] == "set_send_station":
            station_database.set_send(data["data"]["mac"], data["data"]["value"])
        elif data["cmd"] == "get_filter":
            return cards.get_filter()
        elif data["cmd"] == "new_filter":
            return cards.new_filter()
        elif data["cmd"] == "delete_filter":
            return cards.del_filter(data["data"])
        elif data["cmd"] == "edit_filter":
            return cards.edit_filter(data["data"])

def init(_sl):
    global sl, server, th
    sl = _sl
    handler = partial(webserverHandler, sl, app)
    server = ThreadedHTTPServer(('0.0.0.0', 80), handler)
    th = loopThread(server)
    th.start()

def stop():
    global server
    server.shutdown()
