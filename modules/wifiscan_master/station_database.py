import time

import wifiscan_master.mqtt_heartbeat as mqtt_heartbeat

data = {}
activ_data = []
data_mqtt = []
sl = None

def init(_sl):
    global sl
    sl = _sl

def add(ip, rec):
    global data, activ_data, sl, data_mqtt
    t = int(time.time())
    mac = rec["val"]["mac"]
    lc = rec["lc"]
    if not(mac in data):
        sl["log"].debug("new station "+mac)
    data[mac] = rec["val"]
    data[mac]["lc"] = lc
    try:
        for st in data: # Loeschen der Geraete aelter 1 Woche
            _lc = data[st]["lc"]
            if t - _lc > 604800:
                del data[st]
    except:
        pass
    if t - lc < 600:
        sl["log"].debug("aktiv last 10 min "+mac)
        if not(mac in activ_data):
            activ_data.append(mac)
    x = 0
    while x < len(activ_data):
        _lc = data[activ_data[x]]["lc"]
        if t - _lc > 600:
            sl["log"].debug("inactive last 10 min "+activ_data[x])
            if activ_data[x] in data_mqtt:
                mqtt_heartbeat.publish("status/" + activ_data[x], False)
            del activ_data[x]
        x += 1
    if mac in data_mqtt:
        mqtt_heartbeat.publish("status/" + mac, True)

def web_get(mode):
    global data, activ_data, data_mqtt
    out = []
    for rec in data:
        out.append(data[rec])
        out[len(out)-1]["activ"] = rec in activ_data
        if mode:
            out[len(out)-1]['send'] = rec in data_mqtt
    out.sort(key=lambda x: x["mac"].upper(), reverse=False)
    return out

def set_send(mac, value):
     global data_mqtt
     if value:
         if not ( mac in data_mqtt ):
             data_mqtt.append(mac)
     else:
         print(data_mqtt)
         if mac in data_mqtt:
             data_mqtt.remove(mac)
