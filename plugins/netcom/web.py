# smartlife
# 04.2021
# 05.2021

import json
import requests
from urllib.parse import urlparse, parse_qsl

class web:
    def __init__(self, log, smartlife, get_plugins, cfg, cfg_master):
        self.log = log
        self.cfg = cfg
        self.smartlife = smartlife
        self.cfg_master = cfg_master
        log.debug('init')
        self.get_plugins = get_plugins
        self.scan_time = 10

    def doGet(self, path, ip, host):
        self.log.debug(path)
        parsed = urlparse(path)
        path_split = parsed.path.split("/")
        if parsed.path == "/netcom/get_plugins":
            data = self.get_plugins()
            return {"code": 200, "mime": "application/json", "data" : json.dumps(data).encode()}
        elif path_split[1] == "web":
            plugin = self.smartlife.get_plugin(path_split[2])
            data = plugin.web_doGet('/'.join(path_split[3:]), parsed.query)
            data["mime"] = "application/json"
            data["data"] = json.dumps(data["data"]).encode()
            return data
        return {"code": 404}

    def doPost(self, path, data, ip, host):
        self.log.debug(path)
        parsed = urlparse(path)
        path_split = parsed.path.split("/")
        if path_split[1] == "file":
            _data = dict(parse_qsl(parsed.query))
            mod = self.smartlife.get_plugin(_data["modul"])
            if mod:
                if _data["mode"] == "stream":
                    func = getattr(mod, _data["func"])
                    out = func(data, _data)
                    if out["ok"]:
                        return {"code": 200, "mime": "application/json", "data": out["data"]}
        elif parsed.path == "/netcom/rights":
            _data = json.loads(data)
            out = {"domain": False}
            auth = self.smartlife.get_plugin("authentication")
            out = auth.check_rights(_data["user"], _data["domain"], _data["ip"])
            return {"code": 200, "mime": "application/json", "data": out}
        elif parsed.path == "/netcom/menu":
            _data = json.loads(data)
            out = []
            for plugin in self.smartlife.get_all_plugins():
                if "frontend" in plugin.manifest and plugin.manifest["frontend"]:
                    for entry in plugin.pl_get_menus(_data["user"], _data["ip"]):
                        out.append(entry)
            return {"code": 200, "mime": "application/json", "data": out}
        elif parsed.path == "/netcom/data":
            _data = json.loads(data)
            mod = self.smartlife.get_plugin(_data["modul"])
            if mod:
                out = mod.pl_data(_data["user"], _data["cmd"], _data["data"], _data["ip"])
                return {"code": 200, "mime": "application/json", "data": out}
        elif parsed.path == "/netcom/script":
            _data = json.loads(data)
            mod = self.smartlife.get_plugin(_data["modul"])
            if mod:
                script = mod.pl_script()
                return {"code": 200, "data": script, "mime": "text/javascript"}
        elif parsed.path == "/netcom/secret":
            _data = json.loads(data)
            mod = self.smartlife.get_plugin("authentication")
            if mod:
                secret = mod.load_secret(_data["value"])
                return {"code": 200, "data": secret, "mime": "text"}
        elif parsed.path == "/netcom/encode":
            mod = self.smartlife.get_plugin("authentication")
            if mod:
                secret = mod.encode(data)
                return {"code": 200, "data": secret, "mime": "text"}
        elif path_split[1] == "web":
            plugin = self.smartlife.get_plugin(path_split[2])
            data = plugin.web_doPost('/'.join(path_split[3:]), parsed.query, data)
            data["mime"] = "application/json"
            data["data"] = data["data"]
            return data
        return {"code": 404}



    def tick(self):
#        self.log.debug("tick")
        self.scan_time = self.scan_time - 1
        if self.cfg["auto_intervall"] and self.scan_time <= 0:
            self.scan_time = self.cfg["auto_intervall"]
            change = False
            _hosts = self.cfg_master["hosts"]
            l = len(_hosts)
            i = 0
            while i < l:
                entry = _hosts[i]
                if entry["mode"] >= 0:
                    url = "http://" + entry["ip"]  + ":" + str(self.cfg["http_port"]) + "/netcom/get_plugins"
                    if not("alive" in entry):
                        _hosts[i]["alive"] = True
                        change = True
                    if not("plugins" in entry):
                        _hosts[i]["plugins"] = []
                        change = True
                    try:
                        r = requests.get(url)
                        if r.status_code == 200:
                            if not entry["alive"]:
                                entry["alive"] = True
                                change = True   
                            data = r.json()
                            if data["hostname"] == entry["name"]:
                                if not (data["plugins"] == entry["plugins"]):
                                    _hosts[i]["plugins"] = data["plugins"]
                                    change = True
                            else:
                                x = 1/0
                        else:    
                            x = 1/0
                    except:
                        if entry["alive"]:
                            entry["alive"] = False
                            change = True    
                i += 1
            if change:
                self.smartlife.config_update({"hosts": _hosts})