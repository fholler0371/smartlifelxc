# smartlife
# 04.2021
# 05.2021

import os
import time
import json
import requests

from plugins2 import plugin_base
import server2 as server

import netcom.udp as udp
import netcom.web as web

ALIVE_UDP=400
ALIVE_WWW=4000

class integration(plugin_base):
    def __init__(self, smartlife, modul):
        global ALIVE_UDP, ALIVE_WWW
        plugin_base.__init__(self, smartlife, modul)
        if not( "ip" in self.cfg ):
            self.log.error("Missing \"ip\" entry in netcom.yaml")
            os._exit(1)
        if not( "hostname" in self.cfg ):
            self.log.error("Missing \"hostname\" entry in netcom.yaml")
            os._exit(1)
        self.udp = udp.broadcast(self.log, self.smartlife, self.cfg, self.udp_beat, self.cfg_master, self.udp_messages)
        ALIVE_UDP = self.cfg["auto_intervall"]+60
        self.alive = [ALIVE_UDP, ALIVE_WWW]
        self.web = web.web(self.log, self.smartlife, self.get_plugins, self.cfg, self.cfg_master)
        self.server = server.getServer(self.doGet, self.doPost, self.log, self.cfg['http_port'])

    def get_plugins(self):
        self.log.debug("get_plugins")
        out = []
        for entry in self.smartlife.get_all_plugins():
            web_plugin = entry.get_web_path()
            rec = {"name": entry.name, "domain": entry.manifest["domain"]}
            if None != web_plugin:
                rec["web"] = web_plugin
            if "frontend" in entry.manifest and entry.manifest["frontend"]:
                rec["frontend"] = True
            out.append(rec) 
        return {"hostname": self.cfg["hostname"], "plugins": out}

    def doGet(self, path, ip, name):
        global ALIVE_WWW
        self.log.debug(path)
        self.alive[1] = ALIVE_WWW
        return self.web.doGet(path, ip, name)

    def doPost(self, path, data, ip, name):
        global ALIVE_WWW
        self.log.debug(path)
        self.log.debug(data)
        self.alive[1] = ALIVE_WWW
        return self.web.doPost(path, data, ip, name)

    def udp_beat(self):
        global ALIVE_UDP
        self.log.debug("beat")
        self.alive[0] = ALIVE_UDP

    def udp_messages(self, msg):
        self.log.debug(msg)
        data = msg.split(":", 1)
        mod = self.smartlife.get_plugin(data[0].lower())
        if mod and hasattr(mod, "new_remote"):
            mod.new_remote(data[1])
        else:
            self.log.error("missing " + data[0].lower() + " new_remote")

    def get_hostname(self):
        self.log.debug("get_hostname")
        return self.cfg["hostname"]

    def udp_broadcast(self, msg):
        self.log.debug(msg)
        self.udp.broadcast(msg)

    def encode_data(self, data):
        self.log.debug("encode_data")
        found = False
        for host in self.cfg_master["hosts"]:
            for plugin in host["plugins"]:
                if not(found) and plugin["name"] == "authentication":
                    found = True
                    url = "http://" + host["ip"]  + ":" + str(self.cfg_master["netcom_port"]) + "/netcom/encode"
                    try:
                        r = requests.post(url, data=json.dumps(data))
                        if r.status_code == 200:
                            _out = r.json()
                            return _out
                        else:
                            return ""
                    except:
                        return ""

    def run(self):
        self.log.info("run")
        server.threadStart(self.server, self.log)
        self.udp.run()
        while self.running:
            self.udp.tick()
            self.web.tick()
            time.sleep(1)

    def stop(self):
        self.log.info("stop")
        self.running = False
        self.udp.stop()
        server.serverStop(self.server, self.log)
