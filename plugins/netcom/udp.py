# smartlife
# 04.2021
# 05.2021
# 09.2021

import socket
import select
import time
import copy
from threading import Thread
from queue import Queue

class udp(Thread):
    def __init__(self, log, port, queue):
        Thread.__init__(self)
        self.name = "UDP-Client"
        self.log = log
        self.port = port
        self.running = True
        self.queue = queue
        log.debug("init")
        self.client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP) # UDP
        self.client.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
        self.client.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)

    def run(self):
        self.log.debug("run")
        self.client.bind(("", self.port))
        readable = [self.client]
        while self.running:
            r,w,e = select.select(readable,[],[],0)
            for rs in r:
                if rs == self.client:
                    data, a = rs.recvfrom(1024)
                    self.queue.put({"ip": str(a[0]), "text": data.decode()})
                    self.log.debug("udp from: "+str(a[0])+" >> "+data.decode())
            time.sleep(0.5)

    def stop(self):
        self.log.debug("stop")
        self.running = False
        try:
            self.client.close()
        except:
            pass

class broadcast:
    def __init__(self, log, smartlife, cfg, beat, cfg_master, cb):
        self.log = log
        self.log.debug("init")
        self.beat = beat
        self.cfg = cfg
        self.smartlife = smartlife
        self.cfg_master = cfg_master
        self.cb = cb
        self.queue_resv = Queue()
        self.th = udp(log, cfg["port"], self.queue_resv)
        self.ping_time = 2

    def run(self):
        self.log.debug("run")
        self.th.start()

    def sendto(self, ip, port, msg):
        self.log.debug(ip+": "+msg)
        server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
        server.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        server.settimeout(0.2)
        server.sendto(msg.encode(), (ip, port))
        time.sleep(0.01)

    def add_server(self, entry):
        self.log.debug(entry)
        change = False
        found = False
        if "hosts" in self.cfg_master:
            _hosts = self.cfg_master["hosts"]
        else:
            change = True
            _hosts = []
        l = len(_hosts)
        i = 0
        while i < l:
            if _hosts[i]["name"] == entry[0]:
                found = True
                if not _hosts[i]["alive"]:
                    _hosts[i]["alive"] = True
                    change = True
                if _hosts[i]["ip"] != entry[1]:
                    change = True
                    _hosts[i]["ip"] = entry[1]
            i += 1
        if not found and self.cfg["allow_new_clients"]:
            _hosts.append({"name": entry[0], "ip": entry[1], "mode": 0})
            change = True
        if change:    
            self.smartlife.config_update({"hosts": _hosts})

    def broadcast(self, msg):
        self.log.debug(msg)
        ip = ".".join(self.cfg["ip"].split(".")[:-1])+".255"
        self.sendto(ip, self.cfg["port"], msg)

    def tick(self):
#        self.log.debug("tick")
        while not self.queue_resv.empty():
            self.beat()
            entry = self.queue_resv.get(block=False)
            if entry["text"] == "SL:NETCOM:PING":
                text = "SL:NETCOM:RESPOND:"+self.cfg["hostname"]+"|"+self.cfg["ip"]
                self.sendto(entry["ip"], self.cfg["port"], text)
                self.queue_resv.task_done()
            elif entry["text"].startswith("SL:NETCOM:RESPOND:"):
                self.add_server(entry["text"].split(":", 3)[-1].split("|"))
                self.queue_resv.task_done()
            elif entry["text"].startswith("SL:"):
                self.cb(entry["text"].split(":", 1)[1])
                self.queue_resv.task_done()
            else:
                self.log.error(entry)
                self.queue_resv.task_done()
        self.ping_time = self.ping_time - 1
        if self.cfg["auto_intervall"] and self.ping_time <= 0:
            self.ping_time = self.cfg["auto_intervall"]
            self.broadcast("SL:NETCOM:PING")

    def stop(self):
        self.log.debug("stop")
        if self.th:
            self.th.stop()
