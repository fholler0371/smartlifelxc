# smartlife
# 05.2021

class cards:
    def __init__(self, log, db):
        log.debug("init")
        self.log = log
        self.db = db

    def get_cards(self):
        self.log.debug("get_cards")
        out = [{"mod": "fin_card_lbb", "card": "fin_card_lbb_sum", "name": "Amazon VISA Summe"},
               {"mod": "fin_card_lbb", "card": "fin_card_lbb_month", "name": "Amazon VISA Summe Monat"},
               {"mod": "fin_card_lbb", "card": "fin_card_lbb_year", "name": "Amazon VISA Summe Jahr"}]
        return {"allowed": True, "data": out}

    def get_card(self):
        self.log.debug("get_card")
        out = {'sum': 0}
        out = self.db.get_card()
        return {"allowed": True, "data": out}
