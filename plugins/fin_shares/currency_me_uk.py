# smartlife
# 06.2021

from threading import Thread

import requests

class fetch(Thread):
    def __init__(self, log, cb, is_done):
        Thread.__init__(self)
        self.name = "currency collector"
        log.debug("init")
        self.log = log
        self.cb = cb
        self.is_done = is_done

    def run(self):
        self.log.debug("run")
        try: 
            r = requests.get("https://www.currency.me.uk/rates/eur-euro")
            if r.status_code == 200:
                data = r.text
                pos = data.find("currencies")
                if pos == -1:
                    return
                pos = data.find('>', pos)
                data = data[pos+1:]
                pos = data.find('</table>')
                for entry in data[:pos].split('<tr'):
                    entry = entry[entry.find('>', entry.find('href'))+1:]
                    pos = entry.find('(')
                    name = entry[:pos-1]
                    entry = entry[pos+1:]
                    pos = entry.find(')')
                    wkn = entry[:pos]
                    pos = entry.find('<b>')
                    entry = entry[pos+3:]
                    pos = entry.find('</b>')
                    value = entry[:pos]
                    try:
                        value = float(value)
                        if value != 0:
                            self.cb({'name': name, 'wkn': wkn, 'value': value})
                    except:
                        pass
                self.is_done()
        except:
            self.log.error("get currency page")
        

class collector:
    def __init__(self, log, cb):
        log.debug("init")
        self.log = log
        self.cb = cb

    def get_data(self, is_done):
        self.log.debug("get_data")
        th = fetch(self.log, self.cb, is_done)
        th.start()
