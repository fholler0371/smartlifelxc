# smartlife
# 06.2021

from database import database

class shares:
    def __init__(self, log, smartlife):
        log.debug("init")
        self.log = log
        self.shares = database(self.log, smartlife.db_path + "/fin_shares_data.sqlite")

    def get_shares(self, data):
        self.log.debug("get_shares")
        out = {"records": 0, "rows": []}
        self.shares.open()
        row = self.shares.fetchone("select count(id) from aktien where aktiv=1")
        if row:
            out["records"] = row[0]
        sql = "select id, name, typ, d30, d90, d365, points, kurs, wkn from aktien where aktiv=1 order by %s %s limit %s, %s"
        for row in self.shares.fetchall(sql % (data["sortfield"], data["sortorder"], str(data["page"]*data['pagesize']), str(data['pagesize']), )):
            out["rows"].append({'id': row[0], 'name': row[1], 'typ': row[2],'d30': row[3], 'd90': row[4], 'd365': row[5],
                                'points': row[6], 'kurs': row[7], 'wkn': row[8]})
        self.shares.close()
        return {"allowed": True, "data": out}

    def updated(self, id, d):
        self.log.debug("updated")
        self.shares.open()
        row = self.shares.fetchone("select kurs_date, error from aktien where id='%s' and aktiv='1'" % (str(id)))
        if row and row[0] == d.strftime("%Y-%m-%d"):
            self.shares.close()
            return True
        if row:
            if row[1] > 29:
                self.shares.commit("update aktien set aktiv='0' where id='%s'" % (str(id)))
                self.shares.close()
                return True
            else:
                self.shares.commit("update aktien set error='%s', kurs_date='%s' where id='%s'" % (str(row[1]+1),d.strftime("%Y-%m-%d"), str(id)))
            self.shares.close()
            return False
        else:
            self.shares.close()
            return True

    def get_id(self, data):
        self.log.debug(data["wkn"])
        self.shares.open()
        row = self.shares.fetchone("select id from aktien where wkn='%s'" % (data["wkn"].upper()))
        if (not row) or row[0] < 1:
            if not("name" in data):
                data["name"] = data["wkn"]
            sql = "insert into aktien (name, wkn) values ('%s', '%s')"
            self.shares.commit(sql % (data["name"], data["wkn"]))
        row = self.shares.fetchone("select id from aktien where wkn='%s'" % (data["wkn"].upper()))
        self.shares.close()
        return row[0]

    def new_price(self, data, d):
        self.log.debug(data["id"])
        self.shares.open()
        row = self.shares.fetchone("select kurs from aktien where id='%s'" % (str(data["id"])))
        if row and ((row[0] == 0) or ((data["value"] / row[0]) < 10)):
            sql = "update aktien set error='0', kurs='%s', kurs_date='%s' where id='%s'"
            self.shares.commit(sql % (str(data["value"]), d.strftime("%Y-%m-%d"), str(data["id"])))
            self.shares.close()
            return True
        self.shares.close()
        return False

    def next_update_share(self, d):
        self.log.debug("next_update_share")
        id = 0
        self.shares.open()
        self.shares.commit("update aktien set aktiv='0' where error='30'")
        sql = "SELECT id FROM aktien WHERE aktiv = '1' and typ='aktie' and kurs_date<'%s' limit 1"
        row = self.shares.fetchone(sql % (d.strftime("%Y-%m-%d")))
        if row:
            id = row[0]
        self.shares.close()
        return id

    def next_update_index(self, d):
        self.log.debug("next_update_index")
        id = 0
        self.shares.open()
        sql = "SELECT id FROM aktien WHERE aktiv = '1' and typ='index' and kurs_date<'%s' limit 1"
        row = self.shares.fetchone(sql % (d.strftime("%Y-%m-%d")))
        if row:
            id = row[0]
        self.shares.close()
        return id

    def get_base_data(self, id):
        self.log.debug(id)
        self.shares.open()
        sql = "SELECT url, exchange_id, wkn FROM aktien WHERE id='%s'"
        row = self.shares.fetchone(sql % (str(id)))
        self.shares.close()
        if row:
            return [row[0], row[1], row[2]]
        return ["", "", ""]

    def name_by_wkn(self, wkn):
        self.log.debug(wkn)
        self.shares.open()
        row = self.shares.fetchone("select name from aktien where wkn='%s'" % (wkn, ))
        if row:
            return row[0]
        self.shares.close()
        return ""

    def id_by_wkn(self, wkn):
        self.log.debug(wkn)
        self.shares.open()
        row = self.shares.fetchone("select id from aktien where wkn='%s'" % (wkn, ))
        if row:
            return row[0]
        self.shares.close()
        return 0
