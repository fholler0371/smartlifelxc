define(['packery', 'highcharts', 'moment_tz', 'jqxtabs', 'jqxdata', 'jqxgrid', 'jqxgrid_pager', 'jqxgrid_selection', 'jqxgrid_sort', 'jqxdropdownlist',
        'jqxdatetimeinput', 'jqxnumberinput', 'jqxbutton'], function(Packery, H, moment) {
  window.moment = moment
  H.setOptions({
    global: {
      timezone: "Europe/Berlin"
    },
    lang : {
      months : ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
      shortMonths : ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
      weekdays : ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa']
    } 
  })
  var mod = {
    wdata : {},
    wdata_last : 0,
    wdata_call : 0,
    t_update_sum: null,
    t_update_month: null,
    t_update_year: null,
    init: function() {
      if ($('#fin_shares_tab').length == 0) {
        html = '<div id="fin_shares_tab"><ul><li>Status</li><li>Aktien</li><li>Verlauf</li></ul><div id="fin_shares_cards"></div>'
        html += '<div id="fin_shares_aktien"></div><div id="fin_shares_history"></div></div>'
        $('#finanical_left').html(html)
        $('#fin_shares_tab').jqxTabs({height: '100%', width: '100%'})
        $('#fin_shares_tab').off('selected')
        $('#fin_shares_tab').on('selected', function(ev) {
          var id = ev.args.item
          window.module.fin_shares.call_tab(id)
        })
        window.module.fin_shares.call_tab(0)
      }
    },
    check_data : function() {
      var now = new Date()
      var t = Math.round(now.getTime() / 1000)
      if (t - window.module.fin_shares.wdata_last > 300) {
        if (t - window.module.fin_shares.wdata_call > 10) {
          window.module.fin_shares.wdata_call = t
          window.smfetch('/api/fin_shares', {cmd: 'get_card'}).then((data) => {
            if (data.error == undefined) {
              window.module.fin_shares.wdata_last = t
              window.module.fin_shares.wdata = data.data
            }
          })
        }
      }
    },
    update_sum: function() {
      clearTimeout(window.module.fin_shares.t_update_sum)
      window.module.fin_shares.check_data()
      var sum = window.module.fin_shares.wdata.sum
      if (sum != undefined) {
        var ele = $($("body").find('.cards_fin_shares_sum')[0])
        var old = ele.data('last')
        if (old != sum) {
          ele.data('last', sum)
          html  = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">Basisdaten</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 35px;"><span  style="font-weight: 600;">Summe:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 35px;">'
          html += sum.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          ele.html(html)
        }
        window.module.fin_shares.t_update_sum = setTimeout(window.module.fin_shares.update_sum, 600000)
      } else {
        window.module.fin_shares.t_update_sum = setTimeout(window.module.fin_shares.update_sum, 250)
      }
    },
    update_month: function() {
      clearTimeout(window.module.fin_shares.t_update_month)
      window.module.fin_shares.check_data()
      var d = window.module.fin_shares.wdata.month
      if (d != undefined) {
        var ele = $($("body").find('.cards_fin_shares_month')[0])
        var old = ele.data('last')
        if (old != d) {
          ele.data('last', d)
          html  = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">Monat</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 25px;"><span  style="font-weight: 600;">Einahmen:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 25px;">'
          html += d.in.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 45px;"><span  style="font-weight: 600;">Ausgaben:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 45px;">'
          html += d.out.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          ele.html(html)
        }
        window.module.fin_shares.t_update_month = setTimeout(window.module.fin_shares.update_month, 600000)
      } else {
        window.module.fin_shares.t_update_month = setTimeout(window.module.fin_shares.update_month, 250)
      }
    },
    update_year: function() {
      clearTimeout(window.module.fin_shares.t_update_year)
      window.module.fin_shares.check_data()
      var d = window.module.fin_shares.wdata.year
      if (d != undefined) {
        var ele = $($("body").find('.cards_fin_shares_year')[0])
        var old = ele.data('last')
        if (old != d) {
          ele.data('last', d)
          html  = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">Jahr</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 25px;"><span  style="font-weight: 600;">Einahmen:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 25px;">'
          html += d.in.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 45px;"><span  style="font-weight: 600;">Ausgaben:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 45px;">'
          html += d.out.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          ele.html(html)
        }
        window.module.fin_shares.t_update_year = setTimeout(window.module.fin_shares.update_year, 600000)
      } else {
        window.module.fin_shares.t_update_year = setTimeout(window.module.fin_shares.update_year, 250)
      }
    },
    get_card : function(data, div) {
      if (data["card"] == "fin_shares_sum") {
        if (div.find(".cards_fin_shares_sum").length == 0) {
          var html = '<div class="cards_fin_shares_sum card"><span></span></div>'
          div.append(html)
        } else {
//          console.log(data)
        }
        window.module.fin_shares.update_sum()
      } else if (data["card"] == "fin_shares_month") {
        if (div.find(".cards_fin_shares_month").length == 0) {
          var html = '<div class="cards_fin_shares_month card"><span></span></div>'
          div.append(html)
        } else {
//          console.log(data)
        }
        window.module.fin_shares.update_month()
      } else if (data["card"] == "fin_shares_year") {
        if (div.find(".cards_fin_shares_year").length == 0) {
          var html = '<div class="cards_fin_shares_year card"><span></span></div>'
          div.append(html)
        } else {
//          console.log(data)
        }
        window.module.fin_shares.update_year()
      }
    },
    create_cards: function() {
      var ok = 1
      var cards = window.module.fin_shares.cards
      var l = cards.length
      for (var i=0; i<l; i++) {
         if (!(typeof(window.module[cards[i].mod].get_card) === 'function')) {
           console.log("Modul not found: "+cards[i].mod)
           ok = 0
         }
      }
      if (ok == 1) {
        for (var i=0; i<l; i++) {
          window.module[cards[i].mod].get_card(cards[i], $("#fin_shares_cards"))
          require( [ 'jquery-bridget/jquery-bridget' ], function( jQueryBridget ) {
            jQueryBridget( 'packery', Packery, $ );
            var grid = $('#fin_shares_cards').packery({
              itemSelector: '.card',
              gutter: 10
            })
          })
        }
      } else {
        console.log("recheck")
      }
    },
    stop: function() {
    },
    call_tab: function(id) {
      if (id == 0) {
        window.smfetch('/api/fin_shares', {cmd: 'get_cards'}).then((data) => {
          if (data.error == undefined) {
            window.module.fin_shares.cards = data.data
            window.module.fin_shares.cards_check_mod = 10
            window.module.fin_shares.create_cards()
          }
        })
      } else if (id == 1) {
        if ($('#fin_shares_grid').length == 0) {
          var source = {
            datatype: 'json',
            loadServerData: window.module.fin_shares.load_table,
            datafields: [
              { name: 'id' },
              { name: 'name' },
              { name: 'wkn' },
              { name: 'typ' },
              { name: 'kurs', type: 'float'},
              { name: 'd30', type: 'float'},
              { name: 'd90', type: 'float'},
              { name: 'd365', type: 'float'},
              { name: 'points', type: 'float'}
            ],
            id: 'id',
            pagesize: 20,
            url: "http://x.x",
            sortcolumn: 'name',
            sortdirection: 'asc'
          }
          var dataAdapter = new $.jqx.dataAdapter(source);
          $('#fin_shares_aktien').append("<div id='fin_shares_grid'></div>");
          $("#fin_shares_grid").jqxGrid({
            width:'calc( 100% - 3px )',
            height:'calc( 100% - 5px )',
            localization: {
              currencysymbol: " €",
              currencysymbolposition: "after",
              decimalseparator: ",",
              thousandsseparator: ".",
              pagergotopagestring: "Gehe zu:",
              pagershowrowsstring: "Zeige Zeile:",
              pagerrangestring: " von ",
              pagernextbuttonstring: "voriger",
              pagerpreviousbuttonstring: "nächster"
            },
            source: dataAdapter,
            pageable: true,
            sortable: true,
            pagesizeoptions: ['10', '20', '30', '50', '100'],
            virtualmode: true,
            rendergridrows: function(obj){
              return obj.data;
            },
            showtoolbar: true,
            rendertoolbar: function (statusbar) {
              var container = $("<div style='overflow: hidden; position: relative; margin: 5px;'></div>")
              var csvButton = $("<div style='float: left; margin-left: 5px;'><span style='margin-left: 4px; position: relative; top: -3px;'>CSV hochladen</span></div>")
              var calcButton = $("<div style='float: left; margin-left: 5px;'><span style='margin-left: 4px; position: relative; top: -3px;'>berechnen</span></div>")
//              container.append(csvButton)
//              container.append(calcButton)
//              calcButton.jqxButton({  width: 95, height: 20 })
//              calcButton.click(function (event) {
//                window.smfetch('/api/fin_shares', {cmd: 'do_calc'}).then((data) => {
//                  if (data.error == undefined) {
//                    $('#fin_shares_chart').remove()
//                  }
//                })
//              })
              statusbar.append(container)
            },
            columns : [
              {text:'id', datafield:'id', hidden: true, sortable: false },
              {text:'Name', datafield:'name', editable:false},
              {text:'WKN', datafield: 'wkn', editable:false, width:90, sortable: false},
              {text:'Typ', datafield:'typ', editable:false, width:100, sortable: false},
              {text:'Kurs', datafield:'kurs', editable:false, width:100, cellsalign: 'right', cellsformat: 'c2', sortable: false},
              {text:'30-Tage %', datafield:'d30', editable:false, width:100, cellsalign: 'right', cellsformat: 'f2'},
              {text:'90-Tage %', datafield:'d90', editable:false, width:100, cellsalign: 'right', cellsformat: 'f2'},
              {text:'1 Jahr %', datafield:'d365', editable:false, width:100, cellsalign: 'right', cellsformat: 'f2'},
              {text:'Punkte', datafield:'points', editable:false, width:100, cellsalign: 'right', cellsformat: 'f1'}
            ]
          })
          $("#fin_shares_grid").on("sort", window.module.fin_shares.reload_table)
        }
      } else if (id == 2) {
        if ($('#fin_shares_chart').length == 0) {
          $('#fin_shares_history').append("<div id='fin_shares_chart' style='height: 100%; width: 100%'></div>")
          $('#fin_shares_history').css('overflow', 'hidden')
          window.smfetch('/api/fin_shares', {cmd: 'get_history'}).then((data) => {
            if (data.error == undefined) {
              d = data.data
              var series = []
              series.push({name: 'Betrag', data: [], color: '#0066FF', marker: {enabled: false}, lineWidth: 4})
              var len = d.length
              for (var i=0; i<len; i++) {
                var t = new Date(d[i].date)
                series[0].data.push([t.getTime(), d[i].value])
              }
              var settings = {
                chart: { type: 'areaspline' },
                title : { text: '' },
                subtitle : { text: '' },
                yAxis : { title: {text: 'Euro' }},
                xAxis: {
                  type: 'datetime',
                  title: { text: '' }
                },
                series: series
              }
              H.chart('fin_shares_chart', settings)
            }
          })
        }
      }
    },
    reload_table: function() {
      $("#fin_shares_grid").jqxGrid('updatebounddata')
    },
    load_table: function(data, source, callback) {
      window.smfetch('/api/fin_shares', {cmd: 'get_shares', data: {'page': data.pagenum, 'pagesize': data.pagesize,
                                                                   'sortfield': data.sortdatafield, 'sortorder': data.sortorder}}).then((data) => {
        if (data.error == undefined) {
          for (var i=0; i<data.data.rows.length; i++) {
	          data.data.rows[i].date = new Date(data.data.rows[i].date)
	        }
          callback({ records:data.data.rows, totalrecords : data.data.records})
        }
      })
    }
  }
  window.module.fin_shares = mod
  return mod
})
