# smartlife
# 06.2021
# 09.2021

import copy

from database import database

class price:
    def __init__(self, log, smartlife):
        log.debug("init")
        self.log = log
        sql = "CREATE TABLE IF NOT EXISTS `price` (`id` integer PRIMARY KEY AUTOINCREMENT, `date` date, `aktie` integer, `value` numeric)"
        self.db = database(self.log, smartlife.db_path + "/fin_shares_price_$$.sqlite", sql_create=sql)

    def get_by_id(self, id, d):
        res = 0
#        s = d.strftime("%Y-%m-%d")
#        print(s)
        return res

    def add(self, id, d, value):
        self.log.debug("%s %s" % (str(id), str(value)))   
        self.db.open(d.strftime("%Y"))
        row = self.db.fetchone("select id from price where `date`='%s' and `aktie`='%s'" % (d.strftime("%Y-%m-%d"), str(id)))
        if row:
            sql = "update price set value='%s' where id='%s'" % (str(value), str(row[0]))
        else:
            sql = "insert into price (date, aktie, value) values ('%s', '%s', '%s')" % (d.strftime("%Y-%m-%d"), str(id), str(value))
        self.db.commit(sql)
        self.db.close()

    def get_price(self, id, d):
        self.log.debug("get_price")
        value = 0
        year = int(d.strftime("%Y"))
        sql = "select value from price where aktie='%s' and date<='%s' order by date desc limit 1"
        db = copy.copy(self.db)
        while value == 0 and db.exists(year):
            db.open(year)
            row = db.fetchone(sql % (str(id), d.strftime("%Y-%m-%d"), ))
            if row:
                value = row[0]
            db.close()
            year = year - 1
        return value
