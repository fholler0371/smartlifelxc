# smartlife
# 05.2021
# 06.2021

import json
from threading import Timer
from datetime import datetime

from plugins2 import plugin_base

from fin_shares.price import price
from fin_shares.shares import shares

from fin_shares.currency_me_uk import collector as currency
from fin_shares.goldpreis_de import collector as gold
from fin_shares.onvista_de import collector as onvista
from fin_shares.onvista_de_index import collector as onvista_i

class integration(plugin_base):
    def __init__(self, smartlife, modul):
        plugin_base.__init__(self, smartlife, modul)
        self.alive = [999999999]
        self.price = price(self.log, smartlife)
        self.shares = shares(self.log, smartlife)
        d_now = datetime.now()
        d_start = datetime.strptime(d_now.strftime("%Y-%m-%d 11:00:00"), "%Y-%m-%d %H:%M:%S")
        t = (d_start-d_now).seconds
#        t = 15
        self.daily = Timer(t, self.start_daily)
        self.currency = currency(self.log, self.new_value)
        self.gold = gold(self.log, self.new_value)
        self.onvista = onvista(self.log, self.new_value)
        self.onvista_i = onvista_i(self.log, self.new_value)
        self.daily.start()
        self.tick_timer = None

    def start_daily(self):
        self.log.debug("start_daily")
        if self.daily:
            self.daily.cancel()
        d_now = datetime.now()
        d_start = datetime.strptime(d_now.strftime("%Y-%m-%d 11:00:00"), "%Y-%m-%d %H:%M:%S")
        t = (d_start-d_now).seconds
        self.daily = Timer(t, self.start_daily)
        self.tick()

    def tick(self):
        self.log.debug("tick")
        if not self.running:
            return
        if self.tick_timer:
            self.tick_timer.cancel()
        d = datetime.today()
        # gold
        if not self.shares.updated(1, d):
            self.gold.get_data(self.tick)
            self.tick_timer = Timer(5, self.tick)
            self.tick_timer.start()
            return
        # us-dollar currencies
        if not self.shares.updated(2, d): 
            self.currency.get_data(self.tick)
            self.tick_timer = Timer(5, self.tick)
            self.tick_timer.start()
            return
        # index
        id = self.shares.next_update_index(d)
        if id > 0 and not self.shares.updated(id, d):
            url, exchange, wkn = self.shares.get_base_data(id)
            if url=="":
                self.log.error("Need url for "+ wkn)
                self.tick_timer = Timer(5, self.tick)
                self.tick_timer.start()
                return
#            self.onvista.get_data(url, exchange, wkn, self.tick)
            self.onvista_i.get_data(url, wkn, self.tick)
            self.tick_timer = Timer(5, self.tick)
            self.tick_timer.start()
            return
        # aktien
        id = self.shares.next_update_share(d)
        if id > 0 and not self.shares.updated(id, d):
            url, exchange, wkn = self.shares.get_base_data(id)
            if url=="" or exchange=="":
                self.log.error("Need url and exchange for "+ wkn)
                self.tick_timer = Timer(5, self.tick)
                self.tick_timer.start()
                return
#            print(exchange, wkn)
            self.onvista.get_data(url, exchange, wkn, self.tick)
            self.tick_timer = Timer(5, self.tick)
            self.tick_timer.start()
            return
        for entry in self.cfg["master"]:
            mod = self.smartlife.get_plugin(entry)
            if mod:
                mod.calc_hist()

    def new_value(self, data):
        self.log.debug(data)
        if not( "id" in data):
            data["id"] = self.shares.get_id(data)
        d = datetime.today()
        if self.shares.new_price(data, d):
            self.price.add(data["id"], d, data["value"])

    def pl_data(self, user, cmd, _in, ip):
        self.log.debug(cmd)
        rights = self.rights(user, ip)
        if rights["domain"]:
#            if "card" in rights["sub"] or "read" in rights["sub"]:
#                if cmd == "get_cards":
#                    return self.cards.get_cards()
#                elif cmd == "get_card":
#                    return self.cards.get_card()
            if "read" in rights["sub"]:
                if cmd == "get_shares":
                    return self.shares.get_shares(_in)
#                elif cmd == "get_history":
#                    return self.db.get_history()
#            if "write" in rights["sub"]:
#                if cmd == "upload_prefetch":
#                    return self.importer.upload_prefetch(_in)
#                elif cmd == "do_calc":
#                    return self.db.do_calc()
        return {"allowed": False}

    def name_by_wkn(self, wkn):
        self.log.debug(wkn)
        return self.shares.name_by_wkn(wkn)

    def get_price(self, wkn, d):
        self.log.debug(wkn)
        return self.price.get_price(self.shares.id_by_wkn(wkn), d)

    def stop(self):
        self.log.info(self.name)
        self.running = False
        if self.tick_timer:
            self.tick_timer.cancel()
        if self.daily:
            self.daily.cancel()
#        self.db.stop()