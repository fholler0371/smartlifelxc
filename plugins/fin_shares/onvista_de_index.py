# smartlife
# 06.2021

from threading import Thread

import requests

class fetch(Thread):
    def __init__(self, log, url, wkn, cb, is_done):
        Thread.__init__(self)
        self.name = "currency collector"
        log.debug("init")
        self.log = log
        self.cb = cb
        self.is_done = is_done
        self.url = url
        self.wkn = wkn

    def run(self):
        self.log.debug("run")
        try: 
            r = requests.get(self.url)
            if r.status_code == 200:
                data = r.text
                try:
                    pos = data.find("<ul class=\"KURSDATEN\">")
                    pos = data.find('span',pos)
                    pos = data.find('>',pos)
                    data = data[pos+1:]
                    pos = data.find('<')
                    data = data[:pos]
                    value = float(data.replace(".","").replace(",","."))
                    if value != 0:
                       self.cb({"wkn": self.wkn, "value": value})
                except:
                    pass
                self.is_done()
        except:
            self.log.error("get currency page")

class collector:
    def __init__(self, log, cb):
        log.debug("init")
        self.log = log
        self.cb = cb

    def get_data(self, url, wkn, is_done):
        self.log.debug("get_data")
        th = fetch(self.log, url, wkn, self.cb, is_done)
        th.start()
