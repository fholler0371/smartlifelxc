# smartlife
# 09.2021

from threading import Thread, Timer
from random import random
import subprocess
import requests
import time


class check(Thread):
    def __init__(self, log, client):
        Thread.__init__(self)
        log.debug("init")
        self.log = log
        self.client = client

    def run(self):
        self.log.debug("run")
        isOk = True
        if "ping" in self.client and self.client["ping"]:
            cmd = "ping -c 1 %s" % (self.client["ip"])
            result = subprocess.run(cmd.split(" "), stdout=subprocess.PIPE)
            s = result.stdout.decode()
            if s.find("0 received") > 0:
                time.sleep(10)
                result = subprocess.run(cmd.split(" "), stdout=subprocess.PIPE)
                s = result.stdout.decode()
                if s.find("0 received") > 0:
                    isOk = False
        if not isOk:
            requests.get("http://%s/cm?cmnd=Power%%20Off" % (self.client["sonoff"]))
            time.sleep(10)
            requests.get("http://%s/cm?cmnd=Power%%20On" % (self.client["sonoff"]))

class watch:
    def __init__(self, log, clients):
        log.debug("watch")
        self.log = log
        self.clients = clients
        self.running = True
        for i in range(len(self.clients)):
            self.clients[i]["th"] = Timer(int(180+180*random()), self.doit, [i])
            self.clients[i]["th"].start()

    def doit(self, id):
        self.log.debug("doit: "+str(id))
        if self.running:
            self.clients[id]["th"] = Timer(int(180+180*random()), self.doit, [id])
            self.clients[id]["th"].start()
            th = check(self.log, self.clients[id])
            th.start()

    def stop(self):
        self.log.debug("stop")
        self.running = False
        for i in range(len(self.clients)):
            if self.clients[i]["th"]:
                self.clients[i]["th"].cancel()
