# smartlife
# 09.2021

import json

from plugins2 import plugin_base

from neighbor_watch.watch import watch

class integration(plugin_base):
    def __init__(self, smartlife, modul):
        plugin_base.__init__(self, smartlife, modul)
        self.alive = [999999999]
        self.watch = watch(self.log, self.cfg["clients"])

    def stop(self):
        self.log.info("stop")
        self.watch.stop()

