# smartlife
# 07.2021
# 08.2021
# 09.2021

from threading import Thread, Timer
from datetime import datetime, timedelta
import time
import random
import urllib
import urllib.request
import urllib.parse


from database import database

class history(Thread):
    def __init__(self, log, name, book, master):
        Thread.__init__(self)
        log.debug("init")
        self.log = log
        self.master = master
        sql = "CREATE TABLE IF NOT EXISTS history (date string, value numeric, interest numeric)"
        self.hist = database(self.log, name, sql)
        self.book = database(self.log, book)

    def run(self):
        self.log.debug("run")
        self.hist.open()
        self.book.open()
        self.abschreibung()
        d = datetime.strptime("2019-07-03", "%Y-%m-%d")
        row = self.hist.fetchone("select date from history order by date desc limit 1")
        if row:
            d = datetime.strptime(row[0], "%Y-%m-%d")
        d = d - timedelta(days=31)
        if d.strftime("%Y-%m-%d") < "2019-07-03":
            d = datetime.strptime("2019-07-03", "%Y-%m-%d")
        maxDate = datetime.today()
        l = 0
        sql1 = "select date from history where date = '%s'"
        sql2 = "update history set value='%s', interest='%s' where date='%s'"
        sql3 = "insert into history (value, interest, date) values ('%s', '%s', '%s')"
        sql4 = "SELECT SUM(value) FROM bookings WHERE party like 'DE%%' and date<='%s'"
        sql5 = "SELECT SUM(value) FROM bookings WHERE type='Zinsen' and `date`<='%s'"
        sql6 = "select overdue from abschreibung where date <= '%s' order by date desc limit 1"
        while l < 365 and (maxDate.strftime("%Y-%m-%d") >= d.strftime("%Y-%m-%d")):
            row = self.book.fetchone(sql4 % (d.strftime("%Y-%m-%d")))
            invest = float(row[0])
            row = self.book.fetchone(sql5 % (d.strftime("%Y-%m-%d")))
            zins = 0.0
            if row[0] != None:
                zins = float(row[0])
            row = self.book.fetchone(sql6 % (d.strftime("%Y-%m-%d")))
            if row:
                zins = zins - float(row[0])
            if self.hist.fetchone(sql1  % (d.strftime('%Y-%m-%d'), )):
                sql = sql2
            else:
                sql = sql3
            self.hist.execute(sql % (str(invest), str(zins), d.strftime('%Y-%m-%d')))
            d = d + timedelta(days=1)
            l += 1
        self.hist.commit()
        self.book.close()
        self.hist.close()
        if self.master:
            self.master.calc_hist()

    def abschreibung(self):
        d = datetime.now()
        d = d - timedelta(days=31)
        row = self.book.fetchone("SELECT SUM(`remain`)*0.3 as `value` FROM `credits` WHERE `status` = '100'")
        overdue = row[0]
        row = self.book.fetchone("SELECT SUM(`remain`)*0.7 as `value` FROM `credits` WHERE `status` = '5'")
        overdue += row[0]
        row = self.book.fetchone("select `date` from `abschreibung` where `date`='%s'" % (d.strftime("%Y-%m-%d")))
        if row:
            self.book.commit("update `abschreibung` set `overdue`='%s' where `date`='%s'" % (str(overdue), d.strftime("%Y-%m-%d")))
        else:
            self.book.commit("insert into `abschreibung` (`date`, `overdue`) values ('%s', '%s')" % (d.strftime("%Y-%m-%d"), str(overdue)))

class db:
    def __init__(self, log, smartlife, master, remote):
        log.debug("init")
        self.log = log
        self.smartlife = smartlife
        self.master = master
        self.remote = remote
        sql = "CREATE TABLE IF NOT EXISTS bookings (id INTEGER, date string, name string, text string, "
        sql += "verwendungszweck string, value numeric, PRIMARY KEY(id))"
        self.book = database(self.log, smartlife.db_path + "/fin_p2p_bondora.sqlite", sql)
        self.hist = database(self.log, smartlife.db_path + "/fin_p2p_bondora_history.sqlite")
        t = (int(time.time()/86400)+1)*86400-time.time()+60+300*random.random()
        self.timer_calc = Timer(t, self.tick_calc)
        self.timer_calc.start()

    def tick_calc(self):
        self.log.debug("tick_calc")
        t = (int(time.time()/86400)+1)*86400-time.time()+60+300*random.random()
        self.timer_calc = Timer(t, self.tick_calc)
        self.timer_calc.start()
        self.do_calc()

    def get_card(self):
        self.log.debug("get_card")
        out = {"invest": 0, "sum" : 0}
        self.hist.open()
        row = self.hist.fetchone("select value, interest from history order by date desc limit 1")
        if row:
            out['invest'] = float("%.2f" % round(row[0], 2))
            out['sum'] = float("%.2f" % round(row[1], 2)) + out['invest']
        self.hist.close()
        return {"base": out}

    def get_book(self, data):
        self.log.debug(data)
        out = {"records": 0, "rows": [], "active": True}
        self.book.open()
        row = self.book.fetchone("SELECT value FROM var WHERE name='active'")
        if row:
            out["active"] = row[0] == '1'
        row = self.book.fetchone("select count(id) from bookings")
        if row:
            out["records"] = row[0]
        sql = "select id, date, text, type, value from bookings order by date desc limit %s, %s"
        for row in self.book.fetchall(sql % (str(data["page"]*data['pagesize']), str(data['pagesize']), )):
            out["rows"].append({'id': row[0], 'date': row[1], 'text': row[2], 'type': row[3], 'amount': row[4]})
        self.book.close()
        return {"allowed": True, "data": out}

    def add(self, date, name, b_text, v_text, value):
        self.log.debug(date)
        self.book.open()
        sql = "select id from bookings where date='%s' and name='%s' and text='%s' and verwendungszweck='%s' and value='%s'"
        row = self.book.fetchone(sql % (date, name, b_text, v_text, str(value), ))
        if row == None:
            sql = "insert into bookings (date, name, text, verwendungszweck, value) values ('%s', '%s', '%s', '%s', '%s' ) "
            self.book.commit(sql % (date, name, b_text, v_text, str(value), ))
        self.book.close()

    def do_calc(self):
        self.log.debug("do_calc")
        th = history(self.log, self.smartlife.db_path + "/fin_p2p_bondora_history.sqlite", 
            self.smartlife.db_path + "/fin_p2p_bondora.sqlite", self.smartlife.get_plugin(self.master))
        th.start()
        return {"allowed": True, "data": {}}

    def get_history(self):
        self.log.debug("get_history")
        out = []
        self.hist.open()
        for entry in self.hist.fetchall("select date, value, interest from history order by date asc"):
            out.append({'date': entry[0], 'value': entry[1], 'interest': entry[2]})
        self.hist.close()
        return {"allowed": True, "data": out}

    def set_active(self, data):
        self.log.debug(data)
        val = "0"
        if data['state']:
            val = "1"
        self.book.open()
        self.book.commit("UPDATE var set value='%s' WHERE name='active'" % (val))
        self.book.close()
        if data['state']:
            self.remote.grap()
        return {"allowed": True, "data": {}}

    def stop(self):
        self.log.debug("stop")
        if self.timer_calc:
            self.timer_calc.cancel()
            