# smartlife
# 07.2021
# 08.2021
# 09.2021

import json
import random
import time
from datetime import datetime, timedelta
from threading import Thread, Timer

from import_csv import csv
from database import database
from webgraber import webgraber

class collect_data(Thread):
    def __init__(self, log, book, client, user, password, secret, calc):
        Thread.__init__(self)
        log.debug("init")
        self.log = log
        self.book = book
        self.client = client
        self.secret = secret
        self.user = user
        self.password = password
        self.calc = calc

    def run(self):
        self.log.debug("run")
        self.book.open()
        self.book.fetchone("UPDATE var SET value='0' WHERE name='active'")
        if self.grap_www():
            self.make_report()
            self.get_balance()
            self.get_investments()
            self.check_reports()
            self.book.fetchone("UPDATE var SET value='1' WHERE name='active'")
            self.update_trans_type()
        self.book.close()
        if self.calc:
            self.calc()

    def make_report(self):
        self.log.debug("make_report")
        time.sleep(2)
        maxDate = datetime.now()
        minDate = maxDate - timedelta(days=31)
        www = webgraber(self.log)
        www.encoding = "iso-8859-1"
        www.RequestPostJson("https://api.bondora.com/api/v1/report", {'ReportType': 'AccountStatement',
                                                                      'PeriodStart': minDate.strftime("%Y-%m-%d"),
                                                                      'PeriodEnd': maxDate.strftime("%Y-%m-%d")})
        www.addheader('Authorization', ' Bearer ' + self.token)
        www.addheader('Content-Type', 'application/json')
        www.open()
        if www.code == 202:
            payload = json.loads(www.res.read().decode(www.encoding))
            if payload['Success']:
                report_id = payload['Payload']['ReportId']
                sql = "insert into `reports` (`report`) values ('%s')"
                self.book.commit(sql % (report_id))
                sql = "update `var` set `value`='%s' where `name`='report_last'"
                self.book.commit(sql % (maxDate.strftime("%Y-%m-%d")))

    def get_balance(self):
        self.log.debug("get_balance")
        time.sleep(2)
        www = webgraber(self.log)
        www.encoding = "iso-8859-1"
        www.RequestGet("https://api.bondora.com/api/v1/account/balance")
        www.addheader('Authorization', ' Bearer ' + self.token)
        if www.open():
            payload = json.loads(www.page)
            if payload['Success']:
                balance = float(payload['Payload']['Balance'])
                reserved = float(payload['Payload']['Reserved'])
                d = datetime.now()
                sql = "insert into `avaible` (`date`, `balance`, `reserved`) values ('%s', '%s', '%s')"
                self.book.commit(sql % (d.strftime("%Y-%m-%d"), str(balance), str(reserved)))

    def get_investments(self):
        self.log.debug("get_investments")
        time.sleep(2)
        www = webgraber(self.log)
        www.encoding = "iso-8859-1"
        www.RequestGet("https://api.bondora.com/api/v1/account/investments?IsInRepayment=true")
        www.addheader('Authorization', ' Bearer ' + self.token)
        if www.open():
            payload = json.loads(www.page)
            if payload['Success']:
                d = datetime.now()
                for loan in payload["Payload"]:
                    if loan["LastPaymentDate"] == None:
                        loan["LastPaymentDate"] = "1900-01-01"
                    row = self.book.fetchone("select `id` from `credits` where `number`='%s'" % (loan["InvestmentNumber"]))
                    if row == None:
                        if not("DateOfBirth" in loan) or loan["DateOfBirth"] == None:
                            loan["DateOfBirth"] = "1900-01-01"
                        sql = "insert into `credits` (`number`, `date_buy`, `rating`, `invest`, `birth`, `gender`, `interest`, `interest_paid`, "
                        sql += "`paid_last`, `months`, `date_status`, `status`, `paid_next`, `value_next`, `remain`, `re_paid`, `stamp`) values "
                        sql += "('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')"
                        self.book.commit(sql % (loan['InvestmentNumber'], loan['PurchaseDate'][:10], loan['Rating'], str(loan['Amount']), 
                          loan['DateOfBirth'][:10], str(loan['Gender']), str(loan['Interest']), str(loan['InterestRepaid']), loan['LastPaymentDate'][:10],
                          str(loan['LoanDuration']), loan['LoanStatusActiveFrom'][:10], str(loan['LoanStatusCode']), loan['NextPaymentDate'][:10],
                          str(loan['NextPaymentSum']), str(loan['PrincipalRemaining']), str(loan['PrincipalRepaid']), d.strftime("%Y-%m-%d")))
                    else:
                        if loan['NextPaymentDate'] == None:
                            loan['NextPaymentDate'] = '1900-01-01'
                        if loan['NextPaymentSum'] == None:
                            loan['NextPaymentSum'] = 0
                        sql = "update `credits` set `stamp`='%s', `interest_paid`='%s', `paid_last`='%s', `date_status`='%s', "
                        sql += "`status`='%s', `paid_next`='%s', `value_next`='%s', `remain`='%s', `re_paid`='%s' where `id`='%s'"
                        self.book.commit(sql % (d.strftime("%Y-%m-%d"), str(loan['InterestRepaid']), loan['LastPaymentDate'][:10],
                           str(loan['LoanStatusActiveFrom'][:10]), str(loan['LoanStatusCode']), loan['NextPaymentDate'][:10], str(loan['NextPaymentSum']),
                           str(loan['PrincipalRemaining']), str(loan['PrincipalRepaid']), str(row[0])))

    def check_reports(self):
        self.log.debug("check_reports")
        time.sleep(2)
        rows = self.book.fetchall("select `id`, `report` from `reports` where `read`=0")
        if len(rows):
            www = webgraber(self.log)
            www.encoding = "iso-8859-1"
            www.RequestGet("https://api.bondora.com/api/v1/reports")
            www.addheader('Authorization', ' Bearer ' + self.token)
            if www.open():
                payload = json.loads(www.page)
                for i in range(payload['Count']):
                    for entry in rows:
                        if entry[1] == payload['Payload'][i]['ReportId']:
                            www.RequestGet("https://api.bondora.com/api/v1/report/"+entry[1])
                            www.addheader('Authorization', ' Bearer ' + self.token)
                            if www.open():
                                payload_data = json.loads(www.page)
                                if payload_data['Success']:
                                    try:
                                        for book in payload_data['Payload']['Result']:
                                            sql = "select `id` from `bookings` where `bondora_id`='%s'"
                                            book_row = self.book.fetchone(sql % (str(book['Number'])))
                                            if book_row == None:
                                                sql = "insert into `bookings` (`bondora_id`, `value`, `party`, `text`, `loan`, `date`) values "
                                                sql += "('%s', '%s', '%s', '%s', '%s', '%s')"
                                                self.book.commit(sql % (str(book['Number']), str(book['Amount']), str(book['Counterparty']),
                                                  str(book['Description']), str(book['LoanNumber']), str(book['TransferDate'][:10])))
                                    except:
                                        pass
                                    self.book.commit("Update `reports` set `read`=1 where `id`='%s'" % (str(entry[0])))

    def update_trans_type(self):
        self.log.debug("update_trans_type")
        self.book.commit("update `bookings` set type='Einzahlung' where (type='' or type is NULL) and text like '%Deposit%'")
        self.book.commit("update `bookings` set type='Invest' where (type='' or type is NULL) and text like '%AuctionBidWinner%'")
        self.book.commit("update `bookings` set type='Reinvest' where (type='' or type is NULL) and text like '%MainRepaiment%'")
        self.book.commit("update `bookings` set type='Zinsen' where (type='' or type is NULL) and text like '%InterestRepaiment%'")

    def grap_www(self):
        www = webgraber(self.log)
        www.encoding = "iso-8859-1"
        www.RequestGet("https://www.bondora.com/de/oauth/authorize?response_type=code&client_id="+self.client+"&state=001&scope=Investments%20ReportCreate%20ReportRead")
        if www.open() == False:
            self.log.error("Do not get authorize topen page")
            return False
        token = www.getdata(['__RequestVerificationToken', 'value="'],'"')
        if token == "":
            self.log.error("no token")
            return False
        url = "https://www.bondora.com/en/login?ReturnUrl=%2Foauth%2Fauthorize%3Fresponse_type%3Dcode%26client_id%3D"+self.client
        url += "%26state%3D001%26scope%3DInvestments%2520ReportCreate%2520ReportRead&response_type=code&client_id="+self.client
        url += "d&state=001&scope=Investments%20ReportCreate%20ReportRead"
        returl = '/oauth/authorize?response_type=code&client_id='+self.client+'&state=001&scope=Investments%20ReportCreate%20ReportRead'
        www.RequestPost(url, {'__RequestVerificationToken':token, 'Email':self.user, 'Password':self.password, 'returnUrl': returl})
        if www.open() == False:
            self.log.error("login no success")
            return False
        try:
            code = www.last_url.split("code=")[1].split('&')[0]
        except:
            code = ""
        if code == "":
            self.log.error("no code")
            return False
        time.sleep(2)
        www.RequestPost("https://api.bondora.com/oauth/access_token", 
                        {"grant_type": "authorization_code",
                         "client_id": self.client,
                         "client_secret": self.secret,
                         "code": code, 
                         "redirect_uri": "https://api.holler.pro/run/bondora"})
        if www.open() == False:
            self.log.error("Do not get access_token")
            return False
        try:
            self.token = json.loads(www.page)['access_token']
        except:
            self.log.error("no access_token")
            return False
        return True

class remote:
    def __init__(self, log, smartlife, get_secret, login):
        log.debug("init")
        self.calc = None
        self.log = log
        self.login = login
        self.get_secret = get_secret
        self.running = True
        self.book = database(self.log, smartlife.db_path + "/fin_p2p_bondora.sqlite")
        t = (int(time.time()/86400)+1)*86400-time.time()+60+300*random.random()+3600
        self.th = Timer(t, self.grap_tick)
        self.th.start()        

    def grap(self):
        self.log.debug("grap")
        self.book.open()
        row = self.book.fetchone("SELECT value from var WHERE name='active'")
        self.book.close()
        if row and row[0] == "1":
            _client = self.get_secret(self.login["client"])
            _secret = self.get_secret(self.login["secret"])
            _user = self.get_secret(self.login["user"])
            _password = self.get_secret(self.login["password"])
            th = collect_data(self.log, self.book, _client, _user, _password, _secret, self.calc)
            th.start()
        else:
            self.log.error("not active")  

    def grap_tick(self):
        if self.running:
            t = (int(time.time()/86400)+1)*86400-time.time()+60+300*random.random()+3600
            self.th = Timer(t, self.grap_tick)
            self.th.start()
            self.grap() 

    def run(self, calc):
        self.log.debug("run")
        self.calc = calc  
        t = (int(time.time()/86400)+1)*86400-time.time()+60+300*random.random()+3600
        self.th = Timer(t, self.grap_tick)
        self.th.start()  

    def stop(self):
        self.log.debug("run")
        self.running = False
        if self.th:
            self.th.cancel()    
