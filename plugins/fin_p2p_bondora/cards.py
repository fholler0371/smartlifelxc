# smartlife
# 06.2021
# 08.2021

class cards:
    def __init__(self, log, db):
        log.debug("init")
        self.log = log
        self.db = db

    def get_cards(self):
        self.log.debug("get_cards")
        out = [{"mod": "fin_p2p_bondora", "card": "fin_p2p_bondora_sum", "name": "Bondora Summe"}]
#               {"mod": "fin_p2p_bondora", "card": "fin_p2p_bondora_month", "name": "Bondora Giro Monat"},
#               {"mod": "fin_p2p_bondora", "card": "fin_p2p_bondora_year", "name": "Bondora Giro Jahr"}]
        return {"allowed": True, "data": out}

    def get_card(self):
        self.log.debug("get_card")
        out = {'sum': 0}
        out = self.db.get_card()
        return {"allowed": True, "data": out}
