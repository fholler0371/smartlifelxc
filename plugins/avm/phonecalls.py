# smartlife
# 05.2021

from datetime import datetime

from database import database

class phonecalls:
    def __init__(self, log, smartlife):
        log.debug("init")
        self.log = log
        self.smartlife = smartlife
        sql = "CREATE TABLE IF NOT EXISTS phonecalls (type integer, name text, time integer, duration integer, number text)"
        self.db = database(log, smartlife.db_path + "/avm_phonecalls.sqlite", sql)
        self.sql_find = "select number from phonecalls where time='%s'"
        self.sql_insert = "insert into phonecalls (type, name, time, duration, number) values ('%s', '%s', '%s', '%s', '%s')"

    def add(self, record):
        self.log.debug(record)
        if "duration" in record:
            hours, minutes = [int(part) for part in record["duration"].split(':', 1)]
            record["duration"] = (hours*60+minutes)*60
        else:
            record["duration"] = 0
        if "time" in record:
            record["time"] = int(datetime.strptime(record["time"], '%d.%m.%y %H:%M').timestamp())
        else: 
            record["time"] = 0
        if "type" in record:
            record["type"] = int(record["type"])
        else:
            record["type"] = 0
        if record["type"] == 3:
            record["number"] = record["called"]
        elif record["type"] == 1 or record["type"] == 2:
            record["number"] = record["caller"]
        if "called" in record:
            del record["called"] 
        if "caller" in record:
            del record["caller"] 
        if record["type"] > 0 and record["type"] < 10:
            self.db.open()
            if not self.db.fetchone(self.sql_find % (str(record["time"]), )):
                self.db.commit(self.sql_insert % (str(record["type"]), record["name"], str(record["time"]), str(record["duration"]), record["number"], ))
            self.db.close()

    def getlast(self):
        out = []
        self.db.open()
        for entry in self.db.fetchall("select type, name, time, number from phonecalls order by time desc limit 7"):
            rec = {"type": entry[0], "number": entry[1], "time": datetime.fromtimestamp(entry[2]).strftime('%d.%m.%y %H:%M')}
            if rec["number"] == "":
                rec["number"] = entry[3]
            out.append(rec)
        self.db.close()
        return out