define(['module', 'packery', 'jqxtabs', 'jqxdata', 'jqxgrid', 'jqxgrid_selection', 'jqxcheckbox', 'jqxgrid_edit'], 
        function(module, Packery) {
    var mod = {
      t_update_1: null,
      t_update_2: null,
      t_update_3: null,
      wdata_last : 0,
      wdata_call : 0,
      wdata : undefined,
      map : undefined,
      mapmarker : undefined,
      year : 0,
      init : function() {
        window.module.avm.sub = window.module.avm.init_data.p1
        var sub = window.module.avm.sub
        if (sub == "app") {
          html = '<div id="avm_cards" style="width:100%; height:100%; margin: 10px;" class="jqx-widget-content-custom-scheme"></div>'
          $('#client_area').html(html)
        } else {
          html = '<div style="width:100%; height:100%;"><div id="avm_tab"><ul><li>Karten</li><li>Sensoren</li></ul>'
          html += '<div><div id="avm_cards" style="width:100%; height:100%;"></div></div>'
          html += '<div><div id="avm_sensor" style="width:100%; height:100%;"><div id="avm_sensor_grid" style="width:100%; height:100%;"></div></div></div>'
          $('#client_area').html(html+'</div>')
          $(window).off('resize', window.module.avm.resize)
          $(window).on('resize', window.module.avm.resize)
          window.module.avm.resize()
          $('#avm_tab').jqxTabs({ width: '100%', height: '100%', position: 'top'})
          $('#avm_tab').on('selected', function(ev) {
            var selectedTab = ev.args.item
            window.module.avm.call_tab(selectedTab)
          })
        }
        window.module.avm.call_tab(0)
      },
      check_data : function() {
        var now = new Date()
        var t = Math.round(now.getTime() / 1000)
        if (t - window.module.avm.wdata_last > 60) {
          if (t - window.module.avm.wdata_call > 10) {
            window.module.avm.wdata_call = t
            window.smfetch('/api/avm', {cmd: 'get_card'}).then((data) => {
              if (data.error == undefined) {
                window.module.avm.wdata_last = t
                window.module.avm.wdata = data.data
              }
            })
          }
        }
      },
      update_1 : function() {
        clearTimeout(window.module.avm.t_update_1)
        window.module.avm.check_data()
        var cards =  $("#avm_cards").find(".cards_avm_state")
        var len = cards.length
        if (window.module.avm.wdata != undefined) {
          var ele = $(cards[0])
          var d = window.module.avm.wdata.status
          if (JSON.stringify(ele.data('last')) != JSON.stringify(d)) {
            ele.data('last', d)
            html = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">'
            html += 'Status</div>'
            html += '<div style="position: absolute; top: 25px; left: 10px"><b>Verbunden</b></div>'
            html += '<div style="position: absolute; top: 25px; right: 10px">'+d.connected.val+'</div>'
            html += '<div style="position: absolute; top: 45px; left: 10px"><b>Laufzeit</b></div>'
            html += '<div style="position: absolute; top: 45px; right: 10px">'+d.uptime.val+'</div>'
            html += '<div style="position: absolute; top: 65px; left: 10px"><b>IP-Addressen</b></div>'
            html += '<div style="position: absolute; top: 65px; right: 10px'
            html += '">'+d.ip4.val+'</div>'
            html += '<div style="position: absolute; top: 85px; right: 10px'
            html += '">'+d.ip6.val+'</div>'
            html += '<div style="position: absolute; top: 105px; left: 10px"><b>Präfix</b></div>'
            html += '<div style="position: absolute; top: 105px; right: 10px">'+d.ip6pre.val+'</div>'
            html += '<div style="position: absolute; top: 125px; left: 10px"><b>Upload</b></div>'
            html += '<div style="position: absolute; top: 125px; right: 80px"'
            if (d.sent.chart != undefined) {
              html += ' class="cards_chart" data-chart="/api/avm:'+d.sent.chart+'"'
            }
            html += '>'+(d.sent.val/1024).toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2})+' kB/s </div>'
            html += '<div style="position: absolute; top: 125px; right: 10px">'
            html += (d.sent.val/d.sent_max.val*100).toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2})+' %</div>'
            html += '<div style="position: absolute; top: 145px; left: 10px"><b>Download</b></div>'
            html += '<div style="position: absolute; top: 145px; right: 80px"'
            if (d.resv.chart != undefined) {
              html += ' class="cards_chart" data-chart="/api/avm:'+d.resv.chart+'"'
            }
            html += '>'+(d.resv.val/1024).toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2})+' kB/s </div>'
            html += '<div style="position: absolute; top: 145px; right: 10px">'
            html += (d.resv.val/d.resv_max.val*100).toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2})+' %</div>'
            ele.html(html)
          }
          window.module.avm.t_update_1 = setTimeout(window.module.avm.update_1, 15000)
        } else {
          window.module.avm.t_update_1 = setTimeout(window.module.avm.update_1, 250)
        }
      },
      update_2 : function() {
        clearTimeout(window.module.avm.t_update_2)
        window.module.avm.check_data()
        var ele =  $("#avm_cards").find(".cards_avm_transfer")
        if (window.module.avm.wdata != undefined) {
          var d = window.module.avm.wdata.transfer
          if (JSON.stringify(ele.data('last')) != JSON.stringify(d)) {
            ele.data('last', d)
            html = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">'
            html += 'Transfervolumen (Gestern)</div>'
            html += '<div style="position: absolute; top: 25px; left: 10px"><b>gesendet</b></div>'
            html += '<div style="position: absolute; top: 25px; right: 10px"'
            if (d.sent.chart != undefined) {
              html += ' class="cards_chart" data-chart="/api/avm:'+d.sent.chart+'"'
            }
            html += '>'+(d.sent.val/1073741824).toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' GByte</div>'
            html += '<div style="position: absolute; top: 45px; left: 10px"><b>empfangen</b></div>'
            html += '<div style="position: absolute; top: 45px; right: 10px"'
            if (d.received.chart != undefined) {
              html += ' class="cards_chart" data-chart="/api/avm:'+d.received.chart+'"'
            }
            html += '>'+(d.received.val/1073741824).toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' GByte</div>'
            ele.html(html)
          }
          window.module.avm.t_update_2 = setTimeout(window.module.avm.update_2, 30000)
        } else {
          window.module.avm.t_update_2 = setTimeout(window.module.avm.update_2, 250)
        }
      },
      update_3 : function() {
        clearTimeout(window.module.avm.t_update_3)
        window.module.avm.check_data()
        var ele =  $("#avm_cards").find(".cards_avm_phonecalls")
        if (window.module.avm.wdata != undefined) {
          var d = window.module.avm.wdata.phonecalls
          if (JSON.stringify(ele.data('last')) != JSON.stringify(d)) {
            ele.data('last', d)
            html = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">'
            html += 'Anrufe</div>'
            var len = d.length
            for (var i=0; i<len; i++) {
              html += '<div style="position: absolute; top: ' + (i*20+25) + 'px; left: 10px"><img src="/lib/img/telephone_'
              if (d[i].type == 1) {html += 'blue'}  
              if (d[i].type == 2) {html += 'red'} 
              if (d[i].type == 3) {html += 'green'} 
              html += '.svg" style="height: 18px; width: 18px;"></div>'
              html += '<div style="position: absolute; top: ' + (i*20+25) + 'px; left: 35px"><b>' + d[i].number + '</b></div>' 
              html += '<div style="position: absolute; top: ' + (i*20+25) + 'px; right: 10px">' + d[i].time + '</div>' 
            }
            ele.html(html)
          }
          window.module.avm.t_update_3 = setTimeout(window.module.avm.update_3, 30000)
        } else {
          window.module.avm.t_update_3 = setTimeout(window.module.avm.update_3, 250)
        }
      }, 
      get_card : function(data, div) {
        if (data["card"].startsWith("avm_state")) {
          if (div.find(".cards_"+data["card"]).length == 0) {
            var html = $('<div class="cards_state cards_'+data["card"]+' card card_h2"><span></span></div>')
            html.data('card', data.card)
            div.append(html)
          } else {
//            console.log(data)
          }
          window.module.avm.update_1()
        } else if (data["card"].startsWith("avm_transfer")) {
          if (div.find(".cards_"+data["card"]).length == 0) {
            var html = $('<div class="cards_avm_transfer card"><span></span></div>')
            html.data('card', data.card)
            div.append(html)
          } else {
//            console.log(data)
          }
          window.module.avm.update_2()
        } else if (data["card"].startsWith("avm_phonecalls")) {
          if (div.find(".cards_"+data["card"]).length == 0) {
            var html = $('<div class="cards_avm_phonecalls card card_h2"><span></span></div>')
            html.data('card', data.card)
            div.append(html)
          } else {
//            console.log(data)
          }
          window.module.avm.update_3()
        }
      },
      create_cards : function() {
        var ok = 1
        var cards = window.module.avm.cards
        var l = cards.length
        for (var i=0; i<l; i++) {
           if (!(typeof(window.module[cards[i].mod].get_card) === 'function')) {
             console.log("Modul not found: "+cards[i].mod)
             ok = 0
           }
        }
        if (ok == 1) {
          for (var i=0; i<l; i++) {
            window.module[cards[i].mod].get_card(cards[i], $("#avm_cards"))
            require( [ 'jquery-bridget/jquery-bridget' ], function( jQueryBridget ) {
              jQueryBridget( 'packery', Packery, $ );
              var grid = $('#avm_cards').packery({
                itemSelector: '.card',
                gutter: 10
              })
            })
          }
        } else {
          console.log("recheck")
        }
      },
      call_tab: function(id) {
        if (id == 0) {
          window.smfetch('/api/avm', {cmd: 'get_cards'}).then((data) => {
            if (data.error == undefined) {
              window.module.avm.cards = data.data
              window.module.avm.cards_check_mod = 10
              window.module.avm.create_cards()
            }
          })
        } else if (id == 1) {
          window.smfetch('/api/avm', {cmd: 'get_sensor'}).then((data) => {
            if (data.error == undefined) {
              var source = {
                localdata: data.data,
                datatype: "array",
                datafields: [
                  { name: 'topic', type: 'string'},
                  { name: 'value', type: 'string'},
                  { name: 'entry', type: 'string'},
                  { name: 'sent', type: 'boolean'},
                  { name: 'hist_num', type: 'boolean'},
                  { name: 'hist_event', type: 'boolean'}
                ]                
              }
              var dataAdapter = new $.jqx.dataAdapter(source, {
                downloadComplete: function (data, status, xhr) { },
                loadComplete: function (data) { },
                loadError: function (xhr, status, error) { }
              })
              var col = [
                { text: 'Sensor', datafield: 'topic', width: 200, editable: false},
                { text: 'Bezeichnung', datafield: 'entry'},
                { text: 'Wert', datafield: 'value', width: 300, editable: false, cellsalign: 'right'},
                { text: 'Senden', datafield: 'sent', width: 75, columntype: 'checkbox'},
                { text: 'History', datafield: 'hist_num', width: 75, columntype: 'checkbox'},
                { text: 'History Event', datafield: 'hist_event', width: 125, columntype: 'checkbox'}
              ]
              $("#avm_sensor_grid").jqxGrid({
                width: '100%',
                height: '100%',
                source: dataAdapter,
                columns: col,
                editable: true
              })
              $("#avm_sensor_grid").off('cellendedit')
              $("#avm_sensor_grid").on('cellendedit', function (event) {
                var args = event.args;
                window.smfetch('/api/avm', {cmd: 'set_sensor', data: {
                  field: args.datafield, value: args.value, topic: args.row.topic
                }}).then((data) => {})
              })
              $("#avm_sensor").css('padding', 0).css('overflow', 'hidden')
            }
          })
        } 
      },
      stop : function() {
        clearTimeout(window.module.avm.t_update_1)
        clearTimeout(window.module.avm.t_update_2)
        clearTimeout(window.module.avm.t_update_3)
        if (window.module.avm.map != undefined) {
          window.module.avm.map.remove()
          window.module.avm.map = undefined
        }
      },
      resize: function() {
        $('#client_area').children().height($('#client_area').height())
      }
    }
    mod['init_data'] = window.module_const[module.id]
    window.module.avm = mod
    return mod
  })
