# smartlife
# 05.2021
# 09.2021

import os
import json
import time
from threading import Timer

import history_event
import history_num
from database import database

SAVE_INTERVAL = 1800

class values:
    def __init__(self, log, smartlife):
        log.debug("init")
        self.log = log
        self.smartlife = smartlife
        self.jname = smartlife.db_path + "/avm_data.json"
        self.value = {}
        if os.path.isfile(self.jname):
            f = open(self.jname, "r")
            self.value = json.loads(f.read())
            f.close()
        self.name_friendly = smartlife.db_path + "/avm_friendly.json"
        self.friendly = {}
        if os.path.isfile(self.name_friendly):
            f = open(self.name_friendly, "r")
            self.friendly = json.loads(f.read())
            f.close()
        self.name_sent = smartlife.db_path + "/avm_sent.json"
        self.sent = []
        if os.path.isfile(self.name_sent):
            f = open(self.name_sent, "r")
            self.sent = json.loads(f.read())
            f.close()
        self.db_num = history_num.db(self.log, smartlife.db_path + "/avm_history_$$.sqlite", self.get_num_data)
        self.name_num = smartlife.db_path + "/avm_history_num.json"
        self.history_num = []
        if os.path.isfile(self.name_num):
            f = open(self.name_num, "r")
            self.history_num = json.loads(f.read())
            f.close()
        self.name_event = smartlife.db_path + "/avm_history_event.json"
        self.history_event = []
        if os.path.isfile(self.name_event):
            f = open(self.name_event, "r")
            self.history_event = json.loads(f.read())
            f.close()
        self.db_event = history_event.db(self.log, smartlife.db_path + "/avm_history_event_$$.sqlite")
        self.save_timer = None
        self.running = True
        self.rate = {"resv": {"time": 0, "value": 0}, "sent": {"time": 0, "value": 0}}
        self.name_zero = smartlife.db_path + "/avm_zero.json"
        self.zero = []
        if os.path.isfile(self.name_zero):
            f = open(self.name_zero, "r")
            self.zero = json.loads(f.read())
            f.close()
        self.th_volume = None
        sql = "CREATE TABLE IF NOT EXISTS history (time integer, topic text, value float, value_min float, value_max float)"
        self.week = database(self.log, smartlife.db_path + "/avm_history_one_week.sqlite", sql_create=sql)

    def new_values(self, data):
        self.log.debug(data)
        if data["topic"] in self.value:
            old = self.value[data["topic"]]
        else:
            old = None
        self.value[data["topic"]] = data["value"]
        self.db_event.store(old, data, self.history_event)
        store = self.smartlife.get_plugin("data_store")
        if data["topic"] == "core/received":
            if self.rate["resv"]["time"] != 0:
                t = data["ts"] - self.rate["resv"]["time"]
                value = data["value"] - self.rate["resv"]["value"]
                if value > 0:
                    if "core/rate_resv" in self.value:
                        old = self.value["core/rate_resv"]
                    else:
                        old = None
                    self.value["core/rate_resv"] = value/t
                    self.send_out({"topic": "core/rate_resv", "value": data["value"], "ts": data["ts"]}, store)
                    self.db_event.store(old, {"topic": "core/rate_resv", "value": data["value"], "ts": data["ts"]}, self.history_event)
                else:
                    self.zero.append({"ts": self.rate["resv"]["time"], 
                                      "received": self.rate["resv"]["value"],
                                      "sent": self.rate["sent"]["value"]})
                    f = open(self.name_zero, "w")
                    f.write(json.dumps(self.zero))
                    f.close()
                    while len(self.zero) > 25:
                        self.zero.pop(0)
            self.rate["resv"]["time"] = data["ts"]
            self.rate["resv"]["value"] = data["value"]
        elif data["topic"] == "core/sent":
            if self.rate["sent"]["time"] != 0:
                t = data["ts"] - self.rate["sent"]["time"]
                value = data["value"] - self.rate["sent"]["value"]
                if value > 0:
                    if "core/rate_sent" in self.value:
                        old = self.value["core/rate_sent"]
                    else:
                        old = None
                    self.value["core/rate_sent"] = value/t
                    self.send_out({"topic": "core/rate_sent", "value": data["value"], "ts": data["ts"]}, store)
                    self.db_event.store(old, {"topic": "core/rate_sent", "value": data["value"], "ts": data["ts"]}, self.history_event)
            self.rate["sent"]["time"] = data["ts"]
            self.rate["sent"]["value"] = data["value"]
        self.send_out(data, store)

    def send_out(self, data, store):
        self.log.debug(data)
        if store and (data["topic"] in self.sent):
            msg = {"topic": "avm/" + data["topic"], "friendly": "avm/" + data["topic"], "value": data["value"], "ts": data["ts"]}
            if data["topic"] in self.friendly:
                msg["friendly"] = self.friendly[data["topic"]]
            store.new_local(msg)

    def check_change(self, old, data):
        self.log.debug("check_change")
        if data["topic"] in self.history_event and old != data["value"]:
            self.log.error(data)

    def get_sensor(self):
        self.log.debug("get_sensor")
        out = []
        for entry in self.value:
            rec = {"topic": entry, "value": self.value[entry], "entry": "avm/" + entry, 
                   "sent": False, "hist_num": False, "hist_event": False}
            if entry in self.friendly:
                rec["entry"] = self.friendly[entry]
            if entry in self.sent:
                rec["sent"] = True
            if entry in self.history_num:
                rec["hist_num"] = True
            if entry in self.history_event:
                rec["hist_event"] = True
            out.append(rec)
        out = sorted(out, key=lambda x: x["entry"], reverse=False)
        return {"allowed": True, "data": out}

    def set_sensor(self, data):
        self.log.debug(data)
        if data["field"] == "entry":
            self.friendly[data["topic"]] = data["value"]
            f = open(self.name_friendly, "w")
            f.write(json.dumps(self.friendly))
            f.close()
        if data["field"] == "sent":
            if data["value"]:
                if not(data["topic"] in self.sent):
                    self.sent.append(data["topic"])
            else:
                if data["topic"] in self.sent:
                    self.sent.remove(data["topic"])
            f = open(self.name_sent, "w")
            f.write(json.dumps(self.sent))
            f.close()
        if data["field"] == "hist_num":
            if data["value"]:
                if not(data["topic"] in self.history_num):
                    self.history_num.append(data["topic"])
            else:
                if data["topic"] in self.history_num:
                    self.history_num.remove(data["topic"])
            f = open(self.name_num, "w")
            f.write(json.dumps(self.history_num))
            f.close()
        if data["field"] == "hist_event":
            if data["value"]:
                if not(data["topic"] in self.history_event):
                    self.history_event.append(data["topic"])
            else:
                if data["topic"] in self.history_event:
                    self.history_event.remove(data["topic"])
            f = open(self.name_event, "w")
            f.write(json.dumps(self.history_event))
            f.close()
        return {"allowed": True, "data": {}}

    def get_value(self, topic):
        self.log.debug(topic)
        if topic in self.value:
            return self.value[topic]
        return ""

    def get_num_data(self):
        self.log.debug("get_num_data")
        for entry in self.history_num:
            if entry in self.value:
                self.db_num.add(entry, self.value[entry])

    def calc_volume(self):
        t = int(time.time()/86400)
        t_start = (t-1)*86400
        t_end = t*86400
        t = int(((time.time()/3600)+1)*3600)-int(time.time())
        self.th_volume = Timer(t, self.calc_volume)
        self.th_volume.start()
        sql = "select value_max from history where topic='%s' and time < '%s' order by time desc limit 1"
        self.week.open()
        for topic in ['core/received', 'core/sent']:
            row = self.week.fetchone(sql % (topic, str(t_start+1), ))
            if not row:
                return
            volume_start = row[0]
            row = self.week.fetchone(sql % (topic, str(t_end+1), ))
            if not row:
                return
            volume_end = row[0]
            reset = False
            out = volume_start
            for zero in self.zero:
                if zero["ts"] > t_start and zero["ts"] < t_end:
                    volume = zero[topic.split('/')[1]]
                    if (volume - out) > 0:
                        out = volume - out
                    else:
                        out = out + volume
                    reset = True
            if (out - volume + volume_end) > 0:
                out = out - volume + volume_end
            else:
                out = volume_end - volume_start
            self.value["core/daily_" + topic.split("/")[1]] = out

    def get_chart(self, data):
        self.log.debug(data)
        faktor = 1
        out = {"title": "Batterie Telefon [%]", "buttons": "D:W:M:Y:A", "data": [], "unit": "", "pre": 0, "label": "Batterie"}
        if data["sensor"] == "core/daily_received" or data["sensor"] == "core/daily_sent":
            out["buttons"] = "M:Y:A"
            out["unit"] = "GByte"
            out["pre"] = 1
            faktor = 1/1073741824
        if data["sensor"] == "core/daily_received" or data["sensor"] == "core/rate_resv":
            out["label"] = "Empfangen"
        if data["sensor"] == "core/daily_sent" or data["sensor"] == "core/rate_sent":
            out["label"] = "Gesendet"
        if data["sensor"] == "core/rate_resv" or data["sensor"] == "core/rate_sent":
            out["unit"] = "kByte/s"
            out["pre"] = 2
            faktor = 1/1024
        out["title"] = out["label"] + " [" + out["unit"] +"]" 
        out["buttons"] = out["buttons"].replace(data["mode"], data["mode"]+"!")
        out["data"] = self.db_num.get_chart(data["sensor"], data["mode"], faktor=faktor)
        return {"allowed": True, "data": out}

    def run(self):
        self.log.debug("run")
        global SAVE_INTERVAL
        self.save_timer = Timer(SAVE_INTERVAL, self.save_tick)
        self.save_timer.start()
        self.db_num.start()
        self.calc_volume()

    def save_tick(self):
        self.log.debug("save_tick")
        self.save()
        if self.running:
            self.save_timer = Timer(SAVE_INTERVAL, self.save_tick)
            self.save_timer.start()

    def save(self):
        self.log.debug("save")
        f = open(self.jname, "w")
        f.write(json.dumps(self.value))
        f.close()
    
    def stop(self):
        self.log.debug("stop")
        self.running = False
        if self.save_timer:
            self.save_timer.cancel()
        self.save()
        self.db_num.stop()
        if self.th_volume:
            self.th_volume.cancel()
