# smartlife
# 05.2021

import json
import time
from queue import Queue

from plugins2 import plugin_base

from avm.status import status
from avm.data import values
from avm.cards import cards
from avm.phonecalls import phonecalls

ALIVE = 300

class integration(plugin_base):
    def __init__(self, smartlife, modul):
        plugin_base.__init__(self, smartlife, modul)
        global ALIVE
        self.alive = [ALIVE]
        self.login_ok = True
        self.data = values(self.log, smartlife)
        if self.cfg["user"] == "NoFound" or self.cfg["password"] == "NoFound":
            self.login_ok = False
            self.log.error("Logindata not found")
            return
        self.queue = Queue()
        self.phonecalls = phonecalls(self.log, smartlife)
        self.cards = cards(self.log, self.data, self.phonecalls)
        self.status = status(self.log, self.cfg["ip"], self.cfg["user"], self.cfg["password"], self.cfg["phone_interval"], 
            self.queue, self.phonecalls, self.get_secret)

    def pl_get_menus(self, user, ip):
        self.log.debug("user")
        rights = self.rights(user, ip)
        out = []
        if rights["domain"]:
            if "read" in rights["sub"]:
                out.append({'label': 'FritzBox', 'mod': 'avm', 'p1':'full'})
            elif "card" in rights["sub"]:
                out.append({'label': 'FritzBox', 'mod': 'avm', 'p1':'app'})
        return out

    def pl_data(self, user, cmd, _in, ip):
        self.log.debug(cmd)
        rights = self.rights(user, ip)
        if rights["domain"]:
            if "card" in rights["sub"] or "read" in rights["sub"]:
                if cmd == "get_cards":
                    return self.cards.get_cards()
                elif cmd == "get_card":
                    return self.cards.get_card()
                elif cmd == "get_chart":
                    return self.data.get_chart(_in)
            if "read" in rights["sub"]:
                if cmd == "get_sensor":
                    return self.data.get_sensor()
            if "write" in rights["sub"]:
                if cmd == "set_sensor":
                    return self.data.set_sensor(_in)

    def run(self):
        self.log.info("run")
        global ALIVE
        self.data.run()
        while self.running:
            if self.login_ok:
                self.status.tick()
            while not self.queue.empty():
                self.alive[0] = ALIVE
                entry = self.queue.get(block=False)
                self.data.new_values(entry)
                self.queue.task_done()

            time.sleep(1)

    def stop(self):
        self.log.info("stop")
        self.running = False
        self.data.stop()

