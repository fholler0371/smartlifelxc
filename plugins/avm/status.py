from threading import Thread
import time

import fritzconnection as fritz
import xml.etree.ElementTree as ET

class avm(Thread):
    def __init__(self, log, host, user, password, queue, phonecalls, get_all):
        Thread.__init__(self)
        self.name = "avm collector"
        self.daemon = True
        self.host = host
        self.user = user
        self.password = password
        self.queue = queue
        self.log = log
        self.phonecalls = phonecalls
        self.get_all = get_all

    def run(self):
        self.log.debug("read avm status")
        try:
            fc = fritz.FritzConnection(address=self.host, user=self.user, password=self.password)
            t = int(time.time())
            data = fc.call_action("WANCommonIFC1", "GetAddonInfos")
            self.queue.put({"topic": "core/received", "ts": t, "value": int(data["NewX_AVM_DE_TotalBytesReceived64"])})
            self.queue.put({"topic": "core/sent", "ts": t, "value": int(data["NewX_AVM_DE_TotalBytesSent64"])})
            data = fc.call_action("WANIPConn", "GetExternalIPAddress")
            self.queue.put({"topic": "core/ip4", "ts": t, "value": data["NewExternalIPAddress"]})
            data = fc.call_action("WANIPConn", "X_AVM_DE_GetExternalIPv6Address")
            if data["NewValidLifetime"] > 0:
                self.queue.put({"topic": "core/ip6", "ts": t, "value": data["NewExternalIPv6Address"]})
                data = fc.call_action("WANIPConn", "X_AVM_DE_GetIPv6Prefix")
                self.queue.put({"topic": "core/ip6pre", "ts": t, "value": data["NewIPv6Prefix"] + "/" + str(data["NewPrefixLength"])})
            else:
                self.queue.put({"topic": "core/ip6", "ts": t, "value": ""})
                self.queue.put({"topic": "core/ip6pre", "ts": t, "value": ""})
            data = fc.call_action("WANIPConn", "GetStatusInfo")
            self.queue.put({"topic": "core/connected", "ts": t, "value": data["NewConnectionStatus"] == "Connected"})
            self.queue.put({"topic": "core/uptime", "ts": t, "value": data["NewUptime"]})
            data = fc.call_action("WANCommonIFC", "GetCommonLinkProperties")
            self.queue.put({"topic": "core/rate_up", "ts": t, "value": data["NewLayer1UpstreamMaxBitRate"]/8})
            self.queue.put({"topic": "core/rate_down", "ts": t, "value": data["NewLayer1DownstreamMaxBitRate"]/8})
            url = fc.call_action("X_AVM-DE_OnTel1", "GetCallList")["NewCallListURL"]
            if not self.get_all:
                url += "&days=7"
            data = fc.session.get(url).text
            tree = ET.fromstring(data)
            for child in tree:
                rec = {}
                for entry in child:
                    if entry.tag == "Duration":
                        rec["duration"] = entry.text
                    elif entry.tag == "Date":
                        rec["time"] = entry.text
                    elif entry.tag == "Type":
                        rec["type"] = entry.text
                    elif entry.tag == "Called":
                        rec["called"] = entry.text
                    elif entry.tag == "Caller":
                        rec["caller"] = entry.text
                    elif entry.tag == "CallerNumber":
                        rec["caller"] = entry.text
                    elif entry.tag == "Name":
                        if entry.text:
                            rec["name"] = entry.text
                        else:
                            rec["name"] = ""
#                    else:
#                        self.log.error(entry.tag)
#                        if hasattr(entry, "text"):
#                            self.log.error(entry.text)
                if "type" in rec and rec["type"] != "0":
                    self.phonecalls.add(rec)
        except:
            self.log.error("cannot avm status")

class status:
    def __init__(self, log, host, user, password, timeout, queue, phonecalls, get_secret):
        log.debug("init")
        self.log = log
        self.host = host
        self.user = user
        self.password = password
        self.default_timeout = timeout
        self.timeout = 0
        self.queue = queue
        self.phonecalls = phonecalls
        self.first = True
        self.get_secret = get_secret
        
    def tick(self):
        self.log.debug("tick")
        self.timeout = self.timeout - 1
        if self.timeout <= 0:
            self.timeout = self.default_timeout
            if "!" in self.user:
                self.user = self.get_secret(self.user)
            if "!" in self.password:
                self.password = self.get_secret(self.password)    
            th = avm(self.log, self.host, self.user, self.password, self.queue, self.phonecalls, self.first)
            self.first = False
            th.start()