# smartlife
# 05.2021

class cards():
    def __init__(self, log, data, phonecalls):
        log.debug("init")
        self.log = log
        self.data = data
        self.phonecalls = phonecalls

    def get_cards(self):
        self.log.debug("get_cards")
        out = []
        out.append({"mod": "avm", "card": "avm_state", "name": "FritzBox Status"})
        out.append({"mod": "avm", "card": "avm_transfer", "name": "FritzBox Transfervolumen"})
        out.append({"mod": "avm", "card": "avm_phonecalls", "name": "FritzBox Telefon"})
        return {"allowed": True, "data": out}

    def get_card(self):
        self.log.debug("get_card")
        list = {}
        list["status"] = [["ip4", "core/ip4"], 
                ["ip6", "core/ip6"], 
                ["ip6pre", "core/ip6pre"],
                ["connected", "core/connected", "jn"],
                ["uptime", "core/uptime", "hms"],
                ["sent", "core/rate_sent", "num"],
                ["sent_max", "core/rate_up", "num"],
                ["resv", "core/rate_resv", "num"],
                ["resv_max", "core/rate_down", "num"]]
        list["transfer"] = [["sent", "core/daily_sent", "num"],
                            ["received", "core/daily_received", "num"]]
        cards_out = {}
        for card in list:
            out = {}
            for entry in list[card]:
                out[entry[0]] = {"val": self.data.get_value(entry[1])}
                if len(entry) > 2:
                    if entry[2] == "jn":
                        if out[entry[0]]["val"]:
                            out[entry[0]]["val"] = "Ja"
                        else:
                            out[entry[0]]["val"] = "Nein"
                    if entry[2] in ["hms", "num"]:
                        if out[entry[0]]["val"] == "":
                            out[entry[0]]["val"] = 0
                    if entry[2] == "hms":
                        t = out[entry[0]]["val"]
                        hms = ("0" + str(t % 60))[-2:]
                        t = int(t/60)
                        hms = ("0" + str(t % 60))[-2:] + ":" + hms
                        t = int(t/60)
                        out[entry[0]]["val"] = str(t) + ":" + hms
                if entry[1] in self.data.history_num:
                    i = "W"
                    if "daily" in entry[1]:
                        i = "M"
                    out[entry[0]]["chart"] = entry[1]+":"+i
            cards_out[card] = out
        cards_out["phonecalls"] = self.phonecalls.getlast()
        return {"allowed": True, "data": cards_out}

