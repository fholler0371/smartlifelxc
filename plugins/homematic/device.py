# smartlife
# 05.2021

import os
import json

class device:
    def __init__(self, log, smartlife):
        log.debug("init")
        self.log = log
        self.friendly = {}
        self.jname = smartlife.db_path + "/homematic_device_friendly.json"
        if os.path.isfile(self.jname):
            f = open(self.jname, "r")
            self.friendly = json.loads(f.read())
            f.close()

    def new_device(self, name):
        self.log.debug(name)
        if not(name in self.friendly):
            self.friendly[name] = name
            f = open(self.jname, "w")
            f.write(json.dumps(self.friendly))
            f.close()

    def get_devices(self):
        self.log.debug("get_devices")
        out = []
        for entry in self.friendly:
            out.append({"topic": entry, "friendly": self.friendly[entry]})
        out = sorted(out, key=lambda x: x["friendly"], reverse=False)
        return {"allowed": True, "data": out}

    def set_devices(self, data):
        self.log.debug(data)
        if data["field"] == "friendly":
            self.friendly[data["topic"]] = data["value"]
            f = open(self.jname, "w")
            f.write(json.dumps(self.friendly))
            f.close()
        return {"allowed": True, "data": {}}