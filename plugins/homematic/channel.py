# smartlife
# 05.2021
# 09.2021

import os
import json
from threading import Timer

SAVE_INTERVAL = 300

class channel:
    def __init__(self, log, smartlife, device):
        log.debug("init")
        self.log = log
        self.running = True
        self.device = device
        self.values = []
        self.smartlife = smartlife
        self.jname = smartlife.db_path + "/homematic_channel.json"
        if os.path.isfile(self.jname):
            f = open(self.jname, "r")
            self.values = json.loads(f.read())
            f.close()
        self.name_sent = smartlife.db_path + "/homematic_sent.json"
        self.sent = []
        if os.path.isfile(self.name_sent):
            f = open(self.name_sent, "r")
            self.sent = json.loads(f.read())
            f.close()
        global SAVE_INTERVAL
        self.timer = Timer(SAVE_INTERVAL, self.tick)
        self.timer.start()

    def new_value(self, topic, value, ts):
        self.log.debug(topic)
        if topic in self.values:
            self.values[topic]["value"] = value
        else:
            self.values[topic] = {"value": value, "friendly": "homematic/" + topic}
        store = self.smartlife.get_plugin("data_store")
        if store and (topic in self.sent):
            msg = {"topic": "homematic/" + topic, "friendly": self.values[topic]["friendly"], "value": self.values[topic]["value"], "ts": ts}
            if msg["topic"] == msg["friendly"]:
                for entry in self.device.friendly:
                    if entry in msg["friendly"]:
                        msg["friendly"] = msg["friendly"].replace(entry, self.device.friendly[entry])
            store.new_local(msg)

    def get_channels(self):
        self.log.debug("get_channels")
        out = []
        for topic in self.values:
            rec = {"topic": topic, "entry": self.values[topic]["friendly"], "value": self.values[topic]["value"], "sent": False}
            if "homematic/" + rec["topic"] == rec["entry"]:
                for entry in self.device.friendly:
                    if entry in rec["entry"]:
                        rec["entry"] = rec["entry"].replace(entry, self.device.friendly[entry])
            if topic in self.sent:
                rec["sent"] = True
            out.append(rec)
        out = sorted(out, key=lambda x: x["entry"], reverse=False)
        return {"allowed": True, "data": out}

    def set_channels(self, data):
        self.log.debug(data)
        if data["field"] == "entry":
            self.values[data["topic"]]["friendly"] = data["value"]
        if data["field"] == "sent":
            if data["value"]:
                if not(data["topic"] in self.sent):
                    self.sent.append(data["topic"])
            else:
                if data["topic"] in self.sent:
                    self.sent.remove(data["topic"])
            f = open(self.name_sent, "w")
            f.write(json.dumps(self.sent))
            f.close()
        return {"allowed": True, "data": {}}

    def tick(self):
        self.log.debug("tick")
        global SAVE_INTERVAL
        if self.running:
            self.timer = Timer(SAVE_INTERVAL, self.tick)
            self.timer.start()
            self.save()

    def save(self):
        self.log.debug("save")
        f = open(self.jname, "w")
        f.write(json.dumps(self.values))
        f.close()

    def stop(self):
        self.log.debug("stop")
        if self.timer:
            self.timer.cancel()
        self.save()