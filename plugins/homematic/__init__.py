# smartlife
# 05.2021
# 06.2021
# 09.2021

import json
import time

import paho.mqtt.client as mqtt

from plugins2 import plugin_base
from homematic.device import device
from homematic.channel import channel

ALIVE_MQTT = 600

class integration(plugin_base):
    def __init__(self, smartlife, modul):
        global ALIVE_MQTT
        plugin_base.__init__(self, smartlife, modul)
        self.client = mqtt.Client()
        self.client.on_connect = self.on_connect
        self.client.on_disconnect = self.on_disconnect
        self.client.on_message = self.on_message
        self.device = device(self.log, smartlife)
        self.channel = channel(self.log, smartlife, self.device)
        self.alive = [ALIVE_MQTT]

    def on_connect(self, client, userdata, flags, rc):
        self.log.debug("Connected with result code "+str(rc))
        if rc == 0:
            client.subscribe("hm/status/#")
        else:
            self.log.error("connection error "+str(rc))

    def on_disconnect(self, client, userdata, rc):
        self.log.debug("Disconnected with result code "+str(rc))
        if rc != 0:
            time.sleep(2)
            self.client.reconnect()

    def on_message(self, client, userdata, msg):
        global ALIVE_MQTT
        self.log.debug(msg.topic+" "+str(msg.payload))
        self.alive[0] = ALIVE_MQTT
        device = msg.topic.split("/")[2].split(":")[0]
        if not(device == ""):
            try:
                self.device.new_device(device)
            except:
                pass
            try:
                topic = device + "/" + msg.topic.split("/")[3]
            except:
                topic = device + "/DATA"
            if "PRESS" in topic:
                topic += "_" + msg.topic.split("/")[2].split(":")[1]
            _value = msg.payload.decode()
            try:
                value = json.loads(_value)["val"]
            except:
                value = _value
            try:
                ts = json.loads(_value)["ts"]
            except:
                ts = int(time.time())
            self.channel.new_value(topic, value, ts)

    def pl_get_menus(self, user, ip):
        self.log.debug("user")
        rights = self.rights(user, ip)
        out = []
        if rights["domain"]:
            out.append({'label': 'Homematic', 'mod': 'homematic', 'p1':''})
        return out

    def pl_data(self, user, cmd, _in, ip):
        self.log.debug(cmd)
        rights = self.rights(user, ip)
        if rights["domain"]:
            if "read" in rights["sub"]:
                if cmd == "get_devices":
                    return self.device.get_devices()
                elif cmd == "get_channels":
                    return self.channel.get_channels()
            if "write" in rights["sub"]:
                if cmd == "set_devices":
                    return self.device.set_devices(_in)
                elif cmd == "set_channels":
                    return self.channel.set_channels(_in)

    def run(self):
        self.log.info("run")
        self.client.connect(self.cfg["ip"])
        self.client.loop_start()
        while self.running:
            time.sleep(5)
        self.client.disconnect()
        time.sleep(0.25)
        self.client.loop_stop()

    def stop(self):
        self.log.info(self.name)
        self.running = False
        self.channel.stop()
