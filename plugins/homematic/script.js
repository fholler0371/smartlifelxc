define(['module', 'jqxtabs', 'jqxdata', 'jqxgrid', 'jqxgrid_selection', 'jqxcheckbox', 'jqxgrid_edit'], 
        function(module) {
    var mod = {
      init : function() {
        window.module.homematic.sub = window.module.homematic.init_data.p1
        var sub = window.module.homematic.sub
        html = '<div style="width:100%; height:100%;"><div id="homematic_tab"><ul><li>Kanäle</li><li>Geräte</li></ul>'
        html += '<div><div id="homematic_channel_grid" style="width:100%; height:100%;"></div></div>'
        html += '<div><div id="homematic_devices_grid" style="width:100%; height:100%;"></div></div>'
        $('#client_area').html(html+'</div>')
        $(window).off('resize', window.module.homematic.resize)
        $(window).on('resize', window.module.homematic.resize)
        window.module.homematic.resize()
        $('#homematic_tab').jqxTabs({ width: '100%', height: '100%', position: 'top'})
        $('#homematic_tab').on('selected', function(ev) {
          var selectedTab = ev.args.item
          window.module.homematic.call_tab(selectedTab)
        })
        window.module.homematic.call_tab(0)
      },
      call_tab: function(id) {
        if (id == 0) {
          window.smfetch('/api/homematic', {cmd: 'get_channels'}).then((data) => {
            if (data.error == undefined) {
              console.log(data)
              var source = {
                localdata: data.data,
                datatype: "array",
                datafields: [
                  { name: 'topic', type: 'string'},
                  { name: 'entry', type: 'string'},
                  { name: 'value', type: 'string'},
                  { name: 'sent', type: 'bool'}
                ]                
              }
              var dataAdapter = new $.jqx.dataAdapter(source, {
                downloadComplete: function (data, status, xhr) { },
                loadComplete: function (data) { },
                loadError: function (xhr, status, error) { }
              })
              var col = [
                { text: 'Sensor', datafield: 'topic', width: 300, editable: false},
                { text: 'Bezeichnung', datafield: 'entry'},
                { text: 'Wert', datafield: 'value', width: 300, editable: false, cellsalign: 'right'},
                { text: 'Senden', datafield: 'sent', width: 75, columntype: 'checkbox'}
              ]
              $("#homematic_channel_grid").jqxGrid({
                width: '100%',
                height: '100%',
                source: dataAdapter,
                columns: col,
                editable: true
              })
              $("#homematic_channel_grid").off('cellendedit')
              $("#homematic_channel_grid").on('cellendedit', function (event) {
                var args = event.args;
                window.smfetch('/api/homematic', {cmd: 'set_channels', data: {
                  field: args.datafield, value: args.value, topic: args.row.topic
                }}).then((data) => {})
              })
              $("#homematic_channel_grid").parent().css('padding', 0).css('overflow', 'hidden')
            }
          })
        } else if (id == 1) {
          window.smfetch('/api/homematic', {cmd: 'get_devices'}).then((data) => {
            if (data.error == undefined) {
              var source = {
                localdata: data.data,
                datatype: "array",
                datafields: [
                  { name: 'topic', type: 'string'},
                  { name: 'friendly', type: 'string'}
                ]                
              }
              var dataAdapter = new $.jqx.dataAdapter(source, {
                downloadComplete: function (data, status, xhr) { },
                loadComplete: function (data) { },
                loadError: function (xhr, status, error) { }
              })
              var col = [
                { text: 'Sensor', datafield: 'topic', width: 300, editable: false},
                { text: 'Bezeichnung', datafield: 'friendly'}
              ]
              $("#homematic_devices_grid").jqxGrid({
                width: '100%',
                height: '100%',
                source: dataAdapter,
                columns: col,
                editable: true
              })
              $("#homematic_devices_grid").off('cellendedit')
              $("#homematic_devices_grid").on('cellendedit', function (event) {
                var args = event.args;
                window.smfetch('/api/homematic', {cmd: 'set_devices', data: {
                  field: args.datafield, value: args.value, topic: args.row.topic
                }}).then((data) => {})
              })
              $("#homematic_devices_grid").parent().css('padding', 0).css('overflow', 'hidden')
            }
          })
        } 
      },
      stop : function() {
      },
      resize: function() {
        $('#client_area').children().height($('#client_area').height())
      }
    }
    mod['init_data'] = window.module_const[module.id]
    window.module.homematic = mod
    return mod
  })
