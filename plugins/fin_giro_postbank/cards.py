# smartlife
# 05.2021

class cards:
    def __init__(self, log, db):
        log.debug("init")
        self.log = log
        self.db = db

    def get_cards(self):
        self.log.debug("get_cards")
        out = [{"mod": "fin_giro_postbank", "card": "fin_giro_postbank_sum", "name": "postbank Giro Summe"},
               {"mod": "fin_giro_postbank", "card": "fin_giro_postbank_month", "name": "postbank Giro Monat"},
               {"mod": "fin_giro_postbank", "card": "fin_giro_postbank_year", "name": "postbank Giro Jahr"}]
        return {"allowed": True, "data": out}

    def get_card(self):
        self.log.debug("get_card")
        out = {'sum': 0}
        out = self.db.get_card()
        return {"allowed": True, "data": out}
