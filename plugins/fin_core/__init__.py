# smartlife
# 05.2021

import json

from plugins2 import plugin_base

from fin_core.tree import tree

class integration(plugin_base):
    def __init__(self, smartlife, modul):
        plugin_base.__init__(self, smartlife, modul)
        self.alive = [999999999]
        self.tree = tree(self.log)

    def pl_get_menus(self, user, ip):
        self.log.debug(user)
        rights = self.rights(user, ip)
        out = []
        if rights["domain"]:
            if "read" in rights["sub"]:
                out.append({'label': 'Finanzen', 'mod': 'fin_core', 'p1':'full'})
            elif "card" in rights["sub"]:
                out.append({'label': 'Finanzen', 'mod': 'fin_core', 'p1':'app'})
        return out

    def pl_data(self, user, cmd, _in, ip):
        self.log.debug(cmd)
        rights = self.rights(user, ip)
        if rights["domain"]:
            if "read" in rights["sub"]:
                if cmd == "get_tree":
                    return self.tree.get_tree()

