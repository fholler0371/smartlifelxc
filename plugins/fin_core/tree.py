# smartlife
# 05.2021
# 06.2021
# 09.2021

class tree:
    def __init__(self, log):
        log.debug("init")
        self.log = log

    def get_tree(self):
        self.log.debug("get_tree")
        out = []
        out.append({'id': 100, 'parent': -1, 'icon': "lib/img/stock.png", 'text': "Aktien", 'value': "fin_shares"})
        out.append({'id': 200, 'parent': -1, 'icon': "lib/img/money.png",  'text': "Vermögen", 'value': "fin_money"})
        out.append({'id': 300, 'parent': 200, 'icon': "lib/img/anlage.png", 'text': "Anlagen", 'value': "fin_anlage"})
        out.append({'id': 310, 'parent': 300, 'icon': "lib/img/p2p.png", 'text': "P2P", 'value': "fin_p2p"})
        out.append({'id': 311, 'parent': 310, 'icon': "lib/img/auxmoney.png", 'text': "Auxmoney", 'value': "fin_p2p_auxmoney"})
        out.append({'id': 312, 'parent': 310, 'icon': "lib/img/bondora.png", 'text': "Bondora", 'value': "fin_p2p_bondora"})
        out.append({'id': 313, 'parent': 310, 'icon': "lib/img/estateguru.png", 'text': "Estateguru", 'value': "fin_p2p_estateguru"})
        out.append({'id': 320, 'parent': 300, 'icon': "lib/img/lifeinsurance.png", 'text': "Lebensversicherung", "value": "fin_lv"})
        out.append({'id': 321, 'parent': 320, 'icon': "lib/img/gothaer.png", 'text': "Gothaer", "value": "fin_lv_gothaer"})
        out.append({'id': 322, 'parent': 320, 'icon': "lib/img/zurich.png", 'text': "Zurich 1", "value": "fin_lv_zurich_1"})
        out.append({'id': 323, 'parent': 320, 'icon': "lib/img/zurich.png", 'text': "Zurich 2", "value": "fin_lv_zurich_2"})
        out.append({'id': 330, 'parent': 300, 'icon': "lib/img/coin.png", 'text': "Goldmünzen", 'value': "fin_depot_gold"})
        out.append({'id': 340, 'parent': 300, 'icon':'lib/img/consors.png', 'text': 'Consors Depot', 'value': 'fin_depot_consors'})
        out.append({'id': 400, 'parent': 200, 'icon':'lib/img/liabilities.png', 'text': 'Verbindlichkeiten', 'value': 'fin_liabilities'})
        out.append({'id': 410, 'parent': 400, 'icon':'lib/img/credit-card.png', 'text': 'Kreditkarten', 'value': 'fin_card'})
        out.append({'id': 411, 'parent': 410, 'icon':'lib/img/platin.png', 'text': 'Amex Platin', 'value': 'fin_card_amex_platin'})
        out.append({'id': 412, 'parent': 410, 'icon':'lib/img/gold.png', 'text': 'Amex Gold', 'value': 'fin_card_amex_gold'})
        out.append({'id': 413, 'parent': 410, 'icon':'lib/img/barclay.png', 'text': 'Visa Barclay', 'value': 'fin_card_barclay'})
        out.append({'id': 414, 'parent': 410, 'icon':'lib/img/amazon.png', 'text': 'Visa Amazon', 'value': 'fin_card_lbb'})
        out.append({'id': 420, 'parent': 400, 'icon':'lib/img/credits.png', 'text': 'Kredite', 'value': 'fin_credit'})
        out.append({'id': 421, 'parent': 420, 'icon':'lib/img/barclay.png', 'text': 'Kredit Barclay', 'value': 'fin_credit_barclay'})
        out.append({'id': 422, 'parent': 420, 'icon':'lib/img/ikano.png', 'text': 'Kredit Ikano', 'value': 'fin_credit_ikano'})
        out.append({'id': 423, 'parent': 420, 'icon':'lib/img/postbank.png', 'text': 'Kredit Postbank', 'value': 'fin_credit_postbank'})
        out.append({'id': 500, 'parent': 200, 'icon':'lib/img/giro.png', 'text': 'Girokonten', 'value': 'fin_giro'})
        out.append({'id': 510, 'parent': 500, 'icon':'lib/img/postbank.png', 'text': 'Postbank', 'value': 'fin_giro_postbank'})
        out.append({'id': 520, 'parent': 500, 'icon':'lib/img/consors.png', 'text': 'Consors', 'value': 'fin_giro_consors'})
        return {"allowed": True, "data": out}