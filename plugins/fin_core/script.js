define(['module', 'packery', 'jqxtabs', 'jqxsplitter', 'jqxexpander', 'jqxdata', 'jqxtree'], function(module, Packery) {
  var mod = {
    init : function() {
      window.module.fin_core.sub = window.module.fin_core.init_data.p1
      var sub = window.module.fin_core.sub
      if (sub == "app") {
        html = '<div id="fin_core_cards" style="width:100%; height:100%; margin: 10px;" class="jqx-widget-content-custom-scheme"></div>'
        $('#client_area').html(html)
      } else {
        html = '<div style="width:100%; height:100%;"><div id="fin_core_tab"><ul><li>Karten</li><li>Übersicht</li></ul>'
        html += '<div><div id="fin_core_cards" style="width:100%; height:100%;"></div></div>'
        html += '<div style="width:100%; height:100%;" id="finanical_splitter"><div id="finanical_right"></div><div id="finanical_left"></div></div>'
        $('#client_area').html(html+'</div>')
        $(window).off('resize', window.module.fin_core.resize)
        $(window).on('resize', window.module.fin_core.resize)
        window.module.fin_core.resize()
        $('#fin_core_tab').jqxTabs({ width: '100%', height: '100%', position: 'top'})
        $('#fin_core_tab').on('selected', function(ev) {
          var selectedTab = ev.args.item
          window.module.fin_core.call_tab(selectedTab)
        })
      }
      window.module.fin_core.call_tab(0)
    },
    call_tab: function(id) {
      if (id == 0) {
/*        window.smfetch('/api/fin_core', {cmd: 'get_cards'}).then((data) => {
          if (data.error == undefined) {
            window.module.fin_core.cards = data.data
            window.module.fin_core.cards_check_mod = 10
            window.module.fin_core.create_cards()
          }
        })*/
      } else if (id == 1) {
        if ($('#finanical_right').length == 1) {
          $('#finanical_splitter').jqxSplitter({height: '100%', width: '100%', panels: [{ size: 300 }, {min: 300}] })
          html = "<div id='financial_expander'><div>Finanzen</div><div style='overflow: hidden;'><div style='border: none;' id='finacial_tree'>"
          html += "</div></div></div>"
          $('#finanical_right').append(html)
          $('#financial_expander').jqxExpander({ showArrow: false, toggleMode: 'none', width: '100%', height: '100%'})
          window.smfetch('/api/fin_core', {cmd: 'get_tree'}).then((data) => {
            var source = {
              datatype: "array",
              datafields: [
                { name: 'id' },
                { name: 'parent' },
                { name: 'text' },
                { name: 'value' },
                { name: 'icon' }
              ],
              id: 'id',
              localdata: data.data
            }
            var dataAdapter = new $.jqx.dataAdapter(source)
            dataAdapter.dataBind()
            var records = dataAdapter.getRecordsHierarchy('id', 'parent', 'items', [{ name: 'text', map: 'label', icon: 'icon'}])
            $('#finacial_tree').jqxTree({ source: records, width: '100%', height: '100%'})
            $('#finacial_tree').jqxTree('expandAll')
            $('#finacial_tree').jqxTree('selectItem', $("#finacial_tree").find('li:first')[0])
            $('#finacial_tree').on('itemClick', window.module.fin_core.click)
            window.module.fin_core.click()
          })
        }
      }
    },
    click: function(event) {
      var mod = $('#finacial_tree').jqxTree('getSelectedItem').value
      var paths = {}
      paths['mod.'+mod] = 'module/'+mod
      requirejs.config({paths:paths})
      if (window.module[mod] == undefined) {
        requirejs(['mod.'+mod], function(mod) {
          mod.init()
        })
      } else {
        window.module[mod].init()
      }
    },
    resize: function() {
        $('#client_area').children().height($('#client_area').height())
    }  
  }
  mod['init_data'] = window.module_const[module.id]
  window.module.fin_core = mod
  return mod
})