# smartlife
# 06.2021
# 09.2021

class cards:
    def __init__(self, log, db):
        log.debug("init")
        self.log = log
        self.db = db

    def get_cards(self):
        self.log.debug("get_cards")
        out = [{"mod": "fin_money", "card": "fin_money_sum", "name": "Vermögen Summe"},
               {"mod": "fin_money", "card": "fin_money_year", "name": "Jahresgewinn"}]
        return {"allowed": True, "data": out}

    def get_card(self):
        self.log.debug("get_card")
        out = {'sum': 0}
        out = self.db.get_card()
        return {"allowed": True, "data": out}


