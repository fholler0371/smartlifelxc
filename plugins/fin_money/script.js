define(['packery', 'highcharts', 'moment_tz', 'jqxtabs', 'jqxdropdownlist',
        'jqxdatetimeinput', 'jqxnumberinput', 'jqxbutton', 'jqxchart', 'upload'], function(Packery, H, moment) {
  window.moment = moment
  H.setOptions({
    global: {
      timezone: "Europe/Berlin"
    },
    lang : {
      months : ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
      shortMonths : ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
      weekdays : ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa']
    } 
  })
  var mod = {
    wdata : {},
    wdata_last : 0,
    wdata_call : 0,
    t_update_sum: null,
    t_update_month: null,
    t_update_year: null,
    init: function() {
      if ($('#fin_money_tab').length == 0) {
        html = '<div id="fin_money_tab"><ul><li>Status</li><li>Verlauf</li></ul><div id="fin_money_cards"></div>'
        html += '<div id="fin_money_history"></div></div>'
        $('#finanical_left').html(html)
        $('#fin_money_tab').jqxTabs({height: '100%', width: '100%'})
        $('#fin_money_tab').off('selected')
        $('#fin_money_tab').on('selected', function(ev) {
          var id = ev.args.item
          window.module.fin_money.call_tab(id)
        })
        window.module.fin_money.call_tab(0)
      }
    },
    check_data : function() {
      var now = new Date()
      var t = Math.round(now.getTime() / 1000)
      if (t - window.module.fin_money.wdata_last > 300) {
        if (t - window.module.fin_money.wdata_call > 10) {
          window.module.fin_money.wdata_call = t
          window.smfetch('/api/fin_money', {cmd: 'get_card'}).then((data) => {
            if (data.error == undefined) {
              window.module.fin_money.wdata_last = t
              window.module.fin_money.wdata = data.data
            }
          })
        }
      }
    },
    update_sum: function() {
      clearTimeout(window.module.fin_money.t_update_sum)
      window.module.fin_money.check_data()
      var sum = window.module.fin_money.wdata.sum
      if (sum != undefined) {
        var ele = $($("body").find('.cards_fin_money_sum')[0])
        var old = ele.data('last')
        if (old != sum) {
          ele.data('last', sum)
          html  = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">Basisdaten</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 25px;"><span  style="font-weight: 600;">Anlage:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 25px;">'
          html += sum.sum.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 45px;"><span  style="font-weight: 600;">Summe:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 45px;">'
          html += sum.interest.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          ele.html(html)
        }
        window.module.fin_money.t_update_sum = setTimeout(window.module.fin_money.update_sum, 600000)
      } else {
        window.module.fin_money.t_update_sum = setTimeout(window.module.fin_money.update_sum, 250)
      }
    },
    update_month: function() {
      clearTimeout(window.module.fin_money.t_update_month)
      window.module.fin_money.check_data()
      var d = window.module.fin_money.wdata.month
      if (d != undefined) {
        var ele = $($("body").find('.cards_fin_money_month')[0])
        var old = ele.data('last')
        if (old != d) {
          ele.data('last', d)
          html  = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">Monat</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 25px;"><span  style="font-weight: 600;">Einahmen:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 25px;">'
          html += d.in.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 45px;"><span  style="font-weight: 600;">Ausgaben:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 45px;">'
          html += d.out.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          ele.html(html)
        }
        window.module.fin_money.t_update_month = setTimeout(window.module.fin_money.update_month, 600000)
      } else {
        window.module.fin_money.t_update_month = setTimeout(window.module.fin_money.update_month, 250)
      }
    },
    update_year: function() {
      clearTimeout(window.module.fin_money.t_update_year)
      window.module.fin_money.check_data()
      window.module.fin_money.check_data()
      var sum = window.module.fin_money.wdata.year
      if (sum != undefined) {
        var ele = $($("body").find('.cards_fin_money_year')[0])
        var old = ele.data('last')
        if (old != sum) {
          ele.data('last', sum)
          html  = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">Jahresentwicklung</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 25px;"><span  style="font-weight: 600;">Anlage:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 25px;">'
          html += sum.sum.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 45px;"><span  style="font-weight: 600;">Summe:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 45px;">'
          html += sum.interest.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          ele.html(html)
        }
        window.module.fin_money.t_update_year = setTimeout(window.module.fin_money.update_year, 600000)
      } else {
        window.module.fin_money.t_update_year = setTimeout(window.module.fin_money.update_year, 250)
      }
    },
    get_card : function(data, div) {
      if (data["card"] == "fin_money_sum") {
        if (div.find(".cards_fin_money_sum").length == 0) {
          var html = '<div class="cards_fin_money_sum card"><span></span></div>'
          div.append(html)
        } else {
//          console.log(data)
        }
        window.module.fin_money.update_sum()
      } else if (data["card"] == "fin_money_month") {
        if (div.find(".cards_fin_money_month").length == 0) {
          var html = '<div class="cards_fin_money_month card"><span></span></div>'
          div.append(html)
        } else {
//          console.log(data)
        }
        window.module.fin_money.update_month()
      } else if (data["card"] == "fin_money_year") {
        if (div.find(".cards_fin_money_year").length == 0) {
          var html = '<div class="cards_fin_money_year card"><span></span></div>'
          div.append(html)
        } else {
//          console.log(data)
        }
        window.module.fin_money.update_year()
      }
    },
    create_cards: function() {
      var ok = 1
      var cards = window.module.fin_money.cards
      var l = cards.length
      for (var i=0; i<l; i++) {
         if (!(typeof(window.module[cards[i].mod].get_card) === 'function')) {
           console.log("Modul not found: "+cards[i].mod)
           ok = 0
         }
      }
      if (ok == 1) {
        for (var i=0; i<l; i++) {
          window.module[cards[i].mod].get_card(cards[i], $("#fin_money_cards"))
          require( [ 'jquery-bridget/jquery-bridget' ], function( jQueryBridget ) {
            jQueryBridget( 'packery', Packery, $ );
            var grid = $('#fin_money_cards').packery({
              itemSelector: '.card',
              gutter: 10
            })
          })
        }
      } else {
        console.log("recheck")
      }
    },
    stop: function() {
    },
    call_tab: function(id) {
      if (id == 0) {
        window.smfetch('/api/fin_money', {cmd: 'get_cards'}).then((data) => {
          if (data.error == undefined) {
            window.module.fin_money.cards = data.data
            window.module.fin_money.cards_check_mod = 10
            window.module.fin_money.create_cards()
          }
        })
      } else if (id == 1) {
        if ($('#fin_money_chart').length == 0) {
          $('#fin_money_history').append("<div id='fin_money_chart' style='height: 100%; width: 100%'></div>")
          $('#fin_money_history').css('overflow', 'hidden')
          window.smfetch('/api/fin_money', {cmd: 'get_history'}).then((data) => {
            if (data.error == undefined) {
              d = data.data
              var series = []
              series.push({name: 'Gewinn', data: [], color: '#00FF00', marker: {enabled: false}, lineWidth: 4})
              series.push({name: 'Betrag', data: [], color: '#0066FF', marker: {enabled: false}, lineWidth: 4})
              var len = d.length
              for (var i=0; i<len; i++) {
                var t = new Date(d[i].date)
                series[1].data.push([t.getTime(), d[i].value])
                series[0].data.push([t.getTime(), d[i].interest])
              }
              var settings = {
                chart: { type: 'area' },
                title : { text: '' },
                subtitle : { text: '' },
                plotOptions: {
                  area: {
                    stacking: 'normal'
                  }
                },
                yAxis : { title: {text: 'Euro' }},
                xAxis: {
                  type: 'datetime',
                  title: { text: '' }
                },
                series: series
              }
              H.chart('fin_money_chart', settings)
            }
          })
        }
      }
    },
    upload_finished : function() {
      setTimeout(window.module.fin_money.reload_table, 3000)
    },
    reload_table: function() {
      $("#fin_money_bookings").jqxGrid('updatebounddata')
    },
    load_table: function(data, source, callback) {
      window.smfetch('/api/fin_money', {cmd: 'get_book', data: {'page': data.pagenum, 'pagesize': data.pagesize}}).then((data) => {
        if (data.error == undefined) {
          for (var i=0; i<data.data.rows.length; i++) {
	          data.data.rows[i].date = new Date(data.data.rows[i].date)
	        }
          callback({ records:data.data.rows, totalrecords : data.data.records})
        }
      })
    }
  }
  window.module.fin_money = mod
  return mod
})
