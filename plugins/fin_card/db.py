# smartlife
# 05.2021

from threading import Thread, Timer
from datetime import datetime, timedelta
import time
import random

from database import database

class history(Thread):
    def __init__(self, log, name, hist_name, clients, master):
        Thread.__init__(self)
        log.debug("init")
        self.log = log
        self.clients = clients
        self.hist_name = hist_name
        self.master = master
        sql = "CREATE TABLE IF NOT EXISTS history (date string, value numeric, interest numeric)"
        self.hist = database(self.log, name, sql)

    def run(self):
        self.log.debug("run")
        self.hist.open()
        books = []
        for entry in self.clients:
            book = database(self.log, self.hist_name.replace("$$", entry))
            if book.exists():
                book.open()
                books.append({"db": book})
        maxDate = datetime.now()
        row = self.hist.fetchone("select date from history order by date desc limit 1")
        minDate = maxDate
        if row:
            minDate = datetime.strptime(row[0], "%Y-%m-%d")
        else:
            for book in books:
                row = book["db"].fetchone("select date from history order by date asc limit 1")
                if row:
                    if minDate > datetime.strptime(row[0], "%Y-%m-%d"):
                        minDate = datetime.strptime(row[0], "%Y-%m-%d")
        # setze min 31 Tage vor
        minDate = minDate - timedelta(days=31)
        # check minDate gegen Daten
        d = maxDate
        for book in books:
            row = book["db"].fetchone("select date from history order by date asc limit 1")
            if row:
                if d > datetime.strptime(row[0], '%Y-%m-%d'):
                    d = datetime.strptime(row[0], '%Y-%m-%d')
        if d > minDate:
            minDate = d
        # loop
        d = minDate
        sql1 = "select value from history where date <= '%s' order by date desc limit 1"
        sql2 = "select date from history where date = '%s'"
        sql3 = "update history set value='%s' where date='%s'"
        sql4 = "insert into history (value, date, interest) values ('%s', '%s', '0')"
        while d <= maxDate:
            val = 0
            for book in books:
                row = book["db"].fetchone(sql1 % (d.strftime('%Y-%m-%d'), ))
                if row:
                    val = val + row[0]
            if self.hist.fetchone(sql2  % (d.strftime('%Y-%m-%d'), )):
                sql = sql3 % ("%.2f" % round(val, 2), d.strftime('%Y-%m-%d'), )
            else:
                sql = sql4 % ("%.2f" % round(val, 2), d.strftime('%Y-%m-%d'), )
            self.hist.execute(sql)
            d = d + timedelta(days=1)
        self.hist.commit()
        for book in books:
            book["db"].close()
        self.hist.close()
        if self.master:
            self.master.calc_hist()



class db:
    def __init__(self, log, smartlife, clients, master):
        log.debug("init")
        self.log = log
        self.smartlife = smartlife
        self.clients = clients
        self.master = master
        self.hist = database(self.log, smartlife.db_path + "/fin_card_history.sqlite")
        t = (int(time.time()/86400)+1)*86400-time.time()+60+300*random.random()
        self.timer_calc = Timer(t, self.tick_calc)
        self.timer_calc.start()

    def tick_calc(self):
        self.log.debug("tick_calc")
        t = (int(time.time()/86400)+1)*86400-time.time()+60+300*random.random()
        self.timer_calc = Timer(t, self.tick_calc)
        self.timer_calc.start()
        self.do_calc()

    def get_card(self):
        self.log.debug("get_card")
        out = {"sum": 0}
        try:
            self.hist.open()
            sql = "select value from history order by date desc limit 1"
            row = self.hist.fetchone(sql)
            if row:
                out["sum"] = float("%.2f" % round(row[0], 2))
            self.hist.close()
        except:
            pass
        return out

    def do_calc(self):
        self.log.debug("do_calc")
        th = history(self.log, self.smartlife.db_path + "/fin_card_history.sqlite",
            self.smartlife.db_path + "/$$_history.sqlite", self.clients, self.smartlife.get_plugin(self.master))
        th.start()
        return {"allowed": True, "data": {}}

    def get_history(self):
        self.log.debug("get_history")
        out = []
        self.hist.open()
        for entry in self.hist.fetchall("select date, value from history order by date asc"):
            out.append({'date': entry[0], 'value': entry[1]})
        self.hist.close()
        return {"allowed": True, "data": out}

    def stop(self):
        self.log.debug("stop")
        if self.timer_calc:
            self.timer_calc.cancel()
            