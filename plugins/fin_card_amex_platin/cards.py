# smartlife
# 05.2021

class cards:
    def __init__(self, log, db):
        log.debug("init")
        self.log = log
        self.db = db

    def get_cards(self):
        self.log.debug("get_cards")
        out = [{"mod": "fin_card_amex_platin", "card": "fin_card_amex_platin_sum", "name": "Amex Platin Summe"},
               {"mod": "fin_card_amex_platin", "card": "fin_card_amex_platin_month", "name": "Amex Platin Summe Monat"},
               {"mod": "fin_card_amex_platin", "card": "fin_card_amex_platin_year", "name": "Amex Platin Summe Jahr"}]
        return {"allowed": True, "data": out}

    def get_card(self):
        self.log.debug("get_card")
        out = {'sum': 0}
        out = self.db.get_card()
        return {"allowed": True, "data": out}
