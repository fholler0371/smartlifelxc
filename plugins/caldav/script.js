define(['module', 'jqxtabs', 'jqxdata', 'jqxgrid', 'jqxgrid_selection', 'jqxcheckbox', 'jqxgrid_edit'], 
  function(module) {
    var mod = {
      init : function() {
        window.module.caldav.sub = window.module.caldav.init_data.p1
        var sub = window.module.caldav.sub
        if (sub == "full") {
          html = '<div id="caldav_frame" style="width:100%; height:100%;" class="jqx-widget-content-custom-scheme"></div>'
          $('#client_area').html(html)
          setTimeout(window.module.caldav.load_frame, 150)
        }
      },
      load_frame : function() {
        window.smfetch('/api/caldav', {cmd: 'get_cal'}).then((data) => {
          if (data.error == undefined) {
            window.module.caldav.resize()
            html = '<iframe width="100%" height="100%" src="https://home.caldav.holler.pro/index.html?first=' + data.data + '"></iframe>'
            $('#caldav_frame').html(html)
            $('#caldav_frame').children().css('border', 0)
          }
        })
      },
      stop : function() {
      },
      resize: function() {
        $('#client_area').children().height($('#client_area').height())
      }
    }
    mod['init_data'] = window.module_const[module.id]
    window.module.caldav = mod
    return mod
  }
)
