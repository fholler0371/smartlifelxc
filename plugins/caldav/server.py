# smartlife
# 08.2021
# 09.2021

from functools import partial
from http.server import HTTPServer, BaseHTTPRequestHandler
from socketserver import ThreadingMixIn
import urllib.parse as urlparse
from threading import Thread
import requests
import time
import copy
from requests.auth import HTTPBasicAuth

### thread #######################################

class loopThread(Thread):
    def __init__(self, server):
        Thread.__init__(self)
        self.daemon = True
        self.name = "WebProxy"
        self.server = server

    def run(self):
        self.server.serve_forever()

### server #######################################

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    daemon_threads = True
    allow_reuse_address = True

class webserverHandler(BaseHTTPRequestHandler):
    def __init__(self, log, crypt, ip, dataip, restart, restart_cal, *args, **kwargs):
        self.log = log
        self.crypt = crypt
        self.ip = ip
        self.dataip = dataip
        self.restart_cal = restart_cal
        self.restart = restart
        self._session = requests.Session()
        super().__init__(*args, **kwargs)

    def parse_headers(self):
        req_header = {}
        for key in self.headers.keys():
            value = self.headers[key]
            if not (key.lower() in ["accept-encoding", "authorizaion", "referer", "dnt", "accept", "upgrade-insecure-requests", "host", "connection", "cookie"]):
                if not key.lower().startswith("sec"):
                    req_header[key] = value
            elif key.lower() == "referer":
                req_header[key] = value.replace("https://"+"home.calendar.holler.pro", "http://"+self.ip)
            elif key.lower() == "cookie":
                for cookie in value.split("; "):
                    if cookie.startswith("PHPSESSID"):
                        req_header["Cookie"] = cookie
        return req_header

    def log_message(self, format, *args):
        self.log.debug("\n    %s - - [%s] %s" % (self.address_string(), self.log_date_time_string(),format%args))

    def send_request(self, verb, path, body='', headers=None, properties=None):
        _headers = {}
        if headers: 
            for key in headers.keys():
                if not (key.lower() in ["accept-encoding", "authorizaion", "referer", "dnt", "accept", "upgrade-insecure-requests", "host", "connection", "cookie"]):
                    _headers[key] = headers[key]
        response = self._session.request(verb, path, data=body, headers=_headers, auth=HTTPBasicAuth('fholler', 'xxx'), timeout=100)
        return response

    def do_DELETE(self, body=True):
        try:
            mode = ""
            cookies = self.headers["Cookie"]
            if cookies:
                for cookie in cookies.split("; "):
                    cookie_split = cookie.split("=")
                    if cookie_split[0] == "key":
                        mode = self.crypt.decode(cookie_split[1]+"=")
            if mode == "write":
                url = 'http://{}{}'.format(self.dataip, self.path)
                resp = self.send_request('DELETE', url, headers=self.headers)
                self.send_response(resp.status_code)
                for header in resp.headers:
                    if not header.lower().startswith("x-") :
                        if not (header.lower() in ["content-encoding", "vary", "strict-transport-securit"]):
                            self.send_header(header, resp.headers[header])
                self.send_header("Set-Cookie", "key="+self.crypt.get_first_key(mode)+"; Max-Age=7200; Path=/; HttpOnly")
                self.end_headers()
                for chunk in resp.iter_content(chunk_size=8192):
                    self.wfile.write(chunk)  
        except requests.ConnectionError:
            self.restart_cal()
            self.send_response(505)
            self.end_headers()
            self.wfile.write("conectionError".encode()) 
        except Exception as e:
            print(repr(e))
            print(self)
            print(self.path)
            print(resp.headers)        
            print(self.headers)

    def do_PUT(self, body=True):
        try:
            mode = ""
            cookies = self.headers["Cookie"]
            if cookies:
                for cookie in cookies.split("; "):
                    cookie_split = cookie.split("=")
                    if cookie_split[0] == "key":
                        mode = self.crypt.decode(cookie_split[1]+"=")
            if mode == "write":
                url = 'http://{}{}'.format(self.dataip, self.path)
                content_len = int(self.headers['Content-Length'])
                data = self.rfile.read(content_len)
                resp = self.send_request('PUT', url, body=data, headers=self.headers)
                self.send_response(resp.status_code)
                for header in resp.headers:
                    if not header.lower().startswith("x-") :
                        if not (header.lower() in ["content-encoding", "vary", "strict-transport-securit"]):
                            self.send_header(header, resp.headers[header])
                self.send_header("Set-Cookie", "key="+self.crypt.get_first_key(mode)+"; Max-Age=7200; Path=/; HttpOnly")
                self.end_headers()
                for chunk in resp.iter_content(chunk_size=8192):
                    self.wfile.write(chunk)  
        except requests.ConnectionError:
            self.restart_cal()
            self.send_response(505)
            self.end_headers()
            self.wfile.write("conectionError".encode()) 
        except Exception as e:
            print(repr(e))
            print(self)
            print(self.path)
            print(resp.headers)        
            print(self.headers)

    def do_REPORT(self, body=True):
        try:
            mode = ""
            cookies = self.headers["Cookie"]
            if cookies:
                for cookie in cookies.split("; "):
                    cookie_split = cookie.split("=")
                    if cookie_split[0] == "key":
                        mode = self.crypt.decode(cookie_split[1]+"=")
            if mode in ["write", "read", "card"]:
                url = 'http://{}{}'.format(self.dataip, self.path)
                content_len = int(self.headers['Content-Length'])
                data = self.rfile.read(content_len)
                resp = self.send_request('REPORT', url, body=data, headers=self.headers)
                self.send_response(resp.status_code)
                for header in resp.headers:
                    if not header.lower().startswith("x-") :
                        if not (header.lower() in ["strict-transport-securit", "transfer-encoding", "keep-alive", "connection"]):
                            self.send_header(header, resp.headers[header])
                self.send_header("Set-Cookie", "key="+self.crypt.get_first_key(mode)+"; Max-Age=7200; Path=/; HttpOnly")
                self.end_headers()
                for chunk in resp.iter_content(chunk_size=8192):
                    self.wfile.write(chunk)  
        except requests.ConnectionError:
            self.restart_cal()
            self.send_response(505)
            self.end_headers()
            self.wfile.write("conectionError".encode()) 
        except Exception as e:
            print(repr(e))
            print(self)
            print(self.path)
            print(resp.headers)        
            print(self.headers)

    def do_PROPFIND(self, body=True):
        try:
            mode = ""
            cookies = self.headers["Cookie"]
            if cookies:
                for cookie in cookies.split("; "):
                    cookie_split = cookie.split("=")
                    if cookie_split[0] == "key":
                        mode = self.crypt.decode(cookie_split[1]+"=")
            if mode in ["write", "read", "card"]:
                url = 'http://{}{}'.format(self.dataip, self.path)
                content_len = int(self.headers['Content-Length'])
                data = self.rfile.read(content_len)
                resp = self.send_request('PROPFIND', url, body=data, headers=self.headers)
                self.send_response(resp.status_code)
                for header in resp.headers:
                    if not header.lower().startswith("x-") :
                        if not (header.lower() in ["strict-transport-securit", "transfer-encoding", "keep-alive", "connection"]):
                            self.send_header(header, resp.headers[header])
                self.send_header("Set-Cookie", "key="+self.crypt.get_first_key(mode)+"; Max-Age=7200; Path=/; HttpOnly")
                self.end_headers()
                if self.path.find("/calendars/") > 0:
                    self.wfile.write(resp.content)
                else:
                    for chunk in resp.iter_content(chunk_size=32000):
                        self.wfile.write(chunk)
                return  
        except requests.ConnectionError:
            self.restart_cal()
        except ConnectionResetError:
            print("ConnectionResetError")
        except Exception as e:
            print("Error in PROFIND", self.path)
            print(repr(e))
        self.send_response(404)
        self.send_header("Content-Type", "text/html")
        self.end_headers()
        self.wfile.write(("mode: "+mode).encode()) 

    def do_POST(self, body=True):
        try:
            mode = ""
            cookies = self.headers.get("Cookie")
            for cookie in cookies.split("; "):
                    cookie_split = cookie.split("=")
                    if cookie_split[0] == "key":
                        mode = self.crypt.decode(cookie_split[1]+"=")
            if mode in ["write", "read", "card"]:
                url = 'http://{}{}'.format(self.ip, self.path)
                content_len = int(self.headers['Content-Length'])
                data = self.rfile.read(content_len)
                headers = {}
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                headers["Content-Length"] = str(content_len)
                resp = requests.post(url, data=data.decode(), headers=headers)
                self.send_response(resp.status_code)
                content = resp.content
                if resp.headers["Content-Type"].lower().startswith("text/html"):
                    content = content.decode().replace("http://"+self.ip, "https://"+"home.caldav.holler.pro").encode()
                self.send_header("Content-Length", len(content))
                self.send_header("Set-Cookie", "key="+self.crypt.get_first_key(mode)+"; Max-Age=7200; Path=/; HttpOnly")
                self.end_headers()
                self.wfile.write(content)  
                self.wfile.flush()
        except Exception as e:
            print(repr(e))

    def do_GET(self, body=True):
        try:
            _in = urlparse.urlparse(self.path)
            _param = urlparse.parse_qs(_in.query)
            mode = ""
            php = ""
            first = False
            if "first" in _param:
                mode = self.crypt.decode(_param["first"][0].replace(" ", "+"))
                first = True
            else:
                cookies = self.headers.get("Cookie")
                for cookie in cookies.split("; "):
                    cookie_split = cookie.split("=")
                    if cookie_split[0] == "key":
                        mode = self.crypt.decode(cookie_split[1]+"=")
            headers = self.parse_headers()
            if mode in ["write", "read", "card"]:
                if first:
                    url = 'http://{}{}'.format(self.ip, _in.path)
                    h = copy.copy(headers)
                    if "Cookie" in h:
                        del h["Cookie"]
                    if "Referer" in h:
                        del h["Referer"]
                    resp = requests.get(url, headers=[])
                    headers = {}
                else:
                    url = 'http://{}{}'.format(self.ip, self.path)
                    resp = requests.get(url, headers=headers)
                self.send_response(resp.status_code)
                for header in resp.headers:
                    if not header.lower().startswith("x-") :
                        if not (header.lower() in ["content-encoding", "vary", "strict-transport-securit"]):
                            self.send_header(header, resp.headers[header])
                content = resp.content
                if "Content-Type" in resp.headers and resp.headers["Content-Type"].lower().startswith("text/html"):
                    content = content.decode().replace("http://"+self.ip, "https://"+"home.calendar.holler.pro").encode()
                    if self.path.find("index") > -1:
                        content = (content.decode() + "<script>var ii=0;function getint(){ii = ii + 1; fetch('/?_='+ii)};setInterval(getint, 60000);</script>").encode()
                self.send_header("Content-Length", len(content))
                self.send_header("Set-Cookie", "key="+self.crypt.get_first_key(mode)+"; Max-Age=7200; Path=/; HttpOnly")
                self.end_headers()
                self.wfile.write(content)  
                self.wfile.flush()
                return              
        except requests.ConnectionError:
            self.restart()
        except Exception as e:
            print(repr(e))
            pass

class server:
    def __init__(self, log, port, ip, dataip, crypt, restart, restart_cal):
        log.debug("init")
        self.log = log
        self.port = port
        self.crypt = crypt
        self.ip = ip
        self.dataip = dataip
        self.restart_cal = restart_cal
        self.restart = restart
        handler = partial(webserverHandler, log, crypt, ip, dataip, restart, restart_cal)
        self.server = ThreadedHTTPServer(('0.0.0.0', port), handler)
        th = loopThread(self.server)
        th.start()

    def stop(self):
        self.server.shutdown()
        self.server.server_close()
        time.sleep(2)
        try:
            requests.get("http://127.0.0.1:"+str(self.port))
        except:
            pass
        time.sleep(3)
