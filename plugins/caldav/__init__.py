# smartlife
# 08.2021

import json

from plugins2 import plugin_base

from caldav.lxc_controler import controler as lxc_class
from caldav.crypt import crypt
from caldav.server import server

class integration(plugin_base):
    def __init__(self, smartlife, modul):
        plugin_base.__init__(self, smartlife, modul)
        self.alive = [999999999]
        self.lxc = lxc_class(self.log, smartlife.base_path, self.cfg["lxc_name"])
        self.crypt = crypt(self.log, self.cfg["timeout"])
        self.server = server(self.log, self.cfg["port"], self.cfg["ip"], self.cfg["data_ip"], self.crypt, self.restart, self.restart_calendar)

    def restart_calendar(self):
        self.log.error("restart_calendar")
        mod = self.smartlife.get_plugin("calendar_baikal")
        mod.restart()

    def restart(self):
        self.log.error("restart")
        self.lxc.restart()

    def pl_get_menus(self, user, ip):
        self.log.debug("user")
        rights = self.rights(user, ip)
        out = []
        if rights["domain"]:
            if "write" in rights["sub"]:
                out.append({'label': 'Kalender', 'mod': 'caldav', 'p1':'full'})
            elif "read" in rights["sub"]:
                out.append({'label': 'Kalender', 'mod': 'caldav', 'p1':'full'})
            elif "card" in rights["sub"]:
                out.append({'label': 'Kalender', 'mod': 'caldav', 'p1':'full'})
        return out

    def pl_data(self, user, cmd, _in, ip):
        self.log.debug(cmd)
        rights = self.rights(user, ip)
        if rights["domain"]:
            if "write" in rights["sub"]:
                if cmd == "get_cal":
                    return {"allowed": True, "data": self.crypt.get_first_key("write")}
            elif "read" in rights["sub"]:
                if cmd == "get_cal":
                    return {"allowed": True, "data": self.crypt.get_first_key("read")}
            elif "card" in rights["sub"]:
                if cmd == "get_cal":
                    return {"allowed": True, "data": self.crypt.get_first_key("read")}

    def stop(self):
        self.running = False
        self.lxc.stop()
        self.server.stop()

