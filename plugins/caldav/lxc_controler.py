# smartlife
# 07.2021
# 08.2021

import time
from threading import Timer

import lxc_tools

class controler:
    def __init__(self, log, path, container):
        log.debug("init")
        self.running = True
        self.log = log
        self.path = path + "/lxc_images"
        self.lxc = lxc_tools.tools(self.log, self.path, container)
        self.timer = None
        self.check()

    def restart(self):
        self.log.debug("restart")
        self.lxc.stop()
        time.sleep(5000)
        self.lxc.start()
    
    def check(self):
        self.log.debug("check")
        if self.running:
            if self.timer:
                self.timer.cancel()
            self.timer = Timer(300, self.check)
            self.timer.start()
            if not self.lxc.is_running():
                self.lxc.start()

    def stop(self):
        self.log.debug("stop")
        self.running = False
        if self.timer:
            self.timer.cancel()
        self.lxc.stop()