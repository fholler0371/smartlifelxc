# smartlife
# 06.2021

import json
from random import random
from threading import Timer

from plugins2 import plugin_base

from fin_lv.cards import cards
from fin_lv.db import db

class integration(plugin_base):
    def __init__(self, smartlife, modul):
        global ALIVE_WWW
        plugin_base.__init__(self, smartlife, modul)
        self.alive = [999999999]
        self.db = db(self.log, smartlife, self.cfg["clients"], self.cfg["master"])
        self.cards = cards(self.log, self.db)
        self.timer_hist = None

    def pl_data(self, user, cmd, _in, ip):
        self.log.debug(cmd)
        rights = self.rights(user, ip)
        if rights["domain"]:
            if "card" in rights["sub"] or "read" in rights["sub"]:
                if cmd == "get_cards":
                    return self.cards.get_cards()
                elif cmd == "get_card":
                    return self.cards.get_card()
            if "read" in rights["sub"]:
                if cmd == "get_history":
                    return self.db.get_history()
        return {"allowed": False}

    def calc_hist(self):
        self.log.debug("calc_hist")
        if self.timer_hist:
            self.timer_hist.cancel()
        self.timer_hist = Timer(random()*15, self.db.do_calc)
        self.timer_hist.start()

    def stop(self):
        self.log.info(self.name)
        self.running = False
        if self.timer_hist:
            self.timer_hist.cancel()
        self.db.stop()
