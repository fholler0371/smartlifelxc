# smartlife
# 06.2021

import json
from threading import Timer
from random import random

from plugins2 import plugin_base

from fin_depot_consors.cards import cards
from fin_depot_consors.db_eur import db as eur
from fin_depot_consors.db_usd import db as usd
from fin_depot_consors.db_history import db as history
from fin_depot_consors.importer_eur import importer as importer_eur
from fin_depot_consors.importer_usd import importer as importer_usd

class integration(plugin_base):
    def __init__(self, smartlife, modul):
        global ALIVE_WWW
        plugin_base.__init__(self, smartlife, modul)
        self.alive = [999999999]
        self.eur = eur(self.log, smartlife)
        self.usd = usd(self.log, smartlife)
        self.history = history(self.log, smartlife, self.cfg["master"])
        self.cards = cards(self.log, smartlife, self.eur, self.usd)
        self.importer_eur = importer_eur(self.log, smartlife, self.eur, self.history)
        self.importer_usd = importer_usd(self.log, smartlife, self.usd, self.history)
        self.timer_hist = None
        
    def pl_data(self, user, cmd, _in, ip):
        self.log.debug(cmd)
        rights = self.rights(user, ip)
        if rights["domain"]:
            if "card" in rights["sub"] or "read" in rights["sub"]:
                if cmd == "get_cards":
                    return self.cards.get_cards()
                elif cmd == "get_card":
                    return self.cards.get_card()
            if "read" in rights["sub"]:
                if cmd == "get_book_eur":
                    return self.eur.get_book(_in)
                elif cmd == "get_book_usd":
                    return self.usd.get_book(_in)
                elif cmd == "get_book_aktien":
                    return self.eur.get_shares()
                elif cmd == "get_history":
                    return self.history.get_history()
            if "write" in rights["sub"]:
                if cmd == "set_count_eur":
                    return self.eur.set_count(_in)
                if cmd == "upload_prefetch":
                    if _in["sub"] == "book_eur":
                        return self.importer_eur.upload_prefetch(_in)
                    elif _in["sub"] == "book_usd":
                        return self.importer_usd.upload_prefetch(_in)
                elif cmd == "do_calc":
                    return self.history.do_calc()
        return {"allowed": False}

    def uploaded_import_eur(self, data, param):
        self.log.debug(param)
        ok = self.importer_eur.load_data(data)
        self.history.do_calc()
        return {"ok": ok, "data": {}}

    def uploaded_import_usd(self, data, param):
        self.log.debug(param)
        ok = self.importer_usd.load_data(data)
        self.history.do_calc()
        return {"ok": ok, "data": {}}

    def calc_hist(self):
        self.log.debug("calc_hist")
        if self.timer_hist:
            self.timer_hist.cancel()
        self.timer_hist = Timer(random()*15, self.history.do_calc)
        self.timer_hist.start()

    def stop(self):
        self.log.info(self.name)
        self.running = False
        self.history.stop()
