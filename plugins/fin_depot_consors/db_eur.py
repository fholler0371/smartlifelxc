# smartlife
# 06.2021

from threading import Thread, Timer
from datetime import datetime, timedelta
import time
import random

from database import database

class db:
    def __init__(self, log, smartlife):
        log.debug("init")
        self.log = log
        self.smartlife = smartlife
        sql = "CREATE TABLE IF NOT EXISTS bookings (id integer PRIMARY KEY AUTOINCREMENT, date date DEFAULT '1900-01-01',text text,"
        sql += "typ text, value	numeric, anzahl numeric, wkn text)"
        self.book = database(self.log, smartlife.db_path + "/fin_depot_consors_eur.sqlite", sql)

    def get_book(self, data):
        self.log.debug(data)
        out = {"records": 0, "rows": []}
        self.book.open()
        row = self.book.fetchone("select count(id) from bookings")
        if row:
            out["records"] = row[0]
        sql = "select id, date, text, typ, value, anzahl, wkn from bookings order by date desc limit %s, %s"
        for row in self.book.fetchall(sql % (str(data["page"]*data['pagesize']), str(data['pagesize']), )):
            out["rows"].append({'id': row[0], 'date': row[1], 'text': row[2], 'typ': row[3], 'amount': row[4], 'anzahl': row[5], 'wkn': row[6]})
        self.book.close()
        return {"allowed": True, "data": out}

    def get_shares(self):
        self.log.debug("get_shares")
        out = []
        mod = self.smartlife.get_plugin("fin_shares")
        d = datetime.today()
        if mod:
            self.book.open()
            for row in self.book.fetchall("select sum(anzahl), wkn from bookings group by wkn"):
                if row[0] > 0:
                    rec = {"wkn": row[1], "count": row[0], "name": mod.name_by_wkn(row[1]), "amount": row[0]*mod.get_price(row[1], d)}
                    out.append(rec)
            self.book.close()
            out = sorted(out, key=lambda x: x["name"].upper())
        return {"allowed": True, "data": {"records": len(out), "rows": out}}

    def get_sum(self, d):
        self.log.debug("get_sum")
        value = 0
        self.book.open()
        row = self.book.fetchone("select sum(value) from bookings where date<='%s'" % (d.strftime("%Y-%m-%d")))
        if row and row[0]:
            value = row[0]
        self.book.close()
        return value

    def add(self, date, text, typ, value, wkn):
        self.log.debug(date)
        self.book.open()
        sql = "select id from bookings where date='%s' and text='%s' and value='%s' and typ='%s'"
        row = self.book.fetchone(sql % (date, text, str(value), typ, ))
        if row == None:
            sql = "insert into bookings (date, text, typ, value, wkn, anzahl) values ('%s', '%s', '%s', '%s', '%s', '0' ) "
            self.book.commit(sql % (date, text, typ, str(value), wkn, ))
        self.book.close()

    def set_count(self, data):
        self.log.debug("set_count")
        self.book.open()
        sql = "update bookings set anzahl='%s' where id='%s'"
        self.book.commit(sql % (str(data["anzahl"]), str(data["id"])))
        self.book.close()
        return {"allowed": True, "data": {}}

    def stop(self):
        self.log.debug("stop")
            