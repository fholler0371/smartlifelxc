define(['packery', 'highcharts', 'moment_tz', 'jqxtabs', 'jqxdata', 'jqxgrid', 'jqxgrid_pager', 'jqxgrid_selection', 'jqxdropdownlist',
        'jqxdatetimeinput', 'jqxnumberinput', 'jqxbutton', 'jqxchart', 'upload', 'jqxgrid_edit'], function(Packery, H, moment) {
  window.moment = moment
  H.setOptions({
    global: {
      timezone: "Europe/Berlin"
    },
    lang : {
      months : ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
      shortMonths : ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
      weekdays : ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa']
    } 
  })
  var mod = {
    wdata : {},
    wdata_last : 0,
    wdata_call : 0,
    t_update_sum: null,
    t_update_month: null,
    t_update_year: null,
    init: function() {
      if ($('#fin_depot_consors_tab').length == 0) {
        html = '<div id="fin_depot_consors_tab"><ul><li>Status</li><li>Konto €</li><li>Konto $</li><li>Aktien</li><li>Verlauf</li></ul>'
        html += '<div id="fin_depot_consors_cards"></div><div id="fin_depot_consors_book_eur"></div>'
        html += '<div id="fin_depot_consors_book_usd"></div><div id="fin_depot_consors_book_aktien"></div>'
        html += '<div id="fin_depot_consors_history"></div></div>'
        $('#finanical_left').html(html)
        $('#fin_depot_consors_tab').jqxTabs({height: '100%', width: '100%'})
        $('#fin_depot_consors_tab').off('selected')
        $('#fin_depot_consors_tab').on('selected', function(ev) {
          var id = ev.args.item
          window.module.fin_depot_consors.call_tab(id)
        })
        window.module.fin_depot_consors.call_tab(0)
      }
    },
    check_data : function() {
      var now = new Date()
      var t = Math.round(now.getTime() / 1000)
      if (t - window.module.fin_depot_consors.wdata_last > 300) {
        if (t - window.module.fin_depot_consors.wdata_call > 10) {
          window.module.fin_depot_consors.wdata_call = t
          window.smfetch('/api/fin_depot_consors', {cmd: 'get_card'}).then((data) => {
            if (data.error == undefined) {
              window.module.fin_depot_consors.wdata_last = t
              window.module.fin_depot_consors.wdata = data.data
            }
          })
        }
      }
    },
    update_sum: function() {
      clearTimeout(window.module.fin_depot_consors.t_update_sum)
      window.module.fin_depot_consors.check_data()
      var sum = window.module.fin_depot_consors.wdata.sum
      if (sum != undefined) {
        var ele = $($("body").find('.cards_fin_depot_consors_sum')[0])
        var old = ele.data('last')
        if (old != sum) {
          ele.data('last', sum)
          html  = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">Übersicht</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 35px;"><span style="font-weight: 600;">Depot:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 35px;">'
          html += sum.depot.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 63px;"><span style="font-weight: 600;">Verrechnungskonto EUR:</span>'
          html += '</div><div style=" position: absolute; right: 5px; float: right; top: 63px;">'
          html += sum.eur.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 91px;"><span style="font-weight: 600;">Verrechnungskonto USD:</span>'
          html += '</div><div style=" position: absolute; right: 5px; float: right; top: 91px; font-style: italic; font-size: 97%">'
          html += sum.usd.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' $</div>'
          html += '</div><div style=" position: absolute; right: 5px; float: right; top: 111px;">'
          html += sum.usd_eur.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 139px;"><span style="font-weight: 600;">Summe:</span>'
          html += '</div><div style=" position: absolute; right: 5px; float: right; top: 139px; font-weight: 600;">'
          html += sum.sum.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          ele.html(html)
        }
        window.module.fin_depot_consors.t_update_sum = setTimeout(window.module.fin_depot_consors.update_sum, 600000)
      } else {
        window.module.fin_depot_consors.t_update_sum = setTimeout(window.module.fin_depot_consors.update_sum, 250)
      }
    },
    update_month: function() {
      clearTimeout(window.module.fin_depot_consors.t_update_month)
      window.module.fin_depot_consors.check_data()
      var d = window.module.fin_depot_consors.wdata.month
      if (d != undefined) {
        var ele = $($("body").find('.cards_fin_depot_consors_month')[0])
        var old = ele.data('last')
        if (old != d) {
          ele.data('last', d)
          html  = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">Monat</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 25px;"><span  style="font-weight: 600;">Einahmen:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 25px;">'
          html += d.in.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 45px;"><span  style="font-weight: 600;">Ausgaben:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 45px;">'
          html += d.out.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          ele.html(html)
        }
        window.module.fin_depot_consors.t_update_month = setTimeout(window.module.fin_depot_consors.update_month, 600000)
      } else {
        window.module.fin_depot_consors.t_update_month = setTimeout(window.module.fin_depot_consors.update_month, 250)
      }
    },
    update_year: function() {
      clearTimeout(window.module.fin_depot_consors.t_update_year)
      window.module.fin_depot_consors.check_data()
      var d = window.module.fin_depot_consors.wdata.year
      if (d != undefined) {
        var ele = $($("body").find('.cards_fin_depot_consors_year')[0])
        var old = ele.data('last')
        if (old != d) {
          ele.data('last', d)
          html  = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">Jahr</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 25px;"><span  style="font-weight: 600;">Einahmen:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 25px;">'
          html += d.in.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 45px;"><span  style="font-weight: 600;">Ausgaben:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 45px;">'
          html += d.out.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          ele.html(html)
        }
        window.module.fin_depot_consors.t_update_year = setTimeout(window.module.fin_depot_consors.update_year, 600000)
      } else {
        window.module.fin_depot_consors.t_update_year = setTimeout(window.module.fin_depot_consors.update_year, 250)
      }
    },
    get_card : function(data, div) {
      if (data["card"] == "fin_depot_consors_sum") {
        if (div.find(".cards_fin_depot_consors_sum").length == 0) {
          var html = '<div class="cards_fin_depot_consors_sum card card_h2"><span></span></div>'
          div.append(html)
        } else {
//          console.log(data)
        }
        window.module.fin_depot_consors.update_sum()
      } else if (data["card"] == "fin_depot_consors_month") {
        if (div.find(".cards_fin_depot_consors_month").length == 0) {
          var html = '<div class="cards_fin_depot_consors_month card"><span></span></div>'
          div.append(html)
        } else {
//          console.log(data)
        }
        window.module.fin_depot_consors.update_month()
      } else if (data["card"] == "fin_depot_consors_year") {
        if (div.find(".cards_fin_depot_consors_year").length == 0) {
          var html = '<div class="cards_fin_depot_consors_year card"><span></span></div>'
          div.append(html)
        } else {
//          console.log(data)
        }
        window.module.fin_depot_consors.update_year()
      }
    },
    create_cards: function() {
      var ok = 1
      var cards = window.module.fin_depot_consors.cards
      var l = cards.length
      for (var i=0; i<l; i++) {
         if (!(typeof(window.module[cards[i].mod].get_card) === 'function')) {
           console.log("Modul not found: "+cards[i].mod)
           ok = 0
         }
      }
      if (ok == 1) {
        for (var i=0; i<l; i++) {
          window.module[cards[i].mod].get_card(cards[i], $("#fin_depot_consors_cards"))
          require( [ 'jquery-bridget/jquery-bridget' ], function( jQueryBridget ) {
            jQueryBridget( 'packery', Packery, $ );
            var grid = $('#fin_depot_consors_cards').packery({
              itemSelector: '.card',
              gutter: 10
            })
          })
        }
      } else {
        console.log("recheck")
      }
    },
    stop: function() {
    },
    call_tab: function(id) {
      if (id == 0) {
        window.smfetch('/api/fin_depot_consors', {cmd: 'get_cards'}).then((data) => {
          if (data.error == undefined) {
            window.module.fin_depot_consors.cards = data.data
            window.module.fin_depot_consors.cards_check_mod = 10
            window.module.fin_depot_consors.create_cards()
          }
        })
      } else if (id == 1) {
        if ($('#fin_depot_consors_bookings_eur').length == 0) {
          var source = {
            datatype: 'json',
            loadServerData: window.module.fin_depot_consors.load_table_eur,
            datafields: [
              { name: 'id' },
              { name: 'date', type: 'date' },
              { name: 'text' },
              { name: 'typ'},
              { name: 'amount', type: 'float' },
              { name: 'wkn'},
              { name: 'anzahl', type: 'float'}
            ],
            id: 'id',
            pagesize: 20,
            url: "http://x.x"
          }
          var dataAdapter = new $.jqx.dataAdapter(source);
          $('#fin_depot_consors_book_eur').append("<div id='fin_depot_consors_bookings_eur'></div>");
          $("#fin_depot_consors_bookings_eur").jqxGrid({
            width:'calc( 100% - 3px )',
            height:'calc( 100% - 5px )',
            localization: {
              currencysymbol: " €",
              currencysymbolposition: "after",
              decimalseparator: ",",
              thousandsseparator: ".",
              pagergotopagestring: "Gehe zu:",
              pagershowrowsstring: "Zeige Zeile:",
              pagerrangestring: " von ",
              pagernextbuttonstring: "voriger",
              pagerpreviousbuttonstring: "nächster"
            },
            source: dataAdapter,
            pageable: true,
            editable: true,
            pagesizeoptions: ['10', '20', '30', '50', '100'],
            virtualmode: true,
            rendergridrows: function(obj){
              return obj.data;
            },
            showtoolbar: true,
            rendertoolbar: function (statusbar) {
              var container = $("<div style='overflow: hidden; position: relative; margin: 5px;'></div>")
              var csvButton = $("<div style='float: left; margin-left: 5px;'><span style='margin-left: 4px; position: relative; top: -3px;'>CSV hochladen</span></div>")
              var calcButton = $("<div style='float: left; margin-left: 5px;'><span style='margin-left: 4px; position: relative; top: -3px;'>berechnen</span></div>")
              container.append(csvButton)
              container.append(calcButton)
              csvButton.jqxButton({  width: 95, height: 20 })
              csvButton.click(function (event) {
                window.upload.run('fin_depot_consors', 'book_eur', 'Consors Depot € *.csv', '.csv', true, 
                                  window.module.fin_depot_consors.upload_finished_eur)
              })
              calcButton.jqxButton({  width: 95, height: 20 })
              calcButton.click(function (event) {
                window.smfetch('/api/fin_depot_consors', {cmd: 'do_calc'}).then((data) => {
                  if (data.error == undefined) {
                    $('#fin_depot_consors_chart').remove()
                  }
                })
              })
              statusbar.append(container)
            },
            columns : [
              {text:'id', datafield:'id', hidden: true, editable: false},
              {text:'Datum', datafield:'date', width:100,  columntype: 'datetimeinput', cellsformat: 'dd.MM.yyyy', editable: false},
              {text:'Text', datafield:'text', editable: false},
              {text:'Typ', datafield:'typ', width:150, editable: false},
              {text:'Betrag', datafield:'amount', width:100, cellsalign: 'right', cellsformat: 'c2', columntype:'numberinput', editable: false},
              {text:'WKN', datafield:'wkn', width:90, editable: false},
              {text:'Anzahl', datafield:'anzahl', width:100, cellsalign: 'right', cellsformat: 'f2', columntype:'numberinput'}
            ]
          })
        }
        $("#fin_depot_consors_bookings_eur").on('cellendedit', function (event) {
          var args = event.args;
          window.smfetch('/api/fin_depot_consors', {cmd: 'set_count_eur', 
                         data: {'id': args.row.id, 'anzahl': args.value}}).then((data) => {})
        })
      } else if (id == 2) {
        if ($('#fin_depot_consors_bookings_usd').length == 0) {
          var source = {
            datatype: 'json',
            loadServerData: window.module.fin_depot_consors.load_table_usd,
            datafields: [
              { name: 'id' },
              { name: 'date', type: 'date' },
              { name: 'text' },
              { name: 'typ'},
              { name: 'amount', type: 'float' },
              { name: 'wkn'}
            ],
            id: 'id',
            pagesize: 20,
            url: "http://x.x"
          }
          var dataAdapter = new $.jqx.dataAdapter(source);
          $('#fin_depot_consors_book_usd').append("<div id='fin_depot_consors_bookings_usd'></div>");
          $("#fin_depot_consors_bookings_usd").jqxGrid({
            width:'calc( 100% - 3px )',
            height:'calc( 100% - 5px )',
            localization: {
              currencysymbol: " $",
              currencysymbolposition: "after",
              decimalseparator: ",",
              thousandsseparator: ".",
              pagergotopagestring: "Gehe zu:",
              pagershowrowsstring: "Zeige Zeile:",
              pagerrangestring: " von ",
              pagernextbuttonstring: "voriger",
              pagerpreviousbuttonstring: "nächster"
            },
            source: dataAdapter,
            pageable: true,
            editable: true,
            pagesizeoptions: ['10', '20', '30', '50', '100'],
            virtualmode: true,
            rendergridrows: function(obj){
              return obj.data;
            },
            showtoolbar: true,
            rendertoolbar: function (statusbar) {
              var container = $("<div style='overflow: hidden; position: relative; margin: 5px;'></div>")
              var csvButton = $("<div style='float: left; margin-left: 5px;'><span style='margin-left: 4px; position: relative; top: -3px;'>CSV hochladen</span></div>")
              var calcButton = $("<div style='float: left; margin-left: 5px;'><span style='margin-left: 4px; position: relative; top: -3px;'>berechnen</span></div>")
              container.append(csvButton)
              container.append(calcButton)
              csvButton.jqxButton({  width: 95, height: 20 })
              csvButton.click(function (event) {
                window.upload.run('fin_depot_consors', 'book_usd', 'Consors Depot $ *.csv', '.csv', true, 
                                  window.module.fin_depot_consors.upload_finished_usd)
              })
              calcButton.jqxButton({  width: 95, height: 20 })
              calcButton.click(function (event) {
                window.smfetch('/api/fin_depot_consors', {cmd: 'do_calc'}).then((data) => {
                  if (data.error == undefined) {
                    $('#fin_depot_consors_chart').remove()
                  }
                })
              })
              statusbar.append(container)
            },
            columns : [
              {text:'id', datafield:'id', hidden: true, editable: false},
              {text:'Datum', datafield:'date', width:100,  columntype: 'datetimeinput', cellsformat: 'dd.MM.yyyy', editable: false},
              {text:'Text', datafield:'text', editable: false},
              {text:'Typ', datafield:'typ', width:150, editable: false},
              {text:'Betrag', datafield:'amount', width:100, cellsalign: 'right', cellsformat: 'c2', columntype:'numberinput', editable: false},
              {text:'WKN', datafield:'wkn', width:90, editable: false}
            ]
          })
        }
      } else if (id == 3) {
        if ($('#fin_depot_consors_bookings_aktien').length == 0) {
          var source = {
            datatype: 'json',
            loadServerData: window.module.fin_depot_consors.load_table_aktien,
            datafields: [
              { name: 'name' },
              { name: 'wkn' },
              { name: 'count', type: 'float' },
              { name: 'amount', type: 'float' }
            ],
            id: 'id',
            pagesize: 20,
            url: "http://x.x"
          }
          var dataAdapter = new $.jqx.dataAdapter(source);
          $('#fin_depot_consors_book_aktien').append("<div id='fin_depot_consors_bookings_aktien'></div>");
          $("#fin_depot_consors_bookings_aktien").jqxGrid({
            width:'calc( 100% - 3px )',
            height:'calc( 100% - 5px )',
            localization: {
              currencysymbol: " €",
              currencysymbolposition: "after",
              decimalseparator: ",",
              thousandsseparator: "."
            },
            source: dataAdapter,
            columns : [
              {text:'Name', datafield:'name'},
              {text:'WKN', datafield:'wkn', width:90},
              {text:'Anzahl', datafield:'count', width:100, cellsalign: 'right', cellsformat: 'f2'},
              {text:'Betrag', datafield:'amount', width:100, cellsalign: 'right', cellsformat: 'c2'}
            ]
          })
        } else {
          $("#fin_depot_consors_bookings_aktien").jqxGrid('updatebounddata')
        }
      } else if (id == 4) {
        if ($('#fin_depot_consors_chart').length == 0) {
          $('#fin_depot_consors_history').append("<div id='fin_depot_consors_chart' style='height: 100%; width: 100%'></div>")
          $('#fin_depot_consors_history').css('overflow', 'hidden')
          window.smfetch('/api/fin_depot_consors', {cmd: 'get_history'}).then((data) => {
            if (data.error == undefined) {
              d = data.data
              var series = []
              series.push({name: 'Gewinn', data: [], color: '#00FF00', marker: {enabled: false}, lineWidth: 4})
              series.push({name: 'Betrag', data: [], color: '#0066FF', marker: {enabled: false}, lineWidth: 4})
              var len = d.length
              for (var i=0; i<len; i++) {
                var t = new Date(d[i].date)
                series[1].data.push([t.getTime(), d[i].value])
                series[0].data.push([t.getTime(), d[i].interest])
              }
              var settings = {
                chart: { type: 'area' },
                title : { text: '' },
                subtitle : { text: '' },
                plotOptions: {
                  area: {
                    stacking: 'normal'
                  }
                },
                yAxis : { title: {text: 'Euro' }},
                xAxis: {
                  type: 'datetime',
                  title: { text: '' }
                },
                series: series
              }
              H.chart('fin_depot_consors_chart', settings)
            }
          })
        }
      }
    },
    upload_finished_eur : function() {
      setTimeout(window.module.fin_depot_consors.reload_table_eur, 3000)
    },
    reload_table_eur: function() {
      $("#fin_depot_consors_bookings_eur").jqxGrid('updatebounddata')
    },
    load_table_eur: function(data, source, callback) {
      window.smfetch('/api/fin_depot_consors', {cmd: 'get_book_eur', data: {'page': data.pagenum, 'pagesize': data.pagesize}}).then((data) => {
        if (data.error == undefined) {
          for (var i=0; i<data.data.rows.length; i++) {
	          data.data.rows[i].date = new Date(data.data.rows[i].date)
	        }
          callback({ records:data.data.rows, totalrecords : data.data.records})
        }
      })
    },
    upload_finished_usd : function() {
      setTimeout(window.module.fin_depot_consors.reload_table_usd, 3000)
    },
    reload_table_usd: function() {
      $("#fin_depot_consors_bookings_usd").jqxGrid('updatebounddata')
    },
    load_table_usd: function(data, source, callback) {
      window.smfetch('/api/fin_depot_consors', {cmd: 'get_book_usd', data: {'page': data.pagenum, 'pagesize': data.pagesize}}).then((data) => {
        if (data.error == undefined) {
          for (var i=0; i<data.data.rows.length; i++) {
	          data.data.rows[i].date = new Date(data.data.rows[i].date)
	        }
          callback({ records:data.data.rows, totalrecords : data.data.records})
        }
      })
    },
    load_table_aktien: function(data, source, callback) {
      window.smfetch('/api/fin_depot_consors', {cmd: 'get_book_aktien', data: {}}).then((data) => {
        if (data.error == undefined) {
          callback({ records:data.data.rows, totalrecords : data.data.records})
        }
      })
    }
  }
  window.module.fin_depot_consors = mod
  return mod
})
