# smartlife
# 06.2021

from datetime import datetime

class cards:
    def __init__(self, log, smartlife, db, db_usd):
        log.debug("init")
        self.log = log
        self.db = db
        self.db_usd = db_usd
        self.smartlife = smartlife

    def get_cards(self):
        self.log.debug("get_cards")
        out = [{"mod": "fin_depot_consors", "card": "fin_depot_consors_sum", "name": "Consors Depot Summe"}]
#               {"mod": "fin_depot_consors", "card": "fin_depot_consors_month", "name": "Consors Depot Monat"},
#               {"mod": "fin_depot_consors", "card": "fin_depot_consors_year", "name": "Consors Depot Jahr"}]
        return {"allowed": True, "data": out}

    def get_card(self):
        self.log.debug("get_card")
        d = datetime.today()
        out = {"sum": {"depot" : 0, "eur": 0, "usd": 0, "usd_eur": 0, "sum": 0}}
        mod = self.smartlife.get_plugin("fin_shares")
        for entry in self.db.get_shares()["data"]["rows"]:
            out["sum"]["depot"] = out["sum"]["depot"] + entry["amount"]
        out["sum"]["eur"] = self.db.get_sum(d)
        out["sum"]["usd"] = self.db_usd.get_sum(d)
        if mod:
            out["sum"]["usd_eur"] = out["sum"]["usd"] / mod.get_price("USD", d)
        out["sum"]["sum"] = out["sum"]["depot"] + out["sum"]["eur"] + out["sum"]["usd_eur"]
        return {"allowed": True, "data": out}
