# smartlife
# 06.2021

from threading import Thread, Timer
from datetime import datetime, timedelta
import time
import random

from database import database

class history(Thread):
    def __init__(self, log, name, book, master, shares):
        Thread.__init__(self)
        log.debug("init")
        self.log = log
        self.master = master
        self.shares = shares
        sql = "CREATE TABLE IF NOT EXISTS history (date string, value numeric, interest numeric)"
        self.hist = database(self.log, name, sql)
        self.book_eur = database(self.log, book.replace("$$", "eur"))
        self.book_usd = database(self.log, book.replace("$$", "usd"))

    def run(self):
        self.log.debug("run")
        self.hist.open()
        self.book_eur.open()
        self.book_usd.open()
        maxDate = datetime.today()
        row = self.hist.fetchone("select date from history order by date desc limit 1")
        if row:
            minDate = datetime.strptime(row[0], "%Y-%m-%d")
        else:
            row = self.book_eur.fetchone("select date from bookings order by date asc limit 1")
            if not row:
                self.book_eur.close()
                self.hist.close()
                return
            minDate = datetime.strptime(row[0], "%Y-%m-%d")
        # setze min 31 Tage vor
        minDate = minDate - timedelta(days=31)
        # check minDate gegen Daten
        row = self.book_eur.fetchone("select date from bookings order by date asc limit 1")
        if not row:
            self.book.close()
            self.hist.close()
            return
        d = datetime.strptime(row[0], '%Y-%m-%d')
        if d > minDate:
            minDate = d
        # loop
        d = minDate
        days = 0
        sql1 = "select sum(value) from bookings where (typ='Dauerauftragsgutschrift' or typ='EURO-Überweisung') and date <= '%s'"
        sql2 = "select sum(value) from bookings where date <= '%s'"
        sql3 = "select sum(anzahl), wkn from bookings where date <= '%s' group by wkn"
        sql4 = "update history set value='%s', interest='%s' where date='%s'"
        sql5 = "insert into history (value, interest, date) values ('%s', '%s', '%s')"
        while d <= maxDate and days<365:
            amount = 0
            row = self.book_eur.fetchone(sql1 % (d.strftime('%Y-%m-%d'), ))
            if row and row[0]:
                amount = row[0]
            sum = 0
            row = self.book_eur.fetchone(sql2 % (d.strftime('%Y-%m-%d'), ))
            if row and row[0]:
                sum = row[0]
            row = self.book_usd.fetchone(sql2 % (d.strftime('%Y-%m-%d'), ))
            if row and row[0] and self.shares:
                sum += row[0] / self.shares.get_price("USD", d)
            for row in self.book_eur.fetchall(sql3 % (d.strftime('%Y-%m-%d'), )):
                if row[0] > 0:
                    sum += row[0] * self.shares.get_price(row[1], d)
            if self.hist.fetchone("select date from history where date = '%s'"  % (d.strftime('%Y-%m-%d'), )):
                sql =  sql4 % ("%.2f" % round(amount, 2), "%.2f" % round(sum - amount, 2), d.strftime('%Y-%m-%d'), )
            else:
                sql =  sql5 % ("%.2f" % round(amount, 2), "%.2f" % round(sum - amount, 2), d.strftime('%Y-%m-%d'), )
            self.hist.commit(sql)
            days += 1
            d = d + timedelta(days=1)
        return
        self.book_eur.close()
        self.book_usd.close()
        self.hist.close()
        if self.master:
            self.master.calc_hist()

class db:
    def __init__(self, log, smartlife, master):
        log.debug("init")
        self.log = log
        self.smartlife = smartlife
        self.master = master
        self.hist = database(self.log, smartlife.db_path + "/fin_depot_consors_history.sqlite")
        t = (int(time.time()/86400)+1)*86400-time.time()+60+300*random.random()
        self.timer_calc = Timer(t, self.tick_calc)
        self.timer_calc.start()
        self.th = None

    def tick_calc(self):
        self.log.debug("tick_calc")
        t = (int(time.time()/86400)+1)*86400-time.time()+60+300*random.random()
        self.timer_calc = Timer(t, self.tick_calc)
        self.timer_calc.start()
        self.do_calc()

    def do_calc(self):
        self.log.debug("do_calc")
        if not(self.th) or not( self.th.is_alive()):
            self.th = history(self.log, self.smartlife.db_path + "/fin_depot_consors_history.sqlite", 
                self.smartlife.db_path + "/fin_depot_consors_$$.sqlite", self.smartlife.get_plugin(self.master),
                self.smartlife.get_plugin("fin_shares"))
            self.th.start()
        return {"allowed": True, "data": {}}

    def get_history(self):
        self.log.debug("get_history")
        out = []
        self.hist.open()
        for entry in self.hist.fetchall("select date, value, interest from history order by date asc"):
            out.append({'date': entry[0], 'value': entry[1], 'interest': entry[2]})
        self.hist.close()
        return {"allowed": True, "data": out}

    def stop(self):
        self.log.debug("stop")
        if self.timer_calc:
            self.timer_calc.cancel()
            