import key_bcrypt

class login:
    def __init__(self, db):
        self.name = "normal"
        self.label = "Standard"
        self.local = False
        self.db = db

    def login(self, user, passwd, is_local):
        self.db.open()
        sql = "select id, name, password from user where user='" + user + "'"
        row = self.db.fetchone(sql)
        self.db.close()
        if row and len(row[2]) > 0:
            if key_bcrypt.checkpw(passwd, row[2]):
                return {"user": row[0], "name": row[1]}
        return {"user": -1}

    def change_pw(self, user, oldpw, newpw):
        self.db.open()
        sql = "select password from user where id='" + str(user) + "'"
        row = self.db.fetchone(sql)
        if row and len(row[0]) > 0:
            if key_bcrypt.checkpw(oldpw, row[0]):
                pw = key_bcrypt.encode_password(newpw)
                sql = "update user set password='" + pw + "' where id='" + str(user) + "'"
                self.db.commit(sql)
                self.db.close()
                return True
        self.db.close()
        return False
