import time

class login:
    def __init__(self, db):
        self.name = "local"
        self.label = "Lokal"
        self.local = True
        self.db = db

    def get_default_token(self, cfg):
        user_id = -1
        sql = "select id from user where user='" + cfg["local_default_user"] + "'"
        self.db.open()
        row = self.db.fetchone(sql)
        self.db.close()
        if row and row[0]:
            user_id = row[0]
        if user_id > 0:
            return {"user": user_id, "time": int(time.time()+10)}
        else:
            return {"user": user_id, "time": 0}

    def login(self, user, passwd, is_local):
        if is_local:
            self.db.open()
            sql = "select id, name from user where user='" + user + "' and local='1'"
            row = self.db.fetchone(sql)
            self.db.close()
            if row:
                return {"user": row[0], "name": row[1]}
        return {"user": -1}
