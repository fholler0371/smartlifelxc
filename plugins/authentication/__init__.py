# smartlife
# 04.2021
# 05.2021

import os
import json
import time
from urllib.parse import parse_qs
import base64

from netaddr import IPNetwork, IPAddress
from Crypto.Cipher import AES

from plugins2 import plugin_base
import database
import config_yaml

import authentication.login.local as l_local
import authentication.login.normal as l_normal
import authentication.manager as manager

ALIVE=30*24*3600

class integration(plugin_base):
    def __init__(self, smartlife, modul):
        plugin_base.__init__(self, smartlife, modul)
        global ALIVE
        self.alive = [ALIVE]
        self.auth_path = smartlife.base_path + "/.authentication"
        if not os.path.isdir(self.auth_path):
            os.mkdir(self.auth_path)
        key_file = self.auth_path + "/key.dat"
        if os.path.isfile(key_file):
            f = open(key_file, "r")
            self.aeskey = f.read()
            f.close()
        else:
            self.aeskey = key_bcrypt.get_new_key()
            f = open(key_file, "w")
            f.write(self.aeskey)
            f.close()
        self.db = database.database(self.log, self.auth_path + "/authentication.sqlite")
        self.l_login = []
        self.local = l_local.login(self.db)
        self.l_login.append(l_normal.login(self.db))
        self.l_login.append(self.local)
        self.manager = manager.manager(self.log, self.db, self.cfg_master)

    def is_local(self, ip):
        self.log.debug(ip)
        if "local" in self.cfg:
            for entry in self.cfg["local"]:
                if IPAddress(ip) in IPNetwork(entry):
                    return True
        if "local_avm_topic" in self.cfg and "ip4" in self.cfg["local_avm_topic"]:
            data_store = self.smartlife.get_plugin("data_store")
            if data_store:
                local_ip = data_store.get_sensor(self.cfg["local_avm_topic"]["ip4"])
                if local_ip:
                    if local_ip["value"] == ip:
                        return True
                else:
                    self.log.error("not found")    
        return False

    def check_rights(self, user, domain, ip):
        self.db.open()
        global ALIVE
        self.alive[0] = ALIVE
        sql = "select right from rights where user='%s'"
        rows = self.db.fetchall(sql % (str(user), ))
        r = []
        for row in rows:
            r.append(row[0])
        domain_ok = (domain in r or (self.is_local(ip) and (domain+"_local") in r))
        if domain_ok:
            sql = "select right from rights2 where user='%s'"
            rows = self.db.fetchall(sql % (str(user), ))
            r = []
            for row in rows:
                r.append(row[0])
            self.db.close()
            return {"domain": True, "sub": r}
        else:
            self.db.close()
            return {"domain": False}

    def web_doGet(self, path, param):
        self.log.debug(path)
        return {"code": 404}

    def web_doPost(self, path, param, data):
        self.log.debug(path)
        global ALIVE
        self.alive[0] = ALIVE
        if path == "check_token":
            _token = self.decode_token(json.loads(data)["token"])
            if _token["time"] == 0:
                _param = parse_qs(param)
                if "ip" in _param:
                    if self.is_local(_param["ip"][0]):
                        _token = self.local.get_default_token(self.cfg)
            if _token["time"] > time.time():
                data = {"token": {"valid": True, "new": self.encode_token(_token), "name": self.get_name(_token["user"])}}
            else:
                data = {"token": {"valid": False}}
            return {"code": 200, "data": data}
        elif path == "get_method":
            _param = parse_qs(param)
            if "ip" in _param:
                local = self.is_local(_param["ip"][0])
            out = []
            for method in self.l_login:
                if local or not(method.local):
                    out.append({"method": method.name, "label": method.label})
            return {"code": 200, "data": out}
        elif path == "login":
            _data = json.loads(data)
            _param = parse_qs(param)
            for entry in self.l_login:
                if _data["method"] == entry.name:
                    _user = entry.login(_data["user"], _data["passwd"], self.is_local(_param["ip"][0]))
                    if _user["user"] != -1:
                        token = self.encode_token({"user": _user["user"], "time": 0})
                        return {"code": 200, "data": {"allowed": True, "name": _user["name"], "token": {"valid": True, "token": token}}}
            return {"code": 200, "data": {"allowed": False}}
        elif path == "change_pw":
            _data = json.loads(data)
            _token = self.decode_token(_data["token"])
            if _token["user"] > -1:
                for entry in self.l_login:
                    if entry.name == "normal":
                        token = self.encode_token(_token)
                        if entry.change_pw(_token["user"], _data["old"], _data["new"]):
                            return {"code": 200, "data": {"changed": True, "token": {"valid": True, "token": token}}}
                        else:
                            return {"code": 200, "data": {"changed": False, "token": {"valid": True, "token": token}}}
        return {"code": 404}

    def get_name(self, id):
        self.log.debug(id)
        name = "?"
        self.db.open()
        res = self.db.fetchone("select name from user where id='" + str(id) + "'")
        if res:
            name = res[0]
        self.db.close()
        return name

    def encode(self, data):
        self.log.debug("encode")
        cipher = AES.new(self.aeskey[:16].encode(), AES.MODE_EAX)
        ciphertext, tag = cipher.encrypt_and_digest(data.encode())
        return base64.encodebytes(cipher.nonce+ciphertext).decode().replace('\n', '')

    def decode(self, data):
        self.log.debug("decode")
        try:
            ciphertext = base64.decodebytes(data.encode())
            nonce = ciphertext[:AES.block_size]
            ciphertext = ciphertext[AES.block_size:]
            cipher = AES.new(self.aeskey[:16].encode(), AES.MODE_EAX, nonce=nonce)
            text = cipher.decrypt(ciphertext)
            return json.loads(text.decode())
        except:
            return {}

    def encode_token(self, token):
        self.log.debug(token)
        token["time"] = int(time.time()+600)
        _token = json.dumps(token)
        cipher = AES.new(self.aeskey[:16].encode(), AES.MODE_EAX)
        ciphertext, tag = cipher.encrypt_and_digest(_token.encode())
        return base64.encodebytes(cipher.nonce+ciphertext).decode().replace('\n', '')

    def decode_token(self, token):
        self.log.debug(token)
        global ALIVE
        self.alive[0] = ALIVE
        try:
            ciphertext = base64.decodebytes(token.encode())
            nonce = ciphertext[:AES.block_size]
            ciphertext = ciphertext[AES.block_size:]
            cipher = AES.new(self.aeskey[:16].encode(), AES.MODE_EAX, nonce=nonce)
            text = cipher.decrypt(ciphertext)
            token = json.loads(text.decode())
            if not("user" in token) or not("time" in token) or token["time"] < time.time():
                 return {"user": -1, "time": 0}
            return token
        except:
            return {"user": -1, "time": 0}

    def get_rights(self, user, domain, modul, ip):
        self.log.debug(modul)
        local = self.is_local(ip)
        moduls = []
        domains = ['basic']
        out = {"modul": domain in domains, "local": local}
        if not(out["modul"]):
            out["modul"] = modul in moduls
        return out

    def pl_get_menus(self, user, ip):
        self.log.debug("user")
        rights = self.rights(user, ip)
        out = []
        if rights["domain"]:
            if "system" in rights["sub"]:
                out.append({'label': 'Nutzerverwaltung', 'mod': 'authentication', 'p1':'mangement'})
            elif "card" in rights["sub"]:
                out.append({'label': 'Passwort &auml;ndern', 'mod': 'authentication', 'p1':'password'})
        else:
            if self.is_local(ip):
                self.db.open()
                res = self.db.fetchone("select user from user where id='" + str(user) + "'")
                self.db.close()
                if res and res[0] == "admin":
                    out.append({'label': 'Nutzerverwaltung', 'mod': 'authentication', 'p1':'mangement'})
        return out

    def pl_data(self, user, cmd, _in, ip):
        self.log.debug(cmd)
        rights = self.rights(user, ip)
        ok = rights["domain"] and "system" in rights["sub"]
        if not ok:
            if self.is_local(ip):
                self.db.open()
                res = self.db.fetchone("select user from user where id='" + str(user) + "'")
                self.db.close()
                if res and res[0] == "admin":
                    ok = True
        if ok:
            if cmd == "get_user":
                return self.manager.get_user()
            elif cmd == "get_userdata":
                return self.manager.get_userdata(_in)
            elif cmd == "new_user":
                return self.manager.new_user(_in)
            elif cmd == "real_name":
                return self.manager.real_name(_in)
            elif cmd == "passwd":
                return self.manager.passwd(_in)
            elif cmd == "rights":
                return self.manager.rights(_in)
            elif cmd == "reset":
                return self.manager.reset(_in)
        return {"allowed": False}

    def load_secret(self, value):
        self.log.debug("load_secret")
        name = self.auth_path + "/secret.yaml"
        try:
            return config_yaml.config(name).cfg[value]
        except:
            pass
        return "NoFound"
