# smartlife
# 04.2021

import os
import database
import key_bcrypt

def run(base_path, reset):
    auth_path = base_path + "/.authentication"
    if not os.path.isdir(auth_path):
        os.mkdir(auth_path)
    key_file = auth_path + "/key.dat"
    if os.path.isfile(key_file):
        f = open(key_file, "r")
        key = f.read()
        f.close()
    else:
        key = key_bcrypt.get_new_key()
        f = open(key_file, "w")
        f.write(key)
        f.close()
    sql = "CREATE TABLE IF NOT EXISTS user (id integer, user text, name text, password text, local integer, primary key(id))"
    d = database.database(None, auth_path + "/authentication.sqlite", sql)
    d.open()
    if reset:
        sql = "delete from user where user='admin'"
        d.commit(sql)
    sql = "select id from user where user='admin'"
    if not d.fetchone(sql):
        pwd = input("admin password: ")
        hashed = key_bcrypt.encode_password(pwd)
        sql = "insert into user (user, name, password, local) values ('admin', 'Admin', '" + hashed + "', '1')"
        d.commit(sql)
    d.close()
