define(['module', 'jqxinput', 'jqxpasswordinput', 'jqxbutton', 'jqxlistbox', 'jqxwindow'], function(module) {
  var authencitator = {
    user : '',
    init : function() {
      var sub = window.module.authencitator.init_data.p1
      if (sub == 'mangement') {
        html = '<div style="width:100%; height:100%;"><div id="authencitator_user"></div><div style="'
        html += ' top:0; position:absolute; height: 100%;" id="authencitator_main"><table style="margin: 10px;">'
        html += '<tr><td><b>Neuer Nutzer:</b></td><td><input id="new_name" /></td><td></td><td><input id="send_new_name" type="button" '
        html += 'value="Nutzer anlegen" /></td></tr>'
        html += '<tr><td><b>Name:</b></td><td><input id="real_name" /></td><td></td><td><input id="send_real_name" type="button" '
        html += 'value="Nutzernamen senden" /></td></tr>'
        html += '<tr><td><b>Passwort:</b></td><td><input type="password" id="passwd" /></td><td></td><td>'
        html += '<input id="send_passwd" type="button" value="Passwort senden" /></td></tr>'
        html += '<tr><td valign="top"><b>Rechte:</b></td><td><div id="rights"></div></td><td><div id="rights2"></div></td>'
        html += '<td valign="bottom"><input id="send_rights" type="button" value="Rechte senden" /></td></tr>'
        html += '<tr><td><b>Sperren:</b></td><td></td><td></td><td><input id="send_reset" type="button" style="background:red;"'
        html += 'value="Passwort sperren" /></td></tr>'
        html += '</table></div>'
        $('#client_area').html(html+'</div>')
        $(window).off('resize', window.module.authencitator.resize_m)
        $(window).on('resize', window.module.authencitator.resize_m)
        window.module.authencitator.resize_m()
        window.smfetch('/api/authentication', {cmd: 'get_user'}).then((data) => {
          if (data.error == undefined) {
            $("#authencitator_user").jqxListBox({ source: data.data, width: 250, height: '100%' });
            $('#mainTop').off('sizeChange', window.module.authencitator.left_size_m)
            $('#mainTop').on('sizeChange', window.module.authencitator.left_size_m)
            window.module.authencitator.left_size_m()
            $("#authencitator_user").off('select')
            $("#authencitator_user").on('select',  window.module.authencitator.select_user)
            $("#authencitator_user").jqxListBox('selectIndex', 0 )
          }
        })
        $('#new_name, #real_name').jqxInput({width: 250})
        $('#passwd').jqxPasswordInput({width: 250})
        $('#rights, #rights2').jqxListBox({width: 250, height: 350, checkboxes: true})
        $('#send_new_name, #send_real_name, #send_passwd, #send_rights, #send_reset, #send_mfa_activ, #send_mfa_deactiv').jqxButton()
        $('#send_new_name, #send_real_name, #send_passwd, #send_rights, #send_reset, #send_mfa_activ, #send_mfa_deactiv').off('click')
        $('#send_new_name').on('click', function() {
          var name = $('#new_name').jqxInput('val')
          if (name.length > 5) {
            var item = $("#authencitator_user").jqxListBox('getItemByValue', name)
            if (item == undefined) {
              window.smfetch('/api/authentication', {cmd: 'new_user', data: name}).then((data) => {
                if (data.error == undefined) {
                  $("#authencitator_user").jqxListBox('addItem', name )
                  var item = $("#authencitator_user").jqxListBox('getItemByValue', name)
                  $("#authencitator_user").jqxListBox('selectItem', item )
                } else {
                  alert('Fehler beim speichern')
                }
              })
            } else {
              alert('User bereits vorhanden')
            }
          } else {
            alert('Name zu kurz')
          }
        })
        $('#send_real_name').on('click', function() {
          var item = $("#authencitator_user").jqxListBox('getSelectedItem')
          if (item != undefined) {
            var name = item.label,
                r_name = $('#real_name').jqxInput('val')
            window.smfetch('/api/authentication', {cmd: 'real_name', data: {user: name, name: r_name}}).then((data) => {
              if (data.error != undefined) {
                alert('Fehler beim speichern')
              }
            })
          } else {
            alert('Bitte Nutzer selectieren')
          }
        })
        $('#send_passwd').on('click', function() {
          var item = $("#authencitator_user").jqxListBox('getSelectedItem')
          if (item != undefined) {
            var name = item.label,
                passwd = $('#passwd').jqxPasswordInput('val')
            if (passwd == undefined) {
              passwd = ""
            }
            if (passwd.length < 6) {
              alert('Passwort zu kurz')
            } else {
              window.smfetch('/api/authentication', {cmd: 'passwd', data: {user: name, passwd: passwd}}).then((data) => {
                if (data.error != undefined) {
                  alert('Fehler beim speichern')
                }
              })
            }
          } else {
            alert('Bitte Nutzer selectieren')
          }
        })
        $('#send_rights').on('click', function() {
          var item = $("#authencitator_user").jqxListBox('getSelectedItem')
          if (item != undefined) {
            var name = item.label,
                rights = new Array,
                rights2 = new Array,
                items = $("#rights").jqxListBox('getCheckedItems')
                items2 = $("#rights2").jqxListBox('getCheckedItems')
            for (var i = 0; i<items.length; i++) {
              var right = items[i].label
              rights.push(right)
            }
            for (var i = 0; i<items2.length; i++) {
              var right2 = items2[i].label
              rights2.push(right2)
            }
            window.smfetch('/api/authentication', {cmd: 'rights', data: {user: name, rights: rights, rights2: rights2}}).then((data) => {
              if (data.error != undefined) {
                alert('Fehler beim speichern')
              }
            })
          } else {
            alert('Bitte Nutzer selectieren')
          }
        })
        $('#send_reset').on('click', function() {
          var item = $("#authencitator_user").jqxListBox('getSelectedItem')
          if (item != undefined) {
            var name = item.label
            window.smfetch('/api/authentication', {cmd: 'reset', data: name}).then((data) => {
              if (data.error != undefined) {
                alert('Fehler beim speichern')
              }
            })
          } else {
            alert('Bitte Nutzer selectieren')
          }
        })
        $('#send_mfa_activ').on('click', function() {
          var item = $("#authencitator_user").jqxListBox('getSelectedItem')
          if (item != undefined) {
            var name = item.label
            window.smcall({'client': 'authencitator.master', 'cmd':'mfa_activ', 'data': {'user':name}}, function(data) {
              if ($("#authencitator_mfa").length > 0) {
                $("#authencitator_mfa").remove()
              }
              html = '<div id="authencitator_mfa"><div><span>MFA-Code</span></div><div><img src="/lib/img/'+data.data.data+'">'
              html += '<div style="position: absolute;right: 10px;bottom: 10px;"><input type="button" value="Cancel" id="cancelButton" /></div>'
              html += '</div></div>'
              $('body').append(html)
              $('#authencitator_mfa').jqxWindow({width: 550, height: 550, resizable: false, isModal: true})
              $('#authencitator_mfa input').jqxButton()
              $('#authencitator_mfa input').off('click')
              $('#authencitator_mfa input').on('click', function() {
                $('#authencitator_mfa').jqxWindow('close')
                $("#authencitator_mfa").remove()
              })
              $("#authencitator_mfa").off('close')
              $('#authencitator_mfa').on('close', function () {
                $("#authencitator_mfa").remove()
              })
            })
          } else {
            alert('Bitte Nutzer selectieren')
          }
        })
        $('#send_mfa_deactiv').on('click', function() {
          var item = $("#authencitator_user").jqxListBox('getSelectedItem')
          if (item != undefined) {
            var name = item.label
            window.smcall({'client': 'authencitator.master', 'cmd':'mfa_deactiv', 'data': {'user':name}}, function(data) {
              if ($("#authencitator_mfa").length > 0) {
                $("#authencitator_mfa").remove()
              }
              if (!data.data.ok) {
                alert('Fehler beim speichern')
              }
            })
          } else {
            alert('Bitte Nutzer selectieren')
          }
        })
      }
      if (sub == 'password') {
        html = '<div style="width:100%; display: grid; place-items: center;">'
        html += '<table><tr><td span="5"><b>Neues Passwort setzen</b></td></tr>'
        html += '<tr><td>aktuelles Passwort</td><td><input type="password" id="old_passwd"/></td></tr>'
        html += '<tr><td>neues Passwort</td><td><input type="password" id="new_passwd"/></td></tr>'
        html += '<tr><td>wiederholen</td><td><input type="password" id="repeat_passwd"/></td></tr>'
        html += '<tr><tr><td> </td><td> </td></tr>'
        html += '<tr><td></td><td><input style="float:right" id="send_button" type="button" value="Senden" /></td></tr></table>'
        $('#client_area').html(html+'</div>')
        $('#old_passwd, #new_passwd, #repeat_passwd').jqxPasswordInput()
        $('#send_button').jqxButton({disabled:true})
        $('#new_passwd, #repeat_passwd').off('change')
        $('#new_passwd, #repeat_passwd').on('change', function() {
          var one = $('#new_passwd').jqxPasswordInput('val'),
              two = $('#repeat_passwd').jqxPasswordInput('val')
          if (one.length > 5 && one == two) {
            $('#send_button').jqxButton({disabled:false})
          } else {
            $('#send_button').jqxButton({disabled:true})
          }
        })
        $('#send_button').off('click')
        $('#send_button').on('click', function() {
          var one = $('#new_passwd').jqxPasswordInput('val'),
              two = $('#repeat_passwd').jqxPasswordInput('val')
          if (one.length > 5 && one == two) {
            $('#send_button').jqxButton({disabled:true})
            window.smfetch('/authentication/change_pw', {token: sessionStorage.getItem("s_token"), 
              old: $('#old_passwd').val(), new: one}).then((data) => {
              if (data.error == undefined) {
                if (data.changed) {
                  alert('Passwort geändert.')
                } else {
                  alert('Es ist ein Fehler aufgetreten.')
                } 
              }
            })
          } else {
            alert('Neues Passwort zu kurz oder keine Übereinstimmung')
          }
        })
      }
    },
    resize_m : function() {
      $('#client_area').children().height($('#client_area').height())
    },
    stop : function() {
    },
    left_size_m: function() {
      var w = $('#mainTop > .leftMenu').width()
      if ($('#mainTop').hasClass('leftMenuSmall')) {
        $("#authencitator_user").hide()
        $('#authencitator_main').css('width', '100%')
        $('#authencitator_main').css('left', '0')
      } else {
        $("#authencitator_user").show()
        $("#authencitator_user").jqxListBox('width', w)
        $('#authencitator_main').css('width', 'calc( 100% - '+w+'px )')
        $('#authencitator_main').css('left', w+'px')
      }
    },
    select_user : function() {
      window.module.authencitator.user = $("#authencitator_user").jqxListBox('getSelectedItem').label
      window.smfetch('/api/authentication', {cmd: 'get_userdata', data: window.module.authencitator.user}).then((data) => {
        if (data.error == undefined) {
          $('#real_name').jqxInput('val', data.data.name)
          $('#passwd').val("")
          $('#rights').jqxListBox('clear')
          for (var i = 0; i < data.data.avaible.length; i++) {
            $('#rights').jqxListBox('addItem', data.data.avaible[i])
          }
          for (var i = 0; i < data.data.enabled.length; i++) {
            var right =  data.data.enabled[i]
            var item = $("#rights").jqxListBox('getItemByValue', right)
            if (item != undefined) {
              $("#rights").jqxListBox('checkIndex', item.index);
            }
          }
          $('#rights2').jqxListBox('clear')
          $('#rights2').jqxListBox('addItem', 'card')
          $('#rights2').jqxListBox('addItem', 'read')
          $('#rights2').jqxListBox('addItem', 'write')
          $('#rights2').jqxListBox('addItem', 'upload')
          $('#rights2').jqxListBox('addItem', 'sm_level_1')
          $('#rights2').jqxListBox('addItem', 'sm_level_2')
          $('#rights2').jqxListBox('addItem', 'download')
          $('#rights2').jqxListBox('addItem', 'system')
          for (var i = 0; i < data.data.sub.length; i++) {
            var right =  data.data.sub[i]
            var item = $("#rights2").jqxListBox('getItemByValue', right)
            if (item != undefined) {
              $("#rights2").jqxListBox('checkIndex', item.index);
            }
          }
        }
      })
    }
  }
  authencitator['init_data'] = window.module_const[module.id]
  window.module.authencitator = authencitator
  return authencitator
})
