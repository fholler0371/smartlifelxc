# smartlife
# 04.2021

import key_bcrypt
 
class manager:
    def __init__(self, log, db, cfg):
        self.log = log
        self.log.debug("manager")
        self.db = db
        self.cfg = cfg
        self.db.open()
        sql = "CREATE TABLE IF NOT EXISTS rights (user integer, right text)"
        self.db.execute(sql)
        sql = "CREATE TABLE IF NOT EXISTS rights2 (user integer, right text)"
        self.db.commit(sql)
        self.db.close()

    def get_user(self):
        self.db.open()
        out = []
        sql = "select user from user order by user"
        for entry in self.db.fetchall(sql):
            out.append(entry[0])
        self.db.close()
        return {"allowed": True, "data": out}

    def get_userdata(self, user):
        self.db.open()
        sql = "select id, name from user where user='%s'"
        row = self.db.fetchone(sql % (user, ))
        if row:
            user_id = row[0]
            name = row[1]
            avaible = []
            for host in self.cfg["hosts"]:
                for plugin in host["plugins"]:
                    if "frontend" in plugin and plugin["frontend"]:
                        domain = plugin["domain"]
                        if not(domain in avaible):
                            avaible.append(domain)
                            avaible.append(domain + "_local")
            enabled = []
            for right in self.db.fetchall("select right from rights where user='%s'" % (str(user_id), )):
                enabled.append(right[0])
            sub = []
            for right in self.db.fetchall("select right from rights2 where user='%s'" % (str(user_id), )):
                sub.append(right[0])
            self.db.close()
            return {"allowed": True, "data": {"name": name, "avaible": avaible, "enabled": enabled, "sub": sub}}
        self.db.close()
        return {"allowed": False}

    def new_user(self, user):
        self.db.open()
        sql = "select id from user where user='%s'"
        row = self.db.fetchone(sql % (user, ))
        if row and row[0]:
            self.db.close()
            return {"allowed": False}
        sql = "insert into user (user, name, password, local) values ('%s', '%s', '', '1')"
        self.db.commit(sql % (user, user, ))
        self.db.close()
        return {"allowed": True, "data": {}}

    def real_name(self, data):
        self.db.open()
        sql = "update user set name='%s' where user='%s'"
        self.db.commit(sql % (data["name"], data["user"], ))
        self.db.close()
        return {"allowed": True, "data": {}}

    def passwd(self, data):
        passwd = key_bcrypt.encode_password(data["passwd"])
        self.db.open()
        sql = "update user set password='%s' where user='%s'"
        self.db.commit(sql % (passwd, data["user"], ))
        self.db.close()
        return {"allowed": True, "data": {}}

    def rights(self, data):
        self.db.open()
        sql = "select id from user where user='%s'"
        row = self.db.fetchone(sql % (data["user"], ))
        if row:
            user_id = row[0]
            sql = "delete from rights where user='%s'"
            self.db.commit(sql % (str(user_id), ))
            sql = "delete from rights2 where user='%s'"
            self.db.commit(sql % (str(user_id), ))
            sql = "insert into rights (user, right) values ('%s', '%s')"
            for right in data["rights"]:
                self.db.execute(sql % (str(user_id), right, ))
            sql = "insert into rights2 (user, right) values ('%s', '%s')"
            for right in data["rights2"]:
                self.db.execute(sql % (str(user_id), right, ))
            self.db.commit()
            self.db.close()
            return {"allowed": True, "data": {}}
        self.db.close()
        return {"allowed": False}

    def reset(self, user):
        self.db.open()
        sql = "update user set password='' where user='%s'"
        self.db.commit(sql % (user, ))
        self.db.close()
        return {"allowed": True, "data": {}}
