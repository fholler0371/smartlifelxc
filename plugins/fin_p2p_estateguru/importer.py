# smartlife
# 09.2021

import time 
from datetime import datetime

from import_csv import csv

class importer:
    def __init__(self, log, smartlife, db):
        log.debug("init")
        self.log = log
        self.smartlife = smartlife
        self.db = db
        self.is_loading = False

    def upload_prefetch(self, data):
        self.log.debug("prefetch")
        ok = True
        if not ("csv" == data["name"].split(".")[-1].lower()):
            ok = False
        if data["size"] > 100000:
            ok = False
        if ok:
            _data = {"mode": "stream", "modul": "fin_p2p_estateguru", "func": "uploaded_import", "valid": int(time.time()+300)}
            netcom = self.smartlife.get_plugin("netcom")
            _data = netcom.encode_data(_data)
            if _data == "":
                return {"allowed": True, "data": {"ok": False}}
            return {"allowed": True, "data": {"ok": True, "data": _data}}
        else:
            return {"allowed": True, "data": {"ok": False}}

    def load_data(self, data):
        self.log.debug("load_data")
        while self.is_loading:
            time.sleep(0.25)
        self.is_loading = True
        try:
            tab = csv(data.decode(), tab=',', floatformat='en')
            if tab.count > 0 and tab.get_text(0, 'Lening Code') != "":
                i = 0
                while i < tab.count:
                    _code = tab.get_text(i, 'Lening Code')
                    if _code != '':
                        _name = tab.get_text(i, 'Projektname')
                        _country = tab.get_text(i, 'Land')
                        _date = tab.get_date(i, 'Fnanzierungsdatum')
                        _interest = tab.get_float(i, 'Zinssatz')
                        _months = int(tab.get_float(i, 'Anfängliche Kreditlaufzeit'))
                        _start_value = tab.get_float(i, 'Anfänglicher Hauptbetrag')
                        _state = tab.get_text(i, 'Status')
                        _paid_value = tab.get_float(i, 'Zurückgezahlter Hauptbetrag')
                        _days = int(tab.get_float(i, 'Tage verspätet'))
                        self.db.add_credit(_code, _name, _country, _date, _interest, _months, _start_value, _state, _paid_value, _days)
                    i += 1
            else:
                d = datetime.today().strftime('%Y-%m-%d')
                i = 0
                while i < tab.count:
                    _state = tab.get_text(i, 'Cash Flow Status')
                    if _state == "Approved":
                        _date = tab.get_date(i, 'Confirmation Date')
                        if _date <= d:
                            _eid = tab.get_text(i, 'ID')
                            _code = tab.get_text(i, 'Loan Code')
                            _type = tab.get_text(i, 'Cash Flow Type')
                            _value = tab.get_float(i, 'Amount')
                            self.db.add(_eid, _code, _date, _type, _value)
                    i += 1
                self.db.do_calc()
        except Exception as e:
            self.log.error(repr(e))
            self.is_loading = False
            return False
        self.is_loading = False
        return True
