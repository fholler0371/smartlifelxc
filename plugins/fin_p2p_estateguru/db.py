# smartlife
# 09.2021

from threading import Thread, Timer
from datetime import datetime, timedelta
import time
import random
import urllib
import urllib.request
import urllib.parse


from database import database

class history(Thread):
    def __init__(self, log, name, book, master):
        Thread.__init__(self)
        log.debug("init")
        self.log = log
        self.master = master
        sql = "CREATE TABLE IF NOT EXISTS history (date string, value numeric, interest numeric)"
        self.hist = database(self.log, name, sql)
        self.book = database(self.log, book)

    def run(self):
        print("run")
        self.log.debug("run")
        self.hist.open()
        self.book.open()
        self.abschreibung()
        d = datetime.strptime("2019-07-24", "%Y-%m-%d")
        row = self.hist.fetchone("select date from history order by date desc limit 1")
        if row:
            d = datetime.strptime(row[0], "%Y-%m-%d")
        d = d - timedelta(days=31)
        if d.strftime("%Y-%m-%d") < "2019-07-24":
            d = datetime.strptime("2019-07-24", "%Y-%m-%d")
        maxDate = datetime.today()
        l = 0
        sql1 = "select date from history where date = '%s'"
        sql2 = "update history set value='%s', interest='%s' where date='%s'"
        sql3 = "insert into history (value, interest, date) values ('%s', '%s', '%s')"
        sql4 = "select sum(value) from bookings where type='Deposit' and date<='%s'"
        sql5 = "select sum(value) from bookings where (type='Indemnity' or type='Interest') and `date`<='%s'"
        sql6 = "select value from abschreibung where date<='%s' limit 1"
        while l < 365 and (maxDate.strftime("%Y-%m-%d") >= d.strftime("%Y-%m-%d")):
            invest = 0
            row = self.book.fetchone(sql4 % (d.strftime("%Y-%m-%d")))
            if row and row[0]:
                invest = float(row[0])
            row = self.book.fetchone(sql5 % (d.strftime("%Y-%m-%d")))
            zins = 0.0
            if row and row[0]:
                zins = float(row[0])
            row = self.book.fetchone(sql6 % (d.strftime("%Y-%m-%d")))
            if row and row[0]:
                zins = zins - float(row[0])
            if self.hist.fetchone(sql1  % (d.strftime('%Y-%m-%d'), )):
                sql = sql2
            else:
                sql = sql3
            self.hist.execute(sql % (str(invest), str(zins), d.strftime('%Y-%m-%d')))
            d = d + timedelta(days=1)
            l += 1
        self.hist.commit()
        self.book.close()
        self.hist.close()
        if self.master:
            self.master.calc_hist()

    def abschreibung(self):
        d = datetime.now()
        overdue = 0
        row = self.book.fetchone("select sum((start_value-paid_value)/3*1) from credits where not(state='Zurückgezahlt') and days>90 and days<180")
        if row and row[0]:
            overdue = row[0]
        row = self.book.fetchone("select sum((start_value-paid_value)/3*2) from credits where not(state='Zurückgezahlt') and days>180 and days<365")
        if row and row[0]:
            overdue += row[0]
        row = self.book.fetchone("select sum(start_value-paid_value) from credits where not(state='Zurückgezahlt') and days>365")
        if row and row[0]:
            overdue += row[0]
        row = self.book.fetchone("select date from abschreibung where date='%s'" % (d.strftime("%Y-%m-%d")))
        print(d.strftime("%Y-%m-%d"), overdue)
        if row:
            self.book.commit("update `abschreibung` set `value`='%s' where `date`='%s'" % (str(overdue), d.strftime("%Y-%m-%d")))
        else:
            self.book.commit("insert into `abschreibung` (`date`, `value`) values ('%s', '%s')" % (d.strftime("%Y-%m-%d"), str(overdue)))

class db:
    def __init__(self, log, smartlife, master):#, remote):
        log.debug("init")
        self.log = log
        self.smartlife = smartlife
        self.master = master
#        self.remote = remote
        sql = "CREATE TABLE IF NOT EXISTS bookings (id INTEGER PRIMARY KEY AUTOINCREMENT, eid TEXT, loan TEXT, date date, type TEXT, value NUMERIC)"
        self.book = database(self.log, smartlife.db_path + "/fin_p2p_estateguru.sqlite", sql)
        self.hist = database(self.log, smartlife.db_path + "/fin_p2p_estateguru_history.sqlite")
        t = (int(time.time()/86400)+1)*86400-time.time()+60+300*random.random()
        self.timer_calc = Timer(t, self.tick_calc)
        self.timer_calc.start()

    def tick_calc(self):
        self.log.debug("tick_calc")
        t = (int(time.time()/86400)+1)*86400-time.time()+60+300*random.random()
        self.timer_calc = Timer(t, self.tick_calc)
        self.timer_calc.start()
        self.do_calc()

    def get_card(self):
        self.log.debug("get_card")
        out = {"invest": 0, "sum" : 0}
        self.hist.open()
        row = self.hist.fetchone("select value, interest from history order by date desc limit 1")
        if row:
            out['invest'] = float("%.2f" % round(row[0], 2))
            out['sum'] = float("%.2f" % round(row[1], 2)) + out['invest']
        self.hist.close()
        return {"base": out}

    def get_book(self, data):
        self.log.debug(data)
        out = {"records": 0, "rows": []}
        self.book.open()
        row = self.book.fetchone("select count(id) from bookings")
        if row:
            out["records"] = row[0]
        sql = "select id, date, loan, type, value from bookings order by date desc limit %s, %s"
        for row in self.book.fetchall(sql % (str(data["page"]*data['pagesize']), str(data['pagesize']), )):
            out["rows"].append({'id': row[0], 'date': row[1], 'loan': row[2], 'type': row[3], 'amount': row[4]})
        self.book.close()
        return {"allowed": True, "data": out}

    def get_credit(self, data):
        self.log.debug(data)
        out = {"records": 0, "rows": []}
        self.book.open()
        row = self.book.fetchone("select count(id) from credits")
        if row:
            out["records"] = row[0]
        sql = "select id, date, code, name, country, interest, state, (start_value-paid_value) from credits order by date desc limit %s, %s"
        for row in self.book.fetchall(sql % (str(data["page"]*data['pagesize']), str(data['pagesize']), )):
            out["rows"].append({'id': row[0], 'date': row[1], 'code': row[2], 'name': row[3], 'country': row[4],  'interest': row[5],
                                'state': row[6], 'amount': row[7]})
        self.book.close()
        return {"allowed": True, "data": out}

    def add(self, eid, code, date, type, value):
        self.log.debug(date)
        self.book.open()
        sql = "select id from bookings where eid='%s'"
        row = self.book.fetchone(sql % (eid, ))
        if row == None:
            sql = "insert into bookings (eid, loan, date, type, value) values ('%s', '%s', '%s', '%s', '%s' ) "
            self.book.commit(sql % (eid, code, date, type, str(value), ))
        else:
            sql = "update bookings set date='%s', value='%s' where id='%s'"
            self.book.commit(sql % (date, str(value), str(row[0]) ))
        self.book.close()

    def add_credit(self, code, name, country, date, interest, months, start_value, state, paid_value, days):
        self.log.debug(code)
        self.book.open()
        sql = "select id from credits where code='%s'"
        row = self.book.fetchone(sql % (code, ))
        if row == None:
            sql = "insert into credits (code, name, country, date, interest, months, start_value, state, paid_value, days) values "
            sql += "('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')"
            self.book.commit(sql % (code, name, country, date, str(interest), str(months), str(start_value), state,
                                    str(paid_value), str(days)))
        else:
            sql = "update credits set state='%s', paid_value='%s', days='%s' where id='%s'"
            self.book.commit(sql % (state, str(paid_value), str(days), str(row[0])))
        self.book.close()


    def do_calc(self):
        self.log.debug("do_calc")
        th = history(self.log, self.smartlife.db_path + "/fin_p2p_estateguru_history.sqlite", 
            self.smartlife.db_path + "/fin_p2p_estateguru.sqlite", self.smartlife.get_plugin(self.master))
        th.start()
        return {"allowed": True, "data": {}}

    def get_history(self):
        self.log.debug("get_history")
        out = []
        self.hist.open()
        for entry in self.hist.fetchall("select date, value, interest from history order by date asc"):
            out.append({'date': entry[0], 'value': entry[1], 'interest': entry[2]})
        self.hist.close()
        return {"allowed": True, "data": out}

    def stop(self):
        self.log.debug("stop")
        if self.timer_calc:
            self.timer_calc.cancel()
            