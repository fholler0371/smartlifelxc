# smartlife
# 06.2021

from threading import Thread, Timer
from datetime import datetime, timedelta, date
import time
import random

from database import database

class history(Thread):
    def __init__(self, log, name, book, master, start):
        Thread.__init__(self)
        log.debug("init")
        self.log = log
        self.master = master
        sql = "CREATE TABLE IF NOT EXISTS history (date string, value numeric, interest numeric)"
        self.hist = database(self.log, name, sql)
        self.book = database(self.log, book)
        self.d_start = start

    def calc_month_interest(self, d1, d2, v1, v2, f):
        d1 = datetime.strptime(d1, "%Y-%m-%d")
        d2 = datetime.strptime(d2, "%Y-%m-%d")
        monate = (d2.year-d1.year)*12+d2.month-d1.month
        z_min = -100
        z_max = 100
        x = 0
        while int(float(v2)*10000) != int(x*10000):
            x = float(v1)
            for i in range(monate):
                z = (z_max - z_min) / 2 + z_min
                x = x * z + f
            if x < v2:
                z_min = z
            else:
                z_max = z
        return z

    def calc_interest(self):
        d = datetime.today()
        self.book.commit("update `fixed` set `changed`='0'")
        fix_list = self.book.fetchall("SELECT `id`, `date` FROM `fixed` ORDER BY `date`")
        fix_length = len(fix_list)
        fix_length = fix_length - 1
        sql = "SELECT date, value, interest, id, rate FROM `fixed` WHERE `date` >= '%s' ORDER BY `date` LIMIT 2"
        for l1 in range(fix_length):
            fix_entry = fix_list[l1]
            entries = self.book.fetchall(sql % (fix_entry[1]))
            z = self.calc_month_interest(entries[0][0], entries[1][0], entries[0][1], entries[1][1], entries[0][4])
            if float(entries[0][2]) != z:
                self.book.commit("update fixed set interest='%s', changed='1' where id='%s'" % (str(z), str(fix_entry[0])))

    def run(self):
        self.log.debug("run")
        self.hist.open()
        self.book.open()
        self.calc_interest()
        d = self.d_start
        d_max = datetime.today()
        res = self.book.fetchone("SELECT value, interest, rate FROM fixed ORDER BY date LIMIT 1")
        vi = res[0]
        zi = 0
        i = res[1]-1
        res = self.book.fetchone("SELECT date, rate FROM fixed WHERE date > '%s' ORDER BY date LIMIT 1" % (d.strftime("%Y-%m-%d")))
        vm = res[1]
        d_valid = datetime.strptime(res[0], "%Y-%m-%d")
        self.hist.commit('delete from history')
        sql1 = "select value from history where date='%s'"
        sql2 = "insert into history (date, value, interest) values ('%s', '%s', '%s')"
        sql3 = "SELECT value, interest, rate FROM fixed where date='%s'"
        sql4 = "SELECT date FROM fixed WHERE date > '%s' ORDER BY date LIMIT 1"
        while d <= d_max.date():
            if d.day == 1:
                if d == self.d_start:
                    zi += vi * i
                else: #logisches Ende
                    d2 = d
                    d2 = d2 - timedelta(days=1)
                    d2.replace(day=1)
                    try:
                        res = self.hist.fetchone(sql1 % (d2.strftime("%Y-%m-%d")))
                        zi += (res[0]+zi)*i
                    except:
                        zi += zi*i
                vi += vm
                if d == d_valid.date():
                    res = self.book.fetchone(sql3 % d.strftime("%Y-%m-%d"))  
                    vi = res[0]-zi
                    i = res[1]-1
                    vm = res[2]
                    res = self.book.fetchone(sql4 % d.strftime("%Y-%m-%d"))
                    d_valid = datetime.strptime(res[0], "%Y-%m-%d")
            self.hist.commit(sql2 % (d.strftime("%Y-%m-%d"), str(vi), str(zi)))                         
            d = d + timedelta(days=1)
        self.hist.commit()
        self.book.close()
        self.hist.close()
        if self.master:
            self.master.calc_hist()

class db:
    def __init__(self, log, smartlife, master, start):
        log.debug("init")
        self.log = log
        self.smartlife = smartlife
        self.master = master
        sql = "CREATE TABLE IF NOT EXISTS fixed (id INTEGER, date date, "
        sql += "value numeric, interest numeric, changed integer, rate numeric, PRIMARY KEY(id))"
        self.book = database(self.log, smartlife.db_path + "/fin_lv_zurich_2.sqlite", sql)
        self.hist = database(self.log, smartlife.db_path + "/fin_lv_zurich_2_history.sqlite")
        t = (int(time.time()/86400)+1)*86400-time.time()+60+300*random.random()
        self.timer_calc = Timer(t, self.tick_calc)
        self.timer_calc.start()
        self.start = start
        self.th = None

    def tick_calc(self):
        self.log.debug("tick_calc")
        t = (int(time.time()/86400)+1)*86400-time.time()+60+300*random.random()
        self.timer_calc = Timer(t, self.tick_calc)
        self.timer_calc.start()
        self.do_calc()

    def get_card(self):
        self.log.debug("get_card")
        out = {"sum": {"invest": 0, "sum": 0}}
        self.hist.open()
        row = self.hist.fetchone("select value, interest from history order by date desc limit 1") 
        out['sum']['invest'] = float("%.2f" % round(row[0], 2))
        out['sum']['sum'] = float("%.2f" % round(row[0]+row[1], 2))
        self.hist.close()
        return out

    def get_book(self, data):
        self.log.debug(data)
        out = {"records": 0, "rows": []}
        self.book.open()
        row = self.book.fetchone("select count(id) from fixed")
        if row:
            out["records"] = row[0]
        sql = "select id, date, value, rate from fixed order by date desc limit %s, %s"
        for row in self.book.fetchall(sql % (str(data["page"]*data['pagesize']), str(data['pagesize']), )):
            out["rows"].append({'id': row[0], 'date': row[1], 'amount': row[2], 'rate': row[3]})
        self.book.close()
        return {"allowed": True, "data": out}

    def do_calc(self):
        self.log.debug("do_calc")
        if not(self.th) or not( self.th.is_alive()):
            self.th = history(self.log, self.smartlife.db_path + "/fin_lv_zurich_2_history.sqlite", 
                self.smartlife.db_path + "/fin_lv_zurich_2.sqlite", self.smartlife.get_plugin(self.master), self.start)
            self.th.start()
        return {"allowed": True, "data": {}}

    def get_history(self):
        self.log.debug("get_history")
        out = []
        self.hist.open()
        for entry in self.hist.fetchall("select date, value, interest from history order by date asc"):
            out.append({'date': entry[0], 'value': entry[1], 'interest': entry[2]})
        self.hist.close()
        return {"allowed": True, "data": out}

    def stop(self):
        self.log.debug("stop")
        if self.timer_calc:
            self.timer_calc.cancel()
            