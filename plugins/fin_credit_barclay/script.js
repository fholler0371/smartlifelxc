define(['packery', 'highcharts', 'moment_tz', 'jqxtabs', 'jqxdata', 'jqxgrid', 'jqxgrid_pager', 'jqxgrid_selection',
        'jqxdropdownlist', 'jqxdatetimeinput', 'jqxnumberinput', 'jqxbutton'], function(Packery, H, moment) {
  window.moment = moment
  H.setOptions({
    global: {
      timezone: "Europe/Berlin"
    },
    lang : {
      months : ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
      shortMonths : ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
      weekdays : ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa']
    } 
  })
  var mod = {
    wdata : {},
    wdata_last : 0,
    wdata_call : 0,
    t_update_sum: null,
    t_update_interrest: null,
    init: function() {
      if ($('#fin_credit_barclay_tab').length == 0) {
        html = '<div id="fin_credit_barclay_tab"><ul><li>Status</li><li>Buchungen</li><li>Verlauf</li></ul><div id="fin_credit_barclay_cards"></div>'
        html += '<div id="fin_credit_barclay_book"></div><div id="fin_credit_barclay_history"></div></div>'
        $('#finanical_left').html(html)
        $('#fin_credit_barclay_tab').jqxTabs({height: '100%', width: '100%'})
        $('#fin_credit_barclay_tab').off('selected')
        $('#fin_credit_barclay_tab').on('selected', function(ev) {
          var id = ev.args.item
          window.module.fin_credit_barclay.call_tab(id)
        })
        window.module.fin_credit_barclay.call_tab(0)
      }
    },
    check_data : function() {
      var now = new Date()
      var t = Math.round(now.getTime() / 1000)
      if (t - window.module.fin_credit_barclay.wdata_last > 300) {
        if (t - window.module.fin_credit_barclay.wdata_call > 10) {
          window.module.fin_credit_barclay.wdata_call = t
          window.smfetch('/api/fin_credit_barclay', {cmd: 'get_card'}).then((data) => {
            if (data.error == undefined) {
              window.module.fin_credit_barclay.wdata_last = t
              window.module.fin_credit_barclay.wdata = data.data
            }
          })
        }
      }
    },
    update_sum: function() {
      clearTimeout(window.module.fin_credit_barclay.t_update_sum)
      window.module.fin_credit_barclay.check_data()
      var sum = window.module.fin_credit_barclay.wdata.sum
      if (sum != undefined) {
        var ele = $($("body").find('.cards_fin_credit_barclay_sum')[0])
        var old = ele.data('last')
        if (old != sum) {
          ele.data('last', sum)
          html  = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">Basisdaten</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 35px;"><span  style="font-weight: 600;">Summe:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 35px;">'
          html += sum.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          ele.html(html)
        }
        window.module.fin_credit_barclay.t_update_sum = setTimeout(window.module.fin_credit_barclay.update_sum, 600000)
      } else {
        window.module.fin_credit_barclay.t_update_sum = setTimeout(window.module.fin_credit_barclay.update_sum, 250)
      }
    },
    update_interrest: function() {
      clearTimeout(window.module.fin_credit_barclay.t_update_interrest)
      window.module.fin_credit_barclay.check_data()
      var d = window.module.fin_credit_barclay.wdata.interrest
      if (d != undefined) {
        var ele = $($("body").find('.cards_fin_credit_barclay_interrest')[0])
        var old = ele.data('last')
        if (old != d) {
          ele.data('last', d)
          html  = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">Monat</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 25px;"><span  style="font-weight: 600;">Monat:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 25px;">'
          html += d.month.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 45px;"><span  style="font-weight: 600;">Jahr:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 45px;">'
          html += d.year.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          ele.html(html)
        }
        window.module.fin_credit_barclay.t_update_month = setTimeout(window.module.fin_credit_barclay.update_interrest, 600000)
      } else {
        window.module.fin_credit_barclay.t_update_month = setTimeout(window.module.fin_credit_barclay.update_interrest, 250)
      }
    },
    get_card : function(data, div) {
      if (data["card"] == "fin_credit_barclay_sum") {
        if (div.find(".cards_fin_credit_barclay_sum").length == 0) {
          var html = '<div class="cards_fin_credit_barclay_sum card"><span></span></div>'
          div.append(html)
        } else {
          console.log(data)
        }
        window.module.fin_credit_barclay.update_sum()
      } else if (data["card"] == "fin_credit_barclay_interrest") {
        if (div.find(".cards_fin_credit_barclay_interrest").length == 0) {
          var html = '<div class="cards_fin_credit_barclay_interrest card"><span></span></div>'
          div.append(html)
        } else {
          console.log(data)
        }
        window.module.fin_credit_barclay.update_interrest()
      }
    },
    create_cards: function() {
      var ok = 1
      var cards = window.module.fin_credit_barclay.cards
      var l = cards.length
      for (var i=0; i<l; i++) {
         if (!(typeof(window.module[cards[i].mod].get_card) === 'function')) {
           console.log("Modul not found: "+cards[i].mod)
           ok = 0
         }
      }
      if (ok == 1) {
        for (var i=0; i<l; i++) {
          window.module[cards[i].mod].get_card(cards[i], $("#fin_credit_barclay_cards"))
          require( [ 'jquery-bridget/jquery-bridget' ], function( jQueryBridget ) {
            jQueryBridget( 'packery', Packery, $ );
            var grid = $('#fin_credit_barclay_cards').packery({
              itemSelector: '.card',
              gutter: 10
            })
          })
        }
      } else {
        console.log("recheck")
      }
    },
    call_tab: function(id) {
      if (id == 0) {
        window.smfetch('/api/fin_credit_barclay', {cmd: 'get_cards'}).then((data) => {
          if (data.error == undefined) {
            window.module.fin_credit_barclay.cards = data.data
            window.module.fin_credit_barclay.cards_check_mod = 10
            window.module.fin_credit_barclay.create_cards()
          }
        })
      } else if (id == 1) {
        if ($('#fin_credit_barclay_bookings').length == 0) {
          var source = {
            datatype: 'json',
            loadServerData: window.module.fin_credit_barclay.load_table,
              datafields: [
                { name: 'id' },
                { name: 'date', type: 'date' },
                { name: 'text' },
                { name: 'amount', type: 'float' }
              ],
              id: 'id',
              pagesize: 20,
              url: "http://example.com"
          }
          var dataAdapter = new $.jqx.dataAdapter(source);
          $('#fin_credit_barclay_book').append("<div id='fin_credit_barclay_bookings'></div>");
          $("#fin_credit_barclay_bookings").jqxGrid({
            width:'calc( 100% - 3px )',
            height:'calc( 100% - 5px )',
            localization: {
              currencysymbol: " €",
              currencysymbolposition: "after",
              decimalseparator: ",",
              thousandsseparator: ".",
              pagergotopagestring: "Gehe zu:",
              pagershowrowsstring: "Zeige Zeile:",
              pagerrangestring: " von ",
              pagernextbuttonstring: "voriger",
              pagerpreviousbuttonstring: "nächster"
            },
            source: dataAdapter,
            pageable: true,
            pagesizeoptions: ['10', '20', '30', '50', '100'],
            virtualmode: true,
            rendergridrows: function(obj){
              return obj.data;
            },
            columns : [
              {text:'id', datafield:'id', hidden: true},
              {text:'Datum', datafield:'date', width:100,  columntype: 'datetimeinput', cellsformat: 'dd.MM.yyyy'},
              {text:'Text', datafield:'text'},
              {text:'Betrag', datafield:'amount', width:100, cellsalign: 'right', cellsformat: 'c2', columntype:'numberinput'}
            ]
          })
        }
      } else if (id == 2) {
        if ($('#fin_credit_barclay_chart').length == 0) {
            $('#fin_credit_barclay_history').append("<div id='fin_credit_barclay_chart' style='height: 100%; width: 100%'></div>")
            $('#fin_credit_barclay_history').css('overflow', 'hidden')
            window.smfetch('/api/fin_credit_barclay', {cmd: 'get_history'}).then((data) => {
              if (data.error == undefined) {
                d = data.data
                var series = []
                series.push({name: 'Betrag', data: [], color: '#0066FF', marker: {enabled: false}, lineWidth: 4})
                var len = d.length
                for (var i=0; i<len; i++) {
                  var t = new Date(d[i].date)
                  series[0].data.push([t.getTime(), d[i].value])
                }
                var settings = {
                  chart: { type: 'areaspline' },
                  title : { text: '' },
                  subtitle : { text: '' },
                  yAxis : { title: {text: 'Euro' }},
                  xAxis: {
                    type: 'datetime',
                    title: { text: '' }
                  },
                  series: series
                }
                H.chart('fin_credit_barclay_chart', settings)
              }
            })
        }
      }
    },
    load_table: function(data, source, callback) {
      window.smfetch('/api/fin_credit_barclay', {cmd: 'get_book', data: {'page': data.pagenum, 'pagesize': data.pagesize}}).then((data) => {
        if (data.error == undefined) {
          for (var i=0; i<data.data.rows.length; i++) {
            data.data.rows[i].date = new Date(data.data.rows[i].date)
          }
          callback({ records:data.data.rows, totalrecords : data.data.records})
        }
      })
    },
    stop: function() {
    }
  }
  window.module.fin_credit_barclay = mod
  return mod
})
