# smartlife
# 05.2021

import json

from plugins2 import plugin_base

from fin_credit_ikano.cards import cards
from fin_credit_ikano.db import db

class integration(plugin_base):
    def __init__(self, smartlife, modul):
        global ALIVE_WWW
        plugin_base.__init__(self, smartlife, modul)
        self.alive = [999999999]
        self.db = db(self.log, smartlife, self.cfg["master"])
        self.cards = cards(self.log, self.db)

    def pl_data(self, user, cmd, _in, ip):
        self.log.debug(cmd)
        rights = self.rights(user, ip)
        if rights["domain"]:
            if "card" in rights["sub"] or "read" in rights["sub"]:
                if cmd == "get_cards":
                    return self.cards.get_cards()
                elif cmd == "get_card":
                    return self.cards.get_card()
            if "read" in rights["sub"]:
                if cmd == "get_book":
                    return self.db.get_book(_in)
                elif cmd == "get_history":
                    return self.db.get_history()
        return {"allowed": False}

    def stop(self):
        self.log.info(self.name)
        self.running = False
        self.db.stop()