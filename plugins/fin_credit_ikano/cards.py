# smartlife
# 05.2021

class cards:
    def __init__(self, log, db):
        log.debug("init")
        self.log = log
        self.db = db

    def get_cards(self):
        self.log.debug("get_cards")
        out = [{"mod": "fin_credit_ikano", "card": "fin_credit_ikano_sum", "name": "Kredit Ikano Summe"},
           {"mod": "fin_credit_ikano", "card": "fin_credit_ikano_interrest", "name": "Kredit Ikano Zinsen"}]
        return {"allowed": True, "data": out}

    def get_card(self):
        self.log.debug("get_card")
        out = {'sum': 0}
        out = self.db.get_card()
        return {"allowed": True, "data": out}
