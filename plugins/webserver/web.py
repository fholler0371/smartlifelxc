# smartlife
# 04.2021
# 05.2021

import os
import json
import time
from urllib.parse import urlparse, urlencode
import urllib

import requests

class web:
    def __init__(self, log, root, cfg_master, cfg, smartlife):
#    def __init__(self, /* cfg, */ log, /* cb_plugins */):
        self.log = log
        log.debug('init')
        self.mime_types = {"html": "text/html", "css": "text/css", "js": "text/javascript", "woff2": "font/woff2", "png": "image/png", "svg": "image/svg+xml"}
        self.root = root
        self.cfg_master = cfg_master
        self.cfg = cfg
        self.smartlife = smartlife

    def webservice(self, webkeyword):
        for host in self.cfg_master["hosts"]:
            if host["alive"] and host["mode"] >= 0:
                for plugin in host["plugins"]:
                    if "web" in plugin and plugin["web"] == webkeyword:
                        return [plugin["name"], host["ip"]]
        return

    def doGet(self, path, ip, host):
        self.log.debug(path)
        if path == "/":
            path = "/index.html"
        r_path =  (self.root + path).replace("/../", "/").replace("/./", "/").replace("\\", "/")
        if not( os.path.isfile(r_path) ):
            parsed = urlparse(path)
            part_path = parsed.path.split("/")[1]
            remote = self.webservice(part_path)
            if remote != None:
                url = "http://" + remote[1] + ":" + str(self.cfg["netcom_port"]) + "/web/" + remote[0] + "/"
                url += "/".join(parsed.path.split("/")[2:])
                if parsed.query != "":
                    url += "?" + parsed.query + "&"
                else:
                    url += "?"
                url += "ip=" + ip
                try:
                    r = requests.get(url)
                    if r.status_code == 200:
                        return {"code": 200, "data": r.content, "mime": "application/json"}
                except:
                    pass
            else:
                path_split = parsed.path.split("/")
                if (path_split[1] == "lib" and path_split[2] == "module"):
                    modul = path_split[-1].split(".")[0]
                    found = False
                    for host in self.cfg_master["hosts"]:
                        for plugin in host["plugins"]:
                            if not(found) and plugin["name"] == modul:
                                found = True
                                url = "http://" + host["ip"]  + ":" + str(self.cfg_master["netcom_port"]) + "/netcom/script"
                                data = {"modul": modul}
                                try:
                                    r = requests.post(url, data=json.dumps(data))
                                    if r.status_code == 200:
                                        _out = r.content
                                        return {"code": 200, "data": _out, "mime": "text/javascript"}
                                except:
                                    pass
            return {"code": 404}

        ext = r_path.split(".")[-1].lower()
        mime = "text/plain"
        if ext in self.mime_types:
            mime = self.mime_types[ext]
        f = open(r_path, "rb+")
        data = f.read()
        f.close()
        return {"code": 200, "mime": mime, "data" : data, 'cachetime': 604800}

    def doPost(self, path, data, ip, host):
        self.log.debug(path)
        parsed = urlparse(path)
        part_path = parsed.path.split("/")
        remote = self.webservice(part_path[1])
        if remote != None:
            url = "http://" + remote[1] + ":" + str(self.cfg["netcom_port"]) + "/web/" + remote[0] + "/"
            url += "/".join(part_path[2:])
            if parsed.query != "":
                url += "?" + parsed.query + "&"
            else:
                url += "?"
            url += "ip=" + ip
            try:
                r = requests.post(url, data=data)
                if r.status_code == 200:
                    return {"code": 200, "data": json.loads(r.content.decode()), "mime": "application/json"}
            except:
                pass
        elif part_path[1] == "file":
            auth_mod = self.smartlife.get_plugin("authentication")
            if not (auth_mod):
                return {"code": 404}
            try:
                _data = auth_mod.decode(parsed.query.split("=", 1)[1])
                if _data == {}:
                   return {"code": 404} 
            except:
                return {"code": 404}
            if _data["valid"] < time.time():
                return {"code": 404}
            del _data["valid"]
            param = urlencode(_data)
            found = False
            for host in self.cfg_master["hosts"]:
                for plugin in host["plugins"]:
                    if not found and plugin["name"] == _data["modul"]:
                        found = True
                        url = "http://" + host["ip"] + ":" + str(self.cfg_master["netcom_port"]) + "/file/?" + param
                        try:
                            r = requests.post(url, data=data)
                            if r.status_code == 200:
                                return {"code": 200, "data": r.json(), "mime": "application/json"}
                        except:
                            return {"code": 404}
        elif part_path[1] == "api":
            auth_mod = self.smartlife.get_plugin("authentication")
            if not (auth_mod):
                return {"code": 404}
            try:
                _data = json.loads(data)
            except:
                return {"code": 404}
            if not("token" in _data):
                return {"code": 404}
            _token = auth_mod.decode_token(_data["token"])
            if _token["user"] == -1:
                return {"code": 200, "data": {"token": {"valid": False}}, "mime": "application/json"}
            found = False
            modul = part_path[2]
            _in = {}
            if "data" in _data:
                _in = _data["data"]
            _out = {"allowed": False}
            for host in self.cfg_master["hosts"]:
                for plugin in host["plugins"]:
                    if not(found) and plugin["name"] == modul:
                        found = True
                        url = "http://" + host["ip"]  + ":" + str(self.cfg_master["netcom_port"]) + "/netcom/data"
                        data = {"ip": ip, "user": _token["user"], "modul": modul, "cmd": _data["cmd"], "data": _in}
                        try:
                            r = requests.post(url, data=json.dumps(data))
                            if r.status_code == 200:
                                _out = r.json()
                        except:
                            pass
            if not found:
                self.log.error("Modul unbekannt: " + modul)

            if _out["allowed"]:
                data = {"data": _out["data"]}
            else:
                return {"code": 404}
            if _token["user"] == -1:
                return {"code": 200, "data": {"token": {"valid": False}}, "mime": "application/json"}
            else:
                data["token"] = {"valid": True, "token": auth_mod.encode_token(_token)}
                return {"code": 200, "data": data, "mime": "application/json"}
        else:
            self.log.error("Remote: " + str(remote) + " | Path: " + parsed.path)
        return {"code": 404}
