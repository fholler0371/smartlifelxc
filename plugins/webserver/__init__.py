# smartlife
# 04.2021

import time

from plugins2 import plugin_base
import server2 as server

import webserver.web as web

ALIVE_WWW=3600

class integration(plugin_base):
    def __init__(self, smartlife, modul):
        global ALIVE_WWW
        plugin_base.__init__(self, smartlife, modul)
        self.alive = [ALIVE_WWW]
        self.web = web.web(self.log, smartlife.www_path, self.cfg_master, self.cfg, smartlife)
        self.server = server.getServer(self.doGet, self.doPost, self.log, self.cfg['port'])

    def doGet(self, path, ip, name):
        self.log.debug(path)
        self.alive[0] = ALIVE_WWW
        return self.web.doGet(path, ip, name)

    def doPost(self, path, data, ip, name):
        self.log.debug(path)
        self.log.debug(data)
        self.alive[0] = ALIVE_WWW
        return self.web.doPost(path, data, ip, name)

    def run(self):
        self.log.info("run")
        server.threadStart(self.server, self.log)
        while self.running:
            time.sleep(1)
    
    def stop(self):
        self.log.info("stop")
        self.running = False
        server.serverStop(self.server, self.log)