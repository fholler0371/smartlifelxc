# smartlife
# 07.2021
# 08.2021

import json

from plugins2 import plugin_base

from calendar_baikal.lxc_controler import controler as lxc_class
from calendar_baikal.crypt import crypt
from calendar_baikal.server import server

class integration(plugin_base):
    def __init__(self, smartlife, modul):
        plugin_base.__init__(self, smartlife, modul)
        self.alive = [999999999]
        self.lxc = lxc_class(self.log, smartlife.base_path, self.cfg["lxc_name"])
        self.crypt = crypt(self.log, self.cfg["timeout"])
        self.server = server(self.log, self.cfg["port"], self.cfg["ip"], self.crypt)

    def restart(self):
        self.log.debug("user")
        self.lxc.restart()

    def pl_get_menus(self, user, ip):
        self.log.debug("user")
        rights = self.rights(user, ip)
        out = []
        if rights["domain"]:
            if "system" in rights["sub"]:
                out.append({'label': 'Kalender - Admin', 'mod': 'calendar_baikal', 'p1':'sys'})
        return out

    def pl_data(self, user, cmd, _in, ip):
        self.log.debug(cmd)
        rights = self.rights(user, ip)
        if rights["domain"]:
            if "system" in rights["sub"]:
                if cmd == "get_admin":
                    return {"allowed": True, "data": self.crypt.get_first_key("admin")}
            elif "write" in rights["sub"]:
                if cmd == "get_cal":
                    return {"allowed": True, "data": self.crypt.get_first_key("cal")}

    def stop(self):
        self.running = False
        self.lxc.stop()
        self.server.stop()

