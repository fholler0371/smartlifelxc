# smartlife
# 08.2021

from functools import partial
from http.server import HTTPServer, BaseHTTPRequestHandler
from socketserver import ThreadingMixIn
import urllib.parse as urlparse
from threading import Thread
import requests
import time
import copy

### thread #######################################

class loopThread(Thread):
    def __init__(self, server):
        Thread.__init__(self)
        self.daemon = True
        self.name = "WebProxy"
        self.server = server

    def run(self):
        self.server.serve_forever()

### server #######################################

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    daemon_threads = True
    allow_reuse_address = True

class webserverHandler(BaseHTTPRequestHandler):
    def __init__(self, log, crypt, ip, *args, **kwargs):
        self.log = log
        self.crypt = crypt
        self.ip = ip
        super().__init__(*args, **kwargs)

    def parse_headers(self):
        req_header = {}
        for key in self.headers.keys():
            value = self.headers[key]
            if not (key.lower() in ["accept-encoding", "authorizaion", "referer", "dnt", "accept", "upgrade-insecure-requests", "host", "connection", "cookie"]):
                if not key.lower().startswith("sec"):
                    req_header[key] = value
            elif key.lower() == "referer":
                req_header[key] = value.replace("https://"+"home.calendar.holler.pro", "http://"+self.ip)
            elif key.lower() == "cookie":
                for cookie in value.split("; "):
                    if cookie.startswith("PHPSESSID"):
                        req_header["Cookie"] = cookie
        return req_header

    def log_message(self, format, *args):
        self.log.debug("\n    %s - - [%s] %s" % (self.address_string(), self.log_date_time_string(),format%args))

    def do_OPTIONS(self, body=True):
        try:
            _in = urlparse.urlparse(self.path)
            _param = urlparse.parse_qs(_in.query)
            mode = ""
            if "first" in _param:
                mode = self.crypt.decode(_param["first"][0].replace(" ", "+"))
                first = True
            else:
                cookies = self.headers.get("Cookie")
                if cookies:
                    for cookie in cookies.split("; "):
                        cookie_split = cookie.split("=")
                        if cookie_split[0] == "key":
                            mode = self.crypt.decode(cookie_split[1]+"=")
            if mode == "cal":
                self.send_response(200)
                self.send_header("Set-Cookie", "key="+self.crypt.get_first_key(mode)+"; Max-Age=14400; Path=/; HttpOnly")
                self.send_header("Access-Control-Allow-Origin", "*")
                self.send_header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PROPFIND, PROPPATCH, REPORT, PUT, MOVE, DELETE, LOCK, UNLOCK")
                self.send_header("Access-Control-Allow-Headers",
                    "User-Agent, Authorization, Content-type, Depth, If-match, If-None-Match, Lock-Token, "+
                    "Timeout, Destination, Overwrite, X-client, X-Requested-With, Cookie, sec-ch-ua-mobile, sec-ch-ua, Sec-Fetch-Dest, Sec-Fetch-Site, "+
                    "Sec-Fetch-Mode, DNT, Referer, accept, Accept-Encoding, Accept-Language, Cache-Control, Connection, Host, Origin, Pragma")
                self.end_headers()
                self.wfile.write("".encode())  
                self.wfile.flush()
                return  
            else:               
                self.send_response(200)
                self.send_header("Access-Control-Allow-Origin", "*")
                self.send_header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PROPFIND, PROPPATCH, REPORT, PUT, MOVE, DELETE, LOCK, UNLOCK")
                self.send_header("Access-Control-Allow-Headers",
                    "User-Agent, Authorization, Content-type, Depth, If-match, If-None-Match, Lock-Token, "+
                    "Timeout, Destination, Overwrite, X-client, X-Requested-With, Cookie, sec-ch-ua-mobile, sec-ch-ua, Sec-Fetch-Dest, Sec-Fetch-Site, "+
                    "Sec-Fetch-Mode, DNT, Referer, accept, Accept-Encoding, Accept-Language, Cache-Control, Connection, Host, Origin, Pragma")
                self.end_headers()
                return  
        except Exception as e:
            print(repr(e))

    def do_POST(self, body=True):
        try:
            mode = ""
            cookies = self.headers.get("Cookie")
            if cookies:
                for cookie in cookies.split("; "):
                    cookie_split = cookie.split("=")
                    if cookie_split[0] == "key":
                        mode = self.crypt.decode(cookie_split[1]+"=")
            if mode == "admin":
                url = 'http://{}{}'.format(self.ip, self.path)
                content_len = int(self.headers['Content-Length'])
                data = self.rfile.read(content_len)
                headers = {}
                headers["Content-Type"] = "application/x-www-form-urlencoded"
                headers["Content-Length"] = str(content_len)
                resp = requests.post(url, data=data.decode(), headers=headers)
                self.send_response(resp.status_code)
                content = resp.content
                if resp.headers["Content-Type"].lower().startswith("text/html"):
                    content = content.decode().replace("http://"+self.ip, "https://"+"home.calendar.holler.pro").encode()
                self.send_header("Content-Length", len(content))
                self.send_header("Set-Cookie", "key="+self.crypt.get_first_key(mode)+"; Max-Age=14400; Path=/; HttpOnly")
                self.end_headers()
                self.wfile.write(content)  
                self.wfile.flush()
        except Exception as e:
            print(repr(e))

    def do_GET(self, body=True):
        try:
            _in = urlparse.urlparse(self.path)
            _param = urlparse.parse_qs(_in.query)
            mode = ""
            php = ""
            first = False
            if "first" in _param:
                mode = self.crypt.decode(_param["first"][0].replace(" ", "+"))
                first = True
            else:
                cookies = self.headers.get("Cookie")
                if cookies:
                    for cookie in cookies.split("; "):
                        cookie_split = cookie.split("=")
                        if cookie_split[0] == "key":
                            mode = self.crypt.decode(cookie_split[1]+"=")
            headers = self.parse_headers()
            if mode == "admin":
                if first:
                    url = 'http://{}{}'.format(self.ip, _in.path)
                    h = copy.copy(headers)
                    if "Cookie" in h:
                        del h["Cookie"]
                    if "Referer" in h:
                        del h["Referer"]
                    resp = requests.get(url, headers=[])
                    headers = {}
                    if "Set-Cookie" in resp.headers and resp.headers["Set-Cookie"].startswith("PHPSESSID"):
                        php = resp.headers["Set-Cookie"]
                        headers["Cookie"] = php.split(";")[0]
                    headers["Content-Type"] = "application/x-www-form-urlencoded"
                    time.sleep(2)
                    resp = requests.post(url, data="auth=1&login=admin&password=xxx", headers=headers)
                else:
                    url = 'http://{}{}'.format(self.ip, self.path)
                    resp = requests.get(url, headers=headers)
                self.send_response(resp.status_code)
                for header in resp.headers:
                    if not header.lower().startswith("x-") :
                        if not (header.lower() in ["content-encoding", "vary", "strict-transport-securit"]):
                            self.send_header(header, resp.headers[header])
                content = resp.content
                if resp.headers["Content-Type"].lower().startswith("text/html"):
                    content = content.decode().replace("http://"+self.ip, "https://"+"home.calendar.holler.pro").encode()
                self.send_header("Content-Length", len(content))
                self.send_header("Set-Cookie", "key="+self.crypt.get_first_key(mode)+"; Max-Age=14400; Path=/; HttpOnly")
                self.end_headers()
                self.wfile.write(content)  
                self.wfile.flush()
                return              
            if mode == "cal":
                if first:
                    self.send_response(200)
                    self.send_header("Set-Cookie", "key="+self.crypt.get_first_key(mode)+"; Max-Age=14400; Path=/; HttpOnly")
                    self.send_header("Access-Control-Allow-Origin", "*")
                    self.send_header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PROPFIND, PROPPATCH, REPORT, PUT, MOVE, DELETE, LOCK, UNLOCK")
                    self.send_header("Access-Control-Allow-Headers",
                        "User-Agent, Authorization, Content-type, Depth, If-match, If-None-Match, Lock-Token, "+
                        "Timeout, Destination, Overwrite, X-client, X-Requested-With")
                    self.send_header("Content-Length", "0")
                    self.end_headers()
                    self.wfile.flush()
                    return
        except Exception as e:
            print(repr(e))
            pass

class server:
    def __init__(self, log, port, ip, crypt):
        log.debug("init")
        self.log = log
        self.port = port
        self.crypt = crypt
        self.ip = ip
        handler = partial(webserverHandler, log, crypt, ip)
        self.server = ThreadedHTTPServer(('0.0.0.0', port), handler)
        th = loopThread(self.server)
        th.start()

    def stop(self):
        self.server.shutdown()
        self.server.server_close()