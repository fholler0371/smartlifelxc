define(['module', 'jqxtabs', 'jqxdata', 'jqxgrid', 'jqxgrid_selection', 'jqxcheckbox', 'jqxgrid_edit'], 
        function(module) {
    var mod = {
      t_update_1: null,
      t_update_2: null,
      t_update_3: null,
      wdata_last : 0,
      wdata_call : 0,
      wdata : undefined,
      map : undefined,
      mapmarker : undefined,
      year : 0,
      init : function() {
        window.module.calendar_baikal.sub = window.module.calendar_baikal.init_data.p1
        var sub = window.module.calendar_baikal.sub
        if (sub == "sys") {
          window.smfetch('/api/calendar_baikal', {cmd: 'get_admin'}).then((data) => {
            if (data.error == undefined) {
              html = '<div id="calendar_baikal_frame" style="width:100%; height:100%;" class="jqx-widget-content-custom-scheme"></div>'
              $('#client_area').html(html)
              window.module.calendar_baikal.resize()
              html = '<iframe width="100%" height="100%" src="https://home.calendar.holler.pro/admin/?first=' + data.data + '"></iframe>'
              $('#calendar_baikal_frame').html(html)
              $('#calendar_baikal_frame').children().css('border', 0)
            }
          })
        } else {
          html = '<div style="width:100%; height:100%;"><div id="calendar_baikal_tab"><ul><li>Karten</li><li>Sensoren</li></ul>'
          html += '<div><div id="calendar_baikal_cards" style="width:100%; height:100%;"></div></div>'
          html += '<div><div id="calendar_baikal_sensor" style="width:100%; height:100%;"><div id="calendar_baikal_sensor_grid" style="width:100%; height:100%;"></div></div></div>'
          $('#client_area').html(html+'</div>')
          $(window).off('resize', window.module.calendar_baikal.resize)
          $(window).on('resize', window.module.calendar_baikal.resize)
          window.module.calendar_baikal.resize()
          $('#calendar_baikal_tab').jqxTabs({ width: '100%', height: '100%', position: 'top'})
          $('#calendar_baikal_tab').on('selected', function(ev) {
            var selectedTab = ev.args.item
            window.module.calendar_baikal.call_tab(selectedTab)
          })
        }
//        window.module.calendar_baikal.call_tab(0)
      },
      check_data : function() {
        var now = new Date()
        var t = Math.round(now.getTime() / 1000)
        if (t - window.module.calendar_baikal.wdata_last > 60) {
          if (t - window.module.calendar_baikal.wdata_call > 10) {
            window.module.calendar_baikal.wdata_call = t
            window.smfetch('/api/calendar_baikal', {cmd: 'get_card'}).then((data) => {
              if (data.error == undefined) {
                window.module.calendar_baikal.wdata_last = t
                window.module.calendar_baikal.wdata = data.data
              }
            })
          }
        }
      },
      update_1 : function() {
        clearTimeout(window.module.calendar_baikal.t_update_1)
        window.module.calendar_baikal.check_data()
        var cards =  $("#calendar_baikal_cards").find(".cards_calendar_baikal")
        var len = cards.length
        if (window.module.calendar_baikal.wdata != undefined) {
          var ele = $(cards[0])
          var d = window.module.calendar_baikal.wdata.current
          if (ele.data('last') != d) {
            ele.data('last', d)
            html = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">'
            html += 'Aktuelles Wetter</div>'
            html += '<img src="/lib/img/calendar_baikal/' + d.icon + '.png" style="position: relative; top: 25px;">'
            html += '<div style=" position: relative; right: 5px; float: right; top: 34px;"><span  style="font-size: 250%; font-weight: 600;"'
            if (d.temp_chart != undefined) {
              html += ' class="cards_chart" data-chart="/api/calendar_baikal:'+d.temp_chart+'"'
            }
            html += '>'+d.temp.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' °C'
            html += '</span><span'
            if (d.temp_feel_chart.length != undefined) {
              html += ' class="cards_chart"  data-chart="/api/calendar_baikal:'+d.temp_feel_chart+'"'
            }
            html += '>('+d.temp_feel.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' °C)</span></div>'
            html += '<div  style="position: absolute; right: 5px; top: 85px;"><span'
            if (d.press_chart.length > 0) {
              html += ' class="cards_chart"  data-chart="/api/calendar_baikal:'+d.press_chart+'"'
            }
            html += '>'+d.press.toLocaleString('de-DE', {maximumFractionDigits: 0})+' mBar </span>/ <span'
            if (d.hum_chart != undefined) {
              html += ' class="cards_chart" data-chart="/api/calendar_baikal:'+d.hum_chart+'"'
            }
            html += '>'+d.hum.toLocaleString('de-DE', {maximumFractionDigits: 0})+' %</span></div>'
            html += '<div  style="position: absolute; right: 5px; top: 105px;"><span'
            if (d.wspeed_chart.length > 0) {
              html += ' class="cards_chart"  data-chart="/api/calendar_baikal:'+d.wspeed_chart+'"'
            }
            html += '>'+d.wspeed.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})
            html += ' km/h </span>/<span'
            if (d.wdir_chart.length > 0) {
              html += ' class="cards_chart"  data-chart="/api/calendar_baikal:'+d.wdir_chart+'"'
            }
            html += '> '+  d.wdir+ '</span></div>'
            html += '<div  style="position: absolute; right: 5px; top: 125px;"><span'
            if (d.clouds_chart.length > 0) {
              html += ' class="cards_chart"  data-chart="/api/calendar_baikal:'+d.clouds_chart+'"'
            }
            html += '>Wolken: '+d.clouds.toLocaleString('de-DE', {maximumFractionDigits: 0})
            html += ' % </span>/<span'
            if (d.rain_chart.length > 0) {
              html += ' class="cards_chart"  data-chart="/api/calendar_baikal:'+d.rain_chart+'"'
            }
            html += '> Regen: '+ d.rain.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+ ' mm/h</span></div>'
            html += '<div  style="position: absolute; right: 5px; top: 145px;"><span'
            if (d.lux_chart.length > 0) {
              html += ' class="cards_chart"  data-chart="/api/calendar_baikal:'+d.lux_chart+'"'
            }
            html += '> Helligkeit: '
            html += d.lux.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})
            html += ' lux</span></div>'
            ele.html(html)
          }
          window.module.calendar_baikal.t_update_1 = setTimeout(window.module.calendar_baikal.update_1, 15000)
        } else {
          window.module.calendar_baikal.t_update_1 = setTimeout(window.module.calendar_baikal.update_1, 250)
        }
      },
      update_2 : function() {
        clearTimeout(window.module.calendar_baikal.t_update_2)
        window.module.calendar_baikal.check_data()
        var eles =  $("#calendar_baikal_cards").find(".cards_calendar_baikal_pre")
        var len = eles.length
        if (window.module.calendar_baikal.wdata != undefined) {
          for (var i=0; i<len; i++) {
            var ele = $(eles[i])
            var id = ele.data('card').split('_')[2]
            var d = window.module.calendar_baikal.wdata['pre_' + id]
            if (ele.data('last') != d) {d
              ele.data('last', d)
              html = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">'
              html += d.title + '</div>'
              html += '<img src="/lib/img/calendar_baikal/' + d.icon + '.png" style="position: relative; top: 25px;">'
              html += '<div style=" position: absolute; right: 5px; float: right; top: 35px;"><span  style="font-size: 150%; font-weight: 600;">'
              html += d.temp_max.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' °C'
              html += ' - '+d.temp_min.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' °C</span></div>'
              html += '<div style=" position: absolute; right: 5px; float: right; top: 60px;">('
              html += d.temp_feel_max.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' °C'
              html += ' - '+d.temp_feel_min.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' °C)</div>'
              html += '<div  style="position: absolute; right: 5px; top: 78px;">'+d.press.toLocaleString('de-DE', {maximumFractionDigits: 0})+' mBar / '
              html += d.hum.toLocaleString('de-DE', {maximumFractionDigits: 0})+' %</div>'
              html += '<div  style="position: absolute; right: 5px; top: 96px;">'
              html += d.wspeed.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})
              html += ' km/h / '+  d.wdir+ '</div>'
              html += '<div  style="position: absolute; right: 5px; top: 114px;">Regenwahr.: '
              html += d.rain_likely.toLocaleString('de-DE', {maximumFractionDigits: 0}) + ' %</div>'
              html += '<div  style="position: absolute; right: 5px; top: 132px;"> Wolken: '+d.clouds.toLocaleString('de-DE', {maximumFractionDigits: 0})
              html += ' % / Regen: '+ d.rain.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+ ' mm/h</div>'

              ele.html(html)
            }
          }
          window.module.calendar_baikal.t_update_2 = setTimeout(window.module.calendar_baikal.update_2, 30000)
        } else {
          window.module.calendar_baikal.t_update_2 = setTimeout(window.module.calendar_baikal.update_2, 250)
        }
      },
      update_3 : function() {
        clearTimeout(window.module.calendar_baikal.t_update_3)
        window.module.calendar_baikal.check_data()
        var ele =  $("#calendar_baikal_cards").find(".cards_calendar_baikal_phonecalls")
        if (window.module.calendar_baikal.wdata != undefined) {
          var d = window.module.calendar_baikal.wdata.phonecalls
          if (ele.data('last') != d) {
            ele.data('last', d)
            html = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">'
            html += 'Anrufe</div>'
            var len = d.length
            for (var i=0; i<len; i++) {
              html += '<div style="position: absolute; top: ' + (i*20+25) + 'px; left: 10px"><img src="/lib/img/telephone_'
              if (d[i].type == 1) {html += 'blue'}  
              if (d[i].type == 2) {html += 'red'} 
              if (d[i].type == 3) {html += 'green'} 
              html += '.svg" style="height: 18px; width: 18px;"></div>'
              html += '<div style="position: absolute; top: ' + (i*20+25) + 'px; left: 35px"><b>' + d[i].number + '</b></div>' 
              html += '<div style="position: absolute; top: ' + (i*20+25) + 'px; right: 10px">' + d[i].time + '</div>' 
            }
            ele.html(html)
          }
          window.module.calendar_baikal.t_update_3 = setTimeout(window.module.calendar_baikal.update_3, 30000)
        } else {
          window.module.calendar_baikal.t_update_3 = setTimeout(window.module.calendar_baikal.update_3, 250)
        }
      }, 
      get_card : function(data, div) {
        if (data["card"]=='calendar_baikal') {
          if (div.find(".cards_"+data["card"]).length == 0) {
            var html = $('<div class="cards_state cards_'+data["card"]+' card card_h2"><span></span></div>')
            html.data('card', data.card)
            div.append(html)
          } else {
//            console.log(data)
          }
          window.module.calendar_baikal.update_1()
        } else if (data["card"].startsWith("calendar_baikal_pre_")) {
          if (div.find(".cards_"+data["card"]).length == 0) {
            var html = $('<div class="cards_calendar_baikal_pre card card_h2"><span></span></div>')
            html.data('card', data.card)
            div.append(html)
          } else {
//            console.log(data)
          }
          window.module.calendar_baikal.update_2()
        }
      },
      create_cards : function() {
        var ok = 1
        var cards = window.module.calendar_baikal.cards
        var l = cards.length
        for (var i=0; i<l; i++) {
           if (!(typeof(window.module[cards[i].mod].get_card) === 'function')) {
             console.log("Modul not found: "+cards[i].mod)
             ok = 0
           }
        }
        if (ok == 1) {
          for (var i=0; i<l; i++) {
            window.module[cards[i].mod].get_card(cards[i], $("#calendar_baikal_cards"))
            require( [ 'jquery-bridget/jquery-bridget' ], function( jQueryBridget ) {
              jQueryBridget( 'packery', Packery, $ );
              var grid = $('#calendar_baikal_cards').packery({
                itemSelector: '.card',
                gutter: 10
              })
            })
          }
        } else {
          console.log("recheck")
        }
      },
      call_tab: function(id) {
        if (id == 0) {
          window.smfetch('/api/calendar_baikal', {cmd: 'get_cards'}).then((data) => {
            if (data.error == undefined) {
              window.module.calendar_baikal.cards = data.data
              window.module.calendar_baikal.cards_check_mod = 10
              window.module.calendar_baikal.create_cards()
            }
          })
        } else if (id == 1) {
          window.smfetch('/api/calendar_baikal', {cmd: 'get_sensor'}).then((data) => {
            if (data.error == undefined) {
              var source = {
                localdata: data.data,
                datatype: "array",
                datafields: [
                  { name: 'topic', type: 'string'},
                  { name: 'value', type: 'string'},
                  { name: 'entry', type: 'string'},
                  { name: 'sent', type: 'boolean'},
                  { name: 'hist_num', type: 'boolean'},
                  { name: 'hist_event', type: 'boolean'}
                ]                
              }
              var dataAdapter = new $.jqx.dataAdapter(source, {
                downloadComplete: function (data, status, xhr) { },
                loadComplete: function (data) { },
                loadError: function (xhr, status, error) { }
              })
              var col = [
                { text: 'Sensor', datafield: 'topic', width: 350, editable: false},
                { text: 'Bezeichnung', datafield: 'entry'},
                { text: 'Wert', datafield: 'value', width: 300, editable: false, cellsalign: 'right'},
                { text: 'Senden', datafield: 'sent', width: 75, columntype: 'checkbox'},
                { text: 'History', datafield: 'hist_num', width: 75, columntype: 'checkbox'},
                { text: 'History Event', datafield: 'hist_event', width: 125, columntype: 'checkbox'}
              ]
              $("#calendar_baikal_sensor_grid").jqxGrid({
                width: '100%',
                height: '100%',
                source: dataAdapter,
                columns: col,
                editable: true
              })
              $("#calendar_baikal_sensor_grid").off('cellendedit')
              $("#calendar_baikal_sensor_grid").on('cellendedit', function (event) {
                var args = event.args;
                window.smfetch('/api/calendar_baikal', {cmd: 'set_sensor', data: {
                  field: args.datafield, value: args.value, topic: args.row.topic
                }}).then((data) => {})
              })
              $("#calendar_baikal_sensor").css('padding', 0).css('overflow', 'hidden')
            }
          })
        } 
      },
      stop : function() {
        clearTimeout(window.module.calendar_baikal.t_update_1)
        clearTimeout(window.module.calendar_baikal.t_update_2)
        clearTimeout(window.module.calendar_baikal.t_update_3)
        if (window.module.calendar_baikal.map != undefined) {
          window.module.calendar_baikal.map.remove()
          window.module.calendar_baikal.map = undefined
        }
      },
      resize: function() {
        $('#client_area').children().height($('#client_area').height())
      }
    }
    mod['init_data'] = window.module_const[module.id]
    window.module.calendar_baikal = mod
    return mod
  })
