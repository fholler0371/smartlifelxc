define(['packery', 'highcharts', 'moment_tz', 'jqxtabs', 'jqxdropdownlist',
        'jqxdatetimeinput', 'jqxnumberinput', 'jqxbutton', 'jqxchart', 'upload'], function(Packery, H, moment) {
  window.moment = moment
  H.setOptions({
    global: {
      timezone: "Europe/Berlin"
    },
    lang : {
      months : ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
      shortMonths : ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
      weekdays : ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa']
    } 
  })
  var mod = {
    wdata : {},
    wdata_last : 0,
    wdata_call : 0,
    t_update_sum: null,
    t_update_month: null,
    t_update_year: null,
    init: function() {
      if ($('#fin_p2p_tab').length == 0) {
        html = '<div id="fin_p2p_tab"><ul><li>Status</li><li>Verlauf</li></ul><div id="fin_p2p_cards"></div>'
        html += '<div id="fin_p2p_history"></div></div>'
        $('#finanical_left').html(html)
        $('#fin_p2p_tab').jqxTabs({height: '100%', width: '100%'})
        $('#fin_p2p_tab').off('selected')
        $('#fin_p2p_tab').on('selected', function(ev) {
          var id = ev.args.item
          window.module.fin_p2p.call_tab(id)
        })
        window.module.fin_p2p.call_tab(0)
      }fin_shares_d
    },
    check_data : function() {
      var now = new Date()
      var t = Math.round(now.getTime() / 1000)
      if (t - window.module.fin_p2p.wdata_last > 300) {
        if (t - window.module.fin_p2p.wdata_call > 10) {
          window.module.fin_p2p.wdata_call = t
          window.smfetch('/api/fin_p2p', {cmd: 'get_card'}).then((data) => {
            if (data.error == undefined) {
              window.module.fin_p2p.wdata_last = t
              window.module.fin_p2p.wdata = data.data
            }
          })
        }
      }
    },
    update_sum: function() {
      clearTimeout(window.module.fin_p2p.t_update_sum)
      window.module.fin_p2p.check_data()
      var sum = window.module.fin_p2p.wdata.sum
      if (sum != undefined) {
        var ele = $($("body").find('.cards_fin_p2p_sum')[0])
        var old = ele.data('last')
        if (old != sum) {
          ele.data('last', sum)
          html  = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">Basisdaten</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 25px;"><span  style="font-weight: 600;">Anlage:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 25px;">'
          html += sum.sum.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 45px;"><span  style="font-weight: 600;">Summe:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 45px;">'
          html += sum.interest.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          ele.html(html)
        }
        window.module.fin_p2p.t_update_sum = setTimeout(window.module.fin_p2p.update_sum, 600000)
      } else {
        window.module.fin_p2p.t_update_sum = setTimeout(window.module.fin_p2p.update_sum, 250)
      }
    },
    update_month: function() {
      clearTimeout(window.module.fin_p2p.t_update_month)
      window.module.fin_p2p.check_data()
      var d = window.module.fin_p2p.wdata.month
      if (d != undefined) {
        var ele = $($("body").find('.cards_fin_p2p_month')[0])
        var old = ele.data('last')
        if (old != d) {
          ele.data('last', d)
          html  = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">Monat</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 25px;"><span  style="font-weight: 600;">Einahmen:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 25px;">'
          html += d.in.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 45px;"><span  style="font-weight: 600;">Ausgaben:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 45px;">'
          html += d.out.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          ele.html(html)
        }
        window.module.fin_p2p.t_update_month = setTimeout(window.module.fin_p2p.update_month, 600000)
      } else {
        window.module.fin_p2p.t_update_month = setTimeout(window.module.fin_p2p.update_month, 250)
      }
    },
    update_year: function() {
      clearTimeout(window.module.fin_p2p.t_update_year)
      window.module.fin_p2p.check_data()
      var d = window.module.fin_p2p.wdata.year
      if (d != undefined) {
        var ele = $($("body").find('.cards_fin_p2p_year')[0])
        var old = ele.data('last')
        if (old != d) {
          ele.data('last', d)
          html  = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">Jahr</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 25px;"><span  style="font-weight: 600;">Einahmen:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 25px;">'
          html += d.in.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 45px;"><span  style="font-weight: 600;">Ausgaben:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 45px;">'
          html += d.out.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          ele.html(html)
        }
        window.module.fin_p2p.t_update_year = setTimeout(window.module.fin_p2p.update_year, 600000)
      } else {
        window.module.fin_p2p.t_update_year = setTimeout(window.module.fin_p2p.update_year, 250)
      }
    },
    get_card : function(data, div) {
      if (data["card"] == "fin_p2p_sum") {
        if (div.find(".cards_fin_p2p_sum").length == 0) {
          var html = '<div class="cards_fin_p2p_sum card"><span></span></div>'
          div.append(html)
        } else {
//          console.log(data)
        }
        window.module.fin_p2p.update_sum()
      } else if (data["card"] == "fin_p2p_month") {
        if (div.find(".cards_fin_p2p_month").length == 0) {
          var html = '<div class="cards_fin_p2p_month card"><span></span></div>'
          div.append(html)
        } else {
//          console.log(data)
        }
        window.module.fin_p2p.update_month()
      } else if (data["card"] == "fin_p2p_year") {
        if (div.find(".cards_fin_p2p_year").length == 0) {
          var html = '<div class="cards_fin_p2p_year card"><span></span></div>'
          div.append(html)
        } else {
//          console.log(data)
        }
        window.module.fin_p2p.update_year()
      }
    },
    create_cards: function() {
      var ok = 1
      var cards = window.module.fin_p2p.cards
      var l = cards.length
      for (var i=0; i<l; i++) {
         if (!(typeof(window.module[cards[i].mod].get_card) === 'function')) {
           console.log("Modul not found: "+cards[i].mod)
           ok = 0
         }
      }
      if (ok == 1) {
        for (var i=0; i<l; i++) {
          window.module[cards[i].mod].get_card(cards[i], $("#fin_p2p_cards"))
          require( [ 'jquery-bridget/jquery-bridget' ], function( jQueryBridget ) {
            jQueryBridget( 'packery', Packery, $ );
            var grid = $('#fin_p2p_cards').packery({
              itemSelector: '.card',
              gutter: 10
            })
          })
        }
      } else {
        console.log("recheck")
      }
    },
    stop: function() {
    },
    call_tab: function(id) {
      if (id == 0) {
        window.smfetch('/api/fin_p2p', {cmd: 'get_cards'}).then((data) => {
          if (data.error == undefined) {
            window.module.fin_p2p.cards = data.data
            window.module.fin_p2p.cards_check_mod = 10
            window.module.fin_p2p.create_cards()
          }
        })
      } else if (id == 1) {
        if ($('#fin_p2p_chart').length == 0) {
          $('#fin_p2p_history').append("<div id='fin_p2p_chart' style='height: 100%; width: 100%'></div>")
          $('#fin_p2p_history').css('overflow', 'hidden')
          window.smfetch('/api/fin_p2p', {cmd: 'get_history'}).then((data) => {
            if (data.error == undefined) {
              d = data.data
              var series = []
              series.push({name: 'Gewinn', data: [], color: '#00FF00', marker: {enabled: false}, lineWidth: 4})
              series.push({name: 'Betrag', data: [], color: '#0066FF', marker: {enabled: false}, lineWidth: 4})
              var len = d.length
              for (var i=0; i<len; i++) {
                var t = new Date(d[i].date)
                series[1].data.push([t.getTime(), d[i].value])
                series[0].data.push([t.getTime(), d[i].interest])
              }
              var settings = {
                chart: { type: 'area' },
                title : { text: '' },
                subtitle : { text: '' },
                plotOptions: {
                  area: {
                    stacking: 'normal'
                  }
                },
                yAxis : { title: {text: 'Euro' }},
                xAxis: {
                  type: 'datetime',
                  title: { text: '' }
                },
                series: series
              }
              H.chart('fin_p2p_chart', settings)
            }
          })
        }
      }
    },
    upload_finished : function() {
      setTimeout(window.module.fin_p2p.reload_table, 3000)
    },
    reload_table: function() {
      $("#fin_p2p_bookings").jqxGrid('updatebounddata')
    },
    load_table: function(data, source, callback) {
      window.smfetch('/api/fin_p2p', {cmd: 'get_book', data: {'page': data.pagenum, 'pagesize': data.pagesize}}).then((data) => {
        if (data.error == undefined) {
          for (var i=0; i<data.data.rows.length; i++) {
	          data.data.rows[i].date = new Date(data.data.rows[i].date)
	        }
          callback({ records:data.data.rows, totalrecords : data.data.records})
        }
      })
    }
  }
  window.module.fin_p2p = mod
  return mod
})
