# smartlife
# 06.2021

class cards:
    def __init__(self, log, db):
        log.debug("init")
        self.log = log
        self.db = db

    def get_cards(self):
        self.log.debug("get_cards")
        out = [{"mod": "fin_p2p_auxmoney", "card": "fin_p2p_auxmoney_sum", "name": "Auxmoney Summe"}]
#               {"mod": "fin_p2p_auxmoney", "card": "fin_p2p_auxmoney_month", "name": "Auxmoney Giro Monat"},
#               {"mod": "fin_p2p_auxmoney", "card": "fin_p2p_auxmoney_year", "name": "Auxmoney Giro Jahr"}]
        return {"allowed": True, "data": out}

    def get_card(self):
        self.log.debug("get_card")
        out = {'sum': 0}
        out = self.db.get_card()
        return {"allowed": True, "data": out}
