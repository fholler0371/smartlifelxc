# smartlife
# 07.2021

import json
import random
import time
from datetime import datetime, timedelta
from threading import Thread, Timer

from import_csv import csv
from database import database
from webgraber import webgraber

class collect_data(Thread):
    def __init__(self, log, book, user, passwd, fuser, fpasswd, mnext, calc):
        Thread.__init__(self)
        log.debug("init")
        self.log = log
        self.book = book
        self.user = user
        self.passwd = passwd
        self.fuser = fuser
        self.fpasswd = fpasswd
        self.mnext = mnext
        self.calc = calc
        self.csv_current_project = None
        self.csv_archive_project = None
        self.pay_data = None
        self.loan_detail = None
        self.csv1 = None
        self.csv2 = None

    def run(self):
        self.log.debug("run")
        self.book.open()
        self.book.fetchone("UPDATE var SET value='0' WHERE name='active'")
        if self.grap_www():
            if self.loan_detail != None:
                sql = "update `accounts` set `value`='%', `interestPlan`='%s', `firstRepay`='%s' where `auxmoneyid`='%s'"
                self.book.commit(sql % (str(self.loan_detail['value']), str(self.loan_detail['interrest']), self.loan_detail['first'], self.loan_detail['id']))
            if self.pay_data != None:
                self.pay_update()
            if self.csv_current_project:
                self.update_projecte(self.csv_current_project)
            if self.csv_archive_project:
                self.update_projecte(self.csv_archive_project)
        else:
            return
        if self.grap_fin():
            if self.csv1:
                self.update_fintech(self.csv1)
            if self.csv2:
                self.update_fintech(self.csv2)
        else:
            return
        self.book.fetchone("UPDATE var SET value='1' WHERE name='active'")
        self.db_set_fintech_type()
        self.book.close()
        if self.calc:
            self.calc()

    def db_set_fintech_type(self):
        self.log.debug("db_set_fintech_type")
        self.book.commit("update bookings_fintech set type='Einzahlung' WHERE description LIKE ' TA-Nr.%' AND type is null AND value >= '0'")
        self.book.commit("update bookings_fintech set type='Rückzahlung' WHERE description LIKE '%TA-Nr.%' AND type is null AND value < '0'")
        self.book.commit("update bookings_fintech set type='ProjectRepay' WHERE description LIKE 'auxmoney Anlegerauszahlung%' AND type is null")
        self.book.commit("update bookings_fintech set type='Zinsen' WHERE type is null AND description LIKE 'Zinsabschluss%'")

    def update_projecte(self, in_data):
        self.log.debug("update_projecte")
        tab = csv(in_data)
        i = 0
        while i < tab.count:
            nr = tab.get_text(i, 'Kredit Nr.')
            idate = tab.get_date(i, 'Datum')
            adate = tab.get_date(i, 'Auszahlung')
            row = self.book.fetchone("select `id` from `accounts` where `auxmoneyid`='%s'" % (nr))
            if row and idate != "0000-00-00":
                interest = tab.get_float(i, 'Zins (%)')
                laufzeit = tab.get_float(i, 'Laufzeit')
                mvalue = tab.get_float(i, 'Ihre Anlage (EUR)')
                dstatus = tab.get_text(i, 'Status')
                score = tab.get_text(i, 'auxmoney-Score')
                if row:
                    sql = "update `accounts` set `date`='%s', `interest`='%s', `terms`='%s', `myvalue`='%s', `status`='%s', `score`='%s', `payout`='%s' where `id`='%s'"
                    self.book.commit(sql % (idate, str(interest), str(laufzeit), str(mvalue), dstatus, score, adate, str(row[0])))
                else:
                    sql = "insert into `accounts` (`date`, `interest`, `terms`, `myvalue`, `status`, `score`, `payout`, `auxmoneyid`) values ('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')"
                    self.book.commit(sql % (idate, str(interest), str(laufzeit), str(mvalue), dstatus, score, adate, nr))
            i += 1

    def pay_update(self):
        self.log.debug("pay_update")
        for entry in self.pay_data:
            for paytype in self.pay_data[entry]:
                if self.pay_data[entry][paytype] != '0.0':
                    type = 'unkown'
                    if paytype == 'fee':
                        type = 'Gebuehr'
                    elif paytype == 'repay':
                        type = 'Rueckzahlung'
                    elif paytype == 'interest':
                        type = 'Zinsen'
                    sql = "select `id` from `bookings` where `auxmoneyid`='%s' and `date`='%s' and `type`='%s'"
                    row = self.book.fetchone(sql % (entry, self.mnext+"-28", type))
                    if row == None:
                        sql = "insert into `bookings` (`auxmoneyid`, `date`, `type`, `value`) values ('%s', '%s', '%s', '%s')"
                        self.book.execute(sql % (entry, self.mnext+"-28", type, self.pay_data[entry][paytype]))
        last = self.mnext.split('-')
        ldate = datetime(int(last[0]), int(last[1]), 15)
        sdate = (ldate + timedelta(weeks=4)).strftime('%Y-%m')
        self.book.commit("update `var` set `value`='%s' where `name`='next'" % (sdate))

    def update_fintech(self, in_data):
        self.log.debug("update_fintech")
        tab = csv(in_data)
        i = 0
        while i < tab.count:
            _text = tab.get_text(i, 'Buchungsinformationen').replace(',TA-',' TA-').replace(',','')
            try:
                ta = _text.split('TA-Nr.: ')[1].split(' ')[0]
            except:
                ta = ""
            if _text.find("44609-") > - 1:
                trans = _text[_text.find("44609-"):_text.find("44609-")+37]
            else:
                trans = ""
            _date = tab.get_date(i, 'Valuta')
            row = self.book.fetchone("select `id` from `bookings_fintech` where `trans_id`='%s'" % (ta))
            if row:
                pass
            else:
                blz = tab.get_text(i, 'Bankleitzahl')
                account = tab.get_text(i, 'Kontonummer')
                _value = tab.get_float(i, 'Betrag')
                sql = "insert into `bookings_fintech` (`date`, `trans_id`, `bic`, `account`, `b_id`, `description`, `value`) values ('%s', '%s', '%s', '%s', '%s', '%s', '%s')"
                self.book.commit(sql % (_date, ta, blz, account, trans, _text, _value))
            i += 1

    def grap_fin(self):
        self.log.debug("grap fin")
        www = webgraber(self.log)
        www.encoding = "iso-8859-1"
        www.RequestGet('https://konto.biw-bank.de/onlinebanking-auxmoney/loginFormAction.do')
        if www.open() == False:
            self.log.error("do not get login form")
            return False
        www.RequestPost('https://konto.biw-bank.de/onlinebanking-auxmoney/loginFormAction.do?doPopulate=true', {'userId.text':self.fuser, 'pin.text':self.fpasswd, 'loginButton.x':random.randint(8, 36), 'loginButton.y':random.randint(2, 11)})
        if www.open() == False:
            self.log.error("can not login")
            return False
        www.RequestGet('https://konto.biw-bank.de/onlinebanking-auxmoney/postingsFormAction.do?doPopulate=true&accountSelectionForm.accountSelection.selectedItemIndex=0&dateRangeRequestSubForm.searchType.checked=0&dateRangeRequestSubForm.retrievalPeriods.selectedItemIndex=2')
        if www.open() == False:
            self.log.error("do not accout 1 data")
            return False
            www.RequestGet('https://konto.biw-bank.de/onlinebanking-auxmoney/postingsFormAction.do?method=downloadCsv&controlName=postingsForm.accountPostingsTable.standardTableHeaderForm')
            if www.open() == False:
                self.log.error("do not accout 1 data_csv ")
                return False
            else:
                self.csv1 = www.page
        www.RequestGet('https://konto.biw-bank.de/onlinebanking-auxmoney/postingsFormAction.do?doPopulate=true&accountSelectionForm.accountSelection.selectedItemIndex=1&dateRangeRequestSubForm.searchType.checked=0&dateRangeRequestSubForm.retrievalPeriods.selectedItemIndex=2')
        if www.open() == False:
            self.log.error("do not accout 2 data")
            return False
        if www.page.find("Es liegen keine Ums") == -1:
            www.RequestGet('https://konto.biw-bank.de/onlinebanking-auxmoney/postingsFormAction.do?method=downloadCsv&controlName=postingsForm.accountPostingsTable.standardTableHeaderForm')
            if www.open() == False:
                self.log.error("do not accout 2 data_csv ")
                return False
            else: 
                self.csv2 = www.page
        return True

    def grap_www(self):
        www = webgraber(self.log)
        www.encoding = "iso-8859-1"
        www.RequestGet("https://www.auxmoney.com/login")
        if www.open() == False:
            self.log.error("Do not get Login-Page")
            return False
        token = www.getdata(['login[_token]', 'value="'],'"')
        if token == "":
            self.log.error("no token")
            return False
        www.RequestPost('https://www.auxmoney.com/login_check', {'login[_token]':token, 'login[loginUsername]':self.user, 'login[loginPassword]':self.passwd})
        if www.open() == False:
            self.log.error("login no success (maybe ok?)") 
#            return False
        www.RequestGet('https://www.auxmoney.com/anlegercockpit/projects')
        if www.open() == False:
            self.log.error("can not load project page")
            return False
        www.RequestGet('https://www.auxmoney.com/anlegercockpit/projects/export')
        www.addheader('referer', 'https://www.auxmoney.com/anlegercockpit/projects')
        if www.open() == False:
            self.log.error("can not load project page call b")
            return False
        self.csv_current_project = www.page
        www.RequestGet('https://www.auxmoney.com/anlegercockpit/projects/archive')
        if www.open() == False:
            self.log.error("can not load project archiv")
            return False
        www.RequestGet('https://www.auxmoney.com/anlegercockpit/projects/export')
        www.addheader('referer', 'https://www.auxmoney.com/anlegercockpit/projects/archive')
        if www.open() == False:
            self.log.error("can not load project archiv call b")
            return False
        self.csv_archive_project = www.page
        row = self.book.fetchone("SELECT `auxmoneyid` FROM `accounts` WHERE `status` != 'in Bearbeitung' AND `status` != 'storniert' AND `value` = '0' LIMIT 1")
        if row:
            www.RequestGet('https://www.auxmoney.com/anlegercockpit/projects/details/'+row[0])
            if www.open():
                data = '{'+www.getdata(['creditData', '{'],';')
                try:
                    data = json.loads(data)
                    self.loan_detail = {'value':data['loan']/100, 'interrest':data['overview']['interest']['sum']/100, 'first':data['schedule']['1']['date'], 'id':row['auxmoneyid']}
                except:
                    pass
        www.RequestGet('https://www.auxmoney.com/anlegercockpit/returns')
        if www.open() == False:
            self.log.error("no returns data")
            return False
        if www.page.find('data-month="'+self.mnext) > 0:
            www.RequestGet('https://www.auxmoney.com/anlegercockpit/returnflow/ajax/'+self.mnext)
            if www.open() == False:
                self.log.error("ajax return not loaded")
                return False
            pay_projects = www.page.split('<a href')
            self.pay_data = {}
            for i in range(1, len(pay_projects)):
                astart = pay_projects[i].find('>')
                aende = pay_projects[i].find('<',astart)
                account = pay_projects[i][astart+1:aende]
                www.RequestGet('https://www.auxmoney.com/anlegercockpit/projects/details/'+account)
                if www.open() == False:
                    self.log.error("can not load credit page "+account)
                    return
                data = json.loads('{'+www.getdata(['creditData', '{'],';'))
                found = False
                if 'schedule' in data:
                    for entry in data['schedule']:
                        if data['schedule'][entry]['date'].find(self.mnext+'-') > -1:
                            self.pay_data[account] = {'repay':str(data['schedule'][entry]['payedAquittance']/100), 'fee': str(data['schedule'][entry]['payedFee']/100), 'interest': str(data['schedule'][entry]['payedInterest']/100)}
                            found = True
                if not found:
                    for entry in data['unscheduled']:
                        if entry['date'].find('.'+self.mnext[5:7]+'.'+self.mnext[:4]) > -1:
                            self.pay_data[account] = {'repay':str(entry['acquittance']/100), 'fee': '0.0', 'interest': str(entry['interest']/100)}
        return True

class remote:
    def __init__(self, log, smartlife, get_secret, weblogin, finlogin):
        log.debug("init")
        self.calc = None
        self.log = log
        self.weblogin = weblogin
        self.finlogin = finlogin
        self.get_secret = get_secret
        self.running = True
        self.book = database(self.log, smartlife.db_path + "/fin_p2p_auxmoney.sqlite")
        self.th = None

    def grap(self):
        self.log.debug("grap")
        self.book.open()
        row = self.book.fetchone("SELECT value from var WHERE name='active'")
        row2 = self.book.fetchone("SELECT value from var WHERE name='next'")
        self.book.close()
        if row and row[0] == "1":
            if row2:
                mnext = row2[0]
                name = self.get_secret(self.weblogin["name"])
                password = self.get_secret(self.weblogin["password"])
                fname = self.get_secret(self.finlogin["name"])
                fpassword = self.get_secret(self.finlogin["password"])
                th = collect_data(self.log, self.book, name, password, fname, fpassword, mnext, self.calc)
                th.start()
        else:
            self.log.error("not active")  

    def grap_tick(self):
        if self.running:
            t = (int(time.time()/86400)+1)*86400-time.time()+60+300*random.random()+3600
            self.th = Timer(t, self.grap_tick)
            self.th.start()
            self.grap() 

    def run(self, calc):
        self.log.debug("run")
        self.calc = calc  
        t = (int(time.time()/86400)+1)*86400-time.time()+60+300*random.random()+3600
        self.th = Timer(t, self.grap_tick)
        self.th.start()  

    def stop(self):
        self.log.debug("run")
        self.running = False
        if self.th:
            self.th.cancel()    
