# smartlife
# 06.2021
# 07.2021

import json

from plugins2 import plugin_base

from fin_p2p_auxmoney.cards import cards
from fin_p2p_auxmoney.db import db
from fin_p2p_auxmoney.remote import remote

class integration(plugin_base):
    def __init__(self, smartlife, modul):
        global ALIVE_WWW
        plugin_base.__init__(self, smartlife, modul)
        self.alive = [999999999]
        self.remote = remote(self.log, smartlife, self.get_secret, self.cfg["web_login"], self.cfg["fin_login"])
        self.db = db(self.log, smartlife, self.cfg["master"], self.remote)
        self.cards = cards(self.log, self.db)
        self.remote.run(self.db.do_calc)
        
    def pl_data(self, user, cmd, _in, ip):
        self.log.debug(cmd)
        rights = self.rights(user, ip)
        if rights["domain"]:
            if "card" in rights["sub"] or "read" in rights["sub"]:
                if cmd == "get_cards":
                    return self.cards.get_cards()
                elif cmd == "get_card":
                    return self.cards.get_card()
            if "read" in rights["sub"]:
                if cmd == "get_book":
                    return self.db.get_book(_in)
                elif cmd == "get_book_fin":
                    return self.db.get_book_fin(_in)
                elif cmd == "get_book_proj":
                    return self.db.get_book_proj(_in)
                elif cmd == "get_history":
                    return self.db.get_history()
            if "write" in rights["sub"]:
                if cmd == "do_calc":
                    return self.db.do_calc()
                elif cmd == "set_active":
                    return self.db.set_active(_in)
        return {"allowed": False}

    def stop(self):
        self.log.info(self.name)
        self.running = False
        self.db.stop()
        self.remote.stop()
