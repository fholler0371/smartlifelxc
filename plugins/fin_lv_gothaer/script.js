define(['packery', 'highcharts', 'moment_tz', 'jqxtabs', 'jqxdata', 'jqxgrid', 'jqxgrid_pager', 'jqxgrid_selection', 'jqxdropdownlist',
        'jqxdatetimeinput', 'jqxnumberinput', 'jqxbutton', 'upload'], function(Packery, H, moment) {
  window.moment = moment
  H.setOptions({
    global: {
      timezone: "Europe/Berlin"
    },
    lang : {
      months : ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
      shortMonths : ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
      weekdays : ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa']
    } 
  })
  var mod = {
    wdata : {},
    wdata_last : 0,
    wdata_call : 0,
    t_update_sum: null,
    t_update_month: null,
    t_update_year: null,
    init: function() {
      if ($('#fin_lv_gothaer_tab').length == 0) {
        html = '<div id="fin_lv_gothaer_tab"><ul><li>Status</li><li>Buchungen</li><li>Verlauf</li></ul><div id="fin_lv_gothaer_cards"></div>'
        html += '<div id="fin_lv_gothaer_book"></div><div id="fin_lv_gothaer_history"></div></div>'
        $('#finanical_left').html(html)
        $('#fin_lv_gothaer_tab').jqxTabs({height: '100%', width: '100%'})
        $('#fin_lv_gothaer_tab').off('selected')
        $('#fin_lv_gothaer_tab').on('selected', function(ev) {
          var id = ev.args.item
          window.module.fin_lv_gothaer.call_tab(id)
        })
        window.module.fin_lv_gothaer.call_tab(0)
      }
    },
    check_data : function() {
      var now = new Date()
      var t = Math.round(now.getTime() / 1000)
      if (t - window.module.fin_lv_gothaer.wdata_last > 300) {
        if (t - window.module.fin_lv_gothaer.wdata_call > 10) {
          window.module.fin_lv_gothaer.wdata_call = t
          window.smfetch('/api/fin_lv_gothaer', {cmd: 'get_card'}).then((data) => {
            if (data.error == undefined) {
              window.module.fin_lv_gothaer.wdata_last = t
              window.module.fin_lv_gothaer.wdata = data.data
            }
          })
        }
      }
    },
    update_sum: function() {
      clearTimeout(window.module.fin_lv_gothaer.t_update_sum)
      window.module.fin_lv_gothaer.check_data()
      var sum = window.module.fin_lv_gothaer.wdata.sum
      if (sum != undefined) {
        var ele = $($("body").find('.cards_fin_lv_gothaer_sum')[0])
        var old = ele.data('last')
        if (old != sum) {
          ele.data('last', sum)
          html  = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">Basisdaten</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 25px;"><span  style="font-weight: 600;">Kaufpreis:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 25px;">'
          html += sum.invest.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 45px;"><span  style="font-weight: 600;">Wert:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 45px;">'
          html += sum.sum.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          ele.html(html)
        }
        window.module.fin_lv_gothaer.t_update_sum = setTimeout(window.module.fin_lv_gothaer.update_sum, 600000)
      } else {
        window.module.fin_lv_gothaer.t_update_sum = setTimeout(window.module.fin_lv_gothaer.update_sum, 250)
      }
    },
    update_month: function() {
      clearTimeout(window.module.fin_lv_gothaer.t_update_month)
      window.module.fin_lv_gothaer.check_data()
      var d = window.module.fin_lv_gothaer.wdata.month
      if (d != undefined) {
        var ele = $($("body").find('.cards_fin_lv_gothaer_month')[0])
        var old = ele.data('last')
        if (old != d) {
          ele.data('last', d)
          html  = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">Monat</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 25px;"><span  style="font-weight: 600;">Einahmen:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 25px;">'
          html += d.in.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 45px;"><span  style="font-weight: 600;">Ausgaben:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 45px;">'
          html += d.out.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          ele.html(html)
        }
        window.module.fin_lv_gothaer.t_update_month = setTimeout(window.module.fin_lv_gothaer.update_month, 600000)
      } else {
        window.module.fin_lv_gothaer.t_update_month = setTimeout(window.module.fin_lv_gothaer.update_month, 250)
      }
    },
    update_year: function() {
      clearTimeout(window.module.fin_lv_gothaer.t_update_year)
      window.module.fin_lv_gothaer.check_data()
      var d = window.module.fin_lv_gothaer.wdata.year
      if (d != undefined) {
        var ele = $($("body").find('.cards_fin_lv_gothaer_year')[0])
        var old = ele.data('last')
        if (old != d) {
          ele.data('last', d)
          html  = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">Jahr</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 25px;"><span  style="font-weight: 600;">Einahmen:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 25px;">'
          html += d.in.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          html += '<div style=" position: absolute; left: 5px; float: right; top: 45px;"><span  style="font-weight: 600;">Ausgaben:</span></div>'
          html += '<div style=" position: absolute; right: 5px; float: right; top: 45px;">'
          html += d.out.toLocaleString('de-DE', {minimumFractionDigits: 2, maximumFractionDigits: 2}) +' €</div>'
          ele.html(html)
        }
        window.module.fin_lv_gothaer.t_update_year = setTimeout(window.module.fin_lv_gothaer.update_year, 600000)
      } else {
        window.module.fin_lv_gothaer.t_update_year = setTimeout(window.module.fin_lv_gothaer.update_year, 250)
      }
    },
    get_card : function(data, div) {
      if (data["card"] == "fin_lv_gothaer_sum") {
        if (div.find(".cards_fin_lv_gothaer_sum").length == 0) {
          var html = '<div class="cards_fin_lv_gothaer_sum card"><span></span></div>'
          div.append(html)
        } else {
//          console.log(data)
        }
        window.module.fin_lv_gothaer.update_sum()
      } else if (data["card"] == "fin_lv_gothaer_month") {
        if (div.find(".cards_fin_lv_gothaer_month").length == 0) {
          var html = '<div class="cards_fin_lv_gothaer_month card"><span></span></div>'
          div.append(html)
        } else {
//          console.log(data)
        }
        window.module.fin_lv_gothaer.update_month()
      } else if (data["card"] == "fin_lv_gothaer_year") {
        if (div.find(".cards_fin_lv_gothaer_year").length == 0) {
          var html = '<div class="cards_fin_lv_gothaer_year card"><span></span></div>'
          div.append(html)
        } else {
//          console.log(data)
        }
        window.module.fin_lv_gothaer.update_year()
      }
    },
    create_cards: function() {
      var ok = 1
      var cards = window.module.fin_lv_gothaer.cards
      var l = cards.length
      for (var i=0; i<l; i++) {
         if (!(typeof(window.module[cards[i].mod].get_card) === 'function')) {
           console.log("Modul not found: "+cards[i].mod)
           ok = 0
         }
      }
      if (ok == 1) {
        for (var i=0; i<l; i++) {
          window.module[cards[i].mod].get_card(cards[i], $("#fin_lv_gothaer_cards"))
          require( [ 'jquery-bridget/jquery-bridget' ], function( jQueryBridget ) {
            jQueryBridget( 'packery', Packery, $ );
            var grid = $('#fin_lv_gothaer_cards').packery({
              itemSelector: '.card',
              gutter: 10
            })
          })
        }
      } else {
        console.log("recheck")
      }
    },
    stop: function() {
    },
    call_tab: function(id) {
      if (id == 0) {
        window.smfetch('/api/fin_lv_gothaer', {cmd: 'get_cards'}).then((data) => {
          if (data.error == undefined) {
            window.module.fin_lv_gothaer.cards = data.data
            window.module.fin_lv_gothaer.cards_check_mod = 10
            window.module.fin_lv_gothaer.create_cards()
          }
        })
      } else if (id == 1) {
        if ($('#fin_lv_gothaer_bookings').length == 0) {
          var source = {
            datatype: 'json',
            loadServerData: window.module.fin_lv_gothaer.load_table,
            datafields: [
              { name: 'id' },
              { name: 'date', type: 'date' },
              { name: 'rate', type: 'float' },
              { name: 'amount', type: 'float' }
            ],
            id: 'id',
            pagesize: 20,
            url: "http://x.x"
          }
          var dataAdapter = new $.jqx.dataAdapter(source);
          $('#fin_lv_gothaer_book').append("<div id='fin_lv_gothaer_bookings'></div>");
          $("#fin_lv_gothaer_bookings").jqxGrid({
            width:'calc( 100% - 3px )',
            height:'calc( 100% - 5px )',
            localization: {
              currencysymbol: " €",
              currencysymbolposition: "after",
              decimalseparator: ",",
              thousandsseparator: ".",
              pagergotopagestring: "Gehe zu:",
              pagershowrowsstring: "Zeige Zeile:",
              pagerrangestring: " von ",
              pagernextbuttonstring: "voriger",
              pagerpreviousbuttonstring: "nächster"
            },
            source: dataAdapter,
            pageable: true,
            pagesizeoptions: ['10', '20', '30', '50', '100'],
            virtualmode: true,
            rendergridrows: function(obj){
              return obj.data;
            },
            showtoolbar: true,
            rendertoolbar: function (statusbar) {
              var container = $("<div style='overflow: hidden; position: relative; margin: 5px;'></div>")
              var csvButton = $("<div style='float: left; margin-left: 5px;'><span style='margin-left: 4px; position: relative; top: -3px;'>CSV hochladen</span></div>")
              var calcButton = $("<div style='float: left; margin-left: 5px;'><span style='margin-left: 4px; position: relative; top: -3px;'>berechnen</span></div>")
              container.append(csvButton)
              container.append(calcButton)
              csvButton.jqxButton({  width: 95, height: 20 })
              csvButton.click(function (event) {
                window.upload.run('fin_lv_gothaer', 'book', 'Cosors Girokonto *.csv', '.csv', true, 
                                  window.module.fin_lv_gothaer.upload_finished)
              })
              calcButton.jqxButton({  width: 95, height: 20 })
              calcButton.click(function (event) {
                window.smfetch('/api/fin_lv_gothaer', {cmd: 'do_calc'}).then((data) => {
                  if (data.error == undefined) {
                    $('#fin_lv_gothaer_chart').remove()
                  }
                })
              })
              statusbar.append(container)
            },
            columns : [
              {text:'id', datafield:'id', hidden: true},
              {text:'Datum', datafield:'date', width:100,  columntype: 'datetimeinput', cellsformat: 'dd.MM.yyyy'},
              {text:'Rate', datafield:'rate', cellsalign: 'right', cellsformat: 'c2', columntype:'numberinput', width: 200},
              {text:'Betrag', datafield:'amount', cellsalign: 'right', cellsformat: 'c2', columntype:'numberinput'}
            ]
          })
        }
      } else if (id == 2) {
        if ($('#fin_lv_gothaer_chart').length == 0) {
          $('#fin_lv_gothaer_history').append("<div id='fin_lv_gothaer_chart' style='height: 100%; width: 100%'></div>")
          $('#fin_lv_gothaer_history').css('overflow', 'hidden')
          window.smfetch('/api/fin_lv_gothaer', {cmd: 'get_history'}).then((data) => {
            if (data.error == undefined) {
              d = data.data
              var series = []
              series.push({name: 'Gewinn', data: [], color: '#00FF00', marker: {enabled: false}, lineWidth: 4})
              series.push({name: 'Betrag', data: [], color: '#0066FF', marker: {enabled: false}, lineWidth: 4})
              var len = d.length
              for (var i=0; i<len; i++) {
                var t = new Date(d[i].date)
                series[1].data.push([t.getTime(), d[i].value])
                series[0].data.push([t.getTime(), d[i].interest])
              }
              var settings = {
                chart: { type: 'area' },
                title : { text: '' },
                subtitle : { text: '' },
                plotOptions: {
                  area: {
                    stacking: 'normal'
                  }
                },
                yAxis : { title: {text: 'Euro' }},
                xAxis: {
                  type: 'datetime',
                  title: { text: '' }
                },
                series: series
              }
              H.chart('fin_lv_gothaer_chart', settings)
            }
          })
        }
      }
    },
    upload_finished : function() {
      setTimeout(window.module.fin_lv_gothaer.reload_table, 3000)
    },
    reload_table: function() {
      $("#fin_lv_gothaer_bookings").jqxGrid('updatebounddata')
    },
    load_table: function(data, source, callback) {
      window.smfetch('/api/fin_lv_gothaer', {cmd: 'get_book', data: {'page': data.pagenum, 'pagesize': data.pagesize}}).then((data) => {
        if (data.error == undefined) {
          for (var i=0; i<data.data.rows.length; i++) {
	          data.data.rows[i].date = new Date(data.data.rows[i].date)
	        }
          callback({ records:data.data.rows, totalrecords : data.data.records})
        }
      })
    }
  }
  window.module.fin_lv_gothaer = mod
  return mod
})
