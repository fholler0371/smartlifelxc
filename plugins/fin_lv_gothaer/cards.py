# smartlife
# 05.2021

class cards:
    def __init__(self, log, db):
        log.debug("init")
        self.log = log
        self.db = db

    def get_cards(self):
        self.log.debug("get_cards")
        out = [{"mod": "fin_lv_gothaer", "card": "fin_lv_gothaer_sum", "name": "Lebensversicherung Gothaer Summe"}]
#        ,
#               {"mod": "fin_lv_gothaer", "card": "fin_lv_gothaer_month", "name": "Lebensversicherung Zurich 2 Monat"},
#               {"mod": "fin_lv_gothaer", "card": "fin_lv_gothaer_year", "name": "Lebensversicherung Zurich 2 Jahr"}]
        return {"allowed": True, "data": out}

    def get_card(self):
        self.log.debug("get_card")
        out = {'sum': 0}
        out = self.db.get_card()
        return {"allowed": True, "data": out}


