# smartlife
# 10.2021

from functools import partial
from http.server import HTTPServer, BaseHTTPRequestHandler
from socketserver import ThreadingMixIn
import urllib.parse as urlparse
from threading import Thread
import requests
import time
import copy

from oauth.wellknown import wellknown

### thread #######################################

class loopThread(Thread):
    def __init__(self, server):
        Thread.__init__(self)
        self.daemon = True
        self.name = "WebProxy"
        self.server = server

    def run(self):
        self.server.serve_forever()

### server #######################################

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    daemon_threads = True
    allow_reuse_address = True

class webserverHandler(BaseHTTPRequestHandler):
    def __init__(self, log, *args, **kwargs):
        self.log = log
        super().__init__(*args, **kwargs)

    def parse_headers(self):
        req_header = {}
        for key in self.headers.keys():
            value = self.headers[key]
            if not (key.lower() in ["accept-encoding", "authorizaion", "referer", "dnt", "accept", "upgrade-insecure-requests", "host", "connection", "cookie"]):
                if not key.lower().startswith("sec"):
                    req_header[key] = value
            elif key.lower() == "referer":
                req_header[key] = value.replace("https://"+"home.calendar.holler.pro", "http://"+self.ip)
            elif key.lower() == "cookie":
                for cookie in value.split("; "):
                    if cookie.startswith("PHPSESSID"):
                        req_header["Cookie"] = cookie
        return req_header

    def log_message(self, format, *args):
        self.log.debug("\n    %s - - [%s] %s" % (self.address_string(), self.log_date_time_string(),format%args))
        print("%s - - [%s] %s" % (self.address_string(), self.log_date_time_string(),format%args))

    def do_GET(self, body=True):
        _in = urlparse.urlparse(self.path)
        _param = urlparse.parse_qs(_in.query)
        if _in.path == "/.well-known/openid-configuration":
            host = self.headers["X-Host"]
            wellknown(host)
        else:
            print(_param)
            print(_in)
        self.send_response(200)
        self.end_headers()
        self.wfile.write("".encode())  
        self.wfile.flush()

class server:
    def __init__(self, log, port):
        log.debug("init")
        self.log = log
        self.port = port
        handler = partial(webserverHandler, log)
        self.server = ThreadedHTTPServer(('0.0.0.0', port), handler)
        th = loopThread(self.server)
        th.start()

    def stop(self):
        self.server.shutdown()
        self.server.server_close()