# smartlife
# 08.2021

import time
import json
import base64
from Crypto.Cipher import AES

import key_bcrypt

class crypt:
    def __init__(self, log, timeout):
        log.debug("init")
        self.log = log
        self.timeout = timeout
        self.key = key_bcrypt.get_new_key()

    def get_first_key(self, mode):
        self.log.debug(mode)
        rec = {"t": int(time.time()+self.timeout), "m": mode}
        cipher = AES.new(self.key[:16].encode(), AES.MODE_EAX)
        ciphertext, tag = cipher.encrypt_and_digest(json.dumps(rec).encode())
        return base64.encodestring(cipher.nonce+ciphertext).decode().replace('\n', '')

    def decode(self, data):
        self.log.debug("decode")
        mode = ""
        try:
            ciphertext = base64.decodestring(data.encode())
            nonce = ciphertext[:AES.block_size]
            ciphertext = ciphertext[AES.block_size:]
            cipher = AES.new(self.key[:16].encode(), AES.MODE_EAX, nonce=nonce)
            text = cipher.decrypt(ciphertext)
            data = json.loads(text.decode())
            if data["t"] > time.time():
                mode = data["m"]
        except:
            pass
        return mode
