# smartlife
# 05.2021
# 09.2021

import json
import os
import copy
import time
from threading import Timer

from plugins2 import plugin_base

ALIVE_LOCAL = 30000
ALIVE_REMOTE = 30000
SAVE_INTERVAL = 3600

class integration(plugin_base):
    def __init__(self, smartlife, modul):
        plugin_base.__init__(self, smartlife, modul)
        global ALIVE_LOCAL, ALIVE_REMOTE
        self.alive = [ALIVE_LOCAL, ALIVE_REMOTE]
        self.store = {}
        self.jname = smartlife.db_path + "/data_store_data.json"
        if os.path.isfile(self.jname):
            f = open(self.jname, "r")
            self.store = json.loads(f.read())
            f.close()
        self.subscribtion = {}
        self.subname = smartlife.db_path + "/data_store_subscription.json"
        if os.path.isfile(self.subname):
            f = open(self.subname, "r")
            self.subscribtion = json.loads(f.read())
            f.close()
        self.hostname = ""

    def new_local(self, msg):
        self.log.debug(msg)
        global ALIVE_LOCAL
        self.alive[0] = ALIVE_LOCAL
        data = copy.copy(msg)
        netcom = self.smartlife.get_plugin("netcom")
        if self.hostname == "":
            self.hostname = netcom.get_hostname()
        topic = self.hostname + "/" + data["topic"]
        data["topic"] = topic
        data["friendly"] = self.hostname + "/" + data["friendly"]
        data["lc"] = data["ts"]
        if topic in self.store:
            if data["value"] == self.store[topic]["value"]:
                data["lc"] = self.store[topic]["lc"]
        self.store[topic] = data
        netcom.udp_broadcast("SL:DATA_STORE:SENSOR:" + json.dumps(data))
        if data["topic"] in self.subscribtion:
            for out in self.subscribtion[data["topic"]]:
                mod = self.smartlife.get_plugin(out)
                if mod:
                    mod.new_msg(data)
 
    def subscribe(self, topic, app):
        self.log.debug("subscribe")
        if topic in self.subscribtion:
            if not (app in self.subscribtion[topic]):
                self.subscribtion[topic].append(app)
        else:
            self.subscribtion[topic] = [app]

    def unsubscribe(self, app):
        self.log.debug("unsubscribe")
        try:
            for topic in self.subscribtion:
                if app in self.subscribtion[topic]:
                    self.subscribtion[topic].remove(app)
                if length(self.subscribtion[topic]) == 0:
                    del self.subscribtion[topic]
        except:
            pass

    def new_remote(self, msg):
        self.log.debug(msg)
        global ALIVE_REMOTE
        self.alive[1] = ALIVE_REMOTE
        if self.hostname == "":
            netcom = self.smartlife.get_plugin("netcom")
            self.hostname = netcom.get_hostname()    
        data = msg.split(":", 1)
        if data[0] == "SENSOR":
            entry = json.loads(data[1])
            if entry["topic"].split("/")[0] != self.hostname:
                self.store[entry["topic"]] = entry
                if entry["topic"] in self.subscribtion:
                    for out in self.subscribtion[entry["topic"]]:
                        mod = self.smartlife.get_plugin(out)
                        if mod:
                            mod.new_msg(entry)

    def get_sensor(self, topic):
        self.log.debug(topic)
        if topic in self.store:
            return self.store[topic]
        return False
    
    def run(self):
        self.log.info("run")
        global SAVE_INTERVAL
        self.save_timer = Timer(SAVE_INTERVAL, self.save_tick)
        self.save_timer.start()
        while self.running:
            time.sleep(5)

    def save_tick(self):
        self.log.debug("save_tick")
        if self.running:
            self.save_timer = Timer(SAVE_INTERVAL, self.save_tick)
            self.save_timer.start()

    def save(self):
        self.log.debug("save")
        f = open(self.jname, "w")
        f.write(json.dumps(self.store))
        f.close()
        f = open(self.subname, "w")
        f.write(json.dumps(self.subscribtion))
        f.close()
    
    def stop(self):
        self.log.info("stop")
        self.running = False
        if self.save_timer:
            self.save_timer.cancel()
        self.save()

    
    
