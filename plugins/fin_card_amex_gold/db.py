# smartlife
# 05.2021

from threading import Thread, Timer
from datetime import datetime, timedelta
import time
import random

from database import database

class history(Thread):
    def __init__(self, log, name, book, master):
        Thread.__init__(self)
        log.debug("init")
        self.log = log
        self.master = master
        sql = "CREATE TABLE IF NOT EXISTS history (date string, value numeric, interest numeric)"
        self.hist = database(self.log, name, sql)
        self.book = database(self.log, book)

    def run(self):
        self.log.debug("run")
        self.hist.open()
        self.book.open()
        maxDate = datetime.now()
        row = self.hist.fetchone("select date from history order by date desc limit 1")
        if row:
            minDate = datetime.strptime(row[0], "%Y-%m-%d")
        else:
            row = self.book.fetchone("select date from bookings order by date asc limit 1")
            if not row:
                self.book.close()
                self.hist.close()
                return
            minDate = datetime.strptime(row[0], "%Y-%m-%d")
        # setze min 31 Tage vor
        minDate = minDate - timedelta(days=31)
        # check minDate gegen Daten
        row = self.book.fetchone("select date from bookings order by date asc limit 1")
        if not row:
            self.book.close()
            self.hist.close()
            return
        d = datetime.strptime(row[0], '%Y-%m-%d')
        if d > minDate:
            minDate = d
        # loop
        d = minDate
        while d <= maxDate:
            row = self.book.fetchone("select sum(value) from bookings where date <= '%s'" % (d.strftime('%Y-%m-%d'), ))
            val = row[0]
            if self.hist.fetchone("select date from history where date = '%s'"  % (d.strftime('%Y-%m-%d'), )):
                sql = "update history set value='%s' where date='%s'" % ("%.2f" % round(val, 2), d.strftime('%Y-%m-%d'), )
            else:
                sql = "insert into history (value, date, interest) values ('%s', '%s', '0')" % ("%.2f" % round(val, 2), d.strftime('%Y-%m-%d'), )
            self.hist.execute(sql)
            d = d + timedelta(days=1)
        self.hist.commit()
        self.book.close()
        self.hist.close()
        if self.master:
            self.master.calc_hist()

class db:
    def __init__(self, log, smartlife, master):
        log.debug("init")
        self.log = log
        self.smartlife = smartlife
        self.master = master
        sql = "CREATE TABLE IF NOT EXISTS bookings (id INTEGER, date string, text string, value numeric, PRIMARY KEY(id))"
        self.book = database(self.log, smartlife.db_path + "/fin_card_amex_gold.sqlite", sql)
        self.hist = database(self.log, smartlife.db_path + "/fin_card_amex_gold_history.sqlite")
        t = (int(time.time()/86400)+1)*86400-time.time()+60+300*random.random()
        self.timer_calc = Timer(t, self.tick_calc)
        self.timer_calc.start()

    def tick_calc(self):
        self.log.debug("tick_calc")
        t = (int(time.time()/86400)+1)*86400-time.time()+60+300*random.random()
        self.timer_calc = Timer(t, self.tick_calc)
        self.timer_calc.start()
        self.do_calc()

    def get_card(self):
        self.log.debug("get_card")
        out = {"sum": 0, "month": {"interest": 0, "sum": 0}, "year": {"interest": 0, "sum": 0}}
        self.book.open()
        row = self.book.fetchone("select sum(value) from bookings")
        if row and not(row[0] == None):
            out['sum'] = float("%.2f" % round(row[0], 2))
        sql = "select sum(value) from bookings where value>'0' and date>='%s'"
        dm = datetime.now() - timedelta(days=31)
        dy = datetime.now() - timedelta(days=365)
        row = self.book.fetchone(sql % (dm.strftime('%Y-%m-%d'), ))
        if row and not(row[0] == None):
            out["month"]["sum"] = float("%.2f" % round(row[0], 2))
        row = self.book.fetchone(sql % (dy.strftime('%Y-%m-%d'), ))
        if row and not(row[0] == None):
            out["year"]["sum"] = float("%.2f" % round(row[0], 2))
        self.book.close()
        return out

    def get_book(self, data):
        self.log.debug(data)
        out = {"records": 0, "rows": []}
        self.book.open()
        row = self.book.fetchone("select count(id) from bookings")
        if row:
            out["records"] = row[0]
        sql = "select id, date, text, value from bookings order by date desc limit %s, %s"
        for row in self.book.fetchall(sql % (str(data["page"]*data['pagesize']), str(data['pagesize']), )):
            out["rows"].append({'id': row[0], 'date': row[1], 'text': row[2], 'amount': row[3]})
        self.book.close()
        return {"allowed": True, "data": out}

    def add(self, date, text, value):
        self.log.debug(date)
        self.book.open()
        sql = "select id from bookings where date='%s' and text='%s' and value='%s'"
        row = self.book.fetchone(sql % (date, text, str(value), ))
        if row == None:
            sql = "insert into bookings (date, text, value) values ('%s', '%s', '%s' ) "
            self.book.commit(sql % (date, text, str(value), ))
        self.book.close()

    def do_calc(self):
        self.log.debug("do_calc")
        th = history(self.log, self.smartlife.db_path + "/fin_card_amex_gold_history.sqlite", 
            self.smartlife.db_path + "/fin_card_amex_gold.sqlite", self.smartlife.get_plugin(self.master))
        th.start()
        return {"allowed": True, "data": {}}

    def get_history(self):
        self.log.debug("get_history")
        out = []
        self.hist.open()
        for entry in self.hist.fetchall("select date, value from history order by date asc"):
            out.append({'date': entry[0], 'value': entry[1]})
        self.hist.close()
        return {"allowed": True, "data": out}

    def stop(self):
        self.log.debug("stop")
        if self.timer_calc:
            self.timer_calc.cancel()
            