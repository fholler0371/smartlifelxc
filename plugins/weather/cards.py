# smartlife
# 05.2021

from datetime import datetime
import time

from dateutil import tz

WDAY = ["Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Sonnabend", "Sonntag"]

class cards():
    def __init__(self, log, data):
        log.debug("init")
        self.log = log
        self.data = data
        self.max = 16

    def get_cards(self):
        self.log.debug("get_cards")
        out = []
        out.append({"mod": "weather", "card": "weather", "name": "Wetter"})
        i = 0
        while i < self.max:
            out.append({"mod": "weather", "card": "weather_pre_" + str(i), "name": "Wettervorhersage " + str(i)})
            i += 1
        return {"allowed": True, "data": out}

    def get_card(self):
        self.log.debug("get_card")
        out = {"current": {"icon": "", "temp": -99, "temp_feel": -99, "press": -99, "hum": -99, "wspeed": -99,
                           "wdir": -999, "clouds": -99, "rain": -99, "lux": -99}}
        out["current"]["icon"] = self.data.get_value("current/berlin/weather_icon")
        out["current"]["temp"] = self.data.get_value("current/berlin/temperature")
        out["current"]["temp_chart"] = "current/berlin/temperature:W"
        out["current"]["temp_feel"] = self.data.get_value("current/berlin/temperature_feel")
        out["current"]["temp_feel_chart"] = "current/berlin/temperature_feel:W"
        out["current"]["press"] = self.data.get_value("current/berlin/pressure")
        out["current"]["press_chart"] = "current/berlin/pressure:W"
        out["current"]["hum"] = self.data.get_value("current/berlin/humidity")
        out["current"]["hum_chart"] = "current/berlin/humidity:W"
        out["current"]["wspeed"] = self.data.get_value("current/berlin/wind_speed")
        out["current"]["wspeed_chart"] = "current/berlin/wind_speed:W"
        out["current"]["wdir"] = self.data.get_value("current/berlin/wind_rose")
        out["current"]["wdir_chart"] = "current/berlin/wind_dir:W"
        out["current"]["clouds"] = self.data.get_value("current/berlin/clouds")
        out["current"]["clouds_chart"] = "current/berlin/clouds:W"
        out["current"]["rain"] = self.data.get_value("current/berlin/rain")
        out["current"]["rain_chart"] = "current/berlin/rain:W"
        out["current"]["lux"] = self.data.get_value("current/berlin/lux")
        out["current"]["lux_chart"] = "current/berlin/lux:W"
        i = 0
        while i < self.max:
            part = "pre_" + str(i)
            out[part] = {"title": "Tag " +  str(i), "icon": "", "temp_max": -99, "temp_min": -99, "temp_feel_max": -99,
                         "temp_feel_min": -99, "press": -99, "hum": -99, "wspeed": -99, "wdir": -999, "rain_likely": -99,
                         "clouds": -99, "rain": -99}
            if i == 0:
                out[part]["title"] = "Heute"
            elif i == 1:
                out[part]["title"] = "Morgen"
            elif i < 8:
                t = int(time.time())
                t = datetime.utcfromtimestamp(t + i * 86400) 
                from_zone = tz.gettz("UTC")
                to_zone = tz.gettz("Europe/Berlin")
                t = t.replace(tzinfo=from_zone)
                t = t.astimezone(to_zone).weekday()
                out[part]["title"] = WDAY[t]
            else:
                t = int(time.time())
                t = datetime.utcfromtimestamp(t + i * 86400) 
                from_zone = tz.gettz("UTC")
                to_zone = tz.gettz("Europe/Berlin")
                t = t.replace(tzinfo=from_zone)
                out[part]["title"] = t.astimezone(to_zone).strftime("%d.%m.%Y")
            out[part]["icon"] = self.data.get_value("forecast/berlin/" + str(i) + "/weather_icon")
            out[part]["temp_max"] = self.data.get_value("forecast/berlin/" + str(i) + "/temperature_max")
            out[part]["temp_min"] = self.data.get_value("forecast/berlin/" + str(i) + "/temperature_min")
            out[part]["temp_feel_max"] = self.data.get_value("forecast/berlin/" + str(i) + "/temperature_max_feel")
            out[part]["temp_feel_min"] = self.data.get_value("forecast/berlin/" + str(i) + "/temperature_min_feel")
            out[part]["press"] = self.data.get_value("forecast/berlin/" + str(i) + "/pressure")
            out[part]["hum"] = self.data.get_value("forecast/berlin/" + str(i) + "/humidity")
            out[part]["wspeed"] = self.data.get_value("forecast/berlin/" + str(i) + "/wind_speed")
            out[part]["wdir"] = self.data.get_value("forecast/berlin/" + str(i) + "/wind_rose")
            out[part]["rain_likely"] = self.data.get_value("forecast/berlin/" + str(i) + "/rain_likely")
            out[part]["clouds"] = self.data.get_value("forecast/berlin/" + str(i) + "/clouds")
            out[part]["rain"] = self.data.get_value("forecast/berlin/" + str(i) + "/rain")
            i += 1
        return {"allowed": True, "data": out}