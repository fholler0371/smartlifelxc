# smartlife
# 05.2021
# 06.2021

import json
import time
from queue import Queue
import copy

from plugins2 import plugin_base

from weather.remote import remote
from weather.data import data
from weather.cards import cards

ALIVE_REMOTE = 2000

class integration(plugin_base):
    def __init__(self, smartlife, modul):
        global ALIVE_REMOTE
        plugin_base.__init__(self, smartlife, modul)
        ALIVE_REMOTE = 2 * self.cfg["remote"]["interval"] + 600
        self.alive = [ALIVE_REMOTE]
        self.queue = Queue()
        self.remote = remote(self.log, self.cfg["remote"]["interval"], self.cfg["remote"]["location"], self.cfg["remote"]["key"], self.queue,
            self.get_secret)
        self.data = data(self.log, self.smartlife, self.cfg["feel_temp"])
        self.cards = cards(self.log, self.data)

    def pl_get_menus(self, user, ip):
        self.log.debug("user")
        rights = self.rights(user, ip)
        out = []
        if rights["domain"]:
            if "read" in rights["sub"]:
                out.append({'label': 'Wetter', 'mod': 'weather', 'p1':'full'})
            elif "card" in rights["sub"]:
                out.append({'label': 'Wetter', 'mod': 'weather', 'p1':'app'})
        return out

    def pl_data(self, user, cmd, _in, ip):
        self.log.debug(cmd)
        rights = self.rights(user, ip)
        if rights["domain"]:
            if "card" in rights["sub"] or "read" in rights["sub"]:
                if cmd == "get_cards":
                    return self.cards.get_cards()
                elif cmd == "get_card":
                    return self.cards.get_card()
                elif cmd == "get_chart":
                    return self.data.get_chart(_in)
            if "read" in rights["sub"]:
                if cmd == "get_sensor":
                    return self.data.get_sensor()
            if "write" in rights["sub"]:
                if cmd == "set_sensor":
                    return self.data.set_sensor(_in)

    def new_msg(self, msg):
        self.log.debug("new_msg")
        if "homematic" in self.cfg:
            for entry in self.cfg["homematic"]:
                if msg["topic"] == entry["topic"]:
                    rec = copy.copy(msg)
                    rec["topic"] = entry["local"]   
                    self.data.new_data(rec)

    def run(self):
        self.log.info("run")
        global ALIVE_REMOTE
        self.remote.run()
        store = self.smartlife.get_plugin("data_store")
        if store:
            store.unsubscribe(self.name)
            if "homematic" in self.cfg:
                for entry in self.cfg["homematic"]:
                    store.subscribe(entry["topic"], self.name)
        while self.running:
            time.sleep(1)
            while not self.queue.empty():
                self.alive[0] = ALIVE_REMOTE
                entry = self.queue.get(block=False)
                found = False
                if "homematic" in self.cfg:
                    for rec in self.cfg["homematic"]:
                        if rec["local"] == entry["topic"]:
                            found = True
                if "feel_temp" in self.cfg:
                    for rec in self.cfg["feel_temp"]:
                        if rec["temp_feel"] == entry["topic"]:
                            found = True
                if not found:
                    self.data.new_data(entry)
                self.queue.task_done()

    def stop(self):
        self.log.info(self.name)
        self.running = False
        self.remote.stop()
        self.data.stop()


