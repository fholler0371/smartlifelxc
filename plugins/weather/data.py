# smartlife
# 05.2021
# 06.2021
# 09.2021

import os
import json
from threading import Timer
import math
import time

import history_event
import history_num
from database import database

SAVE_INTERVAL = 120

class data:
    def __init__(self, log, smartlife, feel_temp):
        log.debug("init")
        self.log = log
        global SAVE_INTERVAL
        self.values = {}
        self.jname = smartlife.db_path + "/weather_data.json"
        if os.path.isfile(self.jname):
            f = open(self.jname, "r")
            self.values = json.loads(f.read())
            f.close()
        self.name_sent = smartlife.db_path + "/weather_sent.json"
        self.sent = []
        if os.path.isfile(self.name_sent):
            f = open(self.name_sent, "r")
            self.sent = json.loads(f.read())
            f.close()
        self.name_num = smartlife.db_path + "/weather_history_num.json"
        self.history_num = []
        if os.path.isfile(self.name_num):
            f = open(self.name_num, "r")
            self.history_num = json.loads(f.read())
            f.close()
        self.name_event = smartlife.db_path + "/weather_history_event.json"
        self.history_event = []
        if os.path.isfile(self.name_event):
            f = open(self.name_event, "r")
            self.history_event = json.loads(f.read())
            f.close()
        self.db_event = history_event.db(self.log, smartlife.db_path + "/weather_history_event_$$.sqlite")
        self.db_num = history_num.db(self.log, smartlife.db_path + "/weather_history_$$.sqlite", self.get_num_data)
        self.timer = Timer(SAVE_INTERVAL, self.tick)
        self.timer.start()
        self.db_num.start()
        self.feel_temp = feel_temp

    def new_data(self, data):
        self.log.debug(data)
        if data["topic"] in self.values:
            old = self.values[data["topic"]]["value"]
        else:
            old = None
        self.db_event.store(old, data, self.history_event)
        if data["topic"] in self.values:
            if not( data["value"] == self.values[data["topic"]]["value"]):
                self.values[data["topic"]]["value"] = data["value"]
                self.values[data["topic"]]["lc"] = data["ts"]
            self.values[data["topic"]]["ts"] = data["ts"]
        else:
            rec = {"friendly": "weather/" + data["topic"], "value": data["value"], "ts": data["ts"], "lc": data["ts"]}
            self.values[data["topic"]] = rec

    def get_sensor(self):
        self.log.debug("get_sensor")
        out = []
        for entry in self.values:
            rec = {"topic": entry, "value": self.values[entry]["value"], "entry": self.values[entry]["friendly"], 
                   "sent": False, "hist_num": False, "hist_event": False}
            if entry in self.sent:
                rec["sent"] = True
            if entry in self.history_num:
                rec["hist_num"] = True
            if entry in self.history_event:
                rec["hist_event"] = True
            out.append(rec)
        out = sorted(out, key=lambda x: x["entry"], reverse=False)
        return {"allowed": True, "data": out}

    def set_sensor(self, data):
        self.log.debug(data)
        if data["field"] == "entry":
            if data["topic"] in self.values:
                self.values[data["topic"]]["friendly"] = data["value"]
        if data["field"] == "sent":
            if data["value"]:
                if not(data["topic"] in self.sent):
                    self.sent.append(data["topic"])
            else:
                if data["topic"] in self.sent:
                    self.sent.remove(data["topic"])
            f = open(self.name_sent, "w")
            f.write(json.dumps(self.sent))
            f.close()
        if data["field"] == "hist_num":
            if data["value"]:
                if not(data["topic"] in self.history_num):
                    self.history_num.append(data["topic"])
            else:
                if data["topic"] in self.history_num:
                    self.history_num.remove(data["topic"])
            f = open(self.name_num, "w")
            f.write(json.dumps(self.history_num))
            f.close()
        if data["field"] == "hist_event":
            if data["value"]:
                if not(data["topic"] in self.history_event):
                    self.history_event.append(data["topic"])
            else:
                if data["topic"] in self.history_event:
                    self.history_event.remove(data["topic"])
            f = open(self.name_event, "w")
            f.write(json.dumps(self.history_event))
            f.close()
        return {"allowed": True, "data": {}}

    def tick(self):
        self.log.debug("tick")
        self.timer = Timer(SAVE_INTERVAL, self.tick)
        self.timer.start()
        self.save()

    def save(self):
        self.log.debug("save")
        f = open(self.jname, "w")
        f.write(json.dumps(self.values))
        f.close()

    def get_num_data(self):
        self.log.debug("get_num_data")
        # berechnung feel temp 
        if self.feel_temp:
            for entry in self.feel_temp:
                t = self.get_value(entry["temp"])
                if t == "":
                    t = 0
                f = t
                if t > 20:
                    h = self.get_value(entry["hum"])
                    if h == "":
                        h = 0
                    f = -8.784695 + 1.61139411*t + 2.338549*h - 0.14611605*t*h - 0.012308094*t*t - 0.016424828*h*h
                    f += 0.002211732*t*t*h + 0.00072546*t*h*h - 0.000003582*t*t*h*h
                if t < 10:
                    w = self.get_value(entry["wind_speed"])
                    if w == "":
                        w = 0
                    f = 13.12 + 0.6215*t - 11.37*math.pow(w,0.16) + 0.3965*t*math.pow(w,0.16)
                self.new_data({"topic": entry["temp_feel"], "value": f, "ts": int(time.time())})
        for entry in self.history_num:
            if entry in self.values:
                self.db_num.add(entry, self.values[entry]["value"])

    def get_value(self, topic):
        self.log.debug(topic)
        val = ""
        if topic in self.values:
            return self.values[topic]["value"]
        return val

    def get_chart(self, data):
        self.log.debug(data)
        faktor = 1
        out = {"title": "", "buttons": "D:W:M:Y:A", "data": [], "unit": "%", "pre": 0, "label": ""}
        if data["sensor"].find("temperature") > -1:
            out["unit"] = "°C"
            out["pre"] = 1
            out["label"] = "Temperatur"
        elif data["sensor"].find("humidity") > -1:
            out["label"] = "Luftfeuchtigkeit außen"
        elif data["sensor"].find("lux") > -1:
            out["unit"] = "lux"
            out["label"] = "Helligkeit außen"
        elif data["sensor"].find("clouds") > -1:
            out["label"] = "Wolken"
        elif data["sensor"].find("pressure") > -1:
            out["unit"] = "mBar"
            out["label"] = "Luftdruck"
        elif data["sensor"].find("rain") > -1:
            out["unit"] = "mm/h"
            out["label"] = "Regen"
        elif data["sensor"].find("wind_dir") > -1:
            out["unit"] = "°"
            out["label"] = "Windrichtung"
        elif data["sensor"].find("wind_speed") > -1:
            out["unit"] = "km/h"
            out["label"] = "Windgeschwindigkeit"
        if data["sensor"] == "current/berlin/temperature":
            out["label"] = out["label"] + " außen"
        elif data["sensor"] == "current/berlin/temperature_feel":
            out["label"] = "gefühlte " + out["label"]
        out["title"] = out["label"] + " [" + out["unit"] +"]" 
        out["buttons"] = out["buttons"].replace(data["mode"], data["mode"]+"!")
        out["data"] = self.db_num.get_chart(data["sensor"], data["mode"], faktor=faktor)
        return {"allowed": True, "data": out}

    def stop(self):
        self.log.debug("stop")
        if self.timer:
            self.timer.cancel()
        self.save()
        self.db_num.stop()