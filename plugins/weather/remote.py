# smartlife
# 05.2021

import time
from threading import Timer, Thread

import requests

class weatherapi(Thread):
    def __init__(self, log, cities, key, queue):
        Thread.__init__(self)
        self.daemon = True
        self.name = "weatherapi"
        log.debug("init")
        self.log = log
        self.cities = cities
        self.key = key
        self.queue = queue

    def run(self):
        self.log.debug("run")
        server = "https://api.weatherbit.io/v2.0"
        current_endpoint = f'{server}/current'
        daily_endpoint = f'{server}/forecast/daily'
        for loc in self.cities:
            options = str(f'&postal_code={loc["postalcode"]}&country={loc["country"]}&lang={loc["language"]}')
            url = str(f'{current_endpoint}?key={self.key}{options}')
            try: 
                r = requests.get(url)
                if r.status_code == 200:
                    self.decode_current(r.json(), loc["name"])
            except Exception as e:
                self.log.error(repr(e))
                self.log.error("do not get current weather")
            url = str(f'{daily_endpoint}?key={self.key}{options}')
            try: 
                r = requests.get(url)
                if r.status_code == 200:
                    self.decode_daily(r.json(), loc["name"])
            except Exception as e:
                self.log.error(repr(e))
                self.log.error("do not get current weather")

    def decode_daily(self, data, city):
        self.log.debug(data)
        day = -1
        city = city.lower().replace(" ", "_").replace("/", "_")
        for rec in data["data"]:
            day += 1
            for entry in rec:
                ts = rec["ts"]
                label = ""
                if entry == "wind_cdir":
                    label = "wind_rose"
                if entry == "rh":
                    label = "humidity"
                if entry == "pres":
                    label = "pressure"
                if entry == "high_temp":
                    label = "temperature_high"
                if entry == "ozone":
                    label = "ozone"
                if entry == "vis":
                    label = "visibility"
                if entry == "clouds":
                    label = "clouds"
                if entry == "app_min_temp":
                    label = "temperature_min_feel"
                if entry == "app_max_temp":
                    label = "temperature_max_feel"
                if entry == "min_temp":
                    label = "temperature_min"
                if entry == "max_temp":
                    label = "temperature_max"
                if entry == "wind_spd":
                    label = "wind_speed"
                    rec[entry] = rec[entry]*3.6 #konvert zu km/h
                if entry == "pop":
                    label = "rain_likely"
                if entry == "snow":
                    label = "snow"
                if entry == "uv":
                    label = "uv" #Maximum UV Index (0-11+)
                if entry == "weather":
                    label = "weather_icon"
                    rec[entry] = rec[entry]["icon"]
                if entry == "wind_dir":
                    label = "wind_dir"
                if entry == "precip":
                    label = "rain"  #mm/hr
                if not (entry in ["moonrise_ts", "sunset_ts", "wind_gust_spd", "snow_depth", "ts", "sunrise_ts", "wind_cdir_full", "slp", "moon_phase", 
                    "datetime", "moon_phase_lunation", "valid_date", "dewpt", "max_dhi", "clouds_hi", "moonset_ts", "date_time", "temp", "clouds_mid",
                    "clouds_low", "low_temp"]):
                    if label != "":
                        topic = "forecast/" + city + "/" + str(day) + "/" + label
                        out = {"topic": topic, "value": rec[entry], "ts": ts}
                        self.queue.put(out)
                    else:
                        self.log.debug("unkown", entry, rec[entry])
                        self.log.debug(topic)

    def decode_current(self, data, city):
        self.log.debug(data)
        city = city.lower().replace(" ", "_").replace("/", "_")
        if data["count"] > 0:
            rec = data["data"][0]
            ts = rec["ts"]
            city = "no_city"
            if "city_name" in rec:
                city = rec["city_name"].lower().replace(" ", "_").replace("/", "_")
            for entry in rec:
                label = ""
                if entry == "rh":
                    label = "humidity"
                if entry == "pres":
                    label = "pressure"
                if entry == "clouds":
                    label = "clouds"
                if entry == "wind_spd":
                    label = "wind_speed"
                    rec[entry] = rec[entry]*3.6 #konvert zu km/h
                if entry == "wind_cdir":
                    label = "wind_rose"
                if entry == "wind_dir":
                    label = "wind_dir"
                if entry == "vis":
                    label = "visibility" #km
                if entry == "snow":
                    label = "snow"
                if entry == "uv":
                    label = "uv"
                if entry == "precip":
                    label = "rain" #mm/hr
                if entry == "aqi":
                    label = "air_quality_index" #[US - EPA standard 0 - +500]
                if entry == "weather":
                    label = "weather_icon"
                    rec[entry] = rec[entry]["icon"]
                if entry == "temp":
                    label = "temperature"
                if entry == "app_temp":
                    label = "temperature_feel"
                if not( entry in ["pod", "lon", "timezone", "country_code", "ob_time", "ts", "solar_rad", "state_code", "city_name", "wind_cdir_full",
                    "slp", "h_angle", "sunset", "dni", "dewpt", "sunrise", "ghi", "dhi", "lat", "datetime", "station", "elev_angle"] ):
                    if label != "":
                        out = {"topic": "current/" + city + "/" + label, "value": rec[entry], "ts": ts}
                        self.queue.put(out)
                    else:
                        self.log.debug("unkown", entry, rec[entry])


class remote:
    def __init__(self, log, poll_time, cities, key, queue, get_secret):
        log.debug("init")
        self.log = log
        self.th_req = None
        self.poll_time = poll_time
        self.cities = cities
        self.key = key
        self.get_secret = get_secret
        self.queue = queue
        self.running = True
        self.th_req = Timer(60, self.tick)
        self.th_req.start()

    def tick(self):
        self.log.debug("tick")
        if self.running:
            self.th_req = Timer(self.poll_time, self.tick)
            self.th_req.start()
        if "!" in self.key:
            self.key = self.get_secret(self.key)
        api= weatherapi(self.log, self.cities, self.key, self.queue)
        api.start()

    def run(self):
        self.log.debug("run")
        self.th_req = Timer(self.poll_time, self.run)

    def stop(self):
        self.log.debug("stop")
        self.running = False
        if self.th_req:
            self.th_req.cancel()
