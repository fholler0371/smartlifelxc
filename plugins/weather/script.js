define(['module', 'packery', 'jqxtabs', 'jqxdata', 'jqxgrid', 'jqxgrid_selection', 'jqxcheckbox', 'jqxgrid_edit'], 
        function(module, Packery) {
    var mod = {
      t_update_1: null,
      t_update_2: null,
      t_update_3: null,
      wdata_last : 0,
      wdata_call : 0,
      wdata : undefined,
      map : undefined,
      mapmarker : undefined,
      year : 0,
      init : function() {
        window.module.weather.sub = window.module.weather.init_data.p1
        var sub = window.module.weather.sub
        if (sub == "app") {
          html = '<div id="weather_cards" style="width:100%; height:100%; margin: 10px;" class="jqx-widget-content-custom-scheme"></div>'
          $('#client_area').html(html)
        } else {
          html = '<div style="width:100%; height:100%;"><div id="weather_tab"><ul><li>Karten</li><li>Sensoren</li></ul>'
          html += '<div><div id="weather_cards" style="width:100%; height:100%;"></div></div>'
          html += '<div><div id="weather_sensor" style="width:100%; height:100%;"><div id="weather_sensor_grid" style="width:100%; height:100%;"></div></div></div>'
          $('#client_area').html(html+'</div>')
          $(window).off('resize', window.module.weather.resize)
          $(window).on('resize', window.module.weather.resize)
          window.module.weather.resize()
          $('#weather_tab').jqxTabs({ width: '100%', height: '100%', position: 'top'})
          $('#weather_tab').on('selected', function(ev) {
            var selectedTab = ev.args.item
            window.module.weather.call_tab(selectedTab)
          })
        }
        window.module.weather.call_tab(0)
      },
      check_data : function() {
        var now = new Date()
        var t = Math.round(now.getTime() / 1000)
        if (t - window.module.weather.wdata_last > 60) {
          if (t - window.module.weather.wdata_call > 10) {
            window.module.weather.wdata_call = t
            window.smfetch('/api/weather', {cmd: 'get_card'}).then((data) => {
              if (data.error == undefined) {
                window.module.weather.wdata_last = t
                window.module.weather.wdata = data.data
              }
            })
          }
        }
      },
      update_1 : function() {
        clearTimeout(window.module.weather.t_update_1)
        window.module.weather.check_data()
        var cards =  $("#weather_cards").find(".cards_weather")
        var len = cards.length
        if (window.module.weather.wdata != undefined) {
          var ele = $(cards[0])
          var d = window.module.weather.wdata.current
          if (JSON.stringify(ele.data('last')) != JSON.stringify(d)) {
            ele.data('last', d)
            html = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">'
            html += 'Aktuelles Wetter</div>'
            html += '<img src="/lib/img/weather/' + d.icon + '.png" style="position: relative; top: 25px;">'
            html += '<div style=" position: relative; right: 5px; float: right; top: 34px;"><span  style="font-size: 250%; font-weight: 600;"'
            if (d.temp_chart != undefined) {
              html += ' class="cards_chart" data-chart="/api/weather:'+d.temp_chart+'"'
            }
            html += '>'+d.temp.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' °C'
            html += '</span><span'
            if (d.temp_feel_chart.length != undefined) {
              html += ' class="cards_chart"  data-chart="/api/weather:'+d.temp_feel_chart+'"'
            }
            html += '>('+d.temp_feel.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' °C)</span></div>'
            html += '<div  style="position: absolute; right: 5px; top: 85px;"><span'
            if (d.press_chart.length > 0) {
              html += ' class="cards_chart"  data-chart="/api/weather:'+d.press_chart+'"'
            }
            html += '>'+d.press.toLocaleString('de-DE', {maximumFractionDigits: 0})+' mBar </span>/ <span'
            if (d.hum_chart != undefined) {
              html += ' class="cards_chart" data-chart="/api/weather:'+d.hum_chart+'"'
            }
            html += '>'+d.hum.toLocaleString('de-DE', {maximumFractionDigits: 0})+' %</span></div>'
            html += '<div  style="position: absolute; right: 5px; top: 105px;"><span'
            if (d.wspeed_chart.length > 0) {
              html += ' class="cards_chart"  data-chart="/api/weather:'+d.wspeed_chart+'"'
            }
            html += '>'+d.wspeed.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})
            html += ' km/h </span>/<span'
            if (d.wdir_chart.length > 0) {
              html += ' class="cards_chart"  data-chart="/api/weather:'+d.wdir_chart+'"'
            }
            html += '> '+  d.wdir+ '</span></div>'
            html += '<div  style="position: absolute; right: 5px; top: 125px;"><span'
            if (d.clouds_chart.length > 0) {
              html += ' class="cards_chart"  data-chart="/api/weather:'+d.clouds_chart+'"'
            }
            html += '>Wolken: '+d.clouds.toLocaleString('de-DE', {maximumFractionDigits: 0})
            html += ' % </span>/<span'
            if (d.rain_chart.length > 0) {
              html += ' class="cards_chart"  data-chart="/api/weather:'+d.rain_chart+'"'
            }
            if (d.rain == null) {
              d.rain = 0
            }
            html += '> Regen: '+ d.rain.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+ ' mm/h</span></div>'
            html += '<div  style="position: absolute; right: 5px; top: 145px;"><span'
            if (d.lux_chart.length > 0) {
              html += ' class="cards_chart"  data-chart="/api/weather:'+d.lux_chart+'"'
            }
            html += '> Helligkeit: '
            html += d.lux.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})
            html += ' lux</span></div>'
            ele.html(html)
          }
          window.module.weather.t_update_1 = setTimeout(window.module.weather.update_1, 15000)
        } else {
          window.module.weather.t_update_1 = setTimeout(window.module.weather.update_1, 250)
        }
      },
      update_2 : function() {
        clearTimeout(window.module.weather.t_update_2)
        window.module.weather.check_data()
        var eles =  $("#weather_cards").find(".cards_weather_pre")
        var len = eles.length
        if (window.module.weather.wdata != undefined) {
          for (var i=0; i<len; i++) {
            var ele = $(eles[i])
            var id = ele.data('card').split('_')[2]
            var d = window.module.weather.wdata['pre_' + id]
            if (JSON.stringify(ele.data('last')) != JSON.stringify(d)) {
              ele.data('last', d)
              html = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">'
              html += d.title + '</div>'
              html += '<img src="/lib/img/weather/' + d.icon + '.png" style="position: relative; top: 25px;">'
              html += '<div style=" position: absolute; right: 5px; float: right; top: 35px;"><span  style="font-size: 150%; font-weight: 600;">'
              html += d.temp_max.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' °C'
              html += ' - '+d.temp_min.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' °C</span></div>'
              html += '<div style=" position: absolute; right: 5px; float: right; top: 60px;">('
              html += d.temp_feel_max.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' °C'
              html += ' - '+d.temp_feel_min.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+' °C)</div>'
              html += '<div  style="position: absolute; right: 5px; top: 78px;">'+d.press.toLocaleString('de-DE', {maximumFractionDigits: 0})+' mBar / '
              html += d.hum.toLocaleString('de-DE', {maximumFractionDigits: 0})+' %</div>'
              html += '<div  style="position: absolute; right: 5px; top: 96px;">'
              html += d.wspeed.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})
              html += ' km/h / '+  d.wdir+ '</div>'
              html += '<div  style="position: absolute; right: 5px; top: 114px;">Regenwahr.: '
              html += d.rain_likely.toLocaleString('de-DE', {maximumFractionDigits: 0}) + ' %</div>'
              html += '<div  style="position: absolute; right: 5px; top: 132px;"> Wolken: '+d.clouds.toLocaleString('de-DE', {maximumFractionDigits: 0})
              html += ' % / Regen: '+ d.rain.toLocaleString('de-DE', {minimumFractionDigits: 1, maximumFractionDigits: 1})+ ' mm/h</div>'

              ele.html(html)
            }
          }
          window.module.weather.t_update_2 = setTimeout(window.module.weather.update_2, 30000)
        } else {
          window.module.weather.t_update_2 = setTimeout(window.module.weather.update_2, 250)
        }
      },
      update_3 : function() {
        clearTimeout(window.module.weather.t_update_3)
        window.module.weather.check_data()
        var ele =  $("#weather_cards").find(".cards_weather_phonecalls")
        if (window.module.weather.wdata != undefined) {
          var d = window.module.weather.wdata.phonecalls
          if (JSON.stringify(ele.data('last')) != JSON.stringify(d)) {
            ele.data('last', d)
            html = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">'
            html += 'Anrufe</div>'
            var len = d.length
            for (var i=0; i<len; i++) {
              html += '<div style="position: absolute; top: ' + (i*20+25) + 'px; left: 10px"><img src="/lib/img/telephone_'
              if (d[i].type == 1) {html += 'blue'}  
              if (d[i].type == 2) {html += 'red'} 
              if (d[i].type == 3) {html += 'green'} 
              html += '.svg" style="height: 18px; width: 18px;"></div>'
              html += '<div style="position: absolute; top: ' + (i*20+25) + 'px; left: 35px"><b>' + d[i].number + '</b></div>' 
              html += '<div style="position: absolute; top: ' + (i*20+25) + 'px; right: 10px">' + d[i].time + '</div>' 
            }
            ele.html(html)
          }
          window.module.weather.t_update_3 = setTimeout(window.module.weather.update_3, 30000)
        } else {
          window.module.weather.t_update_3 = setTimeout(window.module.weather.update_3, 250)
        }
      }, 
      get_card : function(data, div) {
        if (data["card"]=='weather') {
          if (div.find(".cards_"+data["card"]).length == 0) {
            var html = $('<div class="cards_state cards_'+data["card"]+' card card_h2"><span></span></div>')
            html.data('card', data.card)
            div.append(html)
          } else {
//            console.log(data)
          }
          window.module.weather.update_1()
        } else if (data["card"].startsWith("weather_pre_")) {
          if (div.find(".cards_"+data["card"]).length == 0) {
            var html = $('<div class="cards_weather_pre card card_h2"><span></span></div>')
            html.data('card', data.card)
            div.append(html)
          } else {
//            console.log(data)
          }
          window.module.weather.update_2()
        }
      },
      create_cards : function() {
        var ok = 1
        var cards = window.module.weather.cards
        var l = cards.length
        for (var i=0; i<l; i++) {
           if (!(typeof(window.module[cards[i].mod].get_card) === 'function')) {
             console.log("Modul not found: "+cards[i].mod)
             ok = 0
           }
        }
        if (ok == 1) {
          for (var i=0; i<l; i++) {
            window.module[cards[i].mod].get_card(cards[i], $("#weather_cards"))
            require( [ 'jquery-bridget/jquery-bridget' ], function( jQueryBridget ) {
              jQueryBridget( 'packery', Packery, $ );
              var grid = $('#weather_cards').packery({
                itemSelector: '.card',
                gutter: 10
              })
            })
          }
        } else {
          console.log("recheck")
        }
      },
      call_tab: function(id) {
        if (id == 0) {
          window.smfetch('/api/weather', {cmd: 'get_cards'}).then((data) => {
            if (data.error == undefined) {
              window.module.weather.cards = data.data
              window.module.weather.cards_check_mod = 10
              window.module.weather.create_cards()
            }
          })
        } else if (id == 1) {
          window.smfetch('/api/weather', {cmd: 'get_sensor'}).then((data) => {
            if (data.error == undefined) {
              var source = {
                localdata: data.data,
                datatype: "array",
                datafields: [
                  { name: 'topic', type: 'string'},
                  { name: 'value', type: 'string'},
                  { name: 'entry', type: 'string'},
                  { name: 'sent', type: 'boolean'},
                  { name: 'hist_num', type: 'boolean'},
                  { name: 'hist_event', type: 'boolean'}
                ]                
              }
              var dataAdapter = new $.jqx.dataAdapter(source, {
                downloadComplete: function (data, status, xhr) { },
                loadComplete: function (data) { },
                loadError: function (xhr, status, error) { }
              })
              var col = [
                { text: 'Sensor', datafield: 'topic', width: 350, editable: false},
                { text: 'Bezeichnung', datafield: 'entry'},
                { text: 'Wert', datafield: 'value', width: 300, editable: false, cellsalign: 'right'},
                { text: 'Senden', datafield: 'sent', width: 75, columntype: 'checkbox'},
                { text: 'History', datafield: 'hist_num', width: 75, columntype: 'checkbox'},
                { text: 'History Event', datafield: 'hist_event', width: 125, columntype: 'checkbox'}
              ]
              $("#weather_sensor_grid").jqxGrid({
                width: '100%',
                height: '100%',
                source: dataAdapter,
                columns: col,
                editable: true
              })
              $("#weather_sensor_grid").off('cellendedit')
              $("#weather_sensor_grid").on('cellendedit', function (event) {
                var args = event.args;
                window.smfetch('/api/weather', {cmd: 'set_sensor', data: {
                  field: args.datafield, value: args.value, topic: args.row.topic
                }}).then((data) => {})
              })
              $("#weather_sensor").css('padding', 0).css('overflow', 'hidden')
            }
          })
        } 
      },
      stop : function() {
        clearTimeout(window.module.weather.t_update_1)
        clearTimeout(window.module.weather.t_update_2)
        clearTimeout(window.module.weather.t_update_3)
        if (window.module.weather.map != undefined) {
          window.module.weather.map.remove()
          window.module.weather.map = undefined
        }
      },
      resize: function() {
        $('#client_area').children().height($('#client_area').height())
      }
    }
    mod['init_data'] = window.module_const[module.id]
    window.module.weather = mod
    return mod
  })
