# smartlife
# 04.2021
# 05.2021

from plugins2 import plugin_base
import requests
import json
import time

ALIVE=30*24*3600

class integration(plugin_base):
    def __init__(self, smartlife, modul):
        plugin_base.__init__(self, smartlife, modul)
        global ALIVE
        self.alive = [ALIVE]

    def pl_data(self, user, cmd, _in, ip):
        self.log.debug(cmd)
        global ALIVE
        self.alive[0] = ALIVE
        auth = self.smartlife.get_plugin("authentication")
        rights = auth.get_rights(user, self.manifest["domain"], self.name, ip)
        if rights["modul"]:
            if cmd == "get_menu":
                _out = []
                for host in self.cfg_master["hosts"]:
                    run = False
                    for plugin in host["plugins"]:
                        if "frontend" in plugin and plugin["frontend"] and not run:
                            url = "http://" + host["ip"]  + ":" + str(self.cfg_master["netcom_port"]) + "/netcom/menu"
                            data = {"ip": ip, "user": user}
                            run = True
                            retry = 5
                            error = True
                            while error and retry > 0:
                                error = False
                                retry -= 1
                                try:
                                    r = requests.post(url, data=json.dumps(data), timeout=5)
                                    if r.status_code == 200:
                                        for entry in r.json():
                                            _out.append(entry)
                                except requests.exceptions.Timeout:
                                    self.log.error("Connection Timeout Menu: " + host["ip"])
                                    error = True
                                    time.sleep(1)
                                except  Exception as e:
                                    error = True
                                    time.sleep(1)
                                    self.log.error(repr(e))
                return {"allowed": True, "data": _out}
            elif cmd == "keep_alive":
                return {"allowed": True, "data": {}}
            elif cmd == "check_logout":
                if auth.is_local(ip):
                    return {"allowed": True, "data": {"keep": True}}
                else:
                    return {"allowed": True, "data": {"keep": False}}
            else:
                self.log.error("Unknow command: " + cmd)
        return {"allowed": False}
