# smartlife
# 05.2021

import time 

from import_csv import csv

class importer:
    def __init__(self, log, smartlife, db):
        log.debug("init")
        self.log = log
        self.smartlife = smartlife
        self.db = db

    def upload_prefetch(self, data):
        self.log.debug("prefetch")
        ok = True
        if not ("csv" == data["name"].split(".")[-1].lower()):
            ok = False
        if data["size"] > 100000:
            ok = False
        if ok:
            _data = {"mode": "stream", "modul": "fin_card_barclay", "func": "uploaded_import", "valid": int(time.time()+300)}
            netcom = self.smartlife.get_plugin("netcom")
            _data = netcom.encode_data(_data)
            if _data == "":
                return {"allowed": True, "data": {"ok": False}}
            return {"allowed": True, "data": {"ok": True, "data": _data}}
        else:
            return {"allowed": True, "data": {"ok": False}}

    def load_data(self, data):
        self.log.error("load_data")
        try:
            tab = csv(data.decode("utf-8"), header_start="\"Kreditkartennummer (teilmaskiert)")
            i = 0
            while i < tab.count:
                _date = tab.get_date(i, "Buchungsdatum")
                if _date != '0000-00-00':
                    _text = tab.get_text(i, "Händler (Name, Stadt & Land)")
                    _value = tab.get_float(i, "Betrag in Euro")
                    if _value != 0:
                        self.db.add(_date, _text, _value)
                i += 1
            self.db.do_calc()
        except Exception as e:
            self.log.error(repr(e))
            return False
        return True
