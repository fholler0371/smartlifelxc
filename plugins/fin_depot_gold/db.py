# smartlife
# 06.2021

from threading import Thread, Timer
from datetime import datetime, timedelta
import time
import random

from database import database

class history(Thread):
    def __init__(self, log, name, book, master, mod):
        Thread.__init__(self)
        log.debug("init")
        self.log = log
        self.master = master
        sql = "CREATE TABLE IF NOT EXISTS history (date string, value numeric, interest numeric)"
        self.hist = database(self.log, name, sql)
        self.book = database(self.log, book)
        self.mod = mod

    def run(self):
        self.log.debug("run")
        self.hist.open()
        self.book.open()
        maxDate = datetime.now()
        row = self.hist.fetchone("select date from history order by date desc limit 1")
        if row:
            minDate = datetime.strptime(row[0], "%Y-%m-%d")
        else:
            row = self.book.fetchone("select date from bookings order by date asc limit 1")
            if not row:
                self.book.close()
                self.hist.close()
                return
            minDate = datetime.strptime(row[0], "%Y-%m-%d")
        # setze min 31 Tage vor
        minDate = minDate - timedelta(days=31)
        # check minDate gegen Daten
        row = self.book.fetchone("select date from bookings order by date asc limit 1")
        if not row:
            self.book.close()
            self.hist.close()
            return
        d = datetime.strptime(row[0], '%Y-%m-%d')
        if d > minDate:
            minDate = d
        # loop
        d = minDate
        sql4 = "update history set value='%s', interest='%s' where date='%s'"
        sql5 = "insert into history (value, interest, date) values ('%s', '%s', '%s')"
        while d <= maxDate:
            row = self.book.fetchone("select sum(value), sum(count*weight) from bookings where date <= '%s'" % (d.strftime('%Y-%m-%d'), ))
            val = 0
            w = 0
            invest = 0
            if row and not(row[0] == None):
                val = float("%.2f" % round(row[0], 2))
                w = row[1]
            if self.mod:
                invest = w * self.mod.get_price('GOLD', d)
            if self.hist.fetchone("select date from history where date = '%s'"  % (d.strftime('%Y-%m-%d'), )):
                sql =  sql4 % ("%.2f" % round(val, 2), "%.2f" % round(invest - val, 2), d.strftime('%Y-%m-%d'), )
            else:
                sql =  sql5 % ("%.2f" % round(val, 2), "%.2f" % round(invest - val, 2), d.strftime('%Y-%m-%d'), )
            self.hist.commit(sql)
            d = d + timedelta(days=1)
        self.hist.commit()
        self.book.close()
        self.hist.close()
        if self.master:
            self.master.calc_hist()

class db:
    def __init__(self, log, smartlife, master):
        log.debug("init")
        self.log = log
        self.smartlife = smartlife
        self.master = master
        sql = "CREATE TABLE IF NOT EXISTS bookings (id integer PRIMARY KEY AUTOINCREMENT, date date DEFAULT '1900-01-01',  description text,"
        sql += " count integer DEFAULT 1, weight numeric DEFAULT 1, value numeric DEFAULT 0)"
        self.book = database(self.log, smartlife.db_path + "/fin_depot_gold.sqlite", sql)
        self.hist = database(self.log, smartlife.db_path + "/fin_depot_gold_history.sqlite")
        t = (int(time.time()/86400)+1)*86400-time.time()+60+300*random.random()
        self.timer_calc = Timer(t, self.tick_calc)
        self.timer_calc.start()
        self.th = None

    def tick_calc(self):
        self.log.debug("tick_calc")
        t = (int(time.time()/86400)+1)*86400-time.time()+60+300*random.random()
        self.timer_calc = Timer(t, self.tick_calc)
        self.timer_calc.start()
        self.do_calc()

    def get_card(self):
        self.log.debug("get_card")
        out = {"sum": {"invest": 0, "sum": 0}}
        self.book.open()
        row = self.book.fetchone("select sum(value), sum(count*weight) from bookings") 
        w = 0
        if row and not(row[0] == None):
            out['sum']['invest'] = float("%.2f" % round(row[0], 2))
            w = row[1]
        mod = self.smartlife.get_plugin('fin_shares')
        if mod:
            out['sum']['sum'] = w * mod.get_price('GOLD', datetime.today())
        self.book.close()
        return out

    def get_book(self, data):
        self.log.debug(data)
        out = {"records": 0, "rows": []}
        self.book.open()
        row = self.book.fetchone("select count(id) from bookings")
        if row:
            out["records"] = row[0]
        sql = "select id, date, description, count, weight, value from bookings order by date desc limit %s, %s"
        for row in self.book.fetchall(sql % (str(data["page"]*data['pagesize']), str(data['pagesize']), )):
            out["rows"].append({'id': row[0], 'date': row[1], 'text': row[2], 'count': row[3], 'weight': row[4], 'amount': row[5]})
        self.book.close()
        return {"allowed": True, "data": out}

    def add(self, date, name, b_text, v_text, value):
        self.log.debug(date)
        self.book.open()
        sql = "select id from bookings where date='%s' and name='%s' and text='%s' and verwendungszweck='%s' and value='%s'"
        row = self.book.fetchone(sql % (date, name, b_text, v_text, str(value), ))
        if row == None:
            sql = "insert into bookings (date, name, text, verwendungszweck, value) values ('%s', '%s', '%s', '%s', '%s' ) "
            self.book.commit(sql % (date, name, b_text, v_text, str(value), ))
        self.book.close()

    def do_calc(self):
        self.log.debug("do_calc")
        if not(self.th) or not( self.th.is_alive()):
            self.th = history(self.log, self.smartlife.db_path + "/fin_depot_gold_history.sqlite", 
                self.smartlife.db_path + "/fin_depot_gold.sqlite", self.smartlife.get_plugin(self.master), self.smartlife.get_plugin("fin_shares"))
            self.th.start()
        return {"allowed": True, "data": {}}

    def get_history(self):
        self.log.debug("get_history")
        out = []
        self.hist.open()
        for entry in self.hist.fetchall("select date, value, interest from history order by date asc"):
            out.append({'date': entry[0], 'value': entry[1], 'interest': entry[2]})
        self.hist.close()
        return {"allowed": True, "data": out}

    def new_entry(self):
        self.log.debug("new_entry")
        self.book.open()
        self.book.commit("insert into bookings (date, description) values ('%s', 'Neue Münze')" % (datetime.today().strftime("%Y-%m-%d")))
        self.book.close()
        return {"allowed": True, "data": []}

    def set_data(self, data):
        self.log.debug("set_data")
        field = data['field']
        if field == "text":
            field = "description"
        elif field == "amount":
            field = "value"
        self.book.open()
        self.book.commit("update bookings set %s='%s' where id='%s'" % (field, data["value"], data["id"]))
        self.book.close()
        return {"allowed": True, "data": []}

    def stop(self):
        self.log.debug("stop")
        if self.timer_calc:
            self.timer_calc.cancel()
            