# smartlife
# 06.2021
# 09.2021

import json
from threading import Thread, Timer
from random import random

from plugins2 import plugin_base

from fin_depot_gold.cards import cards
from fin_depot_gold.db import db
from fin_depot_gold.importer import importer

class integration(plugin_base):
    def __init__(self, smartlife, modul):
        global ALIVE_WWW
        plugin_base.__init__(self, smartlife, modul)
        self.alive = [999999999]
        self.db = db(self.log, smartlife, self.cfg["master"])
        self.cards = cards(self.log, self.db)
        self.importer = importer(self.log, smartlife, self.db)
        self.timer_hist = None 

    def pl_data(self, user, cmd, _in, ip):
        self.log.debug(cmd)
        rights = self.rights(user, ip)
        if rights["domain"]:
            if "card" in rights["sub"] or "read" in rights["sub"]:
                if cmd == "get_cards":
                    return self.cards.get_cards()
                elif cmd == "get_card":
                    return self.cards.get_card()
            if "read" in rights["sub"]:
                if cmd == "get_book":
                    return self.db.get_book(_in)
                elif cmd == "get_history":
                    return self.db.get_history()
            if "write" in rights["sub"]:
                if cmd == "new_entry":
                    return self.db.new_entry()
                elif cmd == "set_data":
                    return self.db.set_data(_in)
                elif cmd == "do_calc":
                    return self.db.do_calc()
        return {"allowed": False}

    def calc_hist(self):
        self.log.debug("calc_hist")
        if self.timer_hist:
            self.timer_hist.cancel()
        self.timer_hist = Timer(random()*15, self.db.do_calc)
        self.timer_hist.start()

    def stop(self):
        self.log.info(self.name)
        self.running = False
        self.db.stop()
