# smartlife
# 05.2021

import time 

from import_csv import csv

class importer:
    def __init__(self, log, smartlife, db):
        log.debug("init")
        self.log = log
        self.smartlife = smartlife
        self.db = db

    def upload_prefetch(self, data):
        self.log.debug("prefetch")
        ok = True
        if not ("csv" == data["name"].split(".")[-1].lower()):
            ok = False
        if data["size"] > 100000:
            ok = False
        if ok:
            _data = {"mode": "stream", "modul": "fin_giro_consors", "func": "uploaded_import", "valid": int(time.time()+300)}
            netcom = self.smartlife.get_plugin("netcom")
            _data = netcom.encode_data(_data)
            if _data == "":
                return {"allowed": True, "data": {"ok": False}}
            return {"allowed": True, "data": {"ok": True, "data": _data}}
        else:
            return {"allowed": True, "data": {"ok": False}}

    def load_data(self, data):
        self.log.debug("load_data")
        try:
            tab = csv(data.decode())
            i = 0
            while i < tab.count:
                _date = tab.get_date(i, 'Valuta')
                if _date != '0000-00-00':
                    _name = tab.get_text(i, 'Sender / Empfänger')
                    _iban = tab.get_text(i, 'IBAN / Konto-Nr.')
                    _b_text = tab.get_text(i, 'Buchungstext')
                    _v_text = tab.get_text(i, 'Verwendungszweck')
                    _value = tab.get_float(i, 'Betrag in EUR')
                    self.db.add(_date, _name, _iban, _b_text, _v_text, _value)
                i += 1
            self.db.do_calc()
        except Exception as e:
            self.log.error(repr(e))
            return False
        return True
