# smartlife
# 05.2021

class cards:
    def __init__(self, log, db):
        log.debug("init")
        self.log = log
        self.db = db

    def get_cards(self):
        self.log.debug("get_cards")
        out = [{"mod": "fin_giro_consors", "card": "fin_giro_consors_sum", "name": "Consors Giro Summe"},
               {"mod": "fin_giro_consors", "card": "fin_giro_consors_month", "name": "Consors Giro Monat"},
               {"mod": "fin_giro_consors", "card": "fin_giro_consors_year", "name": "Consors Giro Jahr"}]
        return {"allowed": True, "data": out}

    def get_card(self):
        self.log.debug("get_card")
        out = {'sum': 0}
        out = self.db.get_card()
        return {"allowed": True, "data": out}


