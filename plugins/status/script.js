define(['module', 'packery', 'jqxtabs', 'jqxdata', 'jqxgrid', 'jqxgrid_selection', 'jqxcheckbox', 'jqxgrid_edit'], 
        function(module, Packery) {
    var mod = {
      t_update_1: null,
      t_update_2: null,
      t_update_3: null,
      wdata_last : 0,
      wdata_call : 0,
      wdata : undefined,
      map : undefined,
      mapmarker : undefined,
      year : 0,
      init : function() {
        window.module.status.sub = window.module.status.init_data.p1
        var sub = window.module.status.sub
        if (sub == "app") {
          html = '<div id="status_cards" style="width:100%; height:100%; margin: 10px;" class="jqx-widget-content-custom-scheme"></div>'
          $('#client_area').html(html)
        } else {
          html = '<div style="width:100%; height:100%;"><div id="status_tab"><ul><li>Karten</li><li>Sensoren</li></ul>'
          html += '<div><div id="status_cards" style="width:100%; height:100%;"></div></div>'
          html += '<div><div id="status_sensor" style="width:100%; height:100%;"><div id="status_sensor_grid" style="width:100%; height:100%;"></div></div></div>'
          $('#client_area').html(html+'</div>')
          $(window).off('resize', window.module.status.resize)
          $(window).on('resize', window.module.status.resize)
          window.module.status.resize()
          $('#status_tab').jqxTabs({ width: '100%', height: '100%', position: 'top'})
          $('#status_tab').on('selected', function(ev) {
            var selectedTab = ev.args.item
            window.module.status.call_tab(selectedTab)
          })
        }
        window.module.status.call_tab(0)
      },
      check_data : function() {
        var now = new Date()
        var t = Math.round(now.getTime() / 1000)
        if (t - window.module.status.wdata_last > 15) {
          if (t - window.module.status.wdata_call > 5) {
            window.module.status.wdata_call = t
            window.smfetch('/api/status', {cmd: 'get_card'}).then((data) => {
              if (data.error == undefined) {
                window.module.status.wdata_last = t
                window.module.status.wdata = data.data
              }
            })
          }
        }
      },
      update_1 : function() {
        clearTimeout(window.module.status.t_update_1)
        window.module.status.check_data()
        var cards =  $("#status_cards").find(".cards_status_time")
        var len = cards.length
        if (window.module.status.wdata != undefined) {
          var ele = $(cards[0])
          var d = window.module.status.wdata.time
          if (JSON.stringify(ele.data('last')) != JSON.stringify(d)) {
            ele.data('last', d)
            html = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">'
            html += 'Zeitbasiert</div>'
            html += '<div style=" position: absolute; left: 5px; float: right; top: 35px;">Tag</div>'
            v = "Aus"
            if (d.day.value == 1) {
              v = "An"
            }
            html += '<div style=" position: absolute; right: 25px; float: right; top: 35px;">'+v+'</div>'
            if (d.day.action) {
              html += '<img src="/lib/img/button.png" style="position: absolute; top: 35px; right: 5px; cursor: pointer" id="status_button_day">'
            }
            ele.html(html)
            $('#status_button_day').off("click")
            $('#status_button_day').on("click", function() {
              window.module.status.sent("day", 1 - window.module.status.wdata.time.day.value)
              console.log("day")
            })
          }
          window.module.status.t_update_1 = setTimeout(window.module.status.update_1, 1000)
        } else {
          window.module.status.t_update_1 = setTimeout(window.module.status.update_1, 250)
        }
      },
      update_2 : function() {
        clearTimeout(window.module.status.t_update_2)
        window.module.status.check_data()
        var cards =  $("#status_cards").find(".cards_status_place")
        var len = cards.length
        if (window.module.status.wdata != undefined) {
          var ele = $(cards[0])
          var d = window.module.status.wdata.place
          if (JSON.stringify(ele.data('last')) != JSON.stringify(d)) {
            ele.data('last', d)
            html = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">'
            html += 'Ortsbasiert</div>'
            html += '<div style=" position: absolute; left: 5px; float: right; top: 35px;">Zuhause</div>'
            v = "Aus"
            if (d.home.value == 1) {
              v = "An"
            }
            html += '<div style=" position: absolute; right: 25px; float: right; top: 35px;">'+v+'</div>'
            if (d.home.action) {
              html += '<img src="/lib/img/button.png" style="position: absolute; top: 35px; right: 5px; cursor: pointer" id="status_button_home">'
            }
            ele.html(html)
            $('#status_button_home').off("click")
            $('#status_button_home').on("click", function() {
              window.module.status.sent("home", 1 - window.module.status.wdata.place.home.value)
            })
          }
          window.module.status.t_update_2 = setTimeout(window.module.status.update_2, 1000)
        } else {
          window.module.status.t_update_2 = setTimeout(window.module.status.update_2, 250)
        }
      },
      update_3 : function() {
        clearTimeout(window.module.status.t_update_3)
        window.module.status.check_data()
        var ele =  $("#status_cards").find(".cards_status_phonecalls")
        if (window.module.status.wdata != undefined) {
          var d = window.module.status.wdata.phonecalls
          if (ele.data('last') != d) {
            ele.data('last', d)
            html = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">'
            html += 'Anrufe</div>'
            var len = d.length
            for (var i=0; i<len; i++) {
              html += '<div style="position: absolute; top: ' + (i*20+25) + 'px; left: 10px"><img src="/lib/img/telephone_'
              if (d[i].type == 1) {html += 'blue'}  
              if (d[i].type == 2) {html += 'red'} 
              if (d[i].type == 3) {html += 'green'} 
              html += '.svg" style="height: 18px; width: 18px;"></div>'
              html += '<div style="position: absolute; top: ' + (i*20+25) + 'px; left: 35px"><b>' + d[i].number + '</b></div>' 
              html += '<div style="position: absolute; top: ' + (i*20+25) + 'px; right: 10px">' + d[i].time + '</div>' 
            }
            ele.html(html)
          }
          window.module.status.t_update_3 = setTimeout(window.module.status.update_3, 30000)
        } else {
          window.module.status.t_update_3 = setTimeout(window.module.status.update_3, 250)
        }
      }, 
      get_card : function(data, div) {
        if (data["card"]=='status_time') {
          if (div.find(".cards_"+data["card"]).length == 0) {
            var html = $('<div class="cards_state cards_'+data["card"]+' card"><span></span></div>')
            html.data('card', data.card)
            div.append(html)
          } else {
//            console.log(data)
          }
          window.module.status.update_1()
        } else if (data["card"]=='status_place') {
          if (div.find(".cards_"+data["card"]).length == 0) {
            var html = $('<div class="cards_state cards_'+data["card"]+' card"><span></span></div>')
            html.data('card', data.card)
            div.append(html)
          } else {
//            console.log(data)
          }
          window.module.status.update_2()
        }
      },
      create_cards : function() {
        var ok = 1
        var cards = window.module.status.cards
        var l = cards.length
        for (var i=0; i<l; i++) {
           if (!(typeof(window.module[cards[i].mod].get_card) === 'function')) {
             console.log("Modul not found: "+cards[i].mod)
             ok = 0
           }
        }
        if (ok == 1) {
          for (var i=0; i<l; i++) {
            window.module[cards[i].mod].get_card(cards[i], $("#status_cards"))
            require( [ 'jquery-bridget/jquery-bridget' ], function( jQueryBridget ) {
              jQueryBridget( 'packery', Packery, $ );
              var grid = $('#status_cards').packery({
                itemSelector: '.card',
                gutter: 10
              })
            })
          }
        } else {
          console.log("recheck")
        }
      },
      call_tab: function(id) {
        if (id == 0) {
          window.smfetch('/api/status', {cmd: 'get_cards'}).then((data) => {
            if (data.error == undefined) {
              window.module.status.cards = data.data
              window.module.status.cards_check_mod = 10
              window.module.status.create_cards()
            }
          })
        } else if (id == 1) {
          window.smfetch('/api/status', {cmd: 'get_sensor'}).then((data) => {
            if (data.error == undefined) {
              var source = {
                localdata: data.data,
                datatype: "array",
                datafields: [
                  { name: 'topic', type: 'string'},
                  { name: 'value', type: 'string'},
                  { name: 'entry', type: 'string'},
                  { name: 'sent', type: 'boolean'},
                  { name: 'hist_num', type: 'boolean'},
                  { name: 'hist_event', type: 'boolean'}
                ]                
              }
              var dataAdapter = new $.jqx.dataAdapter(source, {
                downloadComplete: function (data, status, xhr) { },
                loadComplete: function (data) { },
                loadError: function (xhr, status, error) { }
              })
              var col = [
                { text: 'Sensor', datafield: 'topic', width: 350, editable: false},
                { text: 'Bezeichnung', datafield: 'entry'},
                { text: 'Wert', datafield: 'value', width: 300, editable: false, cellsalign: 'right'},
                { text: 'Senden', datafield: 'sent', width: 75, columntype: 'checkbox'},
                { text: 'History', datafield: 'hist_num', width: 75, columntype: 'checkbox'},
                { text: 'History Event', datafield: 'hist_event', width: 125, columntype: 'checkbox'}
              ]
              $("#status_sensor_grid").jqxGrid({
                width: '100%',
                height: '100%',
                source: dataAdapter,
                columns: col,
                editable: true
              })
              $("#status_sensor_grid").off('cellendedit')
              $("#status_sensor_grid").on('cellendedit', function (event) {
                var args = event.args;
                window.smfetch('/api/status', {cmd: 'set_sensor', data: {
                  field: args.datafield, value: args.value, topic: args.row.topic
                }}).then((data) => {})
              })
              $("#status_sensor").css('padding', 0).css('overflow', 'hidden')
            }
          })
        } 
      },
      sent : function(action, value) {
        window.smfetch('/api/status', {cmd: 'set_sensor', data: {action: action, value: value}}).then((data) => {
          if (data.error == undefined) {
            window.module.status.wdata_last = 0
            window.module.status.wdata_call = 0
          }
        })
      },
      stop : function() {
        clearTimeout(window.module.status.t_update_1)
        clearTimeout(window.module.status.t_update_2)
        clearTimeout(window.module.status.t_update_3)
        if (window.module.status.map != undefined) {
          window.module.status.map.remove()
          window.module.status.map = undefined
        }
      },
      resize: function() {
        $('#client_area').children().height($('#client_area').height())
      }
    }
    mod['init_data'] = window.module_const[module.id]
    window.module.status = mod
    return mod
  })
