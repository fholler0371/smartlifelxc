# smartlife
# 09.2021

from datetime import datetime
import time

class cards():
    def __init__(self, log, data):
        log.debug("init")
        self.log = log
        self.data = data
        self.max = 16

    def get_cards(self):
        self.log.debug("get_cards")
        out = []
        out.append({"mod": "status", "card": "status_time", "name": "Zeitbasierte Staties"})
        out.append({"mod": "status", "card": "status_place", "name": "Ortsbasierte Staties"})
        return {"allowed": True, "data": out}

    def get_card(self, level):
        self.log.debug("get_card")
        d = self.data.get_card()
        out = {"time": {"day": {"value": d["day"], "action": level == 2}}}
        out["place"] = {"home": {"value": d["home"], "action": level == 2}}
        return {"allowed": True, "data": out}