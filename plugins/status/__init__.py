# smartlife
# 08.2021
# 09.2021

import json
import time
import copy

from plugins2 import plugin_base

from status.data import data
from status.cards import cards

ALIVE = 99999

class integration(plugin_base):
    def __init__(self, smartlife, modul):
        plugin_base.__init__(self, smartlife, modul)
        global ALIVE
        self.alive = [ALIVE]
        self.data = data(self.log, self.smartlife)
        self.cards = cards(self.log, self.data)

    def new_msg(self, msg):
        self.log.debug("new_msg")
        global ALIVE
        self.alive[0] = ALIVE
        if "sensors" in self.cfg:
            print(msg)
            for entry in self.cfg["sensors"]:
                if msg["topic"] == entry["topic"]:
                    rec = copy.copy(msg)
                    rec["topic"] = entry["local"]   
                    self.data.new_data(rec)

    def pl_get_menus(self, user, ip):
        self.log.debug("user")
        rights = self.rights(user, ip)
        out = []
        if rights["domain"]:
            if "read" in rights["sub"]:
                out.append({'label': 'Status', 'mod': 'status', 'p1':'full'})
            elif "card" in rights["sub"]:
                out.append({'label': 'Status', 'mod': 'status', 'p1':'app'})
        return out

    def pl_data(self, user, cmd, _in, ip):
        self.log.debug(cmd)
        rights = self.rights(user, ip)
        if rights["domain"]:
            level = 0
            if "sm_level_1" in rights["sub"]:
                level = 1
            if "sm_level_2" in rights["sub"]:
                level = 2
            if "card" in rights["sub"] or "read" in rights["sub"]:
                if cmd == "get_cards":
                    return self.cards.get_cards()
                elif cmd == "get_card":
                    return self.cards.get_card(level)
                elif cmd == "set_sensor":
                    return self.data.set_sensor(_in["action"], _in["value"], level)

    def run(self):
        self.log.info("run")
        store = self.smartlife.get_plugin("data_store")
        if store:
            store.unsubscribe(self.name)
            if "sensors" in self.cfg:
                for entry in self.cfg["sensors"]:
                    store.subscribe(entry["topic"], self.name)
        while self.running:
            time.sleep(1)

    def stop(self):
        self.log.info(self.name)
        self.running = False
        self.data.stop()

