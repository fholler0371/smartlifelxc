# smartlife
# 09.2021

SAVE_INTERVAL = 120

import time
import os
import json
from threading import Timer

class data:
    def __init__(self, log, smartlife):
        log.debug("init")
        self.log = log
        self.running = True
        self.home_motion_block = time.time() + 30
        self.jname = smartlife.db_path + "/status_data.json"
        self.data = {}
        if os.path.isfile(self.jname):
            f = open(self.jname, "r")
            self.data = json.loads(f.read())
            f.close()
        self.th = Timer(SAVE_INTERVAL, self.save_tick)
        self.th.start()

    def get_card(self):
        out = {}
        out["day"] = 1
        if "day" in self.data:
            out["day"] = self.data["day"]["value"]
        out["home"] = 1
        if "home" in self.data:
            out["home"] = self.data["home"]["value"]
        return out

    def set_sensor(self, action, value, level):
        self.log.debug(action)
        if action in ["day"]:
            if level > 1:
                if action in ["day"]:
                    if value in [0, 1]:
                        self.set_value(action, value)
        elif action in ["home"]:
            if level > 1:
                if action in ["home"]:
                    if value in [0, 1]:
                        self.set_value(action, value)
        return {"allowed": True, "data": {}}

    def new_data(self, data):
        self.log.debug(data["topic"])
        if data["topic"] == "in/switch_sleep_day":
            self.set_value("day", 1)
        elif data["topic"] == "in/switch_sleep_night":
            self.set_value("day", 0)
        elif data["topic"] == "in/switch_door_home":
            self.set_value("home", 1)
        elif data["topic"] == "in/switch_door_away":
            self.set_value("home", 0)
        elif data["topic"] == "in/motion_door" and data["value"]:
            if self.home_motion_block < time.time():
                self.set_value("home", 1)
        elif data["topic"] == "in/mobile_home":
            val = 0
            if data["value"]:
                val = 1
            if "mobile_home" in self.data:
                if val == 0 and self.data["mobile_home"]["value"] == 1:
                    self.set_value("home", 0)
            self.set_value("mobile_home", val)

    def set_value(self, topic, value):
        self.log.debug(topic+" "+str(value))
        if not (topic in self.data):
            self.data[topic] = {"value": None, "ts": 0}
        if self.data[topic]["value"] != value:
            if topic == "home" and value == 1:
                self.home_motion_block = time.time() + 15
            self.data[topic]["value"] = value
            self.data[topic]["ts"] = int(time.time())

    def save_tick(self):
        self.log.debug("save_tick")
        if self.running:
            self.th = Timer(SAVE_INTERVAL, self.save_tick)
            self.th.start()
            self.save()

    def save(self):
        self.log.debug("save")
        f = open(self.jname, "w")
        f.write(json.dumps(self.data))
        f.close()

    def stop(self):
        self.log.debug("stop")
        self.running = False
        self.th.cancel()
        self.save()