# smartlife
# 04.2021
# 05.2021

import os
import json
from datetime import datetime

class location():
    def __init__(self, log, phone, tracker, smartlife, transition):
        self.log = log
        log.debug('init')
        self.phone = phone
        self.location = {"lat": 0, "long": 0}
        self.tracker = tracker
        self.transition = transition
        self.jName = smartlife.db_path + "/owntrack_location.json"
        self.hName = smartlife.db_path + "/owntrack_history"
        if os.path.isfile(self.jName):
            f = open(self.jName, "r")
            self.location = json.loads(f.read())
            f.close()

    def newData(self, data):
        self.log.debug(data)
        if self.tracker != data["tid"]:
            self.log.error("Wrong tracker id")
            return ""
        if not("conn" in data):
            self.log.error("conn ins missing in location data")
            self.log.error(data)
            data["conn"] = -1
        self.phone.newData(data["batt"], data["bs"], data["conn"])
        if self.location["lat"] != data["lat"] or self.location["long"] != data["lon"]:
            self.location = {"lat": data["lat"], "long": data["lon"]}
            f = open(self.jName, "w")
            f.write(json.dumps(self.location))
            f.close()
        if "inregions" in data:
            self.transition.location_region(data["inregions"])
#        t = data["tst"]
#        lat = data["lat"]
#        lon = data["lon"]
#        alt = data["alt"]
#        speed = data["vel"]
#        self.log.error(t)
#        self.log.error(lat)
#        self.log.error(lon)
#        self.log.error(alt)
#        self.log.error(speed)
