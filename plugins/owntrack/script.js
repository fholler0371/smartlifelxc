define(['module', 'packery', 'jqxtabs', 'jqxdata', 'jqxgrid', 'jqxcheckbox', 'jqxgrid_selection', 'jqxgrid_edit', 'leaflet_load'],
       function(module, Packery) {
    var mod = {
      t_update_1: null,
      t_update_2: null,
      t_update_3: null,
      wdata_last : 0,
      wdata_call : 0,
      wdata : undefined,
      map : undefined,
      mapmarker : undefined,
      year : 0,
      init : function() {
        window.module.owntrack.sub = window.module.owntrack.init_data.p1
        var sub = window.module.owntrack.sub
        if (sub == "app") {
          html = '<div id="owntrack_cards" style="width:100%; height:100%; margin: 10px;" class="jqx-widget-content-custom-scheme"></div>'
          $('#client_area').html(html)
        } else {
          html = '<div style="width:100%; height:100%;"><div id="owntrack_tab"><ul><li>Karten</li><li>Orte</li>'
          if (sub == 'full') {
            html += '<li>Sensoren</li>'
          }
          html += '</ul><div><div id="owntrack_cards" style="width:100%; height:100%;"></div></div>'
          html += '<div><div id="owntrack_places" style="width:100%; height:100%;"><div id="owntrack_place_grid" style="width:100%; height:100%;">'
          html += '</div></div></div>'
          if (sub == 'full') {
            html += '<div><div id="owntrack_sensors" style="width:100%; height:100%;"><div id="owntrack_sensors_grid" style="width:100%; height:100%;">'
            html += '</div></div></div>'
          }
          html += '</div>'
          $('#client_area').html(html+'</div>')
          $(window).off('resize', window.module.owntrack.resize)
          $(window).on('resize', window.module.owntrack.resize)
          window.module.owntrack.resize()
          $('#owntrack_tab').jqxTabs({ width: '100%', height: '100%', position: 'top'})
          $('#owntrack_tab').on('selected', function(ev) {
            var selectedTab = ev.args.item
            window.module.owntrack.call_tab(selectedTab)
          })
        }
        window.module.owntrack.call_tab(0)
      },
      check_data : function() {
        var now = new Date()
        var t = Math.round(now.getTime() / 1000)
        if (t - window.module.owntrack.wdata_last > 120) {
          if (t - window.module.owntrack.wdata_call > 10) {
            window.module.owntrack.wdata_call = t
            window.smfetch('/api/owntrack', {cmd: 'get_card'}).then((data) => {
              if (data.error == undefined) {
                window.module.owntrack.wdata_last = t
                window.module.owntrack.wdata = data.data
              }
            })
          }
        }
      },
      update_1 : function() {
        clearTimeout(window.module.owntrack.t_update_1)
        window.module.owntrack.check_data()
        var cards =  $("#owntrack_cards").find(".cards_owntrack_place")
        var len = cards.length
        if (window.module.owntrack.wdata != undefined) {
          for (var i = 0; i<len; i++) {
            var ele = $(cards[i]),
                c_id = ele.data("card").slice(15)
            var d = window.module.owntrack.wdata.places[c_id]
            if (JSON.stringify(ele.data('last')) != JSON.stringify(d)) {
              ele.data('last', d)
              html = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">'+d.name+'</div>'
              var a = d.i > d.o
              html += '<div style="position: absolute; top: 25px; left: 10px'
              if (a) {
                html += "; font-weight: bold"
              }
              html += '">Ankunft</div>'
              if (d.i > 0) {
                var t = new Date(d.i*1000)
                html += '<div style="position: absolute; top: 25px; right: 10px'
                if (a) {
                  html += "; font-weight: bold"
                }
                html += '">'+t.toLocaleDateString()+" "+t.toLocaleTimeString()+'</div>'
              }
              html += '<div style="position: absolute; top: 45px; left: 10px'
              if (!a) {
                html += "; font-weight: bold"
              }
              html += '">Verlassen</div>'
              if (d.o > 0) {
                var t = new Date(d.o*1000)
                html += '<div style="position: absolute; top: 45px; right: 10px'
                if (!a) {
                  html += "; font-weight: bold"
                }
                html += '">'+t.toLocaleDateString()+" "+t.toLocaleTimeString()+'</div>'
              }
              ele.html(html)
            }
          }
          window.module.owntrack.t_update_1 = setTimeout(window.module.owntrack.update_1, 30000)
        } else {
          window.module.owntrack.t_update_1 = setTimeout(window.module.owntrack.update_1, 250)
        }
      },
      update_2 : function() {
        clearTimeout(window.module.owntrack.t_update_2)
        window.module.owntrack.check_data()
        var ele =  $("#owntrack_cards").find(".cards_owntrack_phone")
        if (window.module.owntrack.wdata != undefined) {
          var d = window.module.owntrack.wdata.phone
          if (JSON.stringify(ele.data('last')) != JSON.stringify(d)) {
            ele.data('last', d)
            html = '<div style="position: absolute; top: 3px; width: 100%; text-align: center; font-weight: bold; font-size:130%">Handy</div>'
            html += '<div style="position: absolute; top: 20px; left: 15px"><img style="width: 32px; height: 32px" src="'
            if (d.conn == 'w') {
              html += "/lib/img/wifi-on.png"
            } else {
              html += "/lib/img/mobil.png"
            }
            html += '"></div>'
            html += '<div style="position: absolute; top: 20px; right: 5px"><img style="width: 32px; height: 32px" src="'
            if (d.bmode == '1') {
              html += "/lib/img/battery.png"
            } else if (d.bmode == '2') {
              html += "/lib/img/battery-load.png"
            } else if (d.bmode == '3') {
              html += "/lib/img/battery-full.png"
            }
            html += '"></div>'
            html += '<div style="position: absolute; top: 25px; right: 40px; text-align: center; font-weight: bold; font-size:130%" '
            html += 'class="cards_chart" data-chart="/api/owntrack:bat:W">'+d.bat+' %</div>'
            ele.html(html)
          }
          window.module.owntrack.t_update_2 = setTimeout(window.module.owntrack.update_2, 30000)
        } else {
          window.module.owntrack.t_update_2 = setTimeout(window.module.owntrack.update_2, 250)
        }
      },
      update_3 : function() {
        clearTimeout(window.module.owntrack.t_update_3)
        window.module.owntrack.check_data()
        var ele =  $("#owntrack_cards").find(".cards_owntrack_map")
        if (window.module.owntrack.wdata != undefined) {
          var d = window.module.owntrack.wdata.location
          if (JSON.stringify(ele.data('last')) != JSON.stringify(d)) {
            ele.data('last', d)
            if (window.module.owntrack.map == undefined) {
              html = '<div id="owntrack_map" style="width: calc( 100% - 10px ); height: calc( 100% - 10px ); margin: 5px"></div>'
              ele.html(html)
              window.module.owntrack.map = L.map('owntrack_map').setView([d.lat, d.long], 16)
              L.tileLayer('https://{s}.tile.openstreetmap.de/{z}/{x}/{y}.png', {
                'attribution':  'Kartendaten &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> Mitwirkende',
                'useCache': true
              }).addTo(window.module.owntrack.map)
              window.module.owntrack.mapmarker = L.layerGroup().addTo(window.module.owntrack.map)
              L.marker([d.lat, d.long]).addTo(window.module.owntrack.mapmarker)
            } else {
              window.module.owntrack.map.setView([d.lat, d.long], 16)
              window.module.owntrack.mapmarker.clearLayers()
              L.marker([d.lat, d.long]).addTo(window.module.owntrack.mapmarker)
            }
          }
          window.module.owntrack.t_update_3 = setTimeout(window.module.owntrack.update_3, 30000)
        } else {
          window.module.owntrack.t_update_3 = setTimeout(window.module.owntrack.update_3, 250)
        }
      },
      get_card : function(data, div) {
        if (data["card"].startsWith("owntrack_place_")) {
          if (div.find(".cards_"+data["card"]).length == 0) {
            var html = $('<div class="cards_owntrack_place cards_'+data["card"]+' card"><span></span></div>')
            html.data('card', data.card)
            div.append(html)
          } else {
//            console.log(data)
          }
          window.module.owntrack.update_1()
        } else if (data["card"].startsWith("owntrack_phone")) {
          if (div.find(".cards_"+data["card"]).length == 0) {
            var html = $('<div class="cards_owntrack_phone card"><span></span></div>')
            html.data('card', data.card)
            div.append(html)
          } else {
//            console.log(data)
          }
          window.module.owntrack.update_2()
        } else if (data["card"].startsWith("owntrack_map")) {
          if (div.find(".cards_"+data["card"]).length == 0) {
            var html = $('<div class="cards_owntrack_map card card_h4"><span></span></div>')
            html.data('card', data.card)
            div.append(html)
          } else {
//            console.log(data)
          }
          window.module.owntrack.update_3()
        }
      },
      create_cards : function() {
        var ok = 1
        var cards = window.module.owntrack.cards
        var l = cards.length
        for (var i=0; i<l; i++) {
           if (!(typeof(window.module[cards[i].mod].get_card) === 'function')) {
             console.log("Modul not found: "+cards[i].mod)
             ok = 0
           }
        }
        if (ok == 1) {
          for (var i=0; i<l; i++) {
            window.module[cards[i].mod].get_card(cards[i], $("#owntrack_cards"))
            require( [ 'jquery-bridget/jquery-bridget' ], function( jQueryBridget ) {
              jQueryBridget( 'packery', Packery, $ );
              var grid = $('#owntrack_cards').packery({
                itemSelector: '.card',
                gutter: 10
              })
            })
          }
        } else {
          console.log("recheck")
        }
      },
      call_tab: function(id) {
        if (id == 0) {
          window.smfetch('/api/owntrack', {cmd: 'get_cards'}).then((data) => {
            if (data.error == undefined) {
              window.module.owntrack.cards = data.data
              window.module.owntrack.cards_check_mod = 10
              window.module.owntrack.create_cards()
            }
          })
        } else if (id == 1) {
          window.smfetch('/api/owntrack', {cmd: 'get_place', 'data': {'year': window.module.owntrack.year}}).then((data) => {
            if (data.error == undefined) {
              window.module.owntrack.year = data.data.year
              var len = data.data.data.length
              for (var i = 0; i < len; i++) {
                data.data.data[i].time = new Date(data.data.data[i].time*1000)
              }
              var source = {
                localdata: data.data.data,
                datatype: "array",
                datafields: [
                  { name: 'time', type: 'date'},
                  { name: 'place', type: 'string'},
                  { name: 'dir', type: 'string'}
                ]
              }
              var dataAdapter = new $.jqx.dataAdapter(source, {
                downloadComplete: function (data, status, xhr) { },
                loadComplete: function (data) { },
                loadError: function (xhr, status, error) { }
              })
              var cellsrenderer = function (row, columnfield, value, defaulthtml, columnproperties) {
                var out = $(defaulthtml)
                out[0].innerHTML = value.toLocaleDateString()+" "+value.toLocaleTimeString()
                return out[0].outerHTML
              }
              var col = [
                { text: 'Zeit', datafield: 'time', width: 200, cellsrenderer: cellsrenderer},
                { text: 'Ort', datafield: 'place'},
                { text: 'Richtung', datafield: 'dir', width: 150}
              ]
              $("#owntrack_place_grid").jqxGrid({
                width: '100%',
                height: '100%',
                source: dataAdapter,
                columns: col,
              })
              $("#owntrack_place_grid").parent().css('padding', 0).css('overflow', 'hidden')
            }
          })
        } else if (id == 2) {
          window.smfetch('/api/owntrack', {cmd: 'get_sensors', 'data': {}}).then((data) => {
            if (data.error == undefined) {
              var source = {
                localdata: data.data,
                datatype: "array",
                datafields: [
                  { name: 'point', type: 'string'},
                  { name: 'send', type: 'bool'}
                ]
              }
              var dataAdapter = new $.jqx.dataAdapter(source, {
                downloadComplete: function (data, status, xhr) { },
                loadComplete: function (data) { },
                loadError: function (xhr, status, error) { }
              })
              var col = [
                { text: 'Ort', datafield: 'point', editable: false},
                { text: 'Senden', datafield: 'send', width: 75, columntype: 'checkbox'}
              ]
              $("#owntrack_sensors_grid").jqxGrid({
                width: '100%',
                height: '100%',
                source: dataAdapter,
                columns: col,
                editable: true
              })
              $("#owntrack_sensors_grid").parent().css('padding', 0).css('overflow', 'hidden')
              $("#owntrack_sensors_grid").off('cellendedit')
              $("#owntrack_sensors_grid").on('cellendedit', function (event) {
                var args = event.args;
                window.smfetch('/api/owntrack', {cmd: 'set_send', data: {
                  value: args.value, point: args.row.point
                }}).then((data) => {})
              })
            }
          })
        }
      },
      stop : function() {
        clearTimeout(window.module.owntrack.t_update_1)
        clearTimeout(window.module.owntrack.t_update_2)
        clearTimeout(window.module.owntrack.t_update_3)
        if (window.module.owntrack.map != undefined) {
          window.module.owntrack.map.remove()
          window.module.owntrack.map = undefined
        }
      },
      resize: function() {
        $('#client_area').children().height($('#client_area').height())
      }
    }
    mod['init_data'] = window.module_const[module.id]
    window.module.owntrack = mod
    return mod
  })
