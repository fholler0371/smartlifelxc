# smartlife
# 04.2021
# 09.2021

import json

from plugins2 import plugin_base

from owntrack.cards import cards
from owntrack.phone import phone
from owntrack.location import location
from owntrack.transition import transition

ALIVE_WWW=4000

class integration(plugin_base):
    def __init__(self, smartlife, modul):
        global ALIVE_WWW
        plugin_base.__init__(self, smartlife, modul)
        self.alive = [ALIVE_WWW]
        self.transition = transition(self.log, self.cfg['tracker'], self.smartlife)
        self.phone  = phone(self.log, self.smartlife)
        self.location  = location(self.log, self.phone, self.cfg['tracker'], self.smartlife, self.transition)
        self.cards = cards(self.log, self.phone, self.transition, self.location)

    def web_doPost(self, path, param, data):
        self.log.debug(path)
        global ALIVE_WWW
        self.alive[0] = ALIVE_WWW
        _data = json.loads(data)
        if '_type' in _data:
            if _data['_type'] == "transition":
                self.transition.newData(_data)
            elif _data['_type'] == "location":
                self.location.newData(_data)
        return {"code": 200, "data": {}}

    def pl_get_menus(self, user, ip):
        self.log.debug("user")
        rights = self.rights(user, ip)
        out = []
        if rights["domain"]:
            if "write" in rights["sub"]:
                out.append({'label': 'OwnTrack', 'mod': 'owntrack', 'p1':'full'})
            elif "read" in rights["sub"]:
                out.append({'label': 'OwnTrack', 'mod': 'owntrack', 'p1':'read'})
            elif "card" in rights["sub"]:
                out.append({'label': 'OwnTrack', 'mod': 'owntrack', 'p1':'app'})
        return out

    def pl_data(self, user, cmd, _in, ip):
        self.log.debug(cmd)
        rights = self.rights(user, ip)
        if rights["domain"]:
            if "card" in rights["sub"] or "read" in rights["sub"]:
                if cmd == "get_cards":
                    return self.cards.get_cards()
                elif cmd == "get_card":
                    return self.cards.get_card()
                elif cmd == "get_chart":
                    return self.cards.get_chart(_in["sensor"], _in["mode"])
            if "read" in rights["sub"]:
                if cmd == "get_place":
                    return self.transition.get_place(_in["year"])
            if "write" in rights["sub"]:
                if cmd == "get_sensors":
                    return self.transition.get_sensors()
                if cmd == "set_send":
                    return self.transition.set_send(_in["point"], _in["value"])
        return {"allowed": False}
        
    def stop(self):
        self.log.info('stop')
        self.running = False
        self.phone.stop()
        self.transition.stop()
