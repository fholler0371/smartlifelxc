# smartlife
# 04.2021

import os
import json
import time
from datetime import datetime
from threading import Timer

from database import database

class transition():
    def __init__(self, log, tracker, smartlife):
        self.log = log
        log.debug("init")
        self.points = {}
        self.tracker = tracker
        self.smartlife = smartlife
        self.jName = smartlife.db_path + "/owntrack_transition.json"
        if os.path.isfile(self.jName):
            f = open(self.jName, "r")
            self.points = json.loads(f.read())
            f.close()
        sql = "CREATE TABLE IF NOT EXISTS history (time integer, zone string, in_out string)"
        self.database = database(log, smartlife.db_path + "/owntrack_transition_history_$$.sqlite", sql_create=sql)
        self.running = True
        self.send_timer = Timer(15, self.auto_send)
        self.send_timer.start()

    def newData(self, data):
        self.log.debug(data)
        if self.tracker != data['tid']:
            self.log.error('Wrong tracker id')
            return ''
        place = data['desc']
        if not (place in self.points):
            self.points[place] = {'_in': -1, '_out': -1}
        event = data['event']
        t = data['tst']
        if event == 'enter':
            self.points[place]['_in'] = t
            self.send(place, True)
        if event == 'leave':
            self.points[place]['_out'] = t
            self.send(place, False)
        f = open(self.jName, 'w')
        f.write(json.dumps(self.points))
        f.close()
        d = datetime.now()
        self.database.open(d.strftime('%Y'))
        sql = "insert into history (time, zone, in_out) values ('"+str(t)+"', '"+place+"' , '"+event+"')"
        self.database.commit(sql)
        self.database.close()

    def location_region(self, data):
        self.log.debug(data)
        t = int(time.time())
        for place in self.points:
            if not(place in data):
                if self.points[place]["_in"] > self.points[place]["_out"]:
                    self.points[place]['_out'] = t
                    self.send(place, False)
                    f = open(self.jName, 'w')
                    f.write(json.dumps(self.points))
                    f.close()
                    event = 'leave'
                    d = datetime.now()
                    self.database.open(d.strftime('%Y'))
                    sql = "insert into history (time, zone, in_out) values ('"+str(t)+"', '"+place+"' , '"+event+"')"
                    self.database.commit(sql)
                    self.database.close()
        for place in data:
            if not (place in self.points):
                self.points[place] = {'_in': -1, '_out': -1}
            if self.points[place]["_in"] < self.points[place]["_out"]:
                self.points[place]['_in'] = t
                self.send(place, True)
                f = open(self.jName, 'w')
                f.write(json.dumps(self.points))
                f.close()
                event = 'enter'
                d = datetime.now()
                self.database.open(d.strftime('%Y'))
                sql = "insert into history (time, zone, in_out) values ('"+str(t)+"', '"+place+"' , '"+event+"')"
                self.database.commit(sql)
                self.database.close()

    def get_place(self, year):
        self.log.debug(year)
        if year == 0:
            year = datetime.now().year
        out = {'year': year, 'data': []}
        if self.database.exists(year):
            self.database.open(year)
            sql = "select time, zone, in_out from history order by time desc"
            for row in self.database.fetchall(sql):
                out['data'].append({'time': row[0], 'place': row[1], 'dir': row[2]})
            self.database.close()
        return {"allowed": True, "data": out}

    def get_sensors(self):
        self.log.debug("get_sensors")
        out = []
        for point in self.points:
            send = False
            if "send" in self.points[point]:
                send = self.points[point]["send"]
            out.append({"point": point, "send":send})
        return {"allowed": True, "data": out}

    def set_send(self, point, value):
        self.log.debug(point)
        self.points[point]["send"] = value
        f = open(self.jName, 'w')
        f.write(json.dumps(self.points))
        f.close()
        return {"allowed": True, "data": []}

    def send(self, point, value):
        self.log.debug(point)
        if "send" in self.points[point]:
            if self.points[point]["send"]:
                store = self.smartlife.get_plugin("data_store")
                msg = {"topic": "owntrack/" + point, "friendly": "owntrack/" + point, "value": value,
                       "ts": max(self.points[point]["_in"], self.points[point]["_out"])}
                store.new_local(msg)

    def auto_send(self):
        self.log.debug("auto_send")
        if self.send_timer:
            self.send_timer.cancel()
        self.send_timer = Timer(180, self.auto_send)
        self.send_timer.start()
        for point in self.points:
            self.send(point, self.points[point]["_in"] > self.points[point]["_out"])

    def stop(self):
        self.log.debug("stop")
        self.running = False
        if self.send_timer:
            self.send_timer.cancel()