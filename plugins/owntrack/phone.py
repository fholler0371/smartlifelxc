# smartlife
# 04.2021
# 05.2021

import os
import json
import time
from datetime import datetime
from threading import Timer

from dateutil import tz

from database import database

class phone():
    def __init__(self, log, smartlife):
        log.debug("init")
        self.log = log
        self.data = {"bat": 0, "bmode": 0, "conn": "w"}
        self.jName = smartlife.db_path + "/owntrack_phone.json"
        if os.path.isfile(self.jName):
            f = open(self.jName, "r")
            self.data = json.loads(f.read())
            f.close()
        self.timer = Timer(3600, self.loop)
        self.timer.start()
        sql = "CREATE TABLE IF NOT EXISTS history (time integer, topic text, value float)"
        self.day = database(self.log, smartlife.db_path + "/owntrack_phone_history_one_day.sqlite", sql_create=sql)
        sql = "CREATE TABLE IF NOT EXISTS history (time integer, topic text, value float, value_min float, value_max float)"
        self.week = database(self.log, smartlife.db_path + "/owntrack_phone_history_one_week.sqlite", sql_create=sql)
        self.week_last = 300*int(time.time()/300)
        self.month = database(self.log, smartlife.db_path + "/owntrack_phone_history_$$.sqlite", sql_create=sql)
        self.month_last = 3600*int(time.time()/3600)
        self.year = database(self.log, smartlife.db_path + "/owntrack_phone_history_$$.sqlite", sql_create=sql)
        self.year_last = 24*3600*int(time.time()/24/3600)
        t = int(time.time())
        x = int(t/60+1)*60+1-t
        self.htimer = Timer(x, self.run_hist)
        self.htimer.start()

    def run_hist(self):
        self.log.debug("run_hist")
        t = int(time.time())
        x = int(t/60+1)*60+1-t
        self.htimer = Timer(x, self.run_hist)
        self.htimer.start()
        self.day.open()
        self.day.execute("delete from history where time < " + str(t-86400))
        self.day.commit("insert into history (time, topic, value) values ('" + str(t) + "', 'battery', '" + str(self.data['bat']) + "')")
        if self.week_last != 300*int(t/300):
            self.week_last = 300*int(t/300)
            self.week.open()
            sql = "delete from history where time < " + str(t - 86400*7)
            self.week.commit(sql)
            sql = "select avg(value) as value, min(value) as value_min, max(value) as value_max from history where topic='battery' and time > '" + str(t - 301) + "'"
            res = self.day.fetchone(sql)
            if res:
                sql = "insert into history (time, topic, value, value_min, value_max) values ('" + str(t) + "', 'battery', '" + str(res[0])
                sql += "', '" + str(res[1]) + "', '" + str(res[2]) + "')"
                self.week.execute(sql)
            self.week.commit()
            d = datetime.utcfromtimestamp(t)
            from_zone = tz.gettz("UTC")
            to_zone = tz.gettz("Europe/Berlin")
            d = d.replace(tzinfo=from_zone)
            d = d.astimezone(to_zone)
            if self.month_last != 3600*int(t/3600):
                self.month_last = 3600*int(t/3600)
                self.month.open(d.strftime("%y%m"))
                sql = "select avg(value) as value, min(value_min) as value_min, max(value_max) as value_max from history where topic='battery'"
                sql += " and time > '"+str(t-3601)+"'"
                res = self.week.fetchone(sql)
                if res:
                    sql = "insert into history (time, topic, value, value_min, value_max) values ('"+str(t)+"', 'battery', '"+str(res[0])
                    sql += "', '"+str(res[1])+"', '"+str(res[2])+"')"
                    self.month.execute(sql)
                self.month.commit()
                self.month.close()
            if self.year_last != 24*3600*int(t/24/3600):
                self.year_last = 24*3600*int(t/24/3600)
                self.year.open(d.strftime("%y"))
                sql = "select avg(value) as value, min(value_min) as value_min, max(value_max) as value_max from history where topic='battery'"
                sql += " and time > '"+str(t-86401)+"'"
                res = self.week.fetchone(sql)
                if res:
                    sql = "insert into history (time, topic, value, value_min, value_max) values ('"+str(t)+"', 'battery', '"+str(res[0])
                    sql += "', '"+str(res[1])+"', '"+str(res[2])+"')"
                    self.year.execute(sql)
                self.year.commit()
                self.year.close()
            self.week.close()
        self.day.close()

    def get_card(self):
        self.log.debug("get_card")
        return self.data

    def loop(self):
        self.log.debug("loop")
        self.saveData()
        self.timer = Timer(3600, self.loop)
        self.timer.start()

    def saveData(self):
        self.log.debug("saveData")
        f = open(self.jName, 'w')
        f.write(json.dumps(self.data))
        f.close()

    def stop(self):
        self.log.debug("stop")
        self.saveData()
        if self.timer:
            self.timer.cancel()
        if self.htimer:
            self.htimer.cancel()

    def newData(self, bat, bmode, conn):
        self.log.debug(bat)
        self.log.debug(bmode)
        self.log.debug(conn)
        if conn == -1 and self.data and "conn" in self.data:
            conn = self.data["conn"]
        self.data = {"bat": bat, "bmode": bmode, "conn": conn}

    def get_place(self, year):
        self.log.debug(year)
        if year == 0:
            year = datetime.now().year
        out = {'year': year, 'data': []}
        name = self.hName+str(year)+'.sqlite'
        if os.path.isfile(name):
            con = sqlite3.connect(name)
            cur = con.cursor()
            sql = "select time, zone, in_out from history order by time desc"
            cur.execute(sql)
            for row in cur.fetchall():
                out['data'].append({'time': row[0], 'place': row[1], 'dir': row[2]})
            con.close()
        return out

    def get_chart(self, mode):
        topic = "battery"
        out = []
        if mode == "D":
            self.day.open()
            sql = "SELECT time, value FROM history WHERE topic='%s' ORDER BY time"
            for row in self.day.fetchall(sql % (topic, )):
                out.append({"time": row[0], "value": row[1]})
            self.day.close()
        elif mode == "W":
            self.week.open()
            sql = "SELECT time, value, value_min, value_max FROM history WHERE topic='%s' ORDER BY time"
            for row in self.week.fetchall(sql % (topic, )):
                out.append({"time": row[0], "value": row[1], "min": row[2], "max": row[3]})
            self.week.close
        elif mode == "M":
            t = int(time.time())
            tstart = t - 30*24*60*60
            d = datetime.utcfromtimestamp(tstart)
            from_zone = tz.gettz('UTC')
            to_zone = tz.gettz('Europe/Berlin')
            d = d.replace(tzinfo=from_zone)
            d = d.astimezone(to_zone)
            if self.month.exists(d.strftime("%y%m")):
                self.month.open(d.strftime("%y%m"))
                sql = "SELECT time, value, value_min, value_max FROM history WHERE topic='%s' and time>'"+str(tstart)+"' ORDER BY time"
                for row in self.month.fetchall(sql % (topic, )):
                    out.append({"time": row[0], "value": row[1], "min": row[2], "max": row[3]})
                self.month.close()
            d = datetime.utcfromtimestamp(t)
            from_zone = tz.gettz('UTC')
            to_zone = tz.gettz('Europe/Berlin')
            d = d.replace(tzinfo=from_zone)
            d = d.astimezone(to_zone)
            if self.month.exists(d.strftime("%y%m")):
                self.month.open(d.strftime("%y%m"))
                sql = "SELECT time, value, value_min, value_max FROM history WHERE topic='%s' and time>'"+str(tstart)+"' ORDER BY time"
                for row in self.month.fetchall(sql % (topic, )):
                    out.append({"time": row[0], "value": row[1], "min": row[2], "max": row[3]})
                self.month.close()
        elif mode == "Y":
            t = int(time.time())
            tstart = t - 365*24*60*60
            d = datetime.utcfromtimestamp(tstart)
            from_zone = tz.gettz('UTC')
            to_zone = tz.gettz('Europe/Berlin')
            d = d.replace(tzinfo=from_zone)
            d = d.astimezone(to_zone)
            if self.year.exists(d.strftime("%y")):
                self.year.open(d.strftime("%y"))
                sql = "SELECT time, value, value_min, value_max FROM history WHERE topic='%s' and time>'"+str(tstart)+"' ORDER BY time"
                for row in self.year.fetchall(sql % (topic, )):
                    out.append({"time": row[0], "value": row[1], "min": row[2], "max": row[3]})
                self.year.close()
            d = datetime.utcfromtimestamp(t)
            from_zone = tz.gettz('UTC')
            to_zone = tz.gettz('Europe/Berlin')
            d = d.replace(tzinfo=from_zone)
            d = d.astimezone(to_zone)
            if self.year.exists(d.strftime("%y")):
                self.year.open(d.strftime("%y"))
                sql = "SELECT time, value, value_min, value_max FROM history WHERE topic='%s' and time>'"+str(tstart)+"' ORDER BY time"
                for row in self.year.fetchall(sql % (topic, )):
                    out.append({"time": row[0], "value": row[1], "min": row[2], "max": row[3]})
                self.year.close()
        elif mode == "A":
            t = int(time.time())
            d = datetime.utcfromtimestamp(t)
            from_zone = tz.gettz('UTC')
            to_zone = tz.gettz('Europe/Berlin')
            d = d.replace(tzinfo=from_zone)
            d = d.astimezone(to_zone)
            yend = int(d.strftime("%y"))
            ystart = yend
            while self.year.exists(('0'+str(ystart))[-2:]):
                ystart -= 1
            ystart += 1
            yend += 1
            while ystart < yend:
                self.year.open(('0'+str(ystart))[-2:])
                sql = "SELECT time, value, value_min, value_max FROM history WHERE topic='%s' ORDER BY time"
                for row in self.year.fetchall(sql % (topic, )):
                    out.append({"time": row[0], "value": row[1], "min": row[2], "max": row[3]})
                self.year.close()
                ystart += 1
        return out