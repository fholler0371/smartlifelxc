# smartlife
# 04.2021

class cards():
    def __init__(self, log, phone, places, location):
        log.debug("init")
        self.log = log
        self.places = places
        self.phone = phone
        self.location = location

    def get_cards(self):
        self.log.debug("get_cards")
        out = []
        for place in self.places.points:
            out.append({"mod": "owntrack", "card": "owntrack_place_"+place.replace(" ", "").lower(), "name": place})
        out.append({"mod": "owntrack", "card": "owntrack_phone", "name": "Owntrack Phone"})
        out.append({"mod": "owntrack", "card": "owntrack_map", "name": "Owntrack Karte"})
        return {"allowed": True, "data": out}

    def get_card(self):
        self.log.debug("get_card")
        out = {'places': {}}
        for place in self.places.points:
            val = self.places.points[place]
            out['places'][place.replace(" ", "").lower()] = {"name": place, "i": val["_in"], "o": val["_out"]}
        out['phone'] = self.phone.get_card()
        out['location'] = self.location.location
        return {"allowed": True, "data": out}

    def get_chart(self, sensor, mode):
        out = {"title": "Batterie Telefon [%]", "buttons": "D:W:M:Y:A", "data": [], "unit": "%", "pre": 0, "label": "Batterie"}
        if sensor == "bat":
            out["buttons"] = out["buttons"].replace(mode, mode+"!")
            out["data"] = self.phone.get_chart(mode)
        return {"allowed": True, "data": out}
