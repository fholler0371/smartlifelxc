import sys
import os
import json

def start(container, restart=True, cmd=False):
    lxcpath = "/".join(os.path.abspath(__file__).split("/")[:-2]) + "/_lxcache"
    lxcpath = lxcpath.replace("/.gitlab/smartlifelxc/core", "")
    res = os.popen("sudo lxc-info -s -P " + lxcpath + " -n " + container + " 2> /dev/null").read()
    if len(res) == 0:
        if cmd:
            print("Container '" + container + "' existiert nicht")
        return
    try:
        f = open(lxcpath + "/../container/" + container + ".json", "r")
        cfg = json.loads(f.read())
        f.close()
        if not "devices" in cfg:
            cfg["devices"] = []
    except:
        if cmd:
            print("Konnte Containerkonfiguration für '" + container + "' nicht lesen")
        return
    running = res.find("RUNNING") > -1
    if running:
        if restart:
            res = os.popen("sudo lxc-stop  -P " + lxcpath + " -n " + container).read()
            res = os.popen("sudo lxc-wait  -P " + lxcpath + " -n " + container + " -s STOPPED").read()
        else:
            if cmd:
                  print("Container '" + container + "' läuft bereits")
            return
    if len(cfg["devices"]) > 0:
        try:
            f = open(lxcpath + "/" + container + "/device.json", "r")
            dev = json.loads(f.read())
            f.close()
        except:
            if cmd:
                print("Devivedatei für '" + container + "' nicht lesen")
            return
        for device in cfg["devices"]:
            if not device["name"] in dev:
                if cmd:
                      print("Kein Device Eintrag für '" + device["name"] + "'")
                return
        os.popen("cp " + lxcpath + "/" + container + "/device.json " + lxcpath + "/" + container + "/rootfs/etc/device.list").read()
    if os.path.isfile(lxcpath + "/" + container + "/sl.cfg"):
        os.popen("cp " + lxcpath + "/" + container + "/sl.cfg " + lxcpath + "/" + container + "/rootfs/etc/sl.cfg").read()
    res = os.popen("sudo lxc-start  -P " + lxcpath + " -n " + container).read()
    res = os.popen("sudo lxc-wait  -P " + lxcpath + " -n " + container + " -s RUNNING").read()
    if len(cfg["devices"]) > 0:
        for device in cfg["devices"]:
           res = os.popen("sudo lxc-device -P " + lxcpath + " -n " + container + " -- add " + dev[device["name"]] + " 2>&1 ").read()
           if len(res) > 0:
               print("Could not bind device")
               res = os.popen("sudo lxc-stop  -P " + lxcpath + " -n " + container).read()
               return

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Es darf nur/muss ein Parameer sein.")
        sys.exit()
    container = sys.argv[1]
    start(container, cmd=True)
