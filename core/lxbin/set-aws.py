import json
import os

key=input('AWS-Key: ')
secret=input('AWS-Secret: ')

basepath = '/'.join(os.path.abspath(__file__).split("/")[:-2])
os.popen("mkdir -p " + basepath + "/config").read()

f = open(basepath+"/config/lxtool_aws.json", "w")
f.write(json.dumps({'key': key, 'secret': secret}, sort_keys=True, indent=4))
f.close()
