import sys
import os

def stop(name, cmd=False):
    lxcpath = "/".join(os.path.abspath(__file__).split("/")[:-2]) + "/_lxcache"
    lxcpath = lxcpath.replace("/.gitlab/smartlifelxc/core", "")
    res = os.popen("sudo lxc-info -s -P " + lxcpath + " -n " + name + " 2> /dev/null").read()
    if len(res) == 0:
        if cmd:
             print("Container '" + name + "' existiert nicht")
        return
    res = os.popen("sudo lxc-stop -P " + lxcpath + " -n " + name + " -t 60" ).read()
    res = os.popen("sudo lxc-wait -P " + lxcpath + " -n " + name + " -s STOPPED" ).read()

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Es darf nur/muss ein Parameer sein.")
        sys.exit()
    container = sys.argv[1]
    stop(container, cmd=True)
