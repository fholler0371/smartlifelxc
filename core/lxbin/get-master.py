import os
import json
import sys

try:
    import boto3
except:
    os.popen(sys.executable + " -m pip install boto3").read()
    import boto3


distro="debian"
release="buster"
arch="armhf"
config={"key":"", "secret":""}

basepath = '/'.join(os.path.abspath(__file__).split("/")[:-2])

try:
   f = open(basepath+"/config/lxtool_aws.json", "r")
   config = json.loads(f.read())
   f.close()
except:
    print("Keine AWS-Zugangsdaten")
    os._exit(1)

os.popen("mkdir -p " + basepath + "/_lxcache").read()

os.popen("sudo rm -r " + basepath + "/_lxcache/master").read()

cmd = "sudo lxc-create -n master -P " + basepath + "/_lxcache "
cmd += "-t /usr/share/lxc/templates/lxc-download  -- -d " + distro + " -r " + release + " -a " + arch
os.popen(cmd).read()

cmd = "sudo tar cfvz lxc_master.tar.gz _lxcache/master"
os.popen(cmd).read()

s3 = boto3.client('s3', aws_access_key_id=config['key'], aws_secret_access_key=config['secret'], region_name='eu-central-1')
try:
    response = s3.upload_file(basepath+'/lxc_master.tar.gz', 'images-smartlife', 'lxc_master.tar.gz')
except Exception as e:
    print(repr(e))

cmd="rm lxc_master.tar.gz"
os.popen(cmd).read()
