import sys
import os
import json
import time

def setup(name, cmd=False):
    basepath = "/".join(os.path.realpath(__file__).split('/')[:-2])
    basepath = basepath.replace("/.gitlab/smartlifelxc/core", "")
    os.popen("mkdir -p " + basepath + "/db").read()
    os.popen("mkdir -p " + basepath + "/log").read()
    file = basepath + "/container/" + name + ".json"
    if not os.path.isfile(file):
        if cmd:
            print("Contaniner-Daten existieren nicht")
        return

    try:
        f = open(file, "r")
        data = json.loads(f.read())
        f.close()
    except:
        if cmd:
            print("Fehler beim lesen der JsonDaten")
        return

    if not os.path.isdir(basepath + "/_lxcache/master"):
        try:
            f = open(basepath + "/config/lxtool_aws.json")
            config = json.loads(f.read())
            f.close()
        except:
            if cmd:
                print("AWS Credential nicht vorhandem")
            os._exit(1)
        try:
            import boto3
        except:
            os.popen(sys.executable + " -m pip install boto3").read()
            import boto3
        try:
            s3 = boto3.client('s3', aws_access_key_id=config['key'], aws_secret_access_key=config['secret'], region_name='eu-central-1')
            s3.download_file('images-smartlife', 'lxc_master.tar.gz', basepath+'/lxc_master.tar.gz')
            os.popen("cd " + basepath + " ; tar -xvzf lxc_master.tar.gz ; sudo rm lxc_master.tar.gz").read()
        except:
            if cmd:
                print("Master konnte nicht geladen werden")
            return
    f = open(basepath + "/_lxcache/master/config", "r")
    lines = f.read().split("\n")
    f.close()
    f = open(basepath + "/_lxcache/master/config", "w")
    for line in lines:
        if not line.startswith("lxc.rootfs.path"):
            f.write(line + "\n")
    f.write("lxc.rootfs.path = " + basepath + "/_lxcache/master/rootfs\n")
    f.close()

    res = os.popen("sudo lxc-info -s -P " + basepath + "/_lxcache/ -n master").read()
    if res.find('RUNNING') > 0:
        os.popen("sudo lxc-stop -P " + basepath + "/_lxcache/ -n master").read()
    res = os.popen("sudo lxc-info -s -P " + basepath.replace("/mnt/usb/", "/") + "/_lxcache/ -n master").read()
    if res.find('RUNNING') > 0:
        os.popen("sudo lxc-stop -P " + basepath.replace("/mnt/usb/", "/") + "/_lxcache/ -n master").read()

    try:
        os.popen("sudo lxc-copy -p " + basepath + "/_lxcache -P " + basepath + "/_lxcache -n master -N " + data['name']).read()
        os.popen("sudo chown -R pi:pi " + basepath + "/_lxcache/" + data['name']).read()
    except:
        if cmd:
            print("Container konnte nicht erstellt werden")
        return

    if not os.path.isfile("/etc/default/lxc-net"):
        f = open(basepath + "/lxc-net", "w")
        f.write('USE_LXC_BRIDGE="true"\nLXC_BRIDGE="lxcbr"\nLXC_ADDR="10.0.0.1"\nLXC_NETMASK="255.0.0.0"\nLXC_NETWORK="10.0.0.0/8"\n')
        f.write('LXC_DHCP_RANGE="10.0.0.100,10.0.254.254"\nLXC_DHCP_MAX="1000"\nLXC_DOMAIN="lxc"')
        f.close()
        os.popen("sudo mv " + basepath + "/lxc-net /etc/default/lxc-net").read()
        os.popen("sudo chown root:root /etc/default/lxc-net").read()
        os.popen("sudo systemctl start lxc-net").read

    f = open(basepath + "/_lxcache/" + data['name'] + "/config", "r")
    cfg = f.read().split('\n')
    f.close()
    out = []
    for line in cfg:
        if not (line.startswith("lxc.net.0") or line.startswith("lxc.mount.entry")):
            out.append(line)
    out.append("lxc.net.0.type = veth")
    out.append("lxc.net.0.flags = up")
    out.append("lxc.net.0.link = lxcbr")
    out.append("lxc.mount.entry = " + basepath + "/db home/db none rw,bind 0.0")
    out.append("lxc.mount.entry = " + basepath + "/log home/log none rw,bind 0.0")
    f = open(basepath + "/_lxcache/" + data['name'] + "/config", "w")
    cfg = f.write("\n".join(out))
    f.close()

    local_ip = os.popen("/sbin/ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep '192.168.'").read().split("\n")[0]
    f_in = open(basepath + "/container/" + data['name'] + ".json", "r")
    f_out = open(basepath + "/_lxcache/" + data['name'] + "/rootfs/etc/config", "w")
    f_out.write(f_in.read().replace("$host_name$", os.uname()[1]).replace("$host_ip$", local_ip))
    f_out.close()
    f_in.close()

    os.popen("sudo mkdir -p " + basepath + "/_lxcache/" + data['name'] + "/rootfs/home/log").read()
    os.popen("sudo mkdir -p " + basepath + "/_lxcache/" + data['name'] + "/rootfs/home/db").read()

    os.popen("sudo cp " + basepath + "/bin/setup_script.sh " + basepath + "/_lxcache/" + data['name'] + "/rootfs/etc/rc.local").read()
    os.popen("sudo chmod +x " + basepath + "/_lxcache/" + data['name'] + "/rootfs/etc/rc.local").read()
    res = os.popen("sudo lxc-info -s -P " + basepath + "/_lxcache/ -n " + data['name']).read()
    if res.find('RUNNING') > 0:
        os.popen("sudo lxc-stop -P " + basepath + "/_lxcache/ -n " + data['name']).read()
    res = os.popen("sudo lxc-info -s -P " + basepath.replace("/mnt/usb/", "/") + "/_lxcache/ -n " + data['name']).read()
    if res.find('RUNNING') > 0:
        os.popen("sudo lxc-stop -P " + basepath.replace("/mnt/usb/", "/") + "/_lxcache/ -n " + data['name']).read()
    os.popen("sudo lxc-start -n " + data['name'] + " -P " + basepath + "/_lxcache").read()
    os.popen("sudo lxc-wait -n " + data['name'] + " -P " + basepath + "/_lxcache -s RUNNING").read()
    os.popen("sudo lxc-wait -n " + data['name'] + " -P " + basepath + "/_lxcache -s STOPPED").read()

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Es darf nur/muss ein Parameer sein.")
        sys.exit()
    container = sys.argv[1]
    setup(container, cmd=True)
