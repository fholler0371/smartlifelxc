import os
import importlib
import sl_auth
from urllib import request
import json
import time

sl = None

def init(_sl):
    global sl
    sl = _sl

def get_menu(packages):
    out = []
    if "authencitator.user" in packages:
        out.append({'label': 'Passwort &auml;ndern', 'mod': 'authencitator', 'p1':'password'})
    if "authencitator.master" in packages:
        out.append({'label': 'Nutzerverwaltung', 'mod': 'authencitator', 'p1':'mangement'})
    return out

def get_data(data):
    if data["client"] == "authencitator.user":
        if data["cmd"] == "new_password":
            if data["data"]["new"] == data["data"]["repeat"] and len(data["data"]["new"]) > 6:
                sl_auth.change_password(data["token"]["user"], data["data"]["new"], data["data"]["old"])
        elif data["cmd"] == "mfa":
            data["data"] = {'ok' :sl_auth.mfa_check(data["token"]["user"], data["data"]["mfa"])}
            if data["data"]["ok"]:
                data["token"]["mfa_out"] = int(time.time()+3600)
            else:
                data["token"]["mfa_out"] = 0
            return data
    if data["client"] == "authencitator.master":
        if data["token"]["mfa"]:
            if time.time() > data["token"]["mfa_out"]:
                data["data"] = {"need_mfa": True}
                return data
        if data["cmd"] == "get_user":
            data["data"] = sl_auth.get_userlist()
        elif data["cmd"] == "get_rights":
            data["data"] = get_rights(data["data"]["user"])
        elif data["cmd"] == "new_user":
            data["data"] = sl_auth.new_user(data["data"]["user"])
        elif data["cmd"] == "real_name":
            data["data"] = sl_auth.set_real_name(data["data"]["user"], data["data"]["real"])
        elif data["cmd"] == "passwd":
            data["data"] = sl_auth.set_passwd(data["data"]["user"], data["data"]["passwd"])
        elif data["cmd"] == "rights":
            data["data"] = sl_auth.set_rights(data["data"]["user"], data["data"]["rights"])
        elif data["cmd"] == "reset":
            data["data"] = sl_auth.reset_pwd(data["data"]["user"])
        elif data["cmd"] == "mfa_activ":
            data["data"] = sl_auth.mfa(data["data"]["user"], True)
        elif data["cmd"] == "mfa_deactiv":
            data["data"] = sl_auth.mfa(data["data"]["user"], False)
    return data

def get_rights(user):
    global sl
    avaible = []
    path = sl["rootpath"] + "lib/module"
    for modul in os.listdir(path):
        mod = importlib.import_module("module." + modul)
        if hasattr(mod, "rights"):
           mod.init(sl)
           for right in mod.rights():
               avaible.append(right)
    for modul in sl["cfg"]["webmodule"]:
        url = "http://" + modul["host"] + ":9999/" + modul["name"] + "/rights"
        req =  request.Request(url)
        try:
            resp = request.urlopen(req, data="[]".encode())
            if resp.code == 200:
                for right in json.loads(resp.read().decode()):
                    avaible.append(right)
        except:
            pass
    avaible.sort(key=lambda x: x.upper(), reverse=False)
    data = sl_auth.get_rights(user)
    data["avaible"] = avaible
    return data

def rights():
    return ['authencitator.master', 'authencitator.user']
