#smartlife 4.0
#2021-10

import os
import socket
import time

def out_msg(msg, mode=0):
    end = "\n"
    out = ""
    OVER = "\r\033[K"
    COL_LIGHT_GREEN = "\033[32m"
    COL_NC = "\033[0m"
    if mode == 0:
        end = ""
        out = COL_NC + " [ ] " + msg + " ..."
    if mode == 1:
        out = COL_NC + OVER + " ["+COL_LIGHT_GREEN+"✓"+COL_NC+"] "+msg
    print(out, end=end, flush=True)
    time.sleep(0.25)

class screen:
    normal = "\033[m"
    menu = "\033[36m"
    fgred = "\033[31m"
    lgreen = "\033[32m"
    entries = []
    
    def clear(self):
        os.system("clear")

    def logo(self):
        print(self.menu)
        print("   _____                      _      _     _  __       _   _      ___")
        print("  / ____|                    | |    | |   (_)/ _|     | | | |    / _ \ ")
        print(" | (___  _ __ ___   __ _ _ __| |_   | |    _| |_ __   | |_| |   | | | |")
        print("  \___ \| '_ ` _ \ / _` | '__| __|  | |   | |  _/ _ \  \__  |   | | | |" )
        print("  ____) | | | | | | (_| | |  | |_   | |___| | ||  __/     | |   | |_| |")
        print(" |_____/|_| |_| |_|\__,_|_|  \___|  |_____|_|_| \___|     |_| _  \___/")
        print(self.normal)

    def host(self):
        print()
        hostname = socket.gethostname()
        try:
            ip = socket.gethostbyname(hostname+".dyn.lan")
        except:
            ip = ""
        print(self.menu+" Host: "+self.lgreen+hostname+self.menu+"        IP: "+self.lgreen+ip+self.normal)

    def showmenu(self):
        print()
        print(self.menu+" "+("*"*77)+self.normal)
        print(self.menu+" *"+(" "*75)+"*"+self.normal)
        l = 1
        for entry in self.entries:
            n = str(l)+") "
            lframe = " *"+(" "*(5-len(n)))
            print(self.menu+lframe+self.lgreen+n+self.menu+entry["l"]+" "*(70-len(entry["l"]))+"*")
            l += 1
        print(self.menu+" *"+(" "*75)+"*"+self.normal)
        print(self.menu+" * "+self.fgred+" x) "+self.menu+"Beenden"+" "*63+"*")
        print(self.menu+" *"+(" "*75)+"*"+self.normal)
        print(self.menu+" "+("*"*77)+self.normal)
        print()
        return input(self.menu+"  Eintrag auswählen: "+self.lgreen)
