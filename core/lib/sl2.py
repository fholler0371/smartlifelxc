# smartlife
# 04.2021

import os
import sys
import subprocess
import time
import threading
import pkg_resources as pkg
import signal

import yaml

import logger2
import key_bcrypt
from config_yaml import config

service_name="smartlife.service"

sl = None

class smartlife:
    def __init__(self, base_path, is_cmd):
        self.base_path = base_path
        self.is_cmd = is_cmd
        self.running = True
        self.cfg_path = base_path + "/config"
        self.log_path = base_path + "/log"
        self.plugin_path = base_path + "/plugins"
        self.www_path = base_path + "/www"
        self.db_path = base_path + "/db"
        signal.signal(signal.SIGINT, self.got_signal)
        signal.signal(signal.SIGTERM, self.got_signal)
        self.cfg = config(self.cfg_path+"/smartlife.yaml", pre={"plugins":{"local":[]}})
        print(self.cfg)
        print(self.cfg.cfg)
        changed = False
        if os.path.isfile(self.cfg_path+"/default_master.yaml"):
            default = config(self.cfg_path+"/default_master.yaml")
        elif os.path.isfile(self.cfg_path+"/default_sub.yaml"):
            default = config(self.cfg_path+"/default_sub.yaml")
        else:
            print("No default configuration")
            os._exit(1)
        if not ("logger" in self.cfg.cfg):
            self.cfg.cfg["logger"] = default.cfg["logger"]
            changed = True
        if not ("init_max" in self.cfg.cfg):
            self.cfg.cfg["init_max"] = default.cfg["init_max"]
            changed = True
        if not ("netcom_port" in self.cfg.cfg):
            self.cfg.cfg["netcom_port"] = default.cfg["netcom_port"]
            changed = True
        for plugin in default.cfg["plugins"]["local"]:
            if not(plugin in self.cfg.cfg["plugins"]["local"]):
                self.cfg.cfg["plugins"]["local"].append(plugin)
                changed = True
        if is_cmd and not ("time_out" in self.cfg.cfg):
            self.cfg.cfg["time_out"] = 60
            changed = True
        if changed:
            self.cfg.save()
        self.log = logger2.init(self.log_path, self.cfg.cfg["logger"], console=is_cmd)
        self.log.info("init")
        self.cfg.set_log(self.log)
        if not ( "key" in self.cfg.cfg ):
            self.log.info("need system key")
            self.cfg.cfg["key"] = key_bcrypt.get_new_key()
            self.cfg.save()
        self.init_max = self.cfg.cfg['init_max']
        self.plugins = {}
        for modul in self.cfg.cfg["plugins"]["local"]:
            self.log.info("Load Plugin: "+modul)
            try:
                res_name = self.plugin_path + "/" + modul + "/requirements.txt"
                if os.path.isfile(res_name):
                    f = open(res_name, "r")
                    requirements = pkg.parse_requirements(f.read())
                    f.close()
                    need_req_run = False
                    for req in requirements:
                        try:
                            pkg.resource_filename(pkg.Requirement.parse(str(req)), modul)
                        except:
                            need_req_run = True
                    if need_req_run:
                        subprocess.run([sys.executable, "-m", "pip", "install", "-r", res_name], capture_output=True)
                mod = __import__(modul)
                self.plugins[modul] = mod.integration(self, modul)
            except Exception as e:
                self.log.error("Plugin not loaded: "+modul)
                exc_type, exc_obj, exc_tb = sys.exc_info()
                fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
                self.log.error(repr(e))
                self.log.error(fname + " (" + str(exc_tb.tb_lineno) + ")" )

    def got_signal(self, signum, frame):
        self.log.info("signal")
        self.running = False

    def get_all_plugins(self):
        self.log.debug("get_all_plugins")
        out = []
        for plugin in self.plugins:
            if self.plugins[plugin].running and self.plugins[plugin].is_alive:
                out.append(self.plugins[plugin])
        return out

    def get_plugin(self, name):
        self.log.debug(name)
        if name in self.plugins:
            plugin = self.plugins[name]
            if plugin.running and plugin.is_alive:
                return plugin

    def run(self):
        self.log.info("run")
        for modul in self.plugins:
            self.plugins[modul].start()
        if self.is_cmd:
            tout = self.cfg.cfg["time_out"] + time.time()
        else:
            tout = 0
        while self.running and self.init_max > 0 and (not self.is_cmd or tout > time.time()):
            time.sleep(5)
            for modul in self.cfg.cfg["plugins"]["local"]:
                found = False
                for t in threading.enumerate():
                    if t.name == modul:
                        found = True
                if not found or not ( modul in self.plugins ):
                    self.log.debug("neu start: "+modul)
                    try:
                        self.init_max = self.init_max - 1
                        mod = __import__(modul)
                        self.plugins[modul] = mod.integration(self, modul)
                        self.plugins[modul].start()
                    except:
                        self.log.error("Plugin not loaded: "+modul)
                else:
                    need_stop = False
                    l = len(self.plugins[modul].alive)
                    i = 0
                    while i<l:
                        self.plugins[modul].alive[i] = self.plugins[modul].alive[i] - 5
                        if self.plugins[modul].alive[i] < 0:
                            need_stop = True
                        i += 1
                    if need_stop:
                        self.log.debug("timeout stop: "+modul)
                        if self.plugins[modul].running:
                            self.plugins[modul].stop()
                        else:
                            self.init_max = self.init_max - 1

    def stop(self):
        self.log.info("stop")
        for modul in self.plugins:
            self.plugins[modul].stop()
        for modul in self.plugins:
            self.plugins[modul].join(30)
        if threading.active_count() > 1:
            for t in threading.enumerate():
                if t != threading.main_thread():
                    self.log.error('Thread not stopped: '+t.name)
        self.log.info("finished")

    def config_update(self, data):
        self.log.debug(data)
        self.cfg.cfg.update(data)
        self.cfg.save()
    
def run(base_path, is_cmd):
    global sl
    start_service = False
    if is_cmd:
        start_service = subprocess.run(["systemctl", "is-active", service_name], capture_output=True).returncode == 0
        if start_service:
            subprocess.run(["sudo", "systemctl", "stop", service_name], capture_output=True)
    sl = smartlife(base_path, is_cmd)
    sl.run()
    sl.stop()
    if start_service:
        subprocess.run(["sudo", "systemctl", "start", service_name], capture_output=True)
