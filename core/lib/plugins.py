import time
import os
from threading import Thread

import config

class plugin_base(Thread):
    def __init__(self, name, cfg, log, cb_plugin):
        log.debug('init')
        Thread.__init__(self)
        self.running = True
        self.daemon = True
        self.name = name
        self.sl_cfg = cfg
        self.log = log
        self.cb_plugin = cb_plugin
        self.alive = [300]
        self.cfg = config.load(cfg['cfgpath'], name, log=log)

    def web_script(self):
        self.log.debug('web_script')
        name = self.sl_cfg['bpath']+'/plugins/'+self.name+'/script.js'
        if os.path.isfile(name):
            f = open(name, "rb")
            script = f.read()
            f.close()
            return script
        else:
           self.log.error('not found: '+name)
        return ""

    def web_menu(self, packages):
        self.log.debug(packages)
        return []

    def web_data(self, data):
        self.log.debug(data)
        return data

    def run(self):
        self.log.debug('run')
        while self.running:
            time.sleep(1)

    def stop(self):
        self.log.debug('stop')
        self.running = False
