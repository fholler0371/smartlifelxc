# smartlife
# 07.2021

import sys
import os
import subprocess
import time

import config_yaml as yaml

frame = "**************************************************"
normal = "\033[m"
menu = "\033[36m"
number = "\033[33m"
fgred = "\033[31m"
OVER = "\r\033[K"
COL_LIGHT_GREEN = "\033[32m"
COL_LIGHT_RED = "\033[31m"
COL_NC = "\033[0m"

def out_msg(msg, mode=None):
    end = "\n"
    out = ""
    if mode == None:
        end = ""
        out = " [ ] " + msg
    elif mode in ["+", "-"]:
        out = OVER + " ["+COL_LIGHT_GREEN+mode+COL_NC+"] "+msg
    elif mode == "1":
        out = OVER + " ["+COL_LIGHT_RED+"✗"+COL_NC+"] "+msg
    elif mode == "0":
        out = OVER + " ["+COL_LIGHT_GREEN+"✓"+COL_NC+"] "+msg
    print(out, end=end, flush=True)
    time.sleep(0.25)

def do_create_cfg(lpath, name, remote):
    msg = "create config for: "+name
    path = "/etc/letsencrypt/live/" + lpath
    out_msg(msg)
    cfg = "server {\n  listen 443 ssl;\n  server_name " + name + ";\n  ssl on;\n  ssl_certificate "
    cfg += path + "/fullchain.pem;\n  ssl_certificate_key "
    cfg += path + "/privkey.pem;\n  ssl_session_cache builtin:1000 shared:SSL:10m;\n  ssl_prefer_server_ciphers on;\n  "
    cfg += "ssl_ciphers HIGH:!aNULL:!eNULL:!LOW:!3DES:!MD5:!RC4;\n  "
    cfg += "add_header Strict-Transport-Security \"max-age=31536000; includeSubDomains\" always;\n  location / {\n    "
    cfg += "proxy_set_header X-Real-IP $remote_addr;\n    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;\n    "
    cfg += "proxy_set_header X-Host $host;\n    "
    cfg += "proxy_pass http://" + remote + ";\n  }\n}" 
    fname = "/etc/nginx/conf.d/" + name + ".conf"
    f = open(fname, "w")
    f.write(cfg)
    f.close()
    out_msg(msg, "0")

def do_delete():
    msg = "remove old data"
    out_msg(msg)
    cmd = "sudo rm /etc/nginx/conf.d/*"
    res = subprocess.run(cmd.split(" "), capture_output=True)
    out_msg(msg, "0")

def do_restart_ngix():
    msg = "restart nginx"
    out_msg(msg)
    cmd = "systemctl restart nginx.service"
    res = subprocess.run(cmd.split(" "), capture_output=True)
    out_msg(msg, "0")

def do_letsencrypt(servers):
    msg = "run letsencrypt"
    out_msg(msg)
    cmd = "sudo certbot certonly --rsa-key-size 4096 --webroot -w /var/www/letsencrypt --keep-until-expiring --expand"
    for server in servers:
        cmd += " -d " + server["name"]
    res = subprocess.run(cmd.split(" "), capture_output=True)
    out_msg(msg, "0")

if __name__ == "__main__" and len(sys.argv) == 2 and sys.argv[1] == "setup":
    path = "/".join(os.path.abspath(__file__).split("/")[:-2])
    path += "/config/proxy.yaml"
    cfg =  yaml.config(path)
    do_delete()
    for server in cfg.cfg["server"]:
        do_create_cfg(cfg.cfg["lpath"], server["name"], server["proxy"])
    do_letsencrypt(cfg.cfg["server"])
    do_restart_ngix()
