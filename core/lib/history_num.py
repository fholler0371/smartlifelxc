# smartlife
# 05.2021

import time
from threading import Timer
from database import database
from datetime import datetime

from dateutil import tz

class db:
    def __init__(self, log, name, cb):
        log.debug("init")
        self.log = log
        self.th = None
        self.running = True
        self.topics = {}
        self.cb = cb
        sql = "CREATE TABLE IF NOT EXISTS history (time integer, topic text, value float)"
        self.day = database(self.log, name.replace("$$", "one_day"), sql_create=sql)
        self.week_last = 300*int(time.time()/300)
        sql = "CREATE TABLE IF NOT EXISTS history (time integer, topic text, value float, value_min float, value_max float)"
        self.week = database(self.log, name.replace("$$", "one_week"), sql_create=sql)
        self.month_last = 3600*int(time.time()/3600)
        self.month = database(self.log, name, sql_create=sql)
        self.year_last = 24*3600*int(time.time()/24/3600)
        self.year = database(self.log, name, sql_create=sql)

    def start(self):
        self.log.debug("start")
        t = time.time()
        t = int((t/60)+1)*60-t
        self.th = Timer(t, self.tick)
        self.th.start()

    def tick(self):
        self.log.debug("tick")
        if self.running:
            t = time.time()
            t = int((t/60)+1)*60-t
            self.th = Timer(t, self.tick)
            self.th.start()
            self.topics = {}
            if self.cb:
                self.cb()
                t = int(time.time())
                self.day.open()
                self.day.execute("delete from history where time < " + str(t-86400))
                sql = "insert into history (time, topic, value) values ('%s', '%s', '%s')"
                for topic in self.topics:
                    self.day.execute(sql % (str(t), topic, str(self.topics[topic]), ))
                    self.day.commit()
                if self.week_last != 300*int(t/300):
                    self.week_last = 300*int(t/300)
                    self.week.open()
                    sql = "delete from history where time < " + str(t - 86400*7)
                    self.week.commit(sql)
                    sql = "select avg(value) as value, min(value) as value_min, max(value) as value_max from history where topic='%s' and time > '%s'"
                    sql2 = "insert into history (time, topic, value, value_min, value_max) values ('%s', '%s', '%s', '%s', '%s')"
                    for topic in self.topics:
                        res = self.day.fetchone(sql % (topic, str(t - 301), ))
                        if res:
                            self.week.execute(sql2 % (str(t), topic, str(res[0]), str(res[1]), str(res[2]), ))
                    self.week.commit()
                    d = datetime.utcfromtimestamp(t)
                    from_zone = tz.gettz("UTC")
                    to_zone = tz.gettz("Europe/Berlin")
                    d = d.replace(tzinfo=from_zone)
                    d = d.astimezone(to_zone)
                    sql = "select avg(value) as value, min(value_min) as value_min, max(value_max) as value_max from history where topic='%s'"
                    sql += " and time > '%s'"
                    if self.month_last != 3600*int(t/3600):
                        self.month_last = 3600*int(t/3600)
                        self.month.open(d.strftime("%y%m"))
                        for topic in self.topics:
                            res = self.week.fetchone(sql % (topic, str(t-3601), ))
                            if res:
                                self.month.execute(sql2 % (str(t), topic, str(res[0]), str(res[1]), str(res[2]), ))
                        self.month.commit()
                        self.month.close()
                    if self.year_last != 24*3600*int(t/24/3600):
                        self.year_last = 24*3600*int(t/24/3600)
                        self.year.open(d.strftime("%y"))
                        for topic in self.topics:
                            res = self.week.fetchone(sql % (topic, str(t-86401), ))
                            if res:
                                self.year.execute(sql2 % (str(t), topic, str(res[0]), str(res[1]), str(res[2]), ))
                        self.year.commit()
                        self.year.close()
                    self.week.close()
                self.day.close()

    def add(self, topic, value):
        self.log.debug(topic)
        self.topics[topic] = value

    def stop(self):
        self.log.debug("stop")
        self.running = False
        if self.th:
            self.th.cancel()

    def get_chart(self, topic, mode, faktor=1):
        self.log.debug(topic)
        out = []
        if mode == "D":
            self.day.open()
            sql = "SELECT time, value FROM history WHERE topic='%s' ORDER BY time"
            for row in self.day.fetchall(sql % (topic, )):
                out.append({"time": row[0], "value": row[1]*faktor})
            self.day.close()
        elif mode == "W":
            self.week.open()
            sql = "SELECT time, value, value_min, value_max FROM history WHERE topic='%s' ORDER BY time"
            for row in self.week.fetchall(sql % (topic, )):
                out.append({"time": row[0], "value": row[1]*faktor, "min": row[2]*faktor, "max": row[3]*faktor})
            self.week.close
        elif mode == "M":
            t = int(time.time())
            tstart = t - 30*24*60*60
            d = datetime.utcfromtimestamp(tstart)
            from_zone = tz.gettz('UTC')
            to_zone = tz.gettz('Europe/Berlin')
            d = d.replace(tzinfo=from_zone)
            d = d.astimezone(to_zone)
            if self.month.exists(d.strftime("%y%m")):
                self.month.open(d.strftime("%y%m"))
                sql = "SELECT time, value, value_min, value_max FROM history WHERE topic='%s' and time>'"+str(tstart)+"' ORDER BY time"
                for row in self.month.fetchall(sql % (topic, )):
                    out.append({"time": row[0], "value": row[1]*faktor, "min": row[2]*faktor, "max": row[3]*faktor})
                self.month.close()
            d = datetime.utcfromtimestamp(t)
            from_zone = tz.gettz('UTC')
            to_zone = tz.gettz('Europe/Berlin')
            d = d.replace(tzinfo=from_zone)
            d = d.astimezone(to_zone)
            if self.month.exists(d.strftime("%y%m")):
                self.month.open(d.strftime("%y%m"))
                sql = "SELECT time, value, value_min, value_max FROM history WHERE topic='%s' and time>'"+str(tstart)+"' ORDER BY time"
                for row in self.month.fetchall(sql % (topic, )):
                    out.append({"time": row[0], "value": row[1]*faktor, "min": row[2]*faktor, "max": row[3]*faktor})
                self.month.close()
        elif mode == "Y":
            t = int(time.time())
            tstart = t - 365*24*60*60
            d = datetime.utcfromtimestamp(tstart)
            from_zone = tz.gettz('UTC')
            to_zone = tz.gettz('Europe/Berlin')
            d = d.replace(tzinfo=from_zone)
            d = d.astimezone(to_zone)
            if self.year.exists(d.strftime("%y")):
                self.year.open(d.strftime("%y"))
                sql = "SELECT time, value, value_min, value_max FROM history WHERE topic='%s' and time>'"+str(tstart)+"' ORDER BY time"
                for row in self.year.fetchall(sql % (topic, )):
                    out.append({"time": row[0], "value": row[1]*faktor, "min": row[2]*faktor, "max": row[3]*faktor})
                self.year.close()
            d = datetime.utcfromtimestamp(t)
            from_zone = tz.gettz('UTC')
            to_zone = tz.gettz('Europe/Berlin')
            d = d.replace(tzinfo=from_zone)
            d = d.astimezone(to_zone)
            if self.year.exists(d.strftime("%y")):
                self.year.open(d.strftime("%y"))
                sql = "SELECT time, value, value_min, value_max FROM history WHERE topic='%s' and time>'"+str(tstart)+"' ORDER BY time"
                for row in self.year.fetchall(sql % (topic, )):
                    out.append({"time": row[0], "value": row[1]*faktor, "min": row[2]*faktor, "max": row[3]*faktor})
                self.year.close()
        elif mode == "A":
            t = int(time.time())
            d = datetime.utcfromtimestamp(t)
            from_zone = tz.gettz('UTC')
            to_zone = tz.gettz('Europe/Berlin')
            d = d.replace(tzinfo=from_zone)
            d = d.astimezone(to_zone)
            yend = int(d.strftime("%y"))
            ystart = yend
            while self.year.exists(('0'+str(ystart))[-2:]):
                ystart -= 1
            ystart += 1
            yend += 1
            while ystart < yend:
                self.year.open(('0'+str(ystart))[-2:])
                sql = "SELECT time, value, value_min, value_max FROM history WHERE topic='%s' ORDER BY time"
                for row in self.year.fetchall(sql % (topic, )):
                    out.append({"time": row[0], "value": row[1]*faktor, "min": row[2]*faktor, "max": row[3]*faktor})
                self.year.close()
                ystart += 1
        return out

