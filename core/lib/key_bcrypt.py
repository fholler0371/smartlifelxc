# smartlife
# 04.2021

import bcrypt

def get_new_key():
    return bcrypt.gensalt().decode()

def encode_password(password):
    return bcrypt.hashpw(password.encode(), get_new_key().encode()).decode()

def checkpw(password, code):
    return bcrypt.checkpw(password.encode(), code.encode())
