from functools import partial
from http.server import HTTPServer, BaseHTTPRequestHandler
from socketserver import ThreadingMixIn
from threading import Thread
import json

### thread #######################################

class loopThread(Thread):
    def __init__(self, server):
        Thread.__init__(self)
        self.daemon = True
        self.name = "WebServer"
        self.server = server

    def run(self):
        self.server.serve_forever()


### server #######################################

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
     """Handle requests in a separate thread."""

class webserverHandler(BaseHTTPRequestHandler):
    def __init__(self, log, cb_get, cb_post, *args, **kwargs):
        self.log = log
        self.cb_get = cb_get
        self.cb_post = cb_post
        super().__init__(*args, **kwargs)
    def log_message(self, format, *args):
        self.log.debug("\n    %s - - [%s] %s" % (self.address_string(), self.log_date_time_string(),format%args))
    def do_GET(self):
        resp = self.cb_get(self.path)
        if resp["code"] == 404:
            self.send_response(404)
            self.send_header('Content-Type', 'text')
            self.end_headers()
            self.wfile.write("404 File not found".encode())
        elif resp["code"] == 200:
            self.send_response(200)
            if 'cachetime' in resp:
                self.send_header('Cache-Control', 'max-age='+str(resp["cachetime"]))
            self.send_header('Content-Type', resp["mime"])
            self.end_headers()
            self.wfile.write(resp["data"])
        else:
            self.send_response(500)
            self.send_header('Content-Type', 'text')
            self.end_headers()
            self.wfile.write("Error 500".encode())
    def do_POST(self):
        content_len = int(self.headers['Content-Length'])
        data = self.rfile.read(content_len).decode()
        try:
            data = json.loads(data)
            data['ip'] = self.headers.get("X-Real-IP")
            data['host'] = self.headers.get("Host")
        except:
            pass
        resp = self.cb_post(self.path, data)
        try:
            if 'ip' in resp['data']:
                del resp['data']['ip']
            if 'host' in resp['data']:
                del resp['data']['host']
        except:
            pass
        if resp["code"] == -200:
            data = resp["data"].encode()
            self.send_response(200)
            self.send_header('Content-Type', 'application/json')
            self.send_header('Content-Length', str(len(data)))
            self.end_headers()
            self.wfile.write(data)
        elif resp["code"] == 200:
            data = json.dumps(resp["data"]).encode()
            self.send_response(200)
            self.send_header('Content-Type', 'application/json')
            self.send_header('Content-Length', str(len(data)))
            self.end_headers()
            self.wfile.write(data)
        elif resp["code"] == 404:
            self.send_response(404)
            self.send_header('Content-Type', 'text')
            self.end_headers()
            self.wfile.write("404 File not found".encode())
        else:
            self.send_response(500)
            self.send_header('Content-Type', 'text')
            self.end_headers()
            self.wfile.write("Error 500".encode())


def getServer(app_get, app_post, log, port):
    log.debug('getServer: '+str(port))
    handler = partial(webserverHandler, log, app_get, app_post)
    server = ThreadedHTTPServer(('0.0.0.0', port), handler)
    return server

def threadStart(server, log):
    log.debug('start loop')
    th = loopThread(server)
    th.start()

def serverStop(server, log):
    log.debug('shutdown')
    server.shutdown()
