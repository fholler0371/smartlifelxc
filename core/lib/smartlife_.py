import __main__
import os
import sys
import importlib
import time
import threading

import config
import logger

class c_smartlife():
    def __init__(self):
        bpath = os.path.dirname(os.path.abspath(__main__.__file__))
        cfgpath = bpath + '/config'
        self.cfg = config.load(cfgpath , __main__.__file__.split('.')[0].split('/')[-1])
        self.cfg['bpath'] = bpath
        self.cfg['cfgpath'] = cfgpath
        self.cfg['logpath'] = bpath + "/log"
        sys.path.insert(0, bpath + "/plugins")
        self.log = logger.init(self.cfg['logpath'], self.cfg["logger"])
        self.log.info("init")
        self.plugins = {}
        self.init_max = 100
        if 'init_max' in self.cfg:
            self.init_max = self.cfg['init_max']
        for modul in self.cfg["plugins"]:
            self.log.debug("Load Plugin: "+modul)
            try:
                mod = __import__(modul)
                self.plugins[modul] = mod.plugin(self.cfg, self.log, self.cb_plugins)
            except:
                self.log.error("Plugin not loaded: "+modul)

    def cb_plugins(self, name):
        self.log.debug(name)
        for modul in self.cfg["plugins"]:
            if modul == name:
                for t in threading.enumerate():
                     if t.name == name:
                         return self.plugins[modul]

    def run(self):
        self.log.info("run")
        for modul in self.plugins:
            self.plugins[modul].start()
        tout = self.cfg['timeout'] + time.time()
        while (self.cfg['timeout'] == -1 or tout > time.time()) and self.init_max > 0:
            time.sleep(15)
            for modul in self.cfg["plugins"]:
                found = False
                for t in threading.enumerate():
                    if t.name == modul:
                        found = True
                if not found or not ( modul in self.plugins ):
                    self.log.debug("neu start: "+modul)
                    try:
                        self.init_max = self.init_max - 1
                        mod = __import__(modul)
                        self.plugins[modul] = mod.plugin(self.cfg, self.log, self.cb_plugins)
                        self.plugins[modul].start()
                    except:
                        self.log.error("Plugin not loaded: "+modul)
                else:
                    need_stop = False
                    l = len(self.plugins[modul].alive)
                    i = 0
                    while i<l:
                        self.plugins[modul].alive[i] = self.plugins[modul].alive[i] - 15
                        if self.plugins[modul].alive[i] < 0:
                            need_stop = True
                        i += 1
                    if need_stop:
                        self.log.debug("timeout stop: "+modul)
                        self.plugins[modul].stop()

    def stop(self):
        self.log.info("stop")
        for modul in self.plugins:
            self.plugins[modul].stop()
            self.plugins[modul].join(30)
        if threading.active_count() > 1:
            for t in threading.enumerate():
                if t != threading.main_thread():
                    self.log.error('Thread not stopped: '+t.name)
        self.log.info("finished")

def run():
    sl = c_smartlife()
    sl.run()
    sl.stop()

