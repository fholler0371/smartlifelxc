# smartlife
# 05.2021
# 09.2021

class csv:
    def __init__(self, data, tab=";", header_start='', floatformat='de'):
        self.header = []
        self.lines = []
        self.floatformat = floatformat
        while data[0] == "\ufeff":
            data = data[1:]
        rows = data.split('\n')
        if header_start != '':
            while len(rows) > 0 and not(rows[0].startswith(header_start)):
                rows = rows[1:]
        for row in rows:
            r_data = []
            while len(row) > 0:
                if row.startswith("\""):
                    r = row.split("\""+tab, 1)
                    if len(r) > 1:
                        r_data.append(r[0][1:])
                        row = r[1]
                    else:
                        r_data.append(r[0][1:-1])
                        row = ""
                else:
                    r = row.split(tab, 1)
                    r_data.append(r[0])
                    if len(r) > 1:
                        row = r[1]
                    else:
                        row = ""
            if self.header == []:
                self.header = r_data
            else:
                if len(r_data) > 1:
                    self.lines.append(r_data)
        self.count = len(self.lines)

    def get_field(self, field):
        i = 0
        for entry in self.header:
            if entry in [field, "\""+field+"\""] :
               return i
            i += 1
        return -1

    def get_date(self, line, field):
        try:
            val = self.get_text(line, field)
            if val == "":
                return '0000-00-00'
            val = val[:10]
            val = val[6:]+'-'+val[3:5]+'-'+val[:2]
        except:
            return '0000-00-00'
        if len(val) != 10:
            if len(val) != 8:
                return '0000-00-00'
            else:
                val = '20' + val
        return val

    def get_text(self, line, field):
        id = self.get_field(field)
        if id == -1:
            return ''
        try:
            if self.lines[line][id].startswith("\""):
                return self.lines[line][id][1:-1]
            else:
                return self.lines[line][id]
        except:
            return ''

    def get_float(self, line, field):
        try:
            val = self.get_text(line, field)
            if self.floatformat == "de":
                val = val.replace(".", "").replace(",", ".")
            val = val.split(" ")[0]
            val = float(val)
            return val
        except:
            return 0
