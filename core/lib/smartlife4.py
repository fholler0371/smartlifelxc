#smartlife 4.0
#2021-10

version = "2021-10"
files_lib = ["config_yaml4", "logger4", "server4"]
files_bin = []

import time
import signal
import copy
import importlib

from config_yaml4 import config
import logger4 as logger
import server4 as server

class smartlife:
    def __init__(self, base_path, app, cmd, version, secret= False):
        self.version = version
        self.cfg = {"path_base": base_path, "app": app, "cmd": cmd} 
        self.cfg["path_app"] = self.cfg["path_base"] + "app/" + self.cfg["app"] + "/"
        self.cfg["path_config"] = self.cfg["path_base"] + "config/"
        self.cfg["path_log"] = self.cfg["path_base"] + "log/"
        self.cfg = config(self.cfg["path_app"] + "config.yaml", pre = self.cfg)
        self.cfg = config(self.cfg.cfg["path_config"] + app + ".yaml", pre = self.cfg.cfg)
        self.log = logger.init(self.cfg)
        self.cfg.log = self.log
        if secret:
            cfg = config(self.cfg.cfg["path_config"] + "secret.yaml", log=self.log)
            self.cfg.secret = copy.copy(cfg.cfg) 
        signal.signal(signal.SIGINT, self.got_signal)
        signal.signal(signal.SIGTERM, self.got_signal)
        self.log.info("init")
        self.log.info("Version: " + self.version)
        self.running = True
        self.starttime = time.time()
        self.module = []

    def check_version(self, new_lib, new_bin):
        self.log.debug("check_versionen")
        if self.cfg.cfg["cmd"]:
            print("Check Versionen")
            print("===============")
        _lib = {}
        _bin = {}
        for lib in new_lib:
            if not lib in _lib:
                _lib[lib] = {"checked" : False, "version" : ""}
        for bin in new_bin:
            if not bin in _bin:
                _bin[bin] = {"checked" : False, "version" : ""}
        recheck = True
        while recheck:
            recheck = False
            new_lib = {}
            new_bin = {}
            for entry in _lib:
                if not(_lib[entry]["checked"]):
                    if self.cfg.cfg["cmd"]:
                        if self.cfg.cfg["cmd"]:
                            print(entry+": ", end="")
                        mod = importlib.import_module(entry.replace("/", "."))
                        if not hasattr(mod, "version"):
                            if self.cfg.cfg["cmd"]:
                                print("no version", end="")
                            else:
                                self.log.error(entry+": no version info")
                        else:
                            if self.cfg.cfg["cmd"]:
                                print("ver: "+mod.version, end=" ")
                            _lib[entry]["version"] = mod.version
                            if mod.version > self.version:
                                if self.cfg.cfg["cmd"]:
                                    print("; main need update !!! ", end=" ")
                                else:
                                    self.log.critical(entry+": main need update")
                        print("")
                        if hasattr(mod, "files_lib"):
                            for _entry in mod.files_lib:
                                if not (_entry in _lib) and not (_entry in new_lib):
                                    new_lib[_entry] = {"checked" : False, "version" : ""}
                                    recheck = True
                        if hasattr(mod, "files_bin"):
                            for _entry in mod.files_bin:
                                if not (_entry in _bin) and not (_entry in new_bin):
                                    new_bin[_entry] = {"checked" : False, "version" : ""}
                                    recheck = True
                        _lib[entry]["checked"] = True
            for entry in _bin:
                if not(_bin[entry]["checked"]):
                    if self.cfg.cfg["cmd"]:
                        if self.cfg.cfg["cmd"]:
                            print(entry+": ", end="")
                        mod = importlib.import_module(entry.replace("/", "."))
                        if not hasattr(mod, "version"):
                            if self.cfg.cfg["cmd"]:
                                print("no version", end="")
                            else:
                                self.log.error(entry+": no version info")
                        else:
                            if self.cfg.cfg["cmd"]:
                                print("ver: "+mod.version, end=" ")
                            _bin[entry]["version"] = mod.version
                            if mod.version > self.version:
                                if self.cfg.cfg["cmd"]:
                                    print("; main need update !!! ", end=" ")
                                else:
                                    self.log.critical(entry+": main need update")
                        print("")
                        if hasattr(mod, "files_lib"):
                            for _entry in mod.files_lib:
                                if not (_entry in _lib) and not (_entry in new_lib):
                                    new_lib[_entry] = {"checked" : False, "version" : ""}
                                    recheck = True
                        if hasattr(mod, "files_bin"):
                            for _entry in mod.files_bin:
                                if not (_entry in _bin) and not (_entry in new_bin):
                                    new_bin[_entry] = {"checked" : False, "version" : ""}
                                    recheck = True
                        _bin[entry]["checked"] = True
            for entry in new_lib:
                _lib[entry] = new_lib[entry]
            for entry in new_bin:
                _bin[entry] = new_bin[entry]

    def got_signal(self, signum, frame):
        self.log.info("signal")
        self.running = False

    def run(self):
        self.log.info("run")
        if "port" in self.cfg.cfg:
            if hasattr(self, "respond"):
                self.server = server.getServer(self)
                if not self.server == None:
                    if not server.threadStart(self):
                        self.running = False
                        self.log.error("server nicht gestartet")
                else:
                    self.running = False
                    self.log.error("server nicht geladen")
            else:
                self.running = False
                self.log.error("respond is missing")
        for modul in self.module:
            if hasattr(modul, "start"):
                modul.start()
        while self.running:
            if self.cfg.cfg["cmd"]:
                limit = 30
                if "time_limit" in self.cfg.cfg:
                    limit = self.cfg.cfg["time_limit"]
                if self.starttime + limit < time.time():
                    self.running = False

    def stop(self):
        self.log.info("stop")
        if hasattr(self, "server") and not server == None:
            server.serverStop(self)
        for modul in self.module:
            if hasattr(modul, "stop"):
                modul.stop()
