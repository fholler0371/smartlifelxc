import os
import sys
import time
import json
import base64
import sqlite3

try:
    import bcrypt
except:
    os.popen(sys.executable + ' -m pip install bcrypt').read()
    import bcrypt
try:
    from Crypto.Cipher import AES
except:
    os.popen(sys.executable + ' -m pip install pycryptodome').read()
    from Crypto.Cipher import AES
try:
    import qrcode
except:
    os.popen(sys.executable + ' -m pip install qrcode[pil]').read()
    import qrcode
try:
    import qyotp
except:
    os.popen(sys.executable + ' -m pip install pyotp').read()
    import pyotp

sl = None
db_name = ""
aes = ""

def init(_sl):
    global sl, db_name
    sl = _sl
    db_name = sl["rootpath"] + "db/user.sqlite3"
    if not( os.path.isfile(db_name) ):
        create_db()
    check_for_master_user()
    check_aes()

def mfa_check(user, token):
    global db_name
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()
    sql = "select mfa from user where user = '%s'"
    cur.execute(sql % user)
    row = cur.fetchone()
    if row:
        totp = pyotp.TOTP(row[0])
        return totp.verify(token)
    return False

def mfa(user, state):
    global db_name
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()
    if state:
        code = base64.b32encode(bcrypt.gensalt()).decode().replace("=", "")
        sql = "update user set mfa='"+code+"' where user = '%s'"
        cur.execute(sql % user)
        conn.commit()
        link = "otpauth://totp/SL:"+user+"?secret="+code+"&issuer=SL"
        img = qrcode.make(link)
        fname = sl["rootpath"] + "www/lib/img/qr_"+code+".png"
        img.save(fname)
        os.popen("sleep 10; rm "+sl["rootpath"]+"www/lib/img/qr_*.png")
        conn.close()
        return {"data":"qr_"+code+".png"}
    else:
        sql = "update user set mfa='' where user = '%s'"
        cur.execute(sql % user)
        conn.commit()
        conn.close()
        return {"ok": True}

def encode(data):
    global aes
    cipher = AES.new(aes[:16].encode(), AES.MODE_EAX)
    data['token']['timeout'] = int(time.time()+900)
    if 'cmd' in data['token']:
        del data['token']['cmd']
    if 'client' in data['token']:
        del data['token']['client']
    token = json.dumps(data['token']).encode()
    ciphertext, tag = cipher.encrypt_and_digest(token)
    data['token'] = base64.encodestring(cipher.nonce+ciphertext).decode().replace('\n', '')
    return data

def decode(data):
    global aes
    try:
        ciphertext = base64.decodestring(data['token'].encode())
        nonce = ciphertext[:AES.block_size]
        ciphertext = ciphertext[AES.block_size:]
        cipher = AES.new(aes[:16].encode(), AES.MODE_EAX, nonce=nonce)
        text = cipher.decrypt(ciphertext)
        data['token'] = json.loads(text.decode())
#        print(data['token'])
        data['login'] = data['token']['timeout'] > time.time()
    except:
        data['login'] = False
    return data


def login(data):
    global db_name
    conn = sqlite3.connect(db_name)
    user = data['data']['user']
    cur = conn.cursor()
    sql = "select passwd, name, id, mfa from user where user= ? "
    cur.execute(sql, (user, ))
    res = cur.fetchone()
    if res:
        user_info = res
    else:
        user_info = ["", "", -1, ""]
    sql = "select right from rights where user='" + str(user_info[2]) + "' order by right"
    cur.execute(sql)
    res = cur.fetchall()
    if not res:
        res = []
    rights = []
    for right in res:
        rights.append(right[0])
    conn.close()
    if len(user_info[0]) > 0 and bcrypt.checkpw(data['data']['passwd'].encode(), user_info[0].encode()):
        rec = [True, user_info[1], rights]
    else:
        rec = [False, user_info[1], []]
    out = data
    out['login'] = rec[0]
    if 'token' in out:
        del out['token']
    if 'data' in out:
        del out['data']
    if out["login"]:
        out["name"] = rec[1]
        out["token"] = {'timeout': 0, 'packages': rec[2], 'user': user, 'mfa': not(user_info[3] == ""), 'mfa_out':0}
    return out

def change_password(user, pw_new, pw_old):
    global db_name
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()
    sql = "select passwd, id from user where user='" + user + "' "
    cur.execute(sql)
    res = cur.fetchone()
    user_id = res[1]
    if bcrypt.checkpw(pw_old.encode(), res[0].encode()):
        salt = bcrypt.gensalt()
        hashed = bcrypt.hashpw(pw_new.encode(), salt).decode()
        sql = "update user set passwd='" + hashed + "' where id='" + str(user_id) + "'"
        cur.execute(sql)
        conn.commit()
    conn.close()

def get_userlist():
    global db_name
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()
    sql = "select user from user order by user"
    cur.execute(sql)
    out = []
    res = cur.fetchall()
    for user in res:
        out.append(user[0])
    conn.close()
    return out

def get_rights(user):
    global db_name
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()
    sql = "select id, name from user where user = ? "
    cur.execute(sql, (user, ))
    res = cur.fetchone()
    name = res[1]
    user_id = res[0]
    sql = "select right from rights where user='" + str(user_id) + "' order by right"
    cur.execute(sql)
    rights = []
    for right in cur.fetchall():
        rights.append(right[0])
    conn.close()
    return {"name": name, "enable": rights}

def new_user(user):
    global db_name
    out = {'ok': True}
    if len(user) < 6:
        out = {'ok': False}
        return out
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()
    sql = "select id from user where user= ? " 
    cur.execute(sql, (user, ))
    res = cur.fetchone()
    if res:
        out = {'ok': False}
        conn.close()
        return out
    sql = "insert into user (user, name, passwd) values ( ? , ? , '')"
    cur.execute(sql, (user, user, ))
    conn.commit()
    conn.close()
    return out

def set_real_name(user, name):
    global db_name
    out = {'ok': True}
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()
    sql = "update user set name= ?  where user= ?"
    cur.execute(sql, (name, user, ))
    conn.commit()
    conn.close()
    return out

def set_passwd(user, passwd):
    global db_name
    out = {'ok': True}
    if len(passwd) < 6:
        out = {'ok': False}
        return out
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()
    salt = bcrypt.gensalt()
    hashed = bcrypt.hashpw(passwd.encode(), salt).decode()
    sql = "update user set passwd='" + hashed  + "' where user= ? "
    cur.execute(sql, (user, ))
    conn.commit()
    conn.close()
    return out

def set_rights(user, rights):
    global db_name
    out = {'ok': True}
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()
    sql = "select id from user where user= ?"
    cur.execute(sql, (user, ))
    res = cur.fetchone()
    if res == None:
        out = {'ok': False}
        return out
    user_id = res[0]
    sql = "delete from rights where user='" + str(user_id) + "'"
    cur.execute(sql)
    conn.commit()
    for right in rights:
        sql = "insert into rights (user, right) values ('" + str(user_id) + "', ? )"
        cur.execute(sql, (right, ))
    conn.commit()
    conn.close()
    return out

def reset_pwd(user):
    global db_name
    out = {'ok': True}
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()
    sql = "update user set passwd='' where user= ? "
    cur.execute(sql, (user, ))
    conn.commit()
    conn.close()
    return out

def create_db():
    global db_name
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()
    sql = "CREATE TABLE IF NOT EXISTS user (id integer PRIMARY KEY, name text NOT NULL, user text Not Null, passwd text Not Null, mfa text default '')"
    cur.execute(sql)
    sql = "CREATE TABLE IF NOT EXISTS rights (id integer PRIMARY KEY, user integer NOT NULL, right text Not Null)"
    cur.execute(sql)
    sql = "CREATE TABLE IF NOT EXISTS aes (id integer PRIMARY KEY, aes_key text NOT NULL)"
    cur.execute(sql)
    conn.commit()
    conn.close()

def check_for_master_user():
    global db_name
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()
    sql = "select user from rights where right='authencitator.master'"
    cur.execute(sql)
    res = cur.fetchone()
    if not res:
        sql = "select id from user where user='admin'"
        cur.execute(sql)
        res = cur.fetchone()
        if not res:
            salt = bcrypt.gensalt()
            hashed = bcrypt.hashpw("admin".encode(), salt).decode()
            sql = "insert into user (name, user, passwd) values ('Admin', 'admin', '" + hashed +  "')"
            cur.execute(sql)
            conn.commit()
            sql = "select id from user where user='admin'"
            cur.execute(sql)
            res = cur.fetchone()
        user_id = str(res[0])
        for right in ['authencitator.master', 'authencitator.user']:
            sql = "select user from rights where right='" + right + "' and user='" + user_id + "'"
            cur.execute(sql)
            res = cur.fetchone()
            if not res:
                sql = "insert into rights (right, user) values ('" + right + "', '" + user_id + "')"
                cur.execute(sql)
                conn.commit()
    conn.close()

def check_aes():
    global db_name, aes
    conn = sqlite3.connect(db_name)
    cur = conn.cursor()
    sql = "SELECT aes_key FROM aes WHERE id='1'"
    cur.execute(sql)
    res = cur.fetchone()
    if res == None:
        aes = bcrypt.gensalt().decode()
        sql = "INSERT INTO aes (aes_key) VALUES ('" + aes + "')"
        cur.execute(sql)
        conn.commit()
    else:
        aes = res[0]
    conn.close()
