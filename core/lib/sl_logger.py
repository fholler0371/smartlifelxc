import logging
from logging.handlers import RotatingFileHandler

hfile = None
logger = None

class MyFormatter(logging.Formatter):
    width = 50
    datefmt='%Y-%m-%d %H:%M:%S'

    def format(cls, record):
        cpath = '%s:%s:%s:%s' % (record.pathname.split('/')[-2], record.module, record.funcName, record.lineno)
        cpath = cpath[-cls.width:].ljust(cls.width)
        record.message = record.getMessage()
        s = "%-8s %s %s : %s" % (record.levelname, cls.formatTime(record, cls.datefmt), cpath, record.getMessage())
        return s

def create(_sl, _path):
    global hfile, logger
    logger = logging.getLogger(_sl["cfg"]["name"])
    file_name = _path + '/' + _sl["cfg"]["name"] + '.log'
    hfile = RotatingFileHandler(file_name, maxBytes=131072, backupCount=10)
    hfile.setFormatter(MyFormatter())
    logger.addHandler(hfile)
    if "log_level" in _sl["cfg"]:
        slevel = _sl["cfg"]["log_level"]
    else:
        slevel = "info"
    if slevel == 'debug':
        logger.setLevel(logging.DEBUG)
    elif slevel == 'info':
        logger.setLevel(logging.INFO)
    elif slevel == 'warning':
        logger.setLevel(logging.WARNING)
    elif slevel == 'error':
        logger.setLevel(logging.ERROR)
    elif slevel == 'critical':
        logger.setLevel(logging.CRITICAL)
    return logger
