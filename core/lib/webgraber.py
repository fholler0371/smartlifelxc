# smartlife
# 07.2021
# 08.2021

import urllib
import urllib.request
import urllib.parse
import json

class NoRedirect(urllib.request.HTTPRedirectHandler):
    def redirect_request(self, req, fp, code, msg, headers, newurl):
        return None

class webgraber(object):
    def __init__(self, log):
        log.debug("init")
        self.log = log
        self.od = urllib.request.build_opener(NoRedirect)
        self.encoding = 'iso-8859-1'
        self.cookies = []

    def RequestGet(self, name):
        self.last_url = name
        self.url = urllib.request.Request(name)
        self.addbrowser()
        self.url.add_header('Cookie','; '.join(self.cookies))

    def RequestPost(self, name, data):
        self.RequestPostRaw(name, urllib.parse.urlencode(data))

    def RequestPostRaw(self, name, data):
        self.last_url = name
        self.url = urllib.request.Request(name, data.encode())
        self.addbrowser()
        self.url.add_header('Cookie','; '.join(self.cookies))
        self.url.add_header('Content-Length',len(data.encode()))
        self.url.add_header('Content-Type','application/x-www-form-urlencoded')

    def RequestPostJson(self, name, data):
        self.last_url = name
        data = json.dumps(data)
        self.url = urllib.request.Request(name, data.encode())
        self.addbrowser()
        self.url.add_header('Cookie','; '.join(self.cookies))
        self.url.add_header('Content-Length',len(data.encode()))

    def open(self):
        try:
            self.res = self.od.open(self.url)
        except urllib.error.HTTPError as e:
            self.res = e
        self.code = self.res.getcode()
        if self.code == 302 or self.code == 301:
            self.scanheadder()
            if self.location != "":
                if self.location.find('://') < 0:
                    h = self.last_url.split('/')
                    self.location = h[0]+'//'+h[2]+self.location
                self.RequestGet(self.location)
                try:
                    self.res = self.od.open(self.url)
                except urllib.error.HTTPError as e:
                    self.res = e
                self.code = self.res.getcode()
            else:
                self.code = 999
        if self.code == 302 or self.code == 301:
            self.scanheadder()
            if self.location != "":
                if self.location.find('://') < 0:
                    h = self.last_url.split('/')
                    self.location = h[0]+'//'+h[2]+self.location
                self.RequestGet(self.location)
                try:
                    self.res = self.od.open(self.url)
                except urllib.error.HTTPError as e:
                    self.res = e
                self.code = self.res.getcode()
            else:
                self.code = 999
        if self.code == 302 or self.code == 301:
            self.scanheadder()
            if self.location != "":
                if self.location.find('://') < 0:
                    h = self.last_url.split('/')
                    self.location = h[0]+'//'+h[2]+self.location
                self.RequestGet(self.location)
                try:
                    self.res = self.od.open(self.url)
                except urllib.error.HTTPError as e:
                    self.res = e
                self.code = self.res.getcode()
            else:
                self.code = 999
        if self.code == 302 or self.code == 301:
            self.scanheadder()
            if self.location != "":
                if self.location.find('://') < 0:
                    h = self.last_url.split('/')
                    self.location = h[0]+'//'+h[2]+self.location
                self.RequestGet(self.location)
                try:
                    self.res = self.od.open(self.url)
                except urllib.error.HTTPError as e:
                    self.res = e
                self.code = self.res.getcode()
            else:
                self.code = 999
        if self.code == 302 or self.code == 301:
            self.scanheadder()
            if self.location != "":
                if self.location.find('://') < 0:
                    h = self.last_url.split('/')
                    self.location = h[0]+'//'+h[2]+self.location
                self.RequestGet(self.location)
                try:
                    self.res = self.od.open(self.url)
                except urllib.error.HTTPError as e:
                    self.res = e
                self.code = self.res.getcode()
            else:
                self.code = 999
        self.scanheadder()
        if self.code == 200 or self.code == 201:
            self.page = self.res.read().decode(self.encoding)
            return True
        else:
            return False

    def openfile(self):
        try:
            self.res = self.od.open(self.url)
        except urllib.error.HTTPError as e:
            self.res = e
        self.code = self.res.getcode()
        if self.code == 302:
            self.scanheadder()
            if self.location != "":
                if self.location.find('://') < 0:
                    h = self.last_url.split('/')
                    self.location = h[0]+'//'+h[2]+self.location    
        try:
            self.res = self.od.open(self.url)
        except urllib.error.HTTPError as e:
            self.res = e
        self.code = self.res.getcode()
        if self.code == 302 or self.code == 301:
            self.scanheadder()
            if self.location != "":
                if self.location.find('://') < 0:
                    h = self.last_url.split('/')
                    self.location = h[0]+'//'+h[2]+self.location
                self.RequestGet(self.location)
                try:
                    self.res = self.od.open(self.url)
                except urllib.error.HTTPError as e:
                    self.res = e
                self.code = self.res.getcode()
            else:
                self.code = 999
        if self.code == 302 or self.code == 301:
            self.scanheadder()
            if self.location != "":
                if self.location.find('://') < 0:
                    h = self.last_url.split('/')
                    self.location = h[0]+'//'+h[2]+self.location
                self.RequestGet(self.location)
                try:
                    self.res = self.od.open(self.url)
                except urllib.error.HTTPError as e:
                    self.res = e
                self.code = self.res.getcode()
            else:
                self.code = 999
        if self.code == 302 or self.code == 301:
            self.scanheadder()
            if self.location != "":
                if self.location.find('://') < 0:
                    h = self.last_url.split('/')
                    self.location = h[0]+'//'+h[2]+self.location
                self.RequestGet(self.location)
                try:
                    self.res = self.od.open(self.url)
                except urllib.error.HTTPError as e:
                    self.res = e
                self.code = self.res.getcode()
            else:
                self.code = 999
        if self.code == 302 or self.code == 301:
            self.scanheadder()
            if self.location != "":
                if self.location.find('://') < 0:
                    h = self.last_url.split('/')
                    self.location = h[0]+'//'+h[2]+self.location
                self.RequestGet(self.location)
                try:
                    self.res = self.od.open(self.url)
                except urllib.error.HTTPError as e:
                    self.res = e
                self.code = self.res.getcode()
                self.RequestGet(self.location)
                try:
                    self.res = self.od.open(self.url)
                except urllib.error.HTTPError as e:
                    self.res = e
                self.code = self.res.getcode()
            else:
                self.code = 999
        if self.code == 302:
            self.scanheadder()
            if self.location != "":
                if self.location.find('://') < 0:
                    h = self.last_url.split('/')
                    self.location = h[0]+'//'+h[2]+self.location
                self.RequestGet(self.location)
                try:
                    self.res = self.od.open(self.url)
                except urllib.error.HTTPError as e:
                    self.res = e
                self.code = self.res.getcode()
            else:
                self.code = 999
        self.scanheadder()
        if self.code == 200:
            self.filedata = self.res.read()
            return True
        else:
            return False

    def addbrowser(self):
        self.url.add_header('user-agent','Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36')
        self.url.add_header('accept-language','de-DE,de;q=0.8,en-US;q=0.6,en;q=0.4,fil;q=0.2')
        self.url.add_header('accept-encoding','')
        self.url.add_header('accept','text/html,application/json,text/javascript,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8')
        self.url.add_header('cache-control','max-age=0')

    def getdata(self, pre, post):
        start = 0
        for s in pre:
            start = self.page.find(s, start)+len(s)
        ende = self.page.find(post, start)
        return self.page[start:ende]

    def scanheadder(self):
        self.location = ''
        self.headers = str(self.res.info()).split('\n')
        for head in self.headers:
            if (head.find('Set-Cookie') == 0) or (head.find('Set-cookie') == 0) or (head.find('set-cookie') == 0):
                self.setcookie(head.split(' ',1)[1].split(';',1)[0])
            elif (head.find('Location') == 0):
                self.location = head.split(' ',1)[1]

    def setcookie(self, cookie):
        found = False
        i = 0
        while i < len(self.cookies):
            if self.cookies[i][:self.cookies[i].find('=')] == cookie[:cookie.find('=')]:
                self.cookies[i] = cookie
                found = True
            i = i + 1
        if not found:
            self.cookies.append(cookie)

    def addheader(self, name, value):
        self.url.headers[name] = value