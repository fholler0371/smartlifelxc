# smartlife
# 04.2021

import os
import sqlite3

class database:
    def __init__(self, log, name, sql_create=""):
        self.log = log
        if log:
            self.log.debug(name)
        self.name = name
        self.sql_create = sql_create
        if (not ("$$" in self.name)) and self.sql_create != "":
            self.open()
            self.commit(self.sql_create)
            self.close()

    def exists(self, id = ""):
        if self.log:
            self.log.debug(id)
        name = self.name.replace("$$", str(id))
        return os.path.isfile(name)

    def open(self, id = ""):
        if self.log:
            self.log.debug(id)
        name = self.name.replace("$$", str(id))
        self.con = sqlite3.connect(name)
        self.cur = self.con.cursor()
        if "$$" in self.name and self.sql_create != "":
            self.commit(self.sql_create)

    def close(self):
        if self.log:
            self.log.debug('close')
        self.con.close()

    def execute(self, sql):
        if self.log:
            self.log.debug(sql)
        self.cur.execute(sql)

    def commit(self, sql=""):
        if self.log:
            self.log.debug(sql)
        if sql != "":
            self.execute(sql)
        self.con.commit()

    def fetchall(self, sql):
        if self.log:
            self.log.debug(sql)
        self.execute(sql)
        return self.cur.fetchall()

    def fetchone(self, sql):
        if self.log:
            self.log.debug(sql)
        self.execute(sql)
        return self.cur.fetchone()
