# smartlife
# 04.2021
# 05.2021

import os
import time
from threading import Thread
import copy
import requests
import json

from logger2 import level_up
from config_yaml import config

class plugin_base(Thread):
    def __init__(self, smartlife, modul):
        Thread.__init__(self)
        self.daemon = True
        self.smartlife = smartlife
        self.alive = [300]
        self.running = True
        self.log = smartlife.log
        self.ready = False
        smartlife.log.info("init: "+modul)
        self.name = modul
        self.cfg_master = smartlife.cfg.cfg
        self.base_path = smartlife.plugin_path + "/" + modul
        self.manifest = config(self.base_path + "/manifest.yaml").cfg
        self.cfg_default = config(self.base_path + "/config.yaml").cfg
        self.cfg_custom = config(smartlife.cfg_path + "/" + modul + ".yaml")
        self.cfg_join()
        use_masterlog = True
        if "debug_level_up" in self.cfg:
            if self.cfg["debug_level_up"]:
                self.log = level_up(smartlife.log, modul)
                use_masterlog = False
        if use_masterlog:
            self.log = smartlife.log

    def cfg_join(self):
        self.log.debug("cfg_join")
        self.cfg = self.cfg_default.copy()
        self.cfg.update(self.cfg_custom.cfg)

    def get_web_path(self):
        self.log.debug("get_web_path")
        if "web_external" in self.cfg:
            return self.cfg["web_external"]

    def web_doGet(self, path, param):
        self.log.debug(path)
        print(path)
        print(param)
        return {"code": 404}
   
    def web_doPost(self, path, param, data):
        self.log.debug(path)
        return {"code": 404}

    def pl_data(self, user, cmd, _in, ip):
        self.log.debug(cmd)
        return {}
   
    def pl_get_menus(self, user, ip):
        self.log.debug(user)
        return []

    def pl_script(self):
        self.log.debug("script")
        name = self.smartlife.plugin_path + "/" + self.name + "/script.js"
        if os.path.isfile(name):
            f = open(name, "rb")
            script = f.read()
            f.close()
        return script

    def rights(self, user, ip):
        self.log.debug("rights")
        mod = self.smartlife.get_plugin("netcom")
        if mod:
            for host in self.cfg_master["hosts"]:
                for plugin in host["plugins"]:
                    if plugin["name"] == "authentication":
                        url = "http://" + host["ip"]  + ":" + str(self.cfg_master["netcom_port"]) + "/netcom/rights"
                        data = {"domain": self.manifest["domain"], "ip": ip, "user": user}
                        try:
                            r = requests.post(url, data=json.dumps(data))
                            if r.status_code == 200:
                                rsv = r.json()
                                return rsv
                        except:
                            pass
            return {"domain": False}
        else:
            return {"domain": False}

    def get_secret(self, text):
        self.log.debug("check_secret")
        out = text
        if text.startswith("secret!"):
            value = text.split("!")[1]
            found = False
            for host in self.cfg_master["hosts"]:
                for plugin in host["plugins"]:
                    if not(found) and plugin["name"] == "authentication":
                        found = True
                        url = "http://" + host["ip"]  + ":" + str(self.cfg_master["netcom_port"]) + "/netcom/secret"
                        data = {"value": value}
                        try:
                            r = requests.post(url, data=json.dumps(data), timeout=5)
                            if r.status_code == 200:
                                out = r.json()
                        except:
                            self.log.error(url)
                            self.log.error(text)
        return out
  
    def run(self):
        self.log.info(self.name)
        while self.running:
            time.sleep(5)

    def stop(self):
        self.log.info(self.name)
        self.running = False
