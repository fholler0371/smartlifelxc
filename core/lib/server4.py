#smartlife 4.0
#2021-10

version = "2021-10"
files_lib = ["logger4"]
files_bin = []

from functools import partial
from http.server import HTTPServer, BaseHTTPRequestHandler
from socketserver import ThreadingMixIn
from threading import Thread
from mimetypes import guess_type
import json
import os

import logger4 as logger

### thread #######################################

class loopThread(Thread):
    def __init__(self, server):
        Thread.__init__(self)
        self.daemon = True
        self.name = "WebServer"
        self.server = server

    def run(self):
        self.server.serve_forever()


### server #######################################

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    daemon_threads = True
    allow_reuse_address = True

class webserverHandler(BaseHTTPRequestHandler):
    def __init__(self, sl, cb_get, cb_post, *args, **kwargs):
        self.sl = sl
        self.cb_get = cb_get
        self.cb_post = cb_post
        super().__init__(*args, **kwargs)

    def log_message(self, format, *args):
        self.sl.log.debug("\n    %s - - [%s] %s" % (self.address_string(), self.log_date_time_string(),format%args))

    def do_GET(self):
        if self.cb_get == None:
            self.sl.log.debug("get not supported")
            self.send_response(404)
            self.send_header('Content-Type', 'text')
            self.end_headers()
            self.wfile.write("".encode())
        else:
            try:
                if self.cb_get(self):
                    pass
                else:
                    path = self.path
                    if path == "/favicon.ico":
                        path = "/lib/img/favicon.png"
                    path = self.sl.cfg.cfg["path_base"] + "www" + path
                    path = path.replace("/../", "/").replace("/./", "/")
                    if os.path.isfile(path):
                        f = open(path, "rb")
                        self.send_result(f.read(), guess_type(path, strict=False)[0])
                        f.close()
                    else:
                        self.sl.log.error("File not exists")
                        self.sl.log.error(path)
                        self.send_response(404)
                        self.send_header('Content-Type', 'text')
                        self.end_headers()
                        self.wfile.write("".encode())
            except Exception as e:
                logger.error(self.sl.log, e)
                self.send_response(404)
                self.send_header('Content-Type', 'text')
                self.end_headers()
                self.wfile.write("".encode())

    def do_POST(self):
        if self.cb_post == None:
            self.sl.log.debug("get not supported")
            self.send_response(404)
            self.send_header('Content-Type', 'text')
            self.end_headers()
            self.wfile.write("".encode())
        else:
            try:
                if self.cb_post(self):
                    pass
                else:
                    path = self.path
                    if path == "/favicon.ico":
                        path = "/lib/img/favicon.png"
                    path = self.sl.cfg.cfg["path_base"] + "www" + path
                    path = path.replace("/../", "/").replace("/./", "/")
                    if os.path.isfile(path):
                        f = open(path, "rb")
                        self.send_result(f.read(), guess_type(path, strict=False)[0])
                        f.close()
                    else:
                        self.sl.log.error("File not exists")
                        self.sl.log.error(path)
                        self.send_response(404)
                        self.send_header('Content-Type', 'text')
                        self.end_headers()
                        self.wfile.write("".encode())
            except Exception as e:
                logger.error(self.sl.log, e)
                self.send_response(404)
                self.send_header('Content-Type', 'text')
                self.end_headers()
                self.wfile.write("".encode())

    def send_result(self, data, type, code=200, redir=""):
        self.send_response(code)
        self.send_header("Content-Type", "type")
        self.send_header('Content-Length', str(len(data)))
        if code == 302:
            self.send_header("Location", redir)
        self.end_headers()
        self.wfile.write(data)

def getServer(sl):
    try:
        port = sl.cfg.cfg["port"]
        sl.log.info('getServer: '+str(port))
        app_get = None
        if hasattr(sl.respond, "get"):
            app_get = sl.respond.get
        app_post = None
        if hasattr(sl.respond, "post"):
            app_post = sl.respond.post
        handler = partial(webserverHandler, sl, app_get, app_post)
        server = ThreadedHTTPServer(('0.0.0.0', port), handler)
        return server
    except Exception as e:
        logger.error(sl.log, e)
        return None

def threadStart(sl):
    try:
        sl.log.info('start loop')
        th = loopThread(sl.server)
        th.start()
        return True
    except Exception as e:
        logger.error(sl.log, e)
        sl.server = None
        return False

def serverStop(sl):
    try:
        sl.log.info('stop loop')
        sl.server.shutdown()
        sl.server.server_close()
    except Exception as e:
        logger.error(sl.log, e)
