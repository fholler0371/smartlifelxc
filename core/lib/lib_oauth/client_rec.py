#smartlife 4.0
#2021-10

import os
import json
import time

class client_rec:
    def __init__(self, sl, wellknown, key):
        self.sl = sl
        self.sl.log.debug("init")
        self.wellknown = wellknown
        self.key = key
        self.rec = None
        self.sw_id = None
        self.path = self.sl.cfg.cfg["path_config"] + "oauth_client_" + self.sl.cfg.cfg["app"] + ".data"
        if os.path.isfile(self.path):
            f = open(self.path, "r")
            self.rec = json.loads(f.read())
            f.close()
        self.check()

    def get_entry(self, name):
        if self.rec and name in self.rec:
            return self.rec[name]
        else:
            return ""

    @property
    def valid(self):
        if self.rec != None:
            self.sw_id = self.get_entry("software_id")
            if self.sw_id == "":
                self.sw_id = None
            if self.get_entry("client_secret_expires_at") == "" :
                self.rec == None
            elif self.get_entry("client_secret_expires_at") - 1209600 < time.time():
                self.sec_rec = None
        if self.get_entry("callback") != self.sl.cfg.cfg["oauth_callback"]:
            self.rec = None
        return self.rec != None

    def check(self):
        self.sl.log.debug("check")
        if (not self.valid) and not(self.wellknown.registration == ""):
            try:
                if self.sw_id == None:
                    self.sw_id = str(uuid()).replace("-", "")
                rec = {"redirects_uris": [self.sl.cfg.cfg["oauth_callback"]],
                        "token_endpoint_auth_method" : "client_secret_post",
                        "token_endpoint_auth_signing_alg" : "RS256",
                        "scope" : self.sl.cfg.cfg["oauth_scope"],
                        "client_name" : self.sl.cfg.cfg["oauth_name"],
                        "software_id" : self.sw_id,
                        "jwks" : json.loads(self.key.public())}
                res = requests.post(self.wellknown.registration, data=json_encode(rec).encode())
                data = json.loads(res.text)
                self.rec = {"software_id" : self.sw_id, "callback" : self.sl.cfg.cfg["oauth_callback"]}
                if "client_id" in data:
                    self.rec["client_id"] = data["client_id"]
                if "client_secret" in data:
                    self.rec["client_secret"] = data["client_secret"]
                if "client_secret_expires_at" in data:
                    self.rec["client_secret_expires_at"] = data["client_secret_expires_at"]
                f = open(self.path, "w")
                f.write(json.dumps(self.rec))
                f.close()
            except Exception as e:
                logger.error(self.sl.log, e)

    @property
    def client_id(self):
        return self.get_entry("client_id")

    @property
    def client_secret(self):
        return self.get_entry("client_secret")
