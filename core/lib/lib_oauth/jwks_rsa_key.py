#smartlife 4.0
#2021-10

import os

from jwcrypto import jwk
from jwcrypto.common import json_decode, json_encode

class jwks_rsa_key:
    def __init__(self, sl, name_="client", type_="rsa" ):
        self.sl = sl
        self.name_ = name_
        self.sl.log.debug("init")
        self.key = None
        self.path = self.sl.cfg.cfg["path_config"] + "oauth_" + name_ + "_" + sl.cfg.cfg["app"] + "." + type_ + ".jwk"
        self.load()
        if self.key == None:
            self.generate()

    def load(self):
        self.sl.log.debug("load")
        if os.path.isfile(self.path):
            f = open(self.path, "r")
            d = json_decode(f.read())
            self.key = jwk.JWK(**d)
            f.close()

    def save(self):
        self.sl.log.debug("save")
        f = open(self.path, "w")
        f.write(self.key.export())
        f.close()

    def generate(self):
        self.sl.log.debug("generate")
        self.key = jwk.JWK.generate(kty='RSA', size=2048, kid=self.sl.cfg.cfg["app"]+"_"+self.name_)
        self.save()

    def public(self):
        self.sl.log.debug("public")
        return self.key.export(private_key=False)
