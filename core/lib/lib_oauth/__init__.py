#smartlife 4.0
#2021-10

version = "2021-10"
files_lib = []
files_bin = []

import requests
from requests.exceptions import ConnectionError
import json
import os
import time
from uuid import uuid4 as uuid

import logger4 as logger

from lib_oauth.jwks_rsa_key import jwks_rsa_key
from lib_oauth.server_wellknown import server_wellknown
from lib_oauth.client_rec import client_rec

class client:
    def __init__(self, sl):
        self.sl = sl
        sl.log.info("auto_reg")
        self.key = jwks_rsa_key(self.sl)
        self.wellknown = server_wellknown(self.sl)
        if not self.wellknown.valid:
            sl.running = False
        self.client_rec = client_rec(self.sl, self.wellknown, self.key)

    def get_code(self, code, req):
        self.sl.log.info(code)
        ok = True
        try:
            data = "grant_type=authorization_code&code=" + code + "&redirect_uri=" + self.sl.cfg.cfg["oauth_callback"]
            data += "&client_id=" + self.client_rec.client_id + "&client_secret=" + self.client_rec.client_secret
            res = requests.post(self.wellknown.token, data=data)
            print(res.status_code, res.text)
            req.send_result(res.text.encode(), "appliction/json")
            return True

#            req.send_result(json.dumps({}).encode(), "appliction/json")
        except Exception as e:
            logger.error(self.sl.log, e)
        return True