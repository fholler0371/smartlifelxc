#smartlife 4.0
#2021-10

import requests
from requests.exceptions import ConnectionError
import json
class server_wellknown:
    def __init__(self, sl):
        self.sl = sl
        self.sl.log.debug("init")
        self.rec = None
        url = url = "https://"+self.sl.cfg.secret["oauth"]+"/.well-known/openid-configuration"
        try:
            res = requests.get(url)
        except ConnectionError as e:
            self.sl.log.error("ConnectionError")
            self.sl.log.error(url)
            return
        if res.status_code != 200:
            sl.log.error("Code: " + str(res.status_code))
            sl.log.error(url)
        else:
            self.rec = json.loads(res.content.decode())

    @property
    def valid(self):
        return self.rec != None

    def get_entry(self, name):
        if self.rec and name in self.rec:
            return self.rec[name]
        else:
            return ""

    @property
    def registration(self):
        return self.get_entry("registration_endpoint")

    @property
    def token(self):
        return self.get_entry("token_endpoint")

    @property
    def authorization(self):
        return self.get_entry("authorization_endpoint")