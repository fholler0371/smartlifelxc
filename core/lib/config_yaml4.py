#smartlife 4.0
#2021-10

version = "2021-10"
files_lib = []
files_bin = []

import os

import yaml

class config:
    def __init__(self, name, log=None, pre={}):
        self.log = log
        if log:
            self.log.debug(name)
        self.name = name
        self.cfg = pre
        if os.path.isfile(name):
            f = open(name, "r")
            self.cfg = pre.copy()
            self.cfg.update(yaml.load(f.read(), Loader=yaml.FullLoader))
            f.close()
        else:
            if log:
                self.log.debug("not found")

    def save(self):
        if self.log:
            self.log.debug("save")
        f = open(self.name, "w")
        yaml.dump(self.cfg, f)
        f.close()

    def set_log(self, log):
        log.debug("set_log")
        self.log = log
