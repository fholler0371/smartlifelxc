# smartlife
# 04.2021

import copy
import logging
from logging.handlers import RotatingFileHandler

class MyFormatter(logging.Formatter):
    width = 50
    datefmt='%Y-%m-%d %H:%M:%S'

    def format(self, record):
        cpath = '%s:%s:%s:%s' % (record.pathname.split('/')[-2], record.module, record.funcName, record.lineno)
        cpath = cpath[-self.width:].ljust(self.width)
        record.message = record.getMessage()
        s = "%-8s %s %s : %s" % (record.levelname, self.formatTime(record, self.datefmt), cpath, record.getMessage())
        return s

def init(path, cfg, console=False):
    if cfg["place"] == "console" or console:
        h = logging.StreamHandler()
    elif cfg["place"] == "file":
        h = RotatingFileHandler(path+"/smartlife.log", maxBytes=cfg["size"], backupCount=cfg["count"])
    h.setFormatter(MyFormatter())
    l = logging.getLogger("smartlife")
    l.addHandler(h)
    if cfg["level"] == "debug":
        l.setLevel(logging.DEBUG)
    elif cfg["level"] == "info":
        l.setLevel(logging.INFO)
    elif cfg["level"] == "error":
        l.setLevel(logging.ERROR)
    return l

def level_up(log, name):
    l = logging.getLogger(name)
    l.addHandler(log.handlers[0])
    l.setLevel(logging.DEBUG)
    return l
