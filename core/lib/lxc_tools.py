# smartlife
# 07.2021

import sys
import os
import subprocess
import time

import config_yaml as yaml

class tools:
    def __init__(self, log, path, container):
        log.debug("init")
        self.log = log
        self.path = path
        self.container = container

    def is_running(self):
        self.log.debug(self.container)
        return self.container in lxc_ls(self.path, "active")

    def start(self):
        self.log.debug(self.container)
        lxc_start(self.path, self.container)

    def stop(self):
        self.log.debug(self.container)
        lxc_stop(self.path, self.container)

frame = "**************************************************"
normal = "\033[m"
menu = "\033[36m"
number = "\033[33m"
fgred = "\033[31m"
OVER = "\r\033[K"
COL_LIGHT_GREEN = "\033[32m"
COL_LIGHT_RED = "\033[31m"
COL_NC = "\033[0m"

def out_msg(msg, mode=None):
    end = "\n"
    out = ""
    if mode == None:
        end = ""
        out = " [ ] " + msg
    elif mode in ["+", "-"]:
        out = OVER + " ["+COL_LIGHT_GREEN+mode+COL_NC+"] "+msg
    elif mode == "1":
        out = OVER + " ["+COL_LIGHT_RED+"✗"+COL_NC+"] "+msg
    elif mode == "0":
        out = OVER + " ["+COL_LIGHT_GREEN+"✓"+COL_NC+"] "+msg
    print(out, end=end, flush=True)
    time.sleep(0.25)

def lxc_ls(path, mode): #active stopped
    cmd = "sudo lxc-ls -P " + path + " -1 --" + mode
    result = subprocess.run(cmd.split(" "), stdout=subprocess.PIPE)
    return result.stdout.decode().split("\n")

def lxc_stop(path, name):
    cmd = "sudo lxc-stop -P " + path + " -n " + name
    res = subprocess.run(cmd.split(" "), capture_output=True)

def lxc_start(path, name):
    cmd = "sudo lxc-start -P " + path + " -n " + name
    res = subprocess.run(cmd.split(" "), capture_output=True)

def lxc_del(path, name):
    cmd = "sudo rm -R " + path + name
    res = subprocess.run(cmd.split(" "), capture_output=True)

def lxc_create(path, cfg):
    cmd = "sudo lxc-copy -P " + path + " -p " + path + " -n master -N " + cfg["name"]
    res = subprocess.run(cmd.split(" "), capture_output=True)
    time.sleep(1)
    cmd = "sudo chown pi:pi -R " + path + cfg["name"]
    res = subprocess.run(cmd.split(" "), capture_output=True)

def lxc_config(path, cfg):
    name = path + cfg["name"] + "/config"
    f = open(name, "r")
    content = f.read().split("\n")
    f.close()
    out = []
    for line in content:
        if "lxc.net.0.type" in line:
            pass
        else:
            out.append(line)
    out.append("lxc.net.0.type = veth")
    out.append("lxc.net.0.flags = up")
    out.append("lxc.net.0.link = lxcbr")
    out.append("lxc.net.0.ipv4.address = " + cfg["cfg"]["ip"] + "/24")
    out.append("lxc.mount.entry = /opt/smartlife opt/smartlife none rw,bind 0.0")
    f = open(name, "w")
    f.write("\n".join(out))
    f.close()
    cmd = "sudo mkdir " + path + cfg["name"] + "/rootfs/opt/smartlife"
    res = subprocess.run(cmd.split(" "), capture_output=True)

def lxc_setup(path, cfg, run):
    out = []
    if run == 0:
        out.append("/sbin/init")
        out.append("sed -i 's/# de_DE.UTF-8 UTF-8/de_DE.UTF-8 UTF-8/g' /etc/locale.gen")
        out.append("rm /etc/localtime")
        out.append("ln -s /usr/share/zoneinfo/Europe/Berlin /etc/localtime")
        out.append("locale-gen")
        out.append("sleep 30")
        out.append("apt update")
        out.append("apt upgrade -y")
        out.append("apt install nano -y")
        out.append("hostnamectl set-hostname " + cfg["name"])
        out.append("sed -i 's/master/" + cfg["name"] + "/g' /etc/hosts")
    for line in cfg["cfg"]["setup"][run]:
        out.append(line)
    out.append("halt")
    name = path + cfg["name"] + "/rootfs/home/setup.sh"
    f = open(name, "w")
    f.write("\n".join(out))
    f.close()
    cmd = "chmod +x " + name
    if run == 0:
        res = subprocess.run(cmd.split(" "), capture_output=True)
        out = ["#!/bin/sh -e", "rm /etc/rc.local", "/home/setup.sh", "exit 0"]
        name = path + cfg["name"] + "/rootfs/etc/rc.local"
        f = open(name, "w")
        f.write("\n".join(out))
        f.close()
        cmd = "chmod +x " + name
        res = subprocess.run(cmd.split(" "), capture_output=True)
    cmd = "sudo lxc-start -P " + path + " -n " + cfg["name"]
    res = subprocess.run(cmd.split(" "), capture_output=True)
    if run == 0:
        cmd = "sudo lxc-wait -P " + path + " -n " + cfg["name"] + " -s STOPPED"
        res = subprocess.run(cmd.split(" "), capture_output=True)
    else:
        time.sleep(10)
        cmd = "sudo lxc-attach -P " + path + " -n " + cfg["name"] + " -- /home/setup.sh"
        res = subprocess.run(cmd.split(" "), capture_output=True)

def do_setup(lxc_path, cfg):
    msg = "check if container is running"
    out_msg(msg)
    if cfg["name"] in lxc_ls(lxc_path, "active"):
        out_msg(msg, "+")
        msg = "stop container"
        out_msg(msg)
        lxc_stop(lxc_path, cfg["name"])
        if cfg["name"] in lxc_ls(lxc_path, "active"):
            out_msg(msg, "1")
        else:
            out_msg(msg, "0")
    else:
        out_msg(msg, "-")
    msg = "delete container if exist"
    out_msg(msg)
    lxc_del(lxc_path, cfg["name"])
    out_msg(msg, "0")
    msg = "create container"
    out_msg(msg)
    lxc_create(lxc_path, cfg)
    out_msg(msg, "0")
    msg = "edit config"
    out_msg(msg)
    lxc_config(lxc_path, cfg)
    out_msg(msg, "0")
    max = len(cfg["cfg"]["setup"])
    run = 0
    while run < max:
        msg = "setup container " + str(run+1)
        out_msg(msg)
        lxc_setup(lxc_path, cfg, run)
        out_msg(msg, "0")
        run += 1
        time.sleep(15)

if __name__ == "__main__" and len(sys.argv) == 2 and sys.argv[1] == "setup":
    path = "/".join(os.path.abspath(__file__).split("/")[:-2])
    lxc_path = path + "/lxc_images/"
    path += "/lxcontainer"
    container = []
    for d in os.listdir(path):
        if os.path.isdir(path+"/"+d):
            entry = {"name": d, "path": path+"/"+d+"/"}
            cfg = yaml.config(entry["path"]+"config.yaml")
            entry["cfg"] = cfg.cfg
            container.append(entry)

    print(menu+frame+normal)
    l = 0
    while l < len(container):
        line = menu+"** "+number
        if l < 11:
            line += " "
        line += str(l+1)+") "+menu+container[l]["cfg"]["label"]
        while len(line) < len(frame)+13:
            line += " "
        line += "**"+normal
        print(line)
        l += 1
    print(menu+frame+normal)
    try_count = 3
    menu_id = -1
    while menu_id == -1 and try_count > 0:
        data = input("Please enter a menu option and enter or "+fgred+"x to exit. "+normal)
        if data == "x":
            try_count = 0
        else:
            try:
                m = int(data)
                if m > 0 and m < len(container)+1:
                    menu_id = m - 1
            except:
                pass
        try_count -= 1
    if menu_id > -1:
        print("")
        do_setup(lxc_path, container[menu_id])
