import os
import sys

try:
    import yaml
except:
    os.popen(sys.executable + ' -m pip install pyyaml --upgrade').read()
    import yaml


def load(path, fname, *_, log=None):
    if log:
        log.debug('Load: '+fname)
    name = path+'/'+fname+'.yaml'
    data = {}
    if os.path.isfile(name):
        try:
            f = open(name, "r")
            data = yaml.safe_load(f.read())
            f.close()
        except:
            if log:
                log.error("Fehler beim lesen: " + name)
            else:
                print("Fehler beim lesen: " + name)
            os._exit(-1)
    else:
        if log:
            log.error("Konfigfile fehlt: " + name)
        else:
            print("Konfigfile fehlt: " + name)
        os._exit(-1)
    return data

