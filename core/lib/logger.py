import logging
from logging.handlers import RotatingFileHandler

class MyFormatter(logging.Formatter):
    width = 50
    datefmt='%Y-%m-%d %H:%M:%S'

    def format(cls, record):
        cpath = '%s:%s:%s:%s' % (record.pathname.split('/')[-2], record.module, record.funcName, record.lineno)
        cpath = cpath[-cls.width:].ljust(cls.width)
        record.message = record.getMessage()
        s = "%-8s %s %s : %s" % (record.levelname, cls.formatTime(record, cls.datefmt), cpath, record.getMessage())
        return s

def init(path, data):
    if data['dest'] == "console":
        h = logging.StreamHandler()
    elif data['dest'] == "file":
        h = RotatingFileHandler(path+"/smartlife.log", maxBytes=65000, backupCount=10)
    h.setFormatter(MyFormatter())
    l = logging.getLogger('sl')
    l.addHandler(h)
    if data['level'] == 'debug':
        l.setLevel(logging.DEBUG)
    elif data['level'] == 'info':
        l.setLevel(logging.INFO)
    if data['level'] == 'error':
        l.setLevel(logging.ERROR)
    return l

