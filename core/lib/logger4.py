#smartlife 4.0
#2021-10

version = "2021-10"
files_lib = []
files_bin = []

import sys
import os
import copy
import logging
from logging.handlers import RotatingFileHandler

class MyFormatter(logging.Formatter):
    width = 50
    datefmt='%Y-%m-%d %H:%M:%S'

    def format(self, record):
        cpath = '%s:%s:%s:%s' % (record.pathname.split('/')[-2], record.module, record.funcName, record.lineno)
        cpath = cpath[-self.width:].ljust(self.width)
        record.message = record.getMessage()
        s = "%-8s %s %s : %s" % (record.levelname, self.formatTime(record, self.datefmt), cpath, record.getMessage())
        return s

def error(log, e):
    exc_type, exc_obj, exc_tb = sys.exc_info()
    log.error(type(e).__name__)
    log.error(exc_obj)
    log.error("/".join(exc_tb.tb_frame.f_code.co_filename.split("/")[-3:]))
    log.error("Line: " + str(exc_tb.tb_lineno))

def init(cfg):
    if cfg.cfg["log"]["place"] == "console" or cfg.cfg["cmd"]:
        h = logging.StreamHandler()
    elif cfg.cfg["log"]["place"] == "file":
        h = RotatingFileHandler(cfg.cfg["path_log"] + cfg.cfg["app"] + ".log", maxBytes=cfg.cfg["log"]["size"], backupCount=cfg.cfg["log"]["count"])
    h.setFormatter(MyFormatter())
    l = logging.getLogger("smartlife")
    l.addHandler(h)
    if cfg.cfg["log"]["level"] == "debug":
        l.setLevel(logging.DEBUG)
    elif cfg.cfg["log"]["level"] == "info":
        l.setLevel(logging.INFO)
    elif cfg.cfg["log"]["level"] == "error":
        l.setLevel(logging.ERROR)

    return l

def level_up(log, name):
    l = logging.getLogger(name)
    l.addHandler(log.handlers[0])
    l.setLevel(logging.DEBUG)
    return l
