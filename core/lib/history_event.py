# smartlife
# 05.2021

from datetime import datetime

from dateutil import tz

import database

class db:
    def __init__(self, log, name):
        log.debug("init")
        self.log = log
        sql = "CREATE TABLE IF NOT EXISTS history (time integer, topic text, value text)"
        self.db = database.database(log, name, sql)

    def store(self, old, data, topic_list):
        self.log.debug("store")
        if data["topic"] in topic_list and old != data["value"]:
            d = datetime.utcfromtimestamp(data["ts"])
            from_zone = tz.gettz("UTC")
            to_zone = tz.gettz("Europe/Berlin")
            d = d.replace(tzinfo=from_zone)
            d = d.astimezone(to_zone)
            self.db.open(d.strftime("%y%m"))
            sql = "insert into history (time, topic, value) values ('%s', '%s', '%s')"
            self.db.commit(sql % (str(data["ts"]), data["topic"], str(data["value"]), ))
            self.db.close()
