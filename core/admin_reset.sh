#!/bin/bash

INSTALL_DIR="/opt/smartlife"

pushd "${INSTALL_DIR}" >/dev/null 2>&1

.env/bin/python smartlife.py reset_admin

popd >/dev/null 2>&1