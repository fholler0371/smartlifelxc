#!/bin/bash

INSTALL_DIR="/opt/smartlife"
LOCAL_GIT="${INSTALL_DIR}/.gitlab"
LOCAL_REPO="${LOCAL_GIT}/smartlifelxc"

CURRENT_DATE=$(date '+%Y%m%d')

pushd "${LOCAL_REPO}" >/dev/null 2>&1
git add .
git commit -m "${CURRENT_DATE}"
git push
popd >/dev/null 2>&1
 