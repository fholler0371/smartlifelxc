postMsg = function(msg) {
 self.clients.matchAll({
   includeUncontrolled: true,
   type: 'window',
 }).then((clients) => {
   if (clients && clients.length) {
     clients[0].postMessage(msg)
   }
 })
}

var version = '25';
var coreID = version + '_core';
var tempID = version + '_tmp';
var cacheIDs = [coreID, tempID];

self.addEventListener('message', (event) => {
  if (event.data && event.data.type === 'cache') {
    postMsg({type: 'log', msg: event.data.msg})
  } else if (event.data && event.data.type === 'clear') {
    caches.keys().then(function (keys) {
      var p = Promise.all(keys.filter(function (key) {
        return true
      }).map(function (key) {
        console.log(key)
        return caches.delete(key)
      })) 
      return p
      console.log("do Clear")
    })
  }
})

// On install, cache some stuff
self.addEventListener('install', function (event) {
  self.skipWaiting()
  event.waitUntil(caches.open(coreID).then(function (cache) {
    var c = cache.addAll(
        [
          '/',
          '/index.html',
          '/manifest.json',
          '/lib/img/favicon.png',
          '/lib/require/require.js',
          '/lib/jquery/jquery.min.js',
          '/lib/jquery/datepicker.js',
          '/lib/packery/packery.pkgd.min.js',
          '/lib/font/Fredoka_One.woff2',
          '/lib/jqwidgets/styles/jqx.base.css',
          '/lib/jqwidgets/styles/jqx.metrodark.css',
          '/lib/jqwidgets/styles/my-jqx.css',
          '/lib/web.css',
          '/lib/main.js',
          '/lib/start.js',
          '/lib/head.js',
          '/lib/jqwidgets/jqxcore.js',
          '/lib/jqwidgets/jqxmenu.js',
          '/lib/jqwidgets/jqxbuttons.js',
          '/lib/jqwidgets/jqxwindow.js',
          '/lib/jqwidgets/jqxinput.js',
          '/lib/jqwidgets/jqxpasswordinput.js',
          '/lib/jqwidgets/jqxlistbox.js',
          '/lib/jqwidgets/jqxscrollbar.js',
          '/lib/jqwidgets/jqxtabs.js'
        ]
    )
    postMsg({type:'event', msg: 'INSTALL'})
    return c
  }));
});

self.addEventListener('activate', function (event) {
  event.waitUntil(caches.keys().then(function (keys) {
    var p = Promise.all(keys.filter(function (key) {
      return !cacheIDs.includes(key);
    }).map(function (key) {
      return caches.delete(key);
    }));
    postMsg({type:'event', msg: 'ACTIVATE'})
    return p
  }));
});

var ignoreRequests = new RegExp('(' + [
    '/module\/(.*)'
  ].join('(\/?)|\\') + ')$')

self.addEventListener('fetch', event => {
  if (event.request.method != 'GET') return;
    var r = caches.match(event.request).then(function (response) {
      if (response) {
        return response
      } else if (ignoreRequests.test(event.request.url)) {
        return fetch(event.request)
      } else {
        postMsg({type:'load', msg: event.request.url})
        var requestToCache = event.request.clone()
        return fetch(requestToCache).then(function(response) {
          if(!response || response.status !== 200) {
            return response;
          }
          var responseToCache = response.clone()
          caches.open(tempID).then(function(cache) {
            cache.put(requestToCache, responseToCache)
          })
          return response
        })
      }
    })
  event.respondWith(r)
});
