define(['jquery'], function() {
  window.auth = {
    islogin : false,
    username : '',
    check_token : function() {
      let token = sessionStorage.getItem('s_token')
      if (token == null) token = ""
      window.smfetch('/authentication/check_token', {token: token}).then((data) => {
        if (data.error == undefined) {
          if (data.token.valid) {
            window.auth.islogin = true
            window.auth.username = data.token.name
            sessionStorage.setItem('s_token', data.token.new)
            window.auth.set_login()
          } else {
            window.auth.islogin = false
          }
        } else {
          window.auth.islogin = false
          console.error(data.error)
        }
      })
    },
    set_login : function() {
      if ($('#loginButton').length > 0) {
        if ($('.jqx-window').length > 0) {
          $('.jqx-window').jqxWindow('close')
        }
        $('#loginButton').hide()
        $('#logoutButton').show()
        $('#user_name').show()
        $('#user_name').text(window.auth.username)
        $('#switchButton').css('right', 'calc( 5rem + '+$('#user_name').width()+'px )')
        window.head.get_menu()
        window.head.tick()
        window.head.get_user_movement()
      } else {
        setTimeout(100, window.auth.set_login)
      }
    },
    set_logout : function() {
      if ($('#loginButton').length > 0) {
        if ($('.jqx-window').length > 0) {
          $('.jqx-window').jqxWindow('close')
        }
        $('#loginButton').show()
        $('#logoutButton').hide()
        $('#user_name').hide()
        $('#switchButton').css('right', '5rem')
        window.head.menu = window.head.makeMenu([])
        if (window.module != undefined) {
          for (const[name, mod] of Object.entries(window.module)) {
            if (mod.stop != undefined) {
              mod.stop()
            }
          }
          window.module.clock.init()
        }
        sessionStorage.removeItem("s_token")
      } else {
        setTimeout(100, window.auth.set_login)
      }
    },
    check_logout: function() {
      window.smfetch('/api/modul_manager', {cmd: 'check_logout'}).then((data) => {
        if (data.error == undefined) {
          if (data.data.keep != undefined && data.data.keep) {
            window.head.get_user_movement()
            window.head.tick()
          } else {
            window.auth.set_logout()
          }
        } else {
          window.auth.set_logout()
        }
      })
    },
    do_logout : function () {
      sessionStorage.removeItem('s_token')
      window.auth.set_logout()
    },
    keep_alive : function() {
      window.smfetch('/api/modul_manager', {cmd: 'keep_alive'}).then((data) => {})
    },
    login : function() {
      window.smfetch('/authentication/get_method', {}).then((data) => {
        if (data.error == undefined) {
          requirejs(['jqxwindow', 'jqxdropdownlist', 'jqxinput', 'jqxpasswordinput', 'jqxbutton'], function() {
            if ($('#login_window').length == 0) {
              var html = '<div id="login_window"><div><span>Anmelden</span></div><div><table>'
              html += '<tr><td><b>Methode: <b></td><td><div id="login_method"></div></td><tr></tr>'
              html += '<tr><td><b>Nutzer: <b></td><td><input type="text" id="login_user"/></td><tr>'
              html += '<tr><td><b>Passwort: <b></td><td><input type="password" id="login_passwd"/></td><tr>'
              html += '<tr><td> </td><td> </td><tr>'
              html += '<tr><td><td><input style="float:right" id="login_button" type="button" value="Anmelden" /></td><tr>'
              html += '</table></div></div>'
              $('body').append(html)
              $('#login_window').jqxWindow({isModal: true, width:375, height: 200, resizable: false})
              $("#login_method").jqxDropDownList({width: 250, height: 30, autoDropDownHeight: true})
              $('#login_user').jqxInput({ height: 30, width: 250, minLength: 4, placeHolder: ""})
              $('#login_passwd').jqxPasswordInput({height: 30, width: 250, minLength: 4, placeHolder: ""})
              $('#login_button').jqxButton({height: 30, width: 200})
              $("#login_method").on('select', function(ev) {
                var args = ev.args
                if (args) {
                  var value = args.item.value
                  $('#login_passwd').jqxPasswordInput({disabled: (value == 'local') })
                }
              })
              $('#login_button').on('click', function() {
                var method = $("#login_method").jqxDropDownList('getSelectedItem').value
                var user = $('#login_user').jqxInput('val')
                var passwd = $('#login_passwd').jqxPasswordInput('val')
                window.smfetch('/authentication/login', {method: method, user: user, passwd: passwd}).then((data) => {
                  if (data.error == undefined) {
                    if (data.allowed) {
                      window.auth.set_logout()
                      sessionStorage.setItem("s_token", data.token.token)
                      window.auth.islogin = true
                      window.auth.username = data.name
                      window.auth.set_login()
                    }
                  }
                })
              })
            }
            $("#login_method").jqxDropDownList('clear')
            for (var i=0; i<data.length; i++) {
              $("#login_method").jqxDropDownList('insertAt', {label: data[i].label, value: data[i].method}, i)
            }
            $("#login_method").jqxDropDownList({selectedIndex: 0})
            $('#login_user').jqxInput('val', '')
            $('#login_passwd').jqxPasswordInput('val', '')
            $('#login_window').jqxWindow('open')
          })
        } else {
          console.error("No login options from server")
        }
      })
    }
  }
  window.auth.check_token()
})