define(['jquery', 'jqxwindow', 'jqxbutton', 'jqxpanel'], function($) {
  window.upload = {
    run : function(modul, sub, title, filter, single, cb) {
      if ($("#uploadwin").length == 0) {
        $("body").append('<div id="uploadwin"><div><span>Hochladen</span></div><div id="uploadwincontent"></div></div>')
        $("#uploadwin").jqxWindow({
          resizable: false,
          height: 400,
          width: 550,
          isModal: true,
          modalOpacity: 0.6
        })
        html = '<div><span style="font-weight: 700;" id="uploadtitle"></span></div>'
        html += '<input type="file" id="uploadfile" style="display: none;" accept="' + filter + '"/>'
        html += '<button id="uploadselect" style="position: absolute; right: 10px; top: 70px;">Auswählen</button>'
        html += '<div id="uploadlist" style="position: absolute; left: 10px; top: 110px;"></div>'
        html += '<button id="uploadstart" style="position: absolute; right: 10px; bottom: 10px;">Hochladen</button>'
        $('#uploadwincontent').html(html)
        $('#uploadselect, #uploadstart').jqxButton()
        $('#uploadselect').on('click', function() {
          $('#uploadfile').val('')
          if (window.upload.single) {
            $('#uploadfile').removeAttr('multiple')
          } else {
            $('#uploadfile').attr('multiple', true)
          }
          $('#uploadfile').click()
        })
        $("#uploadfile").on('change',function(){
          for (i=0; i < this.files.length; i++ ) {
            html = '<div style="width: 500px" class="uploadentry"><img src="/lib/img/close.png" style="cursor: pointer;" class="uploadentrybutton"/>'
            html += '<span style="position: relative; left: 20px;">'
            html += this.files[i].name+'</span></div>'
            var ele = $(html)
            ele.data('file', this.files[i])
            var unit = ' B'
            var size = this.files[i].size
            if (size > 5000) {
              size = size / 1024
              unit = ' kB'
            }
            if (size > 5000) {
              size = size / 1024
              unit = ' MB'
            }
            ele.append('<span style="position: absolute; right: 25px;">' + size.toLocaleString('de-DE', {maximumFractionDigits: 1}) + unit + '</span>')
            $("#uploadlist").jqxPanel('append', ele)
          }
          if (window.upload.single) {
            $('#uploadselect').jqxButton({disabled: true})
          }
          $('#uploadstart').jqxButton({disabled: false})
        })
        $("#uploadlist").jqxPanel({ width: 525, height: 240})
        $("#uploadwin").on("click", ".uploadentrybutton", function(ev) {
          $(ev.currentTarget).parent().remove()
          var len = $('.uploadentry').length
          if (len == 0) {
            $('#uploadselect').jqxButton({disabled: false})            
            $('#uploadstart').jqxButton({disabled: true})            
          }
        })
        $('#uploadstart').on('click', window.upload.upload)
      }
      window.upload.single = single
      window.upload.modul = modul
      window.upload.sub = sub
      window.upload.cb = cb
      $('#uploadselect').jqxButton({disabled: false})
      $('#uploadstart').jqxButton({disabled: true})
      $('#uploadwin').jqxWindow('open')
      $('#uploadtitle').text(title)
      $("#uploadlist").jqxPanel('clearcontent')
    },
    upload : function() {
      var eles = $('.uploadentry')
      if (eles.length > 0) {
        var ele = eles[0],
            f = $(ele).data('file'),
            v = window.upload
        window.smfetch('/api/'+v.modul, {cmd: 'upload_prefetch', data: {name: f.name, size: f.size, sub: v.sub, type: f.type}}).then((data) => {
          console.log(data)
          if (data.error == undefined) {
            if (!data.data.ok) {
              alert("prefetch error: "+f.name)
            } else {
              url = '/file/?_=' + data.data.data
              fetch(url, {method : 'POST', body: f}).then(
                response => response.json()
              ).then(
                success => {
                  if (window.upload.cb != undefined) {
                    window.upload.cb()
                  }
                }
              ).catch(
                error => alert("upload error: "+f.name)
              )
            }
            $(ele).remove()
            var eles = $('.uploadentry')
            if (eles.length > 0) {
              setTimeout(window.upload.upload, 50)
            } else {
              $('#uploadwin').jqxWindow('close')
            }
          } else {
            alert("prefetch error")
          }
        })
      }
    }
  }
})