define(['jquery', 'jqxbutton', 'jqxmenu', 'auth'], function($) {
  window.head = {
    tick_timer: null,
    init: function() {
      html = '<div id="mainTop"><div class="leftMenu" style="height:100%;"><input type="button" id="burgerMenu" />'
      html += '<span id="slogan">Smart Life</span><input type="button" id="menuSmall" /><input type="button" id="reloadButton" />'
      html += '<input type="button" id="fullscreenButton" />'
      html += '</div><span id="head_title"></span><input type="button" id="switchButton" />'
      html += '<span id="user_name"></span>'
      html += '<input type="button" id="logoutButton" /><input type="button" id="loginButton" /></div>'
      html += '<div id="client_area"></div>'
      $('body').html(html)
      $("#menuSmall").jqxButton({ width: '1.75rem', height: '1.75rem', imgSrc: "/lib/img/left-24-white.png", imgHeight: 24, imgWidth: 24})
      var img = "/lib/img/home-white1.svg"
      if ('home'==window.location.hostname.split('.')[0]) {
        img = "/lib/img/cloud-white.svg"
      }
      $("#reloadButton").jqxButton({ width: '2.4rem', height: '2.4rem', imgSrc: "/lib/img/reload.svg", imgHeight: 24, imgWidth: 24})
      $("#fullscreenButton").jqxButton({ width: '2.4rem', height: '2.4rem', imgSrc: "/lib/img/fullscreen.svg", imgHeight: 24, imgWidth: 24})
      if (window.ismobile()) {
        $('body').addClass("mobile")
        $("#fullscreenButton").hide()
        $("#slogan").text('SL')
      }
      $("#switchButton").jqxButton({ width: '2.4rem', height: '2.4rem', imgSrc: img, imgHeight: 36, imgWidth: 36})
      $("#loginButton").jqxButton({ width: '2.4rem', height: '2.4rem', imgSrc: "/lib/img/login-36-white.png", imgHeight: 36, imgWidth: 36})
      $("#logoutButton").jqxButton({ width: '2.4rem', height: '2.4rem', imgSrc: "/lib/img/logout-36-white.png", imgHeight: 36, imgWidth: 36})
      if (window.auth.islogin) {
        $('#login_button').hide()
      } else {
        $("#logoutButton").hide()
      }
      $("#logoutButton").on('click', function() {
        window.head.logout()
      })
      $("#burgerMenu").jqxButton({ width: '2.4rem', height: '2.4rem', imgSrc: "/lib/img/menu-36-white.png", imgHeight: 36, imgWidth: 36})
      $("#burgerMenu").on('click', function() {
        window.head.menu.jqxMenu('open', 16 , 48);
        window.head.menu.off('itemclick')
        window.head.menu.on('itemclick', function(event) {
          var element = event.args;
          if ($(element).data('display') != 'no') {
            for (const[name, mod] of Object.entries(window.module)) {
              if (mod.stop != undefined) {
                mod.stop()
              }
            }
          }
          var mod = $(element).data('mod')
          var p1 = $(element).data('p1')
          var p2 = $(element).data('p2')
          var p3 = $(element).data('p3')
          var paths = {}
          paths['mod.'+mod] = 'module/'+mod
          requirejs.config({paths:paths})
          window.module_const['mod.'+mod] = {p1: p1, p2:p2, p3:p3}
          $('#head_title').html('')
          for (const[name, mod] of Object.entries(window.module)) {
            if (mod.stop != undefined) {
              mod.stop()
            }
          }
          if (window.module[mod] == undefined) {
            requirejs(['mod.'+mod], function(mod) {
              mod.init()
            })
          } else {
             window.module[mod].init_data = {p1: p1, p2:p2, p3:p3}
             window.module[mod].init()
          }
        })
      })
      $("#fullscreenButton, #reloadButton, #switchButton, #loginButton, #logoutButton, #burgerMenu").css('position', 'absolute')
      $("#switchButton > img, #menuSmall > img, #loginButton > img, #logoutButton > img, #burgerMenu > img")
        .css({'top': 1, 'left': 1})
      if (window.ismobile()) {
        $('#reloadButton').css({'left': '8.2rem', 'top': '0.5rem'})
      } else {
        $('#reloadButton').css({'left': '18.2rem', 'top': '0.5rem'})
      }
      $('#fullscreenButton').css({'left': '21.2rem', 'top': '0.5rem'})
      $("#menuSmall").on('click', function() {
        $('#mainTop').toggleClass('leftMenuSmall')
        if ($('#mainTop').hasClass('leftMenuSmall')) {
          $('#slogan').hide()
          $('#menuSmall').jqxButton({ imgSrc: '/lib/img/right-24-white.png' });
          $('#reloadButton').css({'left': '4.8rem'})
          $('#fullscreenButton').css({'left': '7.8rem'})
        } else {
          $('#slogan').show()
          $('#menuSmall').jqxButton({ imgSrc: '/lib/img/left-24-white.png' });
          if (window.ismobile()) {
            $('#reloadButton').css({'left': '8.2rem', 'top': '0.5rem'})
          } else {
            $('#reloadButton').css({'left': '18.2rem', 'top': '0.5rem'})
          }
          $('#fullscreenButton').css({'left': '21.2rem'})
        }
        $("#menuSmall > img").css({'top': 1, 'left': 1})
        mod = $('#client_area').data('min')
        if (mod != undefined) {
          window.module[mod].setMinMenu()
        }
        $('#mainTop').trigger('sizeChange')
      })
      $('#fullscreenButton').on('click', function() {
        if (document.fullscreenElement !== null) {
          $('#fullscreenButton').jqxButton({ imgSrc: '/lib/img/fullscreen.svg' })
          document.exitFullscreen()
          $('#fullscreenButton').css({'position': 'absolute'})
        } else {
          $('#fullscreenButton').jqxButton({ imgSrc: '/lib/img/fullscreen-exit.svg' })
          document.documentElement.requestFullscreen()
          $('#fullscreenButton').css({'position': 'absolute'})
        }
      })
      $('#reloadButton').on('click', function(ev) {
        var sK = ev.shiftKey
        if (sK) {
          navigator.serviceWorker.ready.then( (registration) => {
            if (registration.active) {
              registration.active.postMessage({type: 'clear'})
            }
          })
        }
        window.head.reload_all = sK
        setTimeout(window.head.reload, 1000)
      })
      window.head.menu = window.head.makeMenu([])
      window.module = {}
      window.module_const = {}
      var paths = {}
      paths['mod.clock'] = 'module/clock'
      requirejs.config({paths:paths})
      window.module_const['mod.clock'] = {p1: 0, p2: 0, p3: 0}
      requirejs(['mod.clock'], function(mod) {
        mod.init()
      })
      $('#loginButton').on('click', function() {
        window.auth.login()
      })
      $('*').on('mousemove click mouseup mousedown keydown keypress keyup submit change mouseenter scroll resize dblclick',
                window.head.get_user_movement)
/*      $('#switchButton').on('click', function() {
        var loc = "home"
        if ('home' == window.location.hostname.split('.')[0]) {
          loc = 'cloud'
        }
        window.location.replace('https://'+loc+'.smartlife.holler.pro')
      })
      var loc = "home"
      if ('home' == window.location.hostname.split('.')[0]) {
        loc = 'cloud'
      }
      const now = new Date()
      const secondsSinceEpoch = Math.round(now.getTime() / 1000)
      $('body').append('<iframe id="otherIframe" src="https://'+loc+'.smartlife.holler.pro/iframe.html?_='+secondsSinceEpoch+'" style="visibility: hidden;"></iframe>')
      var iframe = document.getElementById('otherIframe'),
          iframeDoc = iframe.contentDocument || iframe.contentWindow.document;*/
      $(window).on("message", function(e) {
        console.log(e)
      })
      if (window.ismobile()) {
        $(window).on("orientationchange", window.head.rotate)
        window.head.rotate()
      }
    },
    rotate : function() {
      var verical = window.orientation == 0
      if (verical) {
        $('body').addClass("vertical")
        $("#burgerMenu").jqxButton({ width: '4.8rem', height: '4.8rem', imgHeight: 72, imgWidth: 72})
      } else {
        $('body').removeClass("vertical")
        $("#burgerMenu").jqxButton({ width: '2.4rem', height: '2.4rem', imgHeight: 36, imgWidth: 36})
      }
    },
    reload : function() {
      if (window.head.reload_all) {
        navigator.serviceWorker.getRegistrations().then(function(registrations) {
          for(let registration of registrations) {
           registration.unregister()
          } 
        })
      } 
      window.location.reload()
    },
    logout : function() {
      window.auth.do_logout()
    },
    get_menu : function() {
      window.smfetch('/api/modul_manager', {cmd: 'get_menu'}).then((data) => {
        if (data.error == undefined) {
          window.head.menu = window.head.makeMenu(data.data)
        } else {
          window.head.menu = window.head.makeMenu([])
        }
      })
    },
    get_user_movement : function() {
      window.head.count = 600
    },
    tick : function() {
      clearTimeout(window.head.tick_timer)
      window.head.count = window.head.count - 1
      window.head.keep = window.head.keep - 1
      if (sessionStorage.getItem("s_token") != null) {
        window.head.tick_timer = setTimeout(window.head.tick, 1E3)
      }
      if (window.head.count <= 0) {
        window.auth.check_logout()
      }
      if (window.head.keep <= 0) {
        window.head.keep = 60
        window.auth.keep_alive()
      }
    },
    makeMenu : function(data) {
      try {
        window.head.menu.jqxMenu('destroy')
      } catch(err) {}
      html = '<div id="TopMenu" style="visibility: hidden;"><ul>'
      html += '<li data-mod="clock"><a href="#">Uhr</a></li>'
      var l = data.length
      if (l > 0) {
        for (var i=0; i<l; i++) {
          entry = data[i]
          if ('sub' in entry) {
            html += '<li>'+entry.label+'<ul>'
            var l1 = entry.sub.length
            for (var i1=0; i1<l1; i1++) {
              entry1 = entry.sub[i1]
              html += '<li  data-mod="'+entry1.mod+'" data-p1="'+entry1.p1+'" data-p2="'+entry1.p2+'" data-p3="'+entry1.p3+'"'
              if (entry1.display != undefined) {
                if (!entry1.display) {
                  html += ' data-display="no"'
                }
              }
              html += '>'+entry1.label+'</li>'
            }
            html += '</ul></li>'
          } else {
            html += '<li  data-mod="'+entry.mod+'" data-p1="'+entry.p1+'" data-p2="'+entry.p2+'" data-p3="'+entry.p3+'"'
            if (entry.display != undefined) {
              if (!entry.display) {
                html += ' data-display="no"'
              }
            }
            html += '>'+entry.label+'</li>'
          }
        }
      }
      html += '</ul></div>'
      $('body').append(html)
      return $("#TopMenu").jqxMenu({ width: '250px', autoOpenPopup: false, mode: 'popup'})
    },
    menu : false,
    count : 600,
    keep: 60
  }
  return window.head
})
