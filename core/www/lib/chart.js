define("chart", ['highcharts', 'moment_tz', 'jqxwindow', 'jqxloader', 'jqxbuttongroup'], function(H, moment) {
  window.moment = moment
  H.setOptions({
    global: {
      timezone: "Europe/Berlin"
    },
    lang : {
      months : ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
      shortMonths : ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'],
      weekdays : ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa']
    } 
  })
  ch = {
    t: 0,
    tout : undefined,
    modes : [],
    run : function() {
      if ($("#chartwin").length == 0) {
        $("body").append('<div id="chartwin"><div><span>Chart: </span><span id="chartwintitle"></span></div><div id="chartwincontent"></div></div>')
        $("#chartwin").jqxWindow({
          resizable: false,
          height: "80%",
          width: "90%",
          isModal: true,
          modalOpacity: 0.6
        })
      }
      $("#chartwintitle").text("")
      $("#chartwincontent").html('<div id="chartwinloader"></div>')
      $("#chartwinloader").jqxLoader({ width: 100, height: 100, imagePosition: 'center', text: 'Laden ...', autoOpen: true})
      var sdata = window.chart.data.split(":")
      window.smfetch(sdata[0], {cmd: 'get_chart','data': {mode: sdata[2], sensor: sdata[1]}}).then((data) => {
        if (data.error == undefined) {
          var d = data.data
          $("#chartwintitle").text(d.title)
          var ele = $("#chartbuttons")
          if (ele.length == 0) {
            var binfo = d.buttons.split(":"),
                blen = binfo.length,
                bsel = -1
            window.chart.win.mode = binfo
            window.chart.win.pre = data.data.pre
            html = '<div id="chartbuttons">'
            for (var i=0; i<blen; i++) {
              if ("D" == binfo[i][0]) html += '<button style="padding:4px 16px;">Tag</button>'
              if ("W" == binfo[i][0]) html += '<button style="padding:4px 16px;">Woche</button>'
              if ("M" == binfo[i][0]) html += '<button style="padding:4px 16px;">Monat</button>'
              if ("Y" == binfo[i][0]) html += '<button style="padding:4px 16px;">Jahr</button>'
              if ("A" == binfo[i][0]) html += '<button style="padding:4px 16px;">Alles</button>'
              if (binfo[i].length == 2) bsel = i
            }
            html += '</div>'
            $("#chartwincontent").append(html)
            $("#chartbuttons").jqxButtonGroup({mode: 'radio', template: "primary"})
            $('#chartbuttons').off('buttonclick')
            if (bsel > -1) $("#chartbuttons").jqxButtonGroup('setSelection', bsel)
            $('#chartbuttons').on('buttonclick', function() {
              var sel = $('#chartbuttons').jqxButtonGroup('getSelection'),
                  mode = window.chart.win.mode[sel][0],
                  sdata = window.chart.data.split(":")
              $("#chartwinloader").jqxLoader('open')
              window.smfetch(sdata[0], {cmd: 'get_chart','data': {mode: mode, sensor: sdata[1]}}).then((data) => {
                if (data.error == undefined) {
                  window.chart.win.mode = data.data.buttons.split(":")
                  window.chart.win.pre = data.data.pre
                  window.chart.win.t = 300
                  window.chart.win.makechart(data.data)
                }
              })
            })
          }
          window.chart.win.makechart(data.data)
        }
      })
      $("#chartwin").jqxWindow('show')
      window.chart.win.t = 300
      if (window.chart.win.tout != undefined) {
        clearTimeout(window.chart.win.tout)
      }
      window.chart.win.loop()
    },
    makechart : function(d) {
      var len = d.data.length
      if (len == 0) {
        alert("No Chart Data!")
      } else {
        if ($('#chartContainer').length == 0) {
          $("#chartwincontent").append('<div id="chartContainer" style="width: 100%; height: calc( 100% - 35px ); margin-top: 5px;">')
        }
        var series = []
        series.push({name: d.label, data: [], color: '#0066FF', marker: {enabled: false}, lineWidth: 4})
        var diff = 0
        for (var i=0; i<len; i++) {
          series[0].data.push([d.data[i].time * 1000, d.data[i].value])
        }
        if (d.data[0].min != undefined) {
          series.push({name: 'Minimum', data: [], color: '#007F00', dashStyle: 'Dash', marker: {enabled: false}, lineWidth: 1 })
          series.push({name: 'Maximum', data: [], color: '#CF0000', dashStyle: 'Dash', marker: {enabled: false}, lineWidth: 1 })
          var diff = 0
          for (var i=0; i<len; i++) {
            series[1].data.push([d.data[i].time * 1000, d.data[i].min])
            series[2].data.push([d.data[i].time * 1000, d.data[i].max])
          }
        }
        var settings = {
          chart: { type: 'spline' },
          title : { text: '' },
          subtitle : { text: '' },
          yAxis : { title: {text: d.unit }},
          xAxis: {
            type: 'datetime',
            title: { text: '' }
          },
          series: series
        }
        H.chart('chartContainer', settings)
        $("#chartwinloader").jqxLoader('close')
      }
    },
    test : function (d) {
      var len = d.data.length
      if (len == 0) {
        alert("No Chart Data!")
      } else {
        try {
          $('#chartContainer').jqxChart('destroy');
          $('#chartContainer').remove();
        } catch {}
        $("#chartwincontent").append('<div id="chartContainer" style="width: 100%; height: calc( 100% - 35px ); margin-top: 5px;">')
        var diff = 0
        for (var i=0; i<len; i++) {
          d.data[i].date = new Date((d.data[i].time - diff * 60) * 1000)
        }
        var fields = [{name: 'date'}, {name: 'value'}]
        var series = [{ dataField: 'value', displayText: d.label }]
        if (d.data[0].min != undefined) {
          fields[fields.length] = {name: 'min'}
          fields[fields.length] = {name: 'max'}
          series[series.length] = { dataField: 'min', displayText: 'Min' }
          series[series.length] = { dataField: 'max', displayText: 'Max' }
        }
        var source = {
          localdata: d.data,
          datatype: "array",
          datafields: fields
        }
        var dataAdapter = new $.jqx.dataAdapter(source, { async: false, autoBind: true})
        var months = ['Jan', 'Feb', 'Mär', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez']
        var weekdays = ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa']
        var mode = d.buttons.split("!")[0].slice(-1)
        if (mode == "D") {
          var baseIntervall = "hour",
              labelOffset= -5,
              dstart = d.data[0].date
          dstart.setSeconds(0)
          dstart.setMinutes(0)
        }
        if (mode == "W") {
          var baseIntervall = "day",
              labelOffset= -5
              dstart = d.data[0].date
          dstart.setSeconds(0)
          dstart.setMinutes(0)
          dstart.setHours(0)
        }
        if (mode == "M") {
          var baseIntervall = "day",
              labelOffset= -25
              dstart = d.data[0].date
          dstart.setSeconds(0)
          dstart.setMinutes(0)
          dstart.setHours(0)
        }
        if (mode == "Y") {
          var baseIntervall = "month",
              labelOffset= -20
              dstart = d.data[0].date
          dstart.setSeconds(0)
          dstart.setMinutes(0)
          dstart.setHours(0)
          dstart.setDate(1)
        }
        if (mode == "A") {
          var baseIntervall = "year",
              labelOffset= -15
              dstart = d.data[0].date
          dstart.setSeconds(0)
          dstart.setMinutes(0)
          dstart.setHours(0)
          dstart.setDate(1)
          dstart.setMonth(0)
        }
        var settings = {
          title: "",
          description: "",
          enableAnimations: true,
          showLegend: true,
          padding: { left: 10, top: 5, right: 10, bottom: 5 },
          source: dataAdapter,
          toolTipFormatFunction: function (value, itemIndex, serie, group, categoryValue, categoryAxis) {
            var entry = source.localdata[itemIndex],
                date = entry.date.getDate() + '. ' + months[entry.date.getMonth()] + '. ' + entry.date.getFullYear(),
                time = ("0"+entry.date.getHours()).slice(-2)+":"+("0"+entry.date.getMinutes()).slice(-2)
            html = "<table>"
            html += '<tr><td style="text-align: left;"><b>Datum: </b></td><td style="text-align: right;">'+date+'</td></tr>'
            html += '<tr><td style="text-align: left;"><b>Zeit: </b></td><td style="text-align: right;">'+time+'</td></tr>'
            html += '<tr><td style="text-align: left;"><b>Wert: </b></td><td style="text-align: right;">'
            html += entry.value.toLocaleString('de-DE', {maximumFractionDigits: window.chart.win.pre, minimumFractionDigits: window.chart.win.pre})
            html += ' '+d.unit+'</td></tr>'
            if (entry.max != undefined) {
              html += '<tr><td style="text-align: left;"><b>Max: </b></td><td style="text-align: right;">'
              html += entry.max.toLocaleString('de-DE', {maximumFractionDigits: window.chart.win.pre, minimumFractionDigits: window.chart.win.pre})
              html += ' '+d.unit+'</td></tr>'
            }
            if (entry.min != undefined) {
              html += '<tr><td style="text-align: left;"><b>Min: </b></td><td style="text-align: right;">'
              html += entry.min.toLocaleString('de-DE', {maximumFractionDigits: window.chart.win.pre, minimumFractionDigits: window.chart.win.pre})
              html += ' '+d.unit+'</td></tr>'
            }
            html += "</table>"
            return html
          },
          xAxis: {
            dataField: 'date',
            formatFunction: function (value) {
              var mode = window.chart.win.mode.join(':').split("!")[0].slice(-1)
              if (mode == 'D') return value.getHours()+'h'
              if (mode == 'W') return weekdays[value.getDay()]
              if (mode == 'M')return value.getDate() + '-' + months[value.getMonth()] + '-' + value.getFullYear();
              if (mode == 'Y') return months[value.getMonth()] + '-' + value.getFullYear();
              if (mode == 'A') return value.getFullYear();
            },
            type: 'date',
            baseUnit: baseIntervall,
            unitInterval: 1,
            valuesOnTicks: true,
            minValue: dstart,
            maxValue: d.data[len-1].date,
            gridLines: {
              visible: true,
              interval: 1,
              color: '#BCBCBC'
            },
            labels: {
              angle: -45,
              rotationPoint: 'topright',
              offset: { x: 0, y: labelOffset }
            }
          },
          valueAxis: {
            visible: true,
            title: { text: d.unit+'<br>' },
            tickMarks: { color: '#BCBCBC' },
            formatFunction: function (value) {
              return value.toLocaleString('de-DE', {maximumFractionDigits: window.chart.win.pre, minimumFractionDigits: window.chart.win.pre})
            }
          },
          colorScheme: 'scheme04',
          seriesGroups: [{
            type: 'spline',
            series: series
          }]
        }
        $('#chartContainer').jqxChart(settings);
        $("#chartwinloader").jqxLoader('close')
        $($("#chartContainer").find('text').splice(-1)).remove()
        $('#chartwincontent').css('overflow', 'hidden')
      }
    },
    loop : function() {
      clearTimeout(window.chart.win.tout)
      if (window.chart.win.t > 0) {
        window.chart.win.t = window.chart.win.t - 1
        var s = Math.floor(window.chart.win.t/60,0)+':'+('0'+(window.chart.win.t % 60)).slice(-2),
            ele = $("#chartwin").find("#timer")
        if (ele.length == 0) {
          $("#chartwincontent").append('<span id="timer" style="font-size: 75%; position: absolute; right: 5px;"></span>')
          ele = $("#chartwin").find("#timer")
        }
        $(ele[0]).text(s)
        window.chart.win.tout = setTimeout(window.chart.win.loop, 1000)
      } else {
        $("#chartwin").jqxWindow('hide')
        window.chart.win.tout = undefined
      }
    }
  }
  window.chart.win = ch
  return ch
})
