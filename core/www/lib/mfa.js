define(['jqxwindow', 'jqxinput', 'jqxbutton'], function() {
  return function() {
    if ($('#mfa_win').length == 0) {
      html = '<div id="mfa_win"><div><span>Bitte MFA eingeben</span></div><div><input type="text" id="mfa_value" />'
      html += '<div id="mfa_send" style="position: absolute; right: 5px; bottom: 5px;">Senden</div></div></div>'
      $('body').append(html)
      $('#mfa_win').jqxWindow({height:125, width:250, resizable: false, isModal: true})
      $('#mfa_value').jqxInput({placeHolder: 'MFA', width:239})
      $('#mfa_send').jqxButton()
      $('#mfa_send').off('click')
      $('#mfa_send').on('click', function(){
        var val = $('#mfa_value').jqxInput('val')
        window.smcall({'client': 'authencitator.user', 'cmd':'mfa', 'data': {mfa:val}}, function(data) {
          if (data.data.ok) {
            $('#mfa_win').jqxWindow('close')
          } else {
            alert("MFA Code falsch!")
          }
        })
      })
    }
    $('#mfa_value').val('')
    $('#mfa_win').jqxWindow('open')
  }
})
