var load_img = false
func_img = function() {
  fetch('/lib/img/tasmota.png')
  fetch('/lib/img/hm_plug.png')
  fetch('/lib/img/fritzpowerline.png')
  fetch('/lib/img/left-24-white.png')
  fetch('/lib/img/logout-36-white.png')
  fetch('/lib/img/login-36-white.png')
  fetch('/lib/img/menu-36-white.png')
  fetch('/lib/img/right-24-white.png')
  fetch('/lib/module/clock.js')
}
if ('serviceWorker' in navigator) {
  navigator.serviceWorker.register('/sw.js', { scope: '/' }).then(function(reg) {
  }).catch(function(error) {
    console.log('Registration failed with ' + error);
  });
  navigator.serviceWorker.addEventListener("message", (evt) => {
    if (evt.data.type == 'event') {
      if (evt.data.msg == 'ACTIVATE') {
        location.reload()
      }
    } else if (evt.data.type == 'log') {
      console.log('serviceworker: '+evt.data.msg)
    } else if (evt.data.type == 'load') {
      if (evt.data.msg.includes('/lib/img')) {
        if (load_img == false) {
          load_img = true
          setTimeout(func_img, 5000)
        }
      } else {
        console.log('load: '+evt.data.msg)
      }
    } else {
      console.log('Message from SW: '+evt.data.type+' - '+evt.data.msg)
    }
  });
}

window.ismobile = function() {
  if (navigator.userAgent.toLocaleLowerCase().match(/mobile/i) != null) {
    return navigator.userAgent.toLocaleLowerCase().match(/mobile/i)[0] == 'mobile'
  } else {
    return false
  }
}

requirejs.config({
    waitSeconds : 30,
    baseUrl: '/lib',
    packages: [{
       name: 'highcharts',
       main: 'highcharts'
    }],
    paths: {
        start: 'start',
        chart: 'chart',
        auth: 'auth',
        mfa: 'mfa',
        upload: 'upload',
        leaflet_load: 'leaflet_load',
        leaflet: 'leaflet/leaflet',
        moment: 'moment/moment.min',
        moment_tz: 'moment/moment-timezone-with-data-1970-2030.min',
        jquery: 'jquery/jquery.min',
        datepicker: 'jquery/datepicker',
        jqxcore: 'jqwidgets/jqxcore',
        jqxsplitter: 'jqwidgets/jqxsplitter',
        jqxbutton: 'jqwidgets/jqxbuttons',
        jqxdropdownlist: 'jqwidgets/jqxdropdownlist',
        jqxlistbox: 'jqwidgets/jqxlistbox',
        jqxscrollbar: 'jqwidgets/jqxscrollbar',
        jqxdatatable: 'jqwidgets/jqxdatatable',
        jqxdata: 'jqwidgets/jqxdata',
        jqxtabs: 'jqwidgets/jqxtabs',
        jqxgrid: 'jqwidgets/jqxgrid',
        jqxgrid_selection: 'jqwidgets/jqxgrid.selection',
        jqxgrid_edit: 'jqwidgets/jqxgrid.edit',
        jqxgrid_sort: 'jqwidgets/jqxgrid.sort',
        jqxgrid_pager: 'jqwidgets/jqxgrid.pager',
        jqxcheckbox: 'jqwidgets/jqxcheckbox',
        jqxpanel: 'jqwidgets/jqxpanel',
        jqxinput: 'jqwidgets/jqxinput',
        jqxpasswordinput: 'jqwidgets/jqxpasswordinput',
        jqxnumberinput: 'jqwidgets/jqxnumberinput',
        jqxdatetimeinput: 'jqwidgets/jqxdatetimeinput',
        jqxcalendar: 'jqwidgets/jqxcalendar',
        jqxmenu: 'jqwidgets/jqxmenu',
        jqxwindow: 'jqwidgets/jqxwindow',
        jqxgrid_columnsresize: 'jqwidgets/jqxgrid.columnsresize',
        jqxdropdownbutton: 'jqwidgets/jqxdropdownbutton',
        jqxtree: 'jqwidgets/jqxtree',
        jqxchart: 'jqwidgets/jqxchart.core',
        jqxdraw: 'jqwidgets/jqxdraw',
        jqxloader: 'jqwidgets/jqxloader',
        jqxbuttongroup: 'jqwidgets/jqxbuttongroup',
        jqxexpander: 'jqwidgets/jqxexpander',
        packery: 'packery/packery.pkgd.min',
        highcharts : 'highcharts',
        highcharts_exporting: 'highcharts/modules/exporting'
    },
    shim:{
        jqxcore : {
            deps : ['jquery']
          },
        jqxsplitter : {
            deps : ['jqxcore']
          },
        jqxgrid_selection : {
            deps : ['jqxgrid']
          },
        jqxgrid_edit : {
            deps : ['jqxgrid']
          },
        jqxgrid_sort : {
            deps : ['jqxgrid']
          },
        jqxgrid_pager : {
            deps : ['jqxgrid']
          },
        jqxgrid_columnsresize : {
            deps : ['jqxgrid']
          },
        jqxgrid : {
            deps : ['jqxscrollbar']
          },
        jqxtabs : {
            deps : ['jqxcore']
          },
        jqxsdata : {
            deps : ['jqxcore']
          },
        jqxdropdownlist : {
            deps : ['jqxlistbox']
          },
        jqxlistbox : {
            deps : ['jqxscrollbar']
          },
        jqxpanel : {
            deps : ['jqxscrollbar']
          },
        jqxscrollbar : {
            deps : ['jqxbutton']
          },
        jqxdatetimeinput : {
            deps : ['jqxcalendar']
          },
        jqxdatatable : {
            deps : ['jqxscrollbar', 'jqxdata']
          },
        jqxbutton : {
            deps : ['jqxcore']
          },
        jqxfileupload : {
            deps : ['jqxbutton']
          },
          jqxdropdownbutton : {
            deps : ['jqxtree']
          },
        highcharts : {
            deps : ['moment']
          },
        jqxchart : {
            deps : ['jqxdraw']
          }
    }
});

requirejs(['jquery', 'start', 'jqxcore'], function($, mod) {
  window.smfetch = function(url, data) {
    if (url.startsWith('/api/')) {
      data.token = sessionStorage.getItem('s_token')
    }
    let options = {
      method: 'POST',
      body: JSON.stringify(data)
    }
    return fetch(url, options)
    .then(function(response) {
      if (!response.ok) {
        throw Error(response.status + ' ' + response.statusText + ' url:'+response.url)
      }
      return response.json()
    }).then(function(data) {
      if (data.token != undefined && !data.token.valid && window.auth != undefined && window.auth.islogin) {
        window.auth.do_logout()
        throw Error("Login not ok")
      } else {
        if (data.token != undefined && data.token.valid) {
          sessionStorage.setItem('s_token', data.token.token)
        }
        return data
      }
    }).catch(function(error){
      return {error: error}
    })
  } 
  window.smcall = function(data, cb) {
    if (('server' in window) && ('ip' in window.server)) {
      data.client = window.server.ip
    }
    token = sessionStorage.getItem("token")
    if (token != null) {
      data.token = token
    }
    $.ajax({
      url: '/api-core/',
      context: cb,
      method: 'POST',
      crossDomain: true,
      data: JSON.stringify(data),
      dataType: 'json'
    }).done(function(data) {
      if (!data.login) {
        sessionStorage.removeItem("token")
        try {
          window.head.logout()
        } catch(err) {}
      } else {
        sessionStorage.setItem("token", data.token)
      }
      if (data.data != undefined && data.data.need_mfa != undefined) {
        window.module.clock.init()
        requirejs(['mfa'], function(f) {
          f()
        })
      } else {
        this(data)
      }
    })
  }
//  $.jqx.theme = 'metrodark';
  $.jqx.theme = 'custom-scheme';
  $("body").on("click", ".cards_chart", function(ev) {
    var d = $(ev.currentTarget).data("chart")
    if (window.chart == undefined) window.chart = {}
    window.chart.data = d
    if (window.chart.win != undefined) {
      window.chart.win.run()
    } else {
      require(['chart'], function(c) {
        c.run()
      })
    }
  })
  mod.start()
})

