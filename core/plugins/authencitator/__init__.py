import time
import json
from urllib import request

import auth

from plugins import plugin_base

class plugin(plugin_base):
    def __init__(self, cfg, log, cb_plugin):
        log.info('init')
        plugin_base.__init__(self, 'authencitator', cfg, log, cb_plugin)
        self.alive = [7200]
        self.auth = auth.init(cfg, log)

    def web_rights(self):
        self.log.debug('web_rights')
        return ['authencitator.master', 'authencitator.user']


    def web_menu(self, packages):
        self.log.debug(packages)
        out = []
        if "authencitator.user" in packages:
            out.append({'label': 'Passwort &auml;ndern', 'mod': 'authencitator', 'p1':'password'})
        if "authencitator.master" in packages:
            out.append({'label': 'Nutzerverwaltung', 'mod': 'authencitator', 'p1':'mangement'})
        return out

    def web_data(self, data):
        self.log.debug(data)
        if data["client"] == "authencitator.user":
            self.alive[0] = 7200
            if data["cmd"] == "new_passwd":
                if data["data"]["new"] == data["data"]["repeat"] and len(data["data"]["new"]) > 6:
                    self.auth.change_password(data["token"]["user"], data["data"]["new"], data["data"]["old"])
            elif data["cmd"] == "mfa":
                data["data"] = {'ok': self.auth.mfa_check(data["token"]["user"], data["data"]["mfa"])}
                if data["data"]["ok"]:
                    data["token"]["mfa_out"] = int(time.time()+3600)
                else:
                     data["token"]["mfa_out"] = 0
                return data
        if data["client"] == "authencitator.master":
            self.alive[0] = 7200
            if data["token"]["mfa"]:
                if time.time() > data["token"]["mfa_out"]:
                    data["data"] = {"need_mfa": True}
                    return data
            if data["cmd"] == "get_user":
                data["data"] = self.auth.get_userlist()
            elif data["cmd"] == "get_rights":
                data["data"] = self.get_rights(data["data"]["user"])
            elif data["cmd"] == "new_user":
                data["data"] = self.auth.new_user(data["data"]["user"])
            elif data["cmd"] == "real_name":
                data["data"] = self.auth.set_real_name(data["data"]["user"], data["data"]["real"])
            elif data["cmd"] == "passwd":
                data["data"] = self.auth.set_passwd(data["data"]["user"], data["data"]["passwd"])
            elif data["cmd"] == "rights":
                data["data"] = self.auth.set_rights(data["data"]["user"], data["data"]["rights"])
            elif data["cmd"] == "reset":
                data["data"] = self.auth.reset_pwd(data["data"]["user"])
            elif data["cmd"] == "mfa_activ":
                data["data"] = self.auth.mfa(data["data"]["user"], True)
            elif data["cmd"] == "mfa_deactiv":
                data["data"] = self.auth.mfa(data["data"]["user"], False)
        return data

    def get_rights(self, user):
        self.log.debug(user)
        avaible = []
        for modul in self.sl_cfg['plugins']:
            mod = self.cb_plugin(modul)
            if hasattr(mod, 'web_rights'):
                for right in mod.web_rights():
                    avaible.append(right)
        if "remote" in self.sl_cfg:
            for server in self.sl_cfg['remote']:
                for plugin in server["plugins"]:
                    try:
                        url = "http://"+server['server']+":"+str(server['port'])+"/"+plugin+"/rights"
                        req =  request.Request(url)
                        resp = request.urlopen(req, data="[]".encode())
                        if resp.code == 200:
                            for right in json.loads(resp.read().decode()):
                                 avaible.append(right)
                    except:
                        pass
# old lxplugins
        if 'lxplugins' in self.sl_cfg:
            len_server = len(self.sl_cfg['lxplugins'])
            i = 0
            while i < len_server:
                server = self.sl_cfg['lxplugins'][i]['server']
                for plugin in self.sl_cfg['lxplugins'][i]['plugins']:
                    url = "http://" + server + ":9999/" + plugin + "/rights"
                    try:
                        req =  request.Request(url)
                        resp = request.urlopen(req, data="[]".encode())
                        if resp.code == 200:
                            for right in json.loads(resp.read().decode()):
                                 avaible.append(right)
                    except:
                        pass
                i += 1
        avaible.sort(key=lambda x: x.upper(), reverse=False)
        data = self.auth.get_rights(user)
        data["avaible"] = avaible
        return data


    def run(self):
        self.log.info('run')
        while self.running:
            time.sleep(5)

    def stop(self):
        self.log.info('stop')
        self.running = False
