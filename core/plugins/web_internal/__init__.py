import time
import json

from plugins import plugin_base

import server

class plugin(plugin_base):
    def __init__(self, cfg, log, cb_plugin):
        log.info('init')
        plugin_base.__init__(self, 'web_internal', cfg, log, cb_plugin)
        self.alive = [3600]
        self.server = server.getServer(self.doGet, self.doPost, log, self.cfg['port'])

    def doGet(self, path):
        self.log.debug(path)
        self.alive[0] = 3600
        print(self.path)
        return
        return self.web.doGet(path)

    def doPost(self, path, data):
        self.log.debug(path)
        self.log.debug(data)
        self.alive[0] = 3600
        if path.endswith('webpost'):
            ret = ''
            mod = self.cb_plugin(path.split('/')[1])
            if mod:
                if hasattr(mod, 'webPost'):
                    ret = mod.webPost(data)
            return {'code': -200, 'data': ret}
        elif path.endswith('rights'):
            ret = ''
            mod = self.cb_plugin(path.split('/')[1])
            if mod:
                if hasattr(mod, 'web_rights'):
                    ret = mod.web_rights()
            return {'code': -200, 'data': json.dumps(ret)}
        elif path.endswith('menu'):
            ret = ''
            mod = self.cb_plugin(path.split('/')[1])
            if mod:
                if hasattr(mod, 'web_menu'):
                    ret = mod.web_menu(data)
            return {'code': -200, 'data': json.dumps(ret)}
        elif path.endswith('data'):
            ret = ''
            mod = self.cb_plugin(path.split('/')[1])
            if mod:
                if hasattr(mod, 'web_data'):
                    ret = mod.web_data(data)
            return {'code': 200, 'data': ret}
        elif path.endswith('script'):
            ret = ''
            mod = self.cb_plugin(path.split('/')[1])
            if mod:
                if hasattr(mod, 'web_script'):
                    ret = mod.web_script()
            return {'code': -200, 'data': ret.decode()}
        else:
            self.log.debug(path)
        return {'code': 404}

    def run(self):
        self.log.info('run')
        server.threadStart(self.server, self.log)
        while self.running:
            time.sleep(5)

    def stop(self):
        self.log.info('stop')
        self.running = False
        server.serverStop(self.server, self.log)

