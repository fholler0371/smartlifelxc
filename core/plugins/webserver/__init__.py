import time
import json
from urllib import request

from plugins import plugin_base

import server

import webserver.web as web

class plugin(plugin_base):
    def __init__(self, cfg, log, cb_plugin):
        log.info('init')
        plugin_base.__init__(self, 'webserver', cfg, log, cb_plugin)
        self.web = web.web(cfg, log, cb_plugin)
        self.alive = [3600]
        self.server = server.getServer(self.doGet, self.doPost, log, self.cfg['port'])

    def doGet(self, path):
        self.log.debug(path)
        self.alive[0] = 3600
        return self.web.doGet(path)

    def doPost(self, path, data):
        self.log.debug(path)
        self.log.debug(data)
        self.alive[0] = 3600
        if "post" in self.cfg:
            for entry in self.cfg["post"]:
                if entry["path"] == path:
                    res = ''
                    mod = self.cb_plugin(entry["plugin"])
                    if mod and hasattr(mod, 'webPost'):
                        res = mod.webPost(data)
                    elif "remote" in self.sl_cfg:
                        for server in self.sl_cfg['remote']:
                            if entry["plugin"] in server["plugins"]:
                                if type(data) is dict:
                                    _data = json.dumps(data)
                                else:
                                    _data = data
                                try:
                                    url = "http://"+server['server']+":"+str(server['port'])+"/"+entry["plugin"]+"/webpost"
                                    req = request.Request(url)
                                    resp = request.urlopen(req, data=_data.encode())
                                    if resp.code == 200:
                                        res = resp.read().decode()
                                except:
                                    pass
                    return {'code': -200, 'data': res}
        return self.web.doPost(path, data)

    def run(self):
        self.log.info('run')
        server.threadStart(self.server, self.log)
        while self.running:
            time.sleep(5)

    def stop(self):
        self.log.info('stop')
        self.running = False
        server.serverStop(self.server, self.log)

