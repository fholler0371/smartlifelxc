import os
import json
from urllib import request

import auth

class web():
    def __init__(self, cfg, log, cb_plugins):
        self.log = log
        log.debug('init')
        self.mime_types = {"html": "text/html", "css": "text/css", "js": "text/javascript", "woff2": "font/woff2", "png": "image/png", "svg": "image/svg+xml"}
        self.cfg = cfg
        self.plugins = cb_plugins
        self.auth = auth.init(cfg, log)

    def doGet(self, path):
        self.log.debug(path)
        if path == "/":
            path = "/index.html"
        r_path =  (self.cfg['bpath'] + "/www" + path).replace("/../", "/").replace("/./", "/").replace("\\", "/")
        if not( os.path.isfile(r_path) ):
            if "/lib/module/" in r_path and r_path.split(".")[-1] == "js":
                return self.get_modulscript(r_path)
            else:
                return {"code": 404}
        ext = r_path.split(".")[-1].lower()
        mime = "text/plain"
        if ext in self.mime_types:
            mime = self.mime_types[ext]
        f = open(r_path, "rb+")
        data = f.read()
        f.close()
        return {"code": 200, "mime": mime, "data" : data, 'cachetime': 604800}

    def get_modulscript(self, path):
        self.log.debug(path)
        modul = path.split("/")[-1].split(".")[0]
        mod = self.plugins(modul)
        if mod:
            script = mod.web_script()
            if script != "":
                return {"code": 200, "mime": "text/javascript", "data": script, 'cachetime': 3600}
        if "remote" in self.cfg:
            for server in self.cfg['remote']:
                for plugin in server["plugins"]:
                    try:
                        url = "http://"+server['server']+":"+str(server['port'])+"/"+plugin+"/script"
                        req =  request.Request(url)
                        resp = request.urlopen(req, data="[]".encode())
                        if resp.code == 200:
                            text = resp.read()
                            return {"code": 200, "mime": "text/javascript", "data": text}
                    except:
                        pass
# old lxplugins
        if 'lxplugins' in self.cfg:
            len_server = len(self.cfg['lxplugins'])
            i = 0
            while i < len_server:
               if modul in self.cfg['lxplugins'][i]['plugins']:
                   server = self.cfg['lxplugins'][i]['server']
                   url = "http://"self.alive = [ALIVE_UDP]
                       req =  request.Request(url)
                       resp = request.urlopen(req, data="[]".encode())
                       if resp.code == 200:
                           text = resp.read()
                           return {"code": 200, "mime": "text/javascript", "data": text}
                   except:
                       pass
                   print(url)
               i += 1
        return {"code": 404}self.alive = [ALIVE_UDP]

#################################

    def doPost(self, path, data):
        self.log.debug(path)
        self.log.debug(data)
        if path == "/api-core/":
            if "client" in data:
                if "master" == data["client"]:
                    if "get_salt" == data["cmd"]:
                        out = self.auth.login(data)
                    else:
                        out = self.auth.decode(data)
                        out["data"] = []
                        if out["login"]:
                            out["data"] = self.get_menu(out["token"]["packages"])
                    if out["login"]:
                        out = self.auth.encode(out)
                    else:
                        out["token"] = ""
                else:
                    out = self.auth.decode(data)
                    if out["login"]:
                        if out["client"] in out["token"]["packages"]:
                            modul = out["client"].split(".")[0]
                            mod = self.plugins(modul)
                            if mod:
                                if hasattr(mod, 'web_data'):
                                    out = mod.web_data(out)
                                else:
                                    self.log.error(modul+' missing function web_data')
                            else:
                                found = False
                                if "remote" in self.cfg:
                                    for server in self.cfg['remote']:
                                        for modul in server["plugins"]:
                                           found = True
                                           try:
                                               url = "http://"+server['server']+":"+str(server['port'])+"/"+modul+"/data"
                                               req =  request.Request(url)
                                               resp = request.urlopen(req, data=json.dumps(out).encode())
                                               if resp.code == 200:
                                                    out = json.loads(resp.read().decode())
                                           except:
                                               pass
                                if not found:
# old lxplugin
                                    if 'lxplugins' in self.cfg:
                                        len_server = len(self.cfg['lxplugins'])
                                        i = 0
                                        while i < len_server:
                                            if modul in self.cfg['lxplugins'][i]['plugins']:
                                                server = self.cfg['lxplugins'][i]['server']
                                                url = "http://" + server + ":9999/" + modul + "/data"
                                                try:
                                                    req =  request.Request(url)
                                                    resp = request.urlopen(req, data=json.dumps(out).encode())
                                                    if resp.code == 200:
                                                        out["data"] = json.loads(resp.read().decode())
                                                except:
                                                    pass
                                            i += 1
# old lxplugin
                        out = self.auth.encode(out)
                    else:
                        out["token"] = ""
                return {"code": 200, "data": out}
            else:
                return {"code": 404}
        else:
            return {"code": 404}

    def get_menu(self, packages):
        self.log.debug(packages)
        module = []
        for pack in packages:
            if not(pack.split(".")[0] in module):
                module.append(pack.split(".")[0])
        out = []
        for modul in module:
            mod = self.plugins(modul)
            if mod:
                for menu in mod.web_menu(packages):
                    out.append(menu)
        if "remote" in self.cfg:
            for server in self.cfg['remote']:
                for plugin in server["plugins"]:
                    try:
                        url = "http://"+server['server']+":"+str(server['port'])+"/"+plugin+"/menu"
                        req =  request.Request(url)
                        resp = request.urlopen(req, data=json.dumps(packages).encode())
                        if resp.code == 200:
                            for menu in json.loads(resp.read().decode()):
                                out.append(menu)
                    except:
                        pass
# old lx plugin
        if 'lxplugins' in self.cfg:
            len_server = len(self.cfg['lxplugins'])
            i = 0
            while i < len_server:
                server = self.cfg['lxplugins'][i]['server']
                for plugin in self.cfg['lxplugins'][i]['plugins']:
                    if plugin in module:
                        url = "http://" + server + ":9999/" + plugin + "/menu"
                        try:
                            req =  request.Request(url)
                            resp = request.urlopen(req, data=json.dumps(packages).encode())
                            if resp.code == 200:
                                for menu in json.loads(resp.read().decode()):
                                    out.append(menu)
                        except:
                            pass
                i += 1
        return out

