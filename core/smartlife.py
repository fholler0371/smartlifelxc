import json
import os
import importlib
import sys
import time
import signal

sys.path.append(sys.path[0] + "/../modules")
sys.path.append(sys.path[0] + "/lib")

import sl_logger

f = open("/etc/config", "r")
sl = {"cfg": json.loads(f.read()), "running": True}
f.close()
sl["cfg"]['basepath'] = "/home/"
sl["cfg"]['dbpath'] = sl["cfg"]['basepath'] + "db/"

if sl["cfg"]["development"] and os.path.isfile("/etc/rc.local"):
    os.popen("rm /etc/rc.local").read()

os.popen("mkdir -p " + sl["cfg"]['dbpath']).read()
os.popen("mkdir -p " + sl["cfg"]['basepath'] + "/log").read()

sl["log"] = sl_logger.create(sl, sl["cfg"]['basepath'] + "/log")
sl["log"].critical("start")

sl["log"].debug("Loading device.list")
try:
    f = open("/etc/device.list", "r")
    sl["cfg"]["device"] = json.loads(f.read())
    f.close()
except:
    sl["cfg"]["device"] = {}
    
sl["log"].debug("Loading sl.cfg")
try:
    f = open("/etc/sl.cfg", "r")
    sl["cfg"]["const"] = json.loads(f.read())
    f.close()
except:
    sl["cfg"]["const"] = {}

def receive_signal(signum, stack):
    global sl
    if sl:
        sl["log"].debug('Received: '+str(signum))
        sl["running"] = False
signal.signal(signal.SIGTERM, receive_signal)

mod = importlib.import_module(sl["cfg"]["name"])
mod.init(sl)
if sl["running"]:
    mod.run()

if sl["cfg"]["one_shot"]:
    sl["running"] = False

devend = time.time()+60
while sl["running"]:
    time.sleep(1)
    if sl["cfg"]["development"] and devend < time.time():
        sl["running"] = False
    
mod.stop()

sl["log"].critical("stop")
