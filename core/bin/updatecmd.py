#!/opt/smartlife/.env/bin/python

#smartlife 4.0
#2021-10

import sys
import os
import subprocess

base_path = "/opt/smartlife/"

sys.path.append(base_path + "lib")

from screencmd import screen, out_msg
from config_yaml import config

def update_git(gits):
    msg = "set git email"
    out_msg(msg)
    base = config(base_path+"config/secret.yaml", pre={"git": {"email": ""}})
    if base.cfg["git"]["email"] != "":
        cmd = "git config --global user.email " + base.cfg["git"]["email"]
        subprocess.run(cmd.split(" "),  stdout=subprocess.PIPE)
    out_msg(msg, 1)
    for git in gits:
        msg = "update git: " + git
        out_msg(msg)
        olddir = os.getcwd()
        os.chdir(base_path + ".gitlab/" + git)
        cmd = "sudo chown pi:pi -R ./*"
        subprocess.run(cmd.split(" "), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        cmd = "git stash"
        subprocess.run(cmd.split(" "), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        cmd = "git clean --force" 
        subprocess.run(cmd.split(" "), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        cmd = "git pull" 
        subprocess.run(cmd.split(" "), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        os.chdir(olddir)
        out_msg(msg, 1)

def make_link(links):
    for link in links:
        msg = "make link: " + link["dest"]
        out_msg(msg)
        cmd = "rm " + base_path + link["dest"]
        subprocess.run(cmd.split(" "), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        cmd = "ln -s " + base_path + link["src"] + " " + base_path + link["dest"]
        subprocess.run(cmd.split(" "), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out_msg(msg, 1)

def make_exe(exes):
    for exe in exes:
        msg = "make executable: " + exe
        out_msg(msg)
        cmd = "chmod +x " + base_path + exe
        subprocess.run(cmd.split(" "), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out_msg(msg, 1)

def python_pip(pips):
    for pip in pips:
        msg = "check requirements: " + pip
        out_msg(msg)
        cmd = base_path + ".env/bin/pip install " + pip
        subprocess.run(cmd.split(" "), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out_msg(msg, 1)

if __name__ == "__main__":
    update_cfg =  config(base_path+"bin/updatecmd.yaml", pre={"cfg": []})
    if len(update_cfg.cfg["git"]) > 0:
        update_git(update_cfg.cfg["git"])
    if len(update_cfg.cfg["ln"]) > 0:
        make_link(update_cfg.cfg["ln"])
    if len(update_cfg.cfg["exe"]) > 0:
        make_exe(update_cfg.cfg["exe"])
    if len(update_cfg.cfg["pip"]) > 0:
        python_pip(update_cfg.cfg["pip"])
