#!/opt/smartlife/.env/bin/python

#smartlife 4.0
#2021-10

import sys
import os
import subprocess
import socket

base_path = "/opt/smartlife/"

sys.path.append(base_path + "lib")

from screencmd import screen
from config_yaml import config

app_name = "servercmd"
cfg_server = "server"
base_path = "/opt/smartlife/"

files_to_push = ["config/server.yaml", "config/secret.yaml"]
cfg =  config(base_path+"config/"+cfg_server+".yaml", pre={"server": []})

screen = screen()

def server_menu(i, host, ip, sonoff):
    while True:
        screen.clear()
        screen.logo()
        print(screen.menu+" Host: "+screen.lgreen+host+screen.menu+"        IP: "+screen.lgreen+ip+screen.normal)
        screen.entries = []
        screen.entries.append({"l": "Ping", "d": "ping"})
        screen.entries.append({"l": "SSH", "d": "ssh"})
        screen.entries.append({"l": "Cron Start", "d": "cron_start"})
        screen.entries.append({"l": "Cron Stop", "d": "cron_stop"})
        screen.entries.append({"l": "Update", "d": "update"})
        screen.entries.append({"l": "Menu", "d": "menu"})
        res = screen.showmenu()
        if res.lower() == "x":
            return           
        try:
            i = int(res)
        except:
            i = -1
        if i > 0 and i <= len(screen.entries):
            print()
            d = screen.entries[i-1]["d"]
            if d == "ping":
                cmd = "ping -c 5 " + ip
                result = subprocess.run(cmd.split(" "))
                print()
                input("Enter drücken")
            elif d == "menu":
                cmd = "ssh " + ip + " export TERM=xterm && /opt/smartlife/bin/menucmd.py"
                result = subprocess.run(cmd.split(" "))
            elif d == "ssh":
                cmd = "ssh " + ip
                result = subprocess.run(cmd.split(" "))
            elif d == "update":
                cmd = "ssh " + ip + " export TERM=xterm && /opt/smartlife/bin/updatecmd.py"
                result = subprocess.run(cmd.split(" "))
                input("Enter drücken")
            elif d == "cron_start":
                cmd = "ssh " + ip + " export TERM=xterm && sudo /opt/smartlife/bin/croncmd.py"
                result = subprocess.run(cmd.split(" "))
            elif d == "cron_stop":
                cmd = "ssh " + ip + " export TERM=xterm && sudo /opt/smartlife/bin/croncmd.py stop"
                result = subprocess.run(cmd.split(" "))
            else:
                print(i)
                print(ip)
                res = input("Enter drücken")

while True:
    screen.clear()
    screen.logo()
    screen.host()

    screen.entries = []
    for server in cfg.cfg["server"]:
        screen.entries.append({"l": "Host: "+server["host"], "d": server["ip"]})
    screen.entries.append({"l": "Netzwerk Ping", "d": "ping"})
    screen.entries.append({"l": "Push Config", "d": "push"})
    screen.entries.append({"l": "Server hinzufügen", "d": "h"})

    res = screen.showmenu()

    if res.lower() == "x":
        sys.exit()
    try:
        i = int(res)
    except:
        i = -1
    if i > 0 and i <= len(screen.entries):
        print()
        d = screen.entries[i-1]["d"]
        if d == "h":
            new_host = input(" Neuer Hostname: ")
            new_ip = input(" IP-Adresse: ")
            new_sonoff = input(" SonOff: ")
            if new_host != "" and new_ip != "":
                cfg.cfg["server"].append({"host": new_host, "ip": new_ip, "sonoff": new_sonoff})
                cfg.save() 
                cmd = "ssh -o Batchmode=yes -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null " + new_ip + " >/dev/null 2>&1"
                result = subprocess.run(cmd.split(" "),  stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                if result.returncode == 255: #need ssh key exchange
                    if not os.path.isfile("/home/pi/.ssh/id_rsa.pub"): #need keygenerated
                        print("eine Taste drücken")
                        cmd = "ssh-keygen -t rsa -b 4096 -C \"pi@local\""
                        result = subprocess.run(cmd.split(" "),  stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                    cmd = "ssh-copy-id " + new_ip
                    result = subprocess.run(cmd.split(" "))
        elif d == "push":
            for server in cfg.cfg["server"]:
                hostname = socket.gethostname()
                try:
                    ip = socket.gethostbyname(hostname+".dyn.lan")
                except:
                    ip = ""
                if ip != server["ip"]:
                    for file in files_to_push:
                        cmd = "scp " + base_path + file + " " + server["ip"] + ":" + base_path + file
                        subprocess.run(cmd.split(" "),  stdout=subprocess.PIPE)
            input("Konfiguration kopiert")
        elif d == "ping":
            for server in cfg.cfg["server"]:
                cmd = "ping -c 3 " + server["ip"]
                ret = subprocess.run(cmd.split(" "), stdout=subprocess.PIPE,  stderr=subprocess.PIPE)
                lines = ret.stdout.decode().split("\n")
                print(" "+lines[0].split(" ")[1]+" "+" ".join(lines[-3].split(" ")[-5:]))
            input("\nEnter drücken")
        else:
            if "sonoff" in cfg.cfg["server"][i-1]:
                sonoff = cfg.cfg["server"][i-1]["sonoff"]
            else:
                sonoff = ""
            server_menu(i-1, cfg.cfg["server"][i-1]["host"], screen.entries[i-1]["d"], sonoff)
