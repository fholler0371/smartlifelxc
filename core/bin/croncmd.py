#!/opt/smartlife/.env/bin/python

#smartlife 4.0
#2021-10

import sys

from crontab import CronTab

base_path = "/opt/smartlife/"

sys.path.append(base_path + "lib")

disable=False
if len(sys.argv) > 1:
    if sys.argv[1] == "stop":
        disable = True

cron = CronTab(user="root")

found = False

iter = cron.find_comment("reboot appwatch")
for _job in iter:
    if _job.is_enabled() == disable:
        _job.enable(not disable)
        cron.write()
    found = True

if not found:
    job = cron.new(command= base_path + "bin/appwatch.py")
    job.comment = "reboot appwatch"
    job.every_reboot()
    cron.write()

found = False

iter = cron.find_comment("5min appwatch")
for _job in iter:
    if _job.is_enabled() == disable:
        _job.enable(not disable)
        cron.write()
    found = True

if not found:
    job = cron.new(command= base_path + "bin/appwatch.py")
    job.comment = "5min appwatch"
    job.minute.every(5)
    cron.write()
