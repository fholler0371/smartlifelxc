#!/opt/smartlife/.env/bin/python

#smartlife 4.0
#2021-10

import sys
import os
import subprocess

base_path = "/opt/smartlife/"

sys.path.append(base_path + "lib")

from screencmd import screen

screen = screen()
while True:
    screen.clear()
    screen.logo()
    screen.host()

    screen.entries = []
#    screen.entries.append({"l": "Set Cron", "c": "sudo /opt/smartlife/.env/bin/python /opt/smartlife/bin/cronjob.py"})

    res = screen.showmenu()

    if res.lower() == "x":
        sys.exit()
    try:
        i = int(res)
    except:
        i = -1
    if i > 0 and i <= len(screen.entries):
        print()
        if "d" in screen.entries[i-1]:
            if "shell" == screen.entries[i-1]["d"]:
                input("Enter drücken")
        else:
            cmd = screen.entries[i-1]["c"]
            result = subprocess.run(cmd.split(" "))
            input("Enter drücken")