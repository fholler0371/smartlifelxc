import sys
import os

script="""
[Unit]
Description=SmartLife_Webserver
After=network.target

[Service]
ExecStart=%path%/env/bin/python3 -u %path%/smartlife.py
WorkingDirectory=%path%
StandardOutput=inherit
StandardError=inherit
Restart=always
User=pi

[Install]
WantedBy=multi-user.target
"""

def _enable():
    base_path = "/".join(os.path.abspath(__file__).split("/")[:-2])
    os.popen("sudo rm " + base_path + "/smartlife.py 2> /dev/null").read()
    os.popen("ln -s " + base_path + "/bin/smartlife.py " + base_path + "/smartlife.py 2> /dev/null").read()
    os.popen("sudo rm " + base_path + "/service.tmp 2> /dev/null").read()
    f = open(base_path + "/service.tmp", "w")
    f.write(script.replace("%path%", base_path))
    f.close()
    os.popen("sudo chmod 755 " + base_path + "/service.tmp").read()
    os.popen("sudo chown root:root " + base_path + "/service.tmp").read()
    os.popen("sudo cp " + base_path + "/service.tmp /etc/systemd/system/smartlifelxc_www.service").read()
    os.popen("sudo rm " + base_path + "/service.tmp 2> /dev/null").read()
    os.popen("sudo systemctl enable smartlifelxc_www.service").read()

def _disable():
    os.popen("sudo systemctl disable smartlifelxc_www.service").read()

def _start():
    os.popen("sudo systemctl start smartlifelxc_www.service").read()

def _stop():
    os.popen("sudo systemctl stop smartlifelxc_www.service").read()

def _restart():
    os.popen("sudo systemctl restart smartlifelxc_www.service").read()

def main(mode):
    if mode == "enable":
        _enable()
    elif mode == "disable":
        _enable()
    elif mode == "start":
        _start()
    elif mode == "stop":
        _stop()
    elif mode == "restart":
        _restart()
    else:
        print(mode)

if __name__ == "__main__":
    if len(sys.argv) == 2:
        main(sys.argv[1])
