#!/bin/bash

apt update
apt upgrade -y

ln -fs /usr/share/zoneinfo/Europe/Berlin /etc/localtime
dpkg-reconfigure -f noninteractive tzdata

apt install git python3 python3-pip -y

rm /etc/rc.local

pip3 install virtualenv

cd /home

virtualenv /home/.env

/home/.env/bin/pip install paho-mqtt

rm -R /home/.gitlab
mkdir /home/.gitlab
cd /home/.gitlab

git clone https://gitlab.com/fholler0371/smartlifelxc.git

rm /home/smartlife.py
ln -s /home/.gitlab/smartlifelxc/core/smartlife.py /home/smartlife.py

cp /home/.gitlab/smartlifelxc/core/bin/start_script.sh /etc/rc.local
chmod +x /etc/rc.local

halt

exit 0
