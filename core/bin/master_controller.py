import os
import json
import signal
import sys
import time
from http.server import HTTPServer, BaseHTTPRequestHandler
from socketserver import ThreadingMixIn
from functools import partial
from threading import Thread
from urllib import request

try:
    import paho.mqtt.client as mqtt
except:
    os.popen(sys.executable + ' -m pip install paho-mqtt --upgrade').read()
    import paho.mqtt.client as mqtt

sys.path.append("/".join(os.path.abspath(sys.path[0]).split("/")[:-1]) + "/lib")
sys.path.append("/".join(os.path.abspath(sys.path[0]).split("/")[:-1]) + "/lxbin")

import sl_logger
import lx_start

client_data = {}
root_path = "/".join(os.path.abspath(__file__).split("/")[:-2])

f = open(root_path + "/config/master.json")
sl = {"cfg": json.loads(f.read()), "running": True}
f.close()

sl["cfg"]['basepath'] = root_path

sl["cfg"]['name'] = os.path.abspath(__file__).split("/")[-1].split(".")[0]
os.popen("mkdir -p " + sl["cfg"]['basepath'] + "/log").read()

sl["log"] = sl_logger.create(sl, sl["cfg"]['basepath'] + "/log")

client = None
connected = False
subscribtions = []

running = True
def receive_signal(signum, stack):
    global sl
    if sl:
        sl["log"].debug('Received: '+str(signum))
        sl["running"] = False
signal.signal(signal.SIGTERM, receive_signal)

##################### webserver #########################

wserver = None

class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
     """Handle requests in a separate thread."""

class webserverHandler(BaseHTTPRequestHandler):
    def __init__(self, sl, cb, *args, **kwargs):
        self.sl = sl
        self.cb = cb
        super().__init__(*args, **kwargs)
    def log_message(self, format, *args):
        self.sl["log"].debug("\n    %s - - [%s] %s" % (self.address_string(), self.log_date_time_string(),format%args))
    def do_POST(self):
        try:
            content_len = int(self.headers['Content-Length'])
            data = json.loads(self.rfile.read(content_len).decode())
            data, code = self.cb(self.path, data)
            self.send_response(code)
            try:
               x = json.loads(data.decode())
               self.send_header('Content-Type', 'application/json')
            except:
               self.send_header('Content-Type', 'application/javascript')
            self.send_header('Content-Length', str(len(data)))
            self.end_headers()
            self.wfile.write(data)
        except:
            self.send_response(501)
            self.send_header('Content-Type', 'text')
            self.end_headers()
            self.wfile.write("Error 501".encode())

class loopThread(Thread):
    def __init__(self, server):
        Thread.__init__(self)
        self.server = server
    def run(self):
        self.server.serve_forever()

def app(path, data):
    global client_data
    p = path.split("/")
    modul = p[1]
    del p[1]
    p = "/".join(p)
    if modul in client_data:
        req =  request.Request("http://" + client_data[modul] + p, data=json.dumps(data).encode())
        resp = request.urlopen(req)
        if resp.code == 200:
            return [resp.read(), 200]
        else:
            return [{"Modul":"fehler"}, 404]
    else:
        return [{"Modul":"nicht vorhanden"}, 416]

def start_web():
    global sl, wserver
    handler = partial(webserverHandler, sl, app)
    wserver = ThreadedHTTPServer(('0.0.0.0', 9999), handler)
    th = loopThread(wserver)
    th.start()

def stop_web():
    global wserver
    wserver.shutdown()


##################### mqtt ##############################

def on_connect(client, userdata, flags, rc):
    global sl, connected, subscribtions
    log = sl["log"]
    t = int(time.time())
    if rc == 0:
        connected = True
        for topic in subscribtions:
            client.subscribe(topic)
    else:
        connected = False
        log.error("Keine Verbindung zum MQTT-Server "+str(rc))

def on_disconnect(client, userdata, rc):
    global connected
    connected = False
    if rc != 0:
        client.reconnect()

def on_message(client, userdata, msg):
    global client_data, sl
    c_count = len(sl["cfg"]["container"])
    l = 0
    a_topic = msg.topic.split("/")
    while l < c_count:
        container = sl["cfg"]["container"][l]
        if container["mode"] > -1:
            a_con_topic = container["watch"]["topic"].split("/")
            t_count = len(a_con_topic)
            i = 0
            while i < t_count:
                try:
                    if a_con_topic[i] == "+":
                        a_con_topic[i] = a_topic[i]
                except:
                    pass
                i += 1
            new_topic = "/".join(a_con_topic)
            if msg.topic == new_topic:
                sl["cfg"]["container"][l]["last"] = time.time()
                sl["cfg"]["container"][l]["error"] = 0
        l += 1
    if msg.topic.find("/status/_status/ip") > 0:
        _ip = json.loads(msg.payload.decode())["val"]
        _client = msg.topic.split("/")[1]
        client_data[_client] = _ip

def subscribe(topic):
    global subscribtions, connected, client
    subscribtions.append(topic)
    if connected:
        client.subscribe(topic)

####################################################################

def main():
    global client, sl
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_disconnect = on_disconnect
    client.on_message = on_message
    subscribe("sl/+/status/_status/ip")
    client.connect("127.0.0.1", 1883, 60)
    sl["log"].info("start master controller")
    start_web()
    c_count = len(sl["cfg"]["container"])
    l = 0
    while l < c_count:
        container = sl["cfg"]["container"][l]
        sl["cfg"]["container"][l]["error"] = 0
        cpath = sl["cfg"]['basepath'] + "/container/" + container["name"] + ".json"
        try:
            f = open(cpath, "r")
            ccfg = json.loads(f.read())
            f.close()
        except:
            sl["log"].error("could not open cfg: "+cpath)
            sys.exit()
        if not(ccfg["development"]):
            sl["cfg"]["container"][l]["watch"] = ccfg["watch"]
            if ccfg["one_shot"]:
                sl["cfg"]["container"][l]["mode"] = 1
            else:
                sl["cfg"]["container"][l]["mode"] = 0
                sl["cfg"]["container"][l]["last"] = time.time()
                subscribe(container["watch"]["topic"])
                sl["log"].info("start:  "+container["name"])
                lx_start.start(container["name"], restart=False)
        else:
            sl["cfg"]["container"][l]["mode"] = -1
        l += 1

    client.loop_start()

    while sl["running"]:
        time.sleep(1)
        l = 0
        while l < c_count:
            container = sl["cfg"]["container"][l]
            if container["mode"] == 0:
                if container["last"] < time.time() - container["watch"]["intervall"]:
                    sl["cfg"]["container"][l]["error"] += 1
                    sl["cfg"]["container"][l]["last"] = time.time()
                    sl["log"].error("restart:  "+container["name"])
                    lx_start.start(container["name"], restart=True)
                    if container["error"] > 3:
                        os.popen("echo 'cannot restart: " + container["name"] + "' | mail -s 'Fehler Master Controller' pi").read()
                        sl["running"] = False
            l += 1

    client.loop_stop()
    stop_web()
    sl["log"].info("stop master controller")

if __name__ == "__main__":
    main()
