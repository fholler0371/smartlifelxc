import os
import sys
sys.path.insert(0, os.path.dirname(os.path.abspath(__file__))+"/lib")

import smartlife_ as smartlife

if __name__ == "__main__":
    smartlife.run()
