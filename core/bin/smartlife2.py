# smartlife
# 04.2021

import sys
import os


cmd = False
first_run = False
reset_admin = False

base_path = '/'.join(os.path.abspath(__file__).split('/')[:-1])

sys.path.append(base_path+"/lib")
sys.path.append(base_path+"/plugins")



if len(sys.argv) > 1:
    cmd = sys.argv[1]=="cmd"
    first_run = sys.argv[1]=="first_run"
    reset_admin = sys.argv[1]=="reset_admin"
    
if first_run or reset_admin:
    from authentication.first_run import run
    run(base_path, reset_admin)
    os._exit(0)

from sl2 import run
run(base_path, cmd)
