#!/opt/smartlife/.env/bin/python

#smartlife 4.0
#2021-10

import sys
import os
import subprocess

base_path = "/opt/smartlife/"

sys.path.append(base_path + "lib")

f = open(base_path + "log/appwatch", "w")
f.write("test")
f.close()

if __name__ == '__main__':
    if len(sys.argv) > 1:
        if sys.argv[1] == "start":
            app = sys.argv[2]
            app_path = base_path + "app/" + app + "/main.py"
            if os.path.isfile(app_path):
                cmd = base_path + ".env/bin/python " + app_path + " cmd"
                subprocess.run(cmd.split(" "))
            else:
                print("app not found:", app)
