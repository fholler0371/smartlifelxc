import os
import urllib.request

root = "/".join(os.path.abspath(__file__).split("/")[:-2])

p = root + "/www/lib/require/require.js"
with urllib.request.urlopen("https://requirejs.org/docs/release/2.3.6/minified/require.js") as response:
    f = open(p, "wb")
    f.write(response.read())
    f.close()

p = root + "/www/lib/jqwidgets/jqwidgets.zip"
with urllib.request.urlopen("https://github.com/jqwidgets/jQWidgets/archive/master.zip") as response:
    f = open(p, "wb")
    f.write(response.read())
    f.close()
p = root + "/www/lib/jqwidgets"
os.popen("cd "+p+" ; unzip jqwidgets.zip jQWidgets-master/jqwidgets/* ; rm jqwidgets.zip ; cp -f -r jQWidgets-master/jqwidgets/* ./ ; rm -r jQWidgets-master").read()

p = root + "/www/lib/jquery/jquery-3.5.1.min.js"
with urllib.request.urlopen("https://code.jquery.com/jquery-3.5.1.min.js") as response:
    f = open(p, "wb")
    f.write(response.read())
    f.close()

print(root)

