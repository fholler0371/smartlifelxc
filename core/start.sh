#!/bin/bash

INSTALL_DIR="/opt/smartlife"

pushd "${INSTALL_DIR}" >/dev/null 2>&1

.env/bin/python smartlife.py cmd

popd >/dev/null 2>&1
