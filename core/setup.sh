#!/bin/bash

show_logo() {
    clear
    printf "   _____                      _      _     _  __\n"
    printf "  / ____|                    | |    | |   (_)/ _|\n"
    printf " | (___  _ __ ___   __ _ _ __| |_   | |    _| |_ __\n"
    printf "  \___ \| '_ \` _ \ / _\` | '__| __|  | |   | |  _/ _ \ \n"
    printf "  ____) | | | | | | (_| | |  | |_   | |___| | ||  __/\n"
    printf " |_____/|_| |_| |_|\__,_|_|  \___|  |_____|_|_| \___|\n\n"
}

show_menu() {
    normal=`echo "\033[m"`
    menu=`echo "\033[36m"` #Blue
    number=`echo "\033[33m"` #yellow
    bgred=`echo "\033[41m"`
    fgred=`echo "\033[31m"`
    printf "\n${menu}**************************************************${normal}\n"
    printf "${menu}**${number} 1)${menu} Main (with webserver)                     ** ${normal}\n"
    printf "${menu}**${number} 2)${menu} Sub  (without webserver)                  ** ${normal}\n"
    printf "${menu}**${number} 3)${menu} Update                                    ** ${normal}\n"
    printf "${menu}**${number}   ${menu}                                           ** ${normal}\n"
    printf "${menu}**${number} 4)${menu} SmartLife 4.0                             ** ${normal}\n"
    printf "${menu}**${number}   ${menu}                                           ** ${normal}\n"
    printf "${menu}**${number} 5)${menu} Setup LXC                                 ** ${normal}\n"
    printf "${menu}**${number} 6)${menu} Setup LXC - Module                        ** ${normal}\n"
    printf "${menu}**${number}   ${menu}                                           ** ${normal}\n"
    printf "${menu}**${number} 7)${menu} Setup Nginx Proxy                         ** ${normal}\n"
    printf "${menu}**************************************************${normal}\n"
    printf "Please enter a menu option and enter or ${fgred}x to exit. ${normal}"
    read opt
}

INSTALL_DIR="/opt/smartlife"
GithttpsUrl="https://gitlab.com/fholler0371/smartlifelxc.git"
GithttpsUrlEx="https://gitlab.com/fholler0371/smartlifelxc_external.git"
LOCAL_GIT="${INSTALL_DIR}/.gitlab"
LOCAL_REPO="${LOCAL_GIT}/smartlifelxc"
LOCAL_REPO_EX="${LOCAL_GIT}/smartlifelxc_external"
CFG_MASTER="default_master.yaml"
CFG_SUB="default_sub.yaml"

COL_NC='\e[0m' # No Color
COL_LIGHT_GREEN='\e[1;32m'
COL_LIGHT_RED='\e[1;31m'
TICK="[${COL_LIGHT_GREEN}✓${COL_NC}]"
CROSS="[${COL_LIGHT_RED}✗${COL_NC}]"
INFO="[i]"
OVER="\\r\\033[K"

is_command() {
  local check_command="$1"
  command -v "${check_command}" >/dev/null 2>&1
}

make_repo() {
    mkdir -p "${LOCAL_GIT}"
    local directory="${1}"
    sudo rm "${directory}" &> /dev/null
    local remoteRepo="${2}"
    str="Clone ${remoteRepo} into ${directory}"
    printf " %b %s..." "${INFO}" "${str}"
    if [[ -d "${directory}" ]]; then
        str="Unable to clone ${remoteRepo} into ${directory} : Directory already exists"
        printf "%b %b%s\\n" "${OVER}" "${CROSS}" "${str}"
        return 1
    fi
    pushd "${LOCAL_GIT}" &> /dev/null
    git clone -q --depth 20 "${remoteRepo}" &> /dev/null || return $?
    popd  &> /dev/null
    pushd "${directory}" &> /dev/null || return 1
    curBranch=$(git rev-parse --abbrev-ref HEAD)
    printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
    chmod -R a+rX "${directory}"
    popd &> /dev/null || return 1
    return 0
}

update_repo() {
    local directory="${1}"
    local curBranch
    local str="Update repo in ${1}"
    pushd "${directory}" &> /dev/null || return 1
    printf " %b %s..." "${INFO}" "${str}"
    git stash --quiet &> /dev/null || true # Okay for stash failure
    git clean --quiet --force -d || true # Okay for already clean directory
    git pull --quiet &> /dev/null || return $?
    curBranch=$(git rev-parse --abbrev-ref HEAD)
    printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
    chmod -R a+rX "${directory}"
    popd &> /dev/null || return 1
    return 0
}

is_repo() {
    local directory="${1}"
    local rc
    if [[ -d "${directory}" ]]; then
        pushd "${directory}" &> /dev/null || return 1
        git status --short &> /dev/null || rc=$?
    else
        rc=1
    fi
    popd &> /dev/null || return 1
    return "${rc:-0}"
}

getGitFiles() {
  local directory="${1}"
  local remoteRepo="${2}"
  local str="Check for existing repository in ${1}"
  printf " %b %s..." "${INFO}" "${str}"
  if is_repo "${directory}"; then
    printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
    update_repo "${directory}" || { printf "\\n  %b: Could not update local repository. Contact support.%b\\n" "${COL_LIGHT_RED}" "${COL_NC}"; exit 1; }
  else
    printf "%b %b %s\\n" "${OVER}" "${CROSS}" "${str}"
    make_repo "${directory}" "${remoteRepo}" || { printf "\\n %bError: Could not update local repository. Contact support.%b\\n" "${COL_LIGHT_RED}" "${COL_NC}"; exit 1; }
  fi
}

download_from_gitlab() {
  printf "\n"
  str="check for git"
  printf " [ ] %s" "${str}"
  if is_command git ; then
    printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
  else
    str="install git"
    printf "%b [ ] %s" "${OVER}" "${str}"
    sudo apt-get install git &> /dev/null
    if is_command git ; then
      printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
    else
      printf "%b %b %s\\n" "${OVER}" "${CROSS}" "${str}"
      printf " %b Aborting installation: Dependencies could not be installed.\\n" "${CROSS}"
      exit 1
    fi
  fi
  sudo mkdir -p "${INSTALL_DIR}"
  sudo chown pi:pi "${INSTALL_DIR}"
  getGitFiles "${LOCAL_REPO}" "${GithttpsUrl}"
  getGitFiles "${LOCAL_REPO_EX}" "${GithttpsUrlEx}"
}

check_python() {
  str="check for python"
  printf " [ ] %s" "${str}"
  if is_command python3 ; then
    printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
  else
    str="install python3"
    printf "%b [ ] %s" "${OVER}" "${str}"
    sudo apt-get install python3 --fix-missing -y &> /dev/null
    if is_command python3 ; then
      printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
    else
      printf "%b %b %s\\n" "${OVER}" "${CROSS}" "${str}"
      printf " %b Aborting installation: Dependencies could not be installed.\\n" "${CROSS}"
      exit 1
    fi
  fi
  PYTHON_VERSION=`python3 --version | awk '{print $2}'`
  PYTHON_VERSION2="${PYTHON_VERSION:0:3}"
  PARSED_VERSION=$(echo "${PYTHON_VERSION2//./}")
  if [[ "$PARSED_VERSION" -gt "36" ]]; then
    str="version python"
    printf " %b %s %s\\n" "${TICK}" "${str}" "${PYTHON_VERSION2}"
  else
    str="need python 3.7"
    printf " %b %s\\n" "${CROSS}" "${str}"
    exit 1
  fi
  str="check for pip"
  printf " [ ] %s" "${str}"
  if is_command pip3 ; then
    printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
  else
    str="install pip"
    printf "%b [ ] %s" "${OVER}" "${str}"
    sudo apt-get install python3-pip --fix-missing -y &> /dev/null
    if is_command pip3 ; then
      printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
    else
      printf "%b %b %s\\n" "${OVER}" "${CROSS}" "${str}"
      printf " %b Aborting installation: Dependencies could not be installed.\\n" "${CROSS}"
      exit 1
    fi
  fi
  str="create/update virtuel enviroment"
  printf " [ ] %s" "${str}"
  sudo apt update &> /dev/null
  sudo apt install python3-venv --fix-missing -y &> /dev/null
  python3 -m venv "${INSTALL_DIR}/.env"
  printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
}

sh_links() {
  local file="${1}"
  str="link: ${file}"
  printf " [ ] %s" "${str}"
  rm "${INSTALL_DIR}/${file}" &> /dev/null
  ln -s "${LOCAL_REPO}/core/${file}" "${INSTALL_DIR}/${file}" &> /dev/null
  chmod +x "${INSTALL_DIR}/${file}" &> /dev/null
  printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
}

create_dir() {
  local dir="${1}"
  str="directory: ${dir}"
  mkdir -p "${INSTALL_DIR}/${dir}" &> /dev/null
  printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
}

make_links () {
  sh_links setup.sh
  sh_links git_commit.sh
  sh_links start.sh
  sh_links admin_reset.sh
  str="link: smartlife.py"
  printf " [ ] %s" "${str}"
  rm "${INSTALL_DIR}/smartlife.py" &> /dev/null
  ln -s "${LOCAL_REPO}/core/bin/smartlife2.py" "${INSTALL_DIR}/smartlife.py" &> /dev/null
  printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
  str="link: bin"
  printf " [ ] %s" "${str} ..."
  rm "${INSTALL_DIR}/bin" &> /dev/null
  ln -s "${LOCAL_REPO}/core/bin" "${INSTALL_DIR}/bin" &> /dev/null
  chmod +x "${LOCAL_REPO}/core/bin/menucmd.py"
  chmod +x "${LOCAL_REPO}/core/bin/servercmd.py"
  chmod +x "${LOCAL_REPO}/core/bin/updatecmd.py"
  printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
  str="link: lib"
  printf " [ ] %s" "${str}"
  rm "${INSTALL_DIR}/lib" &> /dev/null
  ln -s "${LOCAL_REPO}/core/lib" "${INSTALL_DIR}/lib" &> /dev/null
  printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
  str="link: plugins"
  printf " [ ] %s" "${str}"
  rm "${INSTALL_DIR}/plugins" &> /dev/null
  ln -s "${LOCAL_REPO}/plugins" "${INSTALL_DIR}/plugins" &> /dev/null
  printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
  str="link: lxcontainer"
  printf " [ ] %s" "${str}"
  rm "${INSTALL_DIR}/lxcontainer" &> /dev/null
  ln -s "${LOCAL_REPO}/lxcontainer" "${INSTALL_DIR}/lxcontainer" &> /dev/null
  printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
  str="link: www"
  printf " [ ] %s" "${str}"
  rm "${INSTALL_DIR}/www" &> /dev/null
  ln -s "${LOCAL_REPO}/core/www" "${INSTALL_DIR}/www" &> /dev/null
  printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
  str="link: require"
  printf " [ ] %s" "${str}"
  rm "${INSTALL_DIR}/www/lib/require/require.js" &> /dev/null
  ln -s "${LOCAL_REPO_EX}/require/require.js" "${INSTALL_DIR}/www/lib/require/require.js" &> /dev/null
  printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
  str="link: jquery"
  printf " [ ] %s" "${str}"
  rm "${INSTALL_DIR}/www/lib/jquery/jquery.min.js" &> /dev/null
  ln -s "${LOCAL_REPO_EX}/jquery/jquery-3.6.0.min.js" "${INSTALL_DIR}/www/lib/jquery/jquery.min.js" &> /dev/null
  printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
  str="link: jqwidgets"
  printf " [ ] %s" "${str}"
  rm "${INSTALL_DIR}/www/lib/jqwidgets" &> /dev/null
  ln -s "${LOCAL_REPO_EX}/jqwidgets" "${INSTALL_DIR}/www/lib/jqwidgets" &> /dev/null
  printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
  str="link: packery"
  printf " [ ] %s" "${str}"
  rm -R "${INSTALL_DIR}/www/lib/packery" &> /dev/null
  ln -s "${LOCAL_REPO_EX}/packery" "${INSTALL_DIR}/www/lib/packery" &> /dev/null
  printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
  str="link: leaflet"
  printf " [ ] %s" "${str}"
  rm -R "${INSTALL_DIR}/www/lib/leaflet" &> /dev/null
  ln -s "${LOCAL_REPO_EX}/leaflet" "${INSTALL_DIR}/www/lib/leaflet" &> /dev/null
  printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
  str="link: Highcharts"
  printf " [ ] %s" "${str}"
  rm -R "${INSTALL_DIR}/www/lib/highcharts" &> /dev/null
  ln -s "${LOCAL_REPO_EX}/highcharts" "${INSTALL_DIR}/www/lib/highcharts" &> /dev/null
  printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
  str="link: Moments"
  printf " [ ] %s" "${str}"
  rm -R "${INSTALL_DIR}/www/lib/moment" &> /dev/null
  ln -s "${LOCAL_REPO_EX}/moment" "${INSTALL_DIR}/www/lib/moment" &> /dev/null
  printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
  str="link: Weather Icons"
  printf " [ ] %s" "${str}"
  rm -R "${INSTALL_DIR}/www/lib/img/weather" &> /dev/null
  ln -s "${LOCAL_REPO_EX}/weather_icons" "${INSTALL_DIR}/www/lib/img/weather" &> /dev/null
  printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
  create_dir config
  create_dir log
  create_dir db
}

load_requierements() {
  str="python requierements"
  printf " [ ] %s" "${str}"
  "${INSTALL_DIR}/.env/bin/pip" install -r "${LOCAL_REPO}/core/requirements.txt" &> /dev/null
  printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
}

check_config() {
  need="${1}"
  has="0"
  str="check config"
  folder="${INSTALL_DIR}/config/"
  printf folder
  printf " [ ] %s" "${str}"
  res="${TICK}"
  if [[ -f "${folder}${CFG_MASTER}" ]]; then
    has="1"
    str="config: ${CFG_MASTER}"
    if [ "${need}" = "2" ]; then
      res="${CROSS}"
    fi
  fi
  if [[ -f "${folder}${CFG_SUB}" ]]; then
    has="2"
    str="config: ${CFG_SUB}"
    if [ "${need}" = "1" ]; then
      res="${CROSS}"
    fi
  fi
  if [ "${need}" = "0" ]; then
    if [ "${has}" = "0" ]; then
      res="${CROSS}"
    fi
  fi
  printf "%b %b %s\\n" "${OVER}" "${res}" "${str}"
  if [ "${CROSS}" = "${res}" ]; then
    printf " %b Aborting installation: Wrong default configuration.\\n" "${CROSS}"
    exit 1
  fi
}

copy_config() {
  if [ "${1}" = "1" ]; then
    file="${CFG_MASTER}"
  fi
  if [ "${1}" = "2" ]; then
    file="${CFG_SUB}"
  fi
  str="copy default config: ${file}"
  printf " [ ] %s" "${str}"
  cp "${LOCAL_REPO}/core/${file}" "${INSTALL_DIR}/config/${file}" &> /dev/null
  printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
}

install_sytemend() {
  str="systemend"
  printf " [ ] %s" "${str}"
  sudo systemctl stop smartlife &> /dev/null
  sudo systemctl disable smartlife &> /dev/null
  sudo cp "${LOCAL_REPO}/smartlife.service" /etc/systemd/system/smartlife.service &> /dev/null
  sudo systemctl enable smartlife &> /dev/null
  sudo systemctl start smartlife &> /dev/null
  printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
}

install_lxc() {
  str="check for lxc"
  printf " [ ] %s" "${str}"
  if is_command lxc-ls ; then
    printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
  else
    str="install lxc"
    printf "%b [ ] %s" "${OVER}" "${str}"
    sudo apt-get install lxc bridge-utils --fix-missing -y &> /dev/null
    if is_command lxc-ls ; then
      printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
    else
      printf "%b %b %s\\n" "${OVER}" "${CROSS}" "${str}"
      printf " %b Aborting installation: Dependencies could not be installed.\\n" "${CROSS}"
      exit 1
    fi
  fi
}

setup_lxc_net() {
  str="setup lxc-net"
  printf " [ ] %s" "${str}"
  sudo systemctl stop smartlife &> /dev/null
  sudo systemctl disable smartlife &> /dev/null
  sudo cp "${LOCAL_REPO}/lxc-net" /etc/default/lxc-net &> /dev/null
  sudo systemctl enable lxc_net &> /dev/null
  sudo systemctl start lxc_net &> /dev/null
  sudo systemctl restart lxc_net &> /dev/null
  printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
}

get_lxc_master() {
  str="get lxc master debian buster"
  printf " [ ] %s" "${str}"
  sudo lxc-create -n master -P "${INSTALL_DIR}/lxc_images" -t /usr/share/lxc/templates/lxc-download  -- -d debian -r buster -a armhf --no-validate
  printf "%b %b %s\\n" "${OVER}" "${TICK}" "${str}"
}

call_lxc_setup() {
  "${INSTALL_DIR}/.env/bin/python" "${INSTALL_DIR}/lib/lxc_tools.py" setup
}

call_ngix_setup() {
  sudo "${INSTALL_DIR}/.env/bin/python" "${INSTALL_DIR}/lib/proxy_tools.py" setup
}

first_run() {
  "${INSTALL_DIR}/.env/bin/python3" "${INSTALL_DIR}/smartlife.py" first_run
}

opt="0"

while [ $opt != '' ]
    do
    if [ $opt = '' ]; then
      exit;
    else
      case $opt in
        0)
            show_logo
            show_menu
        ;;
        1)
            printf "\nMain installation selected";
            download_from_gitlab;
            check_python;
            make_links;
            load_requierements;
            check_config 1;
            copy_config 1;
            first_run;
            install_sytemend;
            printf "\nHit Key";
            read test
            exit;
        ;;
        2)
            printf "\nSub installation selected";
            download_from_gitlab;
            check_python;
            make_links;
            load_requierements;
            check_config 2;
            copy_config 2;
            install_sytemend;
            printf "\nHit Key";
            read test
            exit;
        ;;
        3)
            printf "\nUpdate selected";
            download_from_gitlab;
            check_python;
            make_links;
            load_requierements;
            check_config 0;
            copy_config "${has}"
            install_sytemend;
            printf "\nHit Key";
            read test
            exit;
        ;;
        4)
            /opt/smartlife/bin/menucmd.py;
            exit;
        ;;
        5)
            printf "\nLXC Setup\n";
            install_lxc;
            create_dir lxc_images;
            setup_lxc_net;
            get_lxc_master;
            printf "\nHit Key";
            read test
            exit;
        ;;
        6)
            show_logo;
            call_lxc_setup;
            printf "\nHit Key";
            read test
            exit;
        ;;
        7)
            printf "\nNginx and LetsEncrypt\n";
            call_ngix_setup;
            printf "\nHit Key";
            read test
            exit;
        ;;
        x)exit;
        ;;
        \n)exit;
        ;;
        *)
            printf "\nPick an option from the menu";
            read test
            show_logo;
            show_menu;
        ;;
      esac
    fi
done

