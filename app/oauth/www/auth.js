requirejs.config({
    baseUrl: '/lib',
    paths: {
        jquery: 'jquery/jquery.min',
        jqxcore: 'jqwidgets/jqxcore',
        jqxbutton: 'jqwidgets/jqxbuttons',
        jqxscrollbar: 'jqwidgets/jqxscrollbar',
        jqxpanel: 'jqwidgets/jqxpanel',
        jqxinput: 'jqwidgets/jqxinput',
        jqxpasswordinput: 'jqwidgets/jqxpasswordinput'
    },
    shim:{
        jqxbutton : {
            deps : ['jqxcore']
        },
        jqxpasswordinput : {
            deps : ['jqxcore']
        },
        jqxinput : {
            deps : ['jqxcore']
        },
        jqxscrollbar : {
            deps : ['jqxbutton']
        },
        jqxpanel : {
            deps : ['jqxscrollbar', 'jqxbutton']
        }
    }
})

var token = ''

function postData(url = '', data = {}) {
    return fetch(url, {
        method: 'POST', 
        mode: 'cors', 
        cache: 'no-cache',
        headers: {
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + token
        },
        redirect: 'follow', 
        referrerPolicy: 'no-referrer', 
        body: JSON.stringify(data) 
    })
    .then((response) => {
        this.res = response
        if (response.status >= 200 && response.status <= 299) {
            return response.json();
        } else {
            return {'error': response.status}
        }
    }).catch((error) => {
        if (this.res) {
            if (this.res.redirected) {
                window.location.assign(this.res.url)
                return {'error': 0}
            }
        }
        return {'error': 1}
    })
}

function get(url = '') {
    return fetch(url, {
        method: 'GET', 
        mode: 'cors', 
        cache: 'no-cache',
        headers: {
            'Authorization' : 'Bearer ' + token
        },
        redirect: 'follow', 
        referrerPolicy: 'no-referrer', 
    })
    .then((response) => {
        if (response.status >= 200 && response.status <= 299) {
            try {
                return response.json()
            } catch {
                return {}
            }
        } else {
            return {'error': response.status}
        }
    }).catch((error) => {
        // Handle the error
        return {'error': 1}
    })
}

epoch = function() {
    var now = new Date()
    return Math.round(now / 1000)
}

loginPage = function() {
    requirejs(['jqxpanel', 'jqxinput', 'jqxpasswordinput', 'jqxbutton'], function() {
        $.jqx.theme = 'metro'
        var step = 1
        $('html').css('height', '100%')
        $('body').css({height: '100%', margin: 0})
        $('body').css({display: 'flex', 'justify-content': 'center', 'align-items': 'center', overflow: 'hidden'})
        $('body').append('<div id="panel"></div>')
        $('#panel').jqxPanel({height: 250, width: 400}).css('border', 'solid 1px')
        $('#panel').jqxPanel('append', '<h1 style="margin-left: 10px">SmartLife - Anmeldung</h1>')
        var table = '<table style="width:100%"><tr><td><b>Anwendung:<b></td><td style="width:100%">'
        table += auth_app + '</td></tr><tr><td style="height: 15px"> </td><td></td></tr><tr><td><b><span id="field-label">'
        table += 'Nutzer:</span></b></td><td><input type="text" id="input_text"/><input type="password" id="input_pass"/></td></tr>'
        table += '</tr><tr><td style="height: 20px"> </td><td></td></tr><tr><td></td><td>'
        table += '<input style="margin-top: 20px" value="Weiter" type="button" id="next" />'
        table += '</td></tr></table>'
        $('#panel').jqxPanel('append', table)
        $('#input_text').jqxInput({width: $('#input_text').parent().width()-30})
        $('#input_pass').jqxPasswordInput({width: $('#input_text').parent().width()-30}).hide()
        $('#next').jqxButton({ width: 120, height: 40 }).css({float: 'right', 'margin-right': '20px'})
        $('#next').on('click', function() {
            if (step == 1) {
                $('#next').jqxButton('disabled', true)
                postData('/login', {username: $('#input_text').val(), action: 'user_name', id:auth_id}).then(data => {
                    if (data.error != null) {
                        if (data.error > 0) {
                            alert("Es ist ein Fehler aufgetreten!\nSeite wird neu geladen!")
                            window.location.reload()
                        }   
                    } else {
                        if (data.step) {
                            if (data.url != null) {
                                window.location.assign(data.url)
                            }
                            if (data.user != null) {
                                sessionStorage.setItem('user', data.user)
                            }
                            $('#field-label').text('Passwort')
                            $('#input_text').hide()
                            $('#input_pass').show()
                            step = 2
                            $('#next').jqxButton('disabled', false)
                        } else {
                            console.log(data)
                            alert("Fehler bei der Anmeldung!\nSeite wird neu geladen!")
                            window.location.reload() 
                        }
                    }
                })
            }
        })
    })
}

requirejs(['jquery'], function($) {
    $('head').append('<link rel="stylesheet" href="/lib/jqwidgets/styles/jqx.base.css" type="text/css" />')
    $('head').append('<link rel="stylesheet" href="/lib/jqwidgets/styles/jqx.metro.css" type="text/css" />')
    $(document).ready(function() {
        user = sessionStorage.getItem('user')
        if (user != null) {
            var state
            sessionStorage.getItem('user')
            postData('/login', {user: user, action: 'check_user', id:auth_id}).then(data => {
                console.log(data)
                if (data.error != null) {
                    loginPage()
                } else {
                    if (data.url == "") {
                        loginPage()
                    } else {
                        window.location.assign(data.url)
                    }
                }
            })
        } else {
            loginPage()
        }
    })
})