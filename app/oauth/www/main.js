requirejs.config({
    baseUrl: '/lib',
    paths: {
        jquery: 'jquery/jquery.min'
    }
})

var token = ''

function postData(url = '', data = {}) {
    return fetch(url, {
        method: 'POST', 
        mode: 'cors', 
        cache: 'no-cache',
        headers: {
            'Content-Type': 'application/json',
            'Authorization' : 'Bearer ' + token
        },
        redirect: 'follow', 
        referrerPolicy: 'no-referrer', 
        body: JSON.stringify(data) 
    })
    .then((response) => {
        if (response.status >= 200 && response.status <= 299) {
            return response.json();
        } else {
            return {'error': response.status}
        }
    }).catch((error) => {
        // Handle the error
        return {'error': 1}
    })
}

function get(url = '') {
    return fetch(url, {
        method: 'GET', 
        mode: 'cors', 
        cache: 'no-cache',
        headers: {
            'Authorization' : 'Bearer ' + token
        },
        redirect: 'follow', 
        referrerPolicy: 'no-referrer', 
    })
    .then((response) => {
        if (response.status >= 200 && response.status <= 299) {
            try {
                return response.json()
            } catch {
                return {}
            }
        } else {
            return {'error': response.status}
        }
    }).catch((error) => {
        // Handle the error
        return {'error': 1}
    })
}

epoch = function() {
    var now = new Date()
    return Math.round(now / 1000)
}

doLogin = function() {
    $(document).ready(function() {
        postData('/api', {'action': 'get_sope'}).then(data => {
            if (data.error != null) {
                $('body').append("<h1>Sorry, something happend</h1>")
            } else {
                if (data.auth) {
                    var state = epoch()
                    sessionStorage.setItem('state', state)
                    window.location.assign(data.auth+state)
                }
                console.log(data)
            }
        })
    })
}

requirejs(['jquery'], function($) {
    if (window.location.pathname == '/callback') {
        const urlParams = new URLSearchParams(window.location.search)
        const state=urlParams.get('state')
        if (sessionStorage.getItem('state') != state) {
            sessionStorage.removeItem('state')
            window.location.assign('/')
        } else {
            postData('/api', {'action': 'check_code', 'code': urlParams.get('code')}).then(data => {
                if (data.error != null) {
                    doLogin()
                }
            })
        }   
    } else {
        doLogin()
    }
})