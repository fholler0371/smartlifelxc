# smartlife
# 10.2021

version = "2021-10"
files_lib = []
files_bin = []

import json

def wellknown(host):
    out = {"issuer" : "https://"+host}
    out["authorization_endpoint"] = "https://" + host + "/authorize"
    out["token_endpoint"] = "https://" + host + "/token"
    out["user_info"] = "https://" + host + "/userinfo"
    out["jwks_uri"] = "https://" + host + "/.well-known/jwks.json"
    out["registration_endpoint"] = "https://" + host + "/registration"
    out["scopes_supported"] = ["openid"]
    out["response_types_supported"] = ["code", "id_token", "token id_token"]
    out["subject_types_supported"] = ["pairwise", "public"]
    out["id_token_signing_alg_values_supported"] = ["RS256"]
    return json.dumps(out)