#smartlife 4.0
#2021-10

version = "2021-10"
files_lib = ["lib_oauth"]
files_bin = ["oauth/wellknown"]

import socket
import json
import os
import time
from uuid import uuid4 as uuid
from urllib.parse import urlparse, parse_qs, quote_plus
import copy

from lib_oauth import client

from oauth.wellknown import wellknown

class clients:
    def __init__(self, sl):
        self.sl = sl
        self.sl.log.debug("init")
        self.rec = []
        self.path = sl.cfg.cfg["path_config"] + "oauth_server.data"
        if os.path.isfile(self.path):
            try:
                f = open(self.path, "r")
                self.rec = json.loads(f.read())
                f.close()
            except:
                self.rec = []

    def registration(self, data):
        self.sl.log.debug("registration")
        i = 0 # check if registered already
        found = False
        while i < len(self.rec) and not found:
            if self.rec[i]["software_id"] == data["software_id"]:
                found = True
            i = i + 1
        if found:
            i = i - 1
            if "jwks" in self.rec[i]: # remove old key for safty
                del self.rec[i]["jwks"]
            self.rec[i].update(data)
        else:
            self.rec.append(data)
            i = len(self.rec) - 1
        if not("client_id" in self.rec[i]): # add clinet id
            self.rec[i]["client_id"] = str(uuid()).replace("-", "")
            self.rec[i]["client_id_issued_at"] = int(time.time())
        ok = False
        if "client_secret" in self.rec[i]:
            if "client_secret_expires_at" in self.rec[i] and (self.rec[i]["client_secret_expires_at"]-15552000) > time.time():
                ok = True
        if not found: # add client secret
            self.rec[i]["client_secret"] = (str(uuid())+str(uuid())).replace("-", "")
            self.rec[i]["client_secret_expires_at"] = int(time.time()+63072000)
        self.save()
        return self.rec[i]

    def save(self):
        self.sl.log.debug("save")
        f = open(self.path, "w")
        f.write(json.dumps(self.rec))
        f.close()

    def get_by_client_id(self, id):
        self.sl.log.debug("get_by_client_id")
        res = None
        for entry in self.rec:
            if entry["client_id"] == id:
                res = entry
        return entry    

class login_processes:
    def __init__(self, sl):
        self.sl = sl
        self.sl.log.debug("init")
        self.rec = []
        self.last = None
        self.process_id = 0

    def add(self, client, req):
        self.sl.log.debug("add")
        self.last = {"time": int(time.time()+120), "client": client, "req": req, "uuid": (str(uuid())+str(uuid())).replace("-", "")}
        self.rec.append(self.last)

    @property
    def last_id(self):
        return self.last["uuid"]

    @property
    def last_name(self):
        return self.last["client"]["client_name"]

    def cleanup_old(self):
        self.sl.log.debug("cleanup_old")
        id = -1
        l = 0
        for entry in self.rec:
            if entry["time"] < time.time():  
                id = l
            l = l + 1
        if id > -1:
            del self.rec[id]
            return True
        return False

    def get_processid_by_uuid(self, uuid):
        self.sl.log.debug("get_processid_by_uuid")
        while self.cleanup_old():
            pass
        self.process_id = -1
        l = 0
        for entry in self.rec:
            if entry["uuid"] == uuid:  
                self.process_id = l
            l = l + 1
        return self.process_id

    def get_username(self):
        return self.rec[self.process_id]["user_name"]

    def set_username(self, username):
        self.rec[self.process_id]["user_name"] = username

    user_name = property(get_username, set_username)

    @property
    def get_local(self):
        if "local" in self.rec[self.process_id]:
            return self.rec[self.process_id]["local"]
        else:
            return False

    def set_local(self, local):
        self.rec[self.process_id]["local"] = local
        if local:
            self.rec[self.process_id]["mfa"] = True
            self.rec[self.process_id]["is_login"] = True

    local = property(get_local, set_local)

    @property
    def is_login(self):
        if "is_login" in self.rec[self.process_id]:
            return self.rec[self.process_id]["is_login"]
        else:
            return False
        
    @property
    def redirect_uri_valid(self):
        return self.rec[self.process_id]["req"]["redirect_uri"][0] in self.rec[self.process_id]["client"]["redirects_uris"]

    def gen_codes(self):
        self.rec[self.process_id]["code"] = (str(uuid())+str(uuid())).replace("-", "")
        self.rec[self.process_id]["user_id"] = (str(uuid())+str(uuid())).replace("-", "")
        self.rec[self.process_id]["user_timeout"] = time.time()+108000

    @property
    def redirect_uri(self):
        return self.rec[self.process_id]["req"]["redirect_uri"][0]

    @property
    def code(self):
        return self.rec[self.process_id]["code"]

    @property
    def user_id(self):
        return self.rec[self.process_id]["user_id"]

    @property
    def state(self):
        return self.rec[self.process_id]["req"]["state"][0]

    @property
    def entry(self):
        return self.rec[self.process_id]

class active_user:
    def __init__(self, sl):
        self.sl = sl
        self.sl.log.debug("init")
        self.rec = []

    def add(self, rec):
        self.sl.log.debug("add")
        self.rec.append(copy.copy(rec))

    def cleanup_old(self):
        self.sl.log.debug("cleanup_old")
        id = -1
        l = 0
        for entry in self.rec:
            if entry["time"] < time.time():  
                id = l
            l = l + 1
        if id > -1:
            del self.rec[id]
            return True
        return False

    def userid_by_code(self, code):
        self.sl.log.debug("userid_by_code")
        while self.cleanup_old():
            pass
        self.user_id = -1
        l = 0
        for user in self.rec:
            if code == user["code"]:
                self.user_id = l
            l += 0
        return self.user_id

    def userid_by_id(self, id):
        self.sl.log.debug("userid_by_id")
        while self.cleanup_old():
            pass
        self.user_id = -1
        l = 0
        for user in self.rec:
            print(user)
            if id == user["user_id"]:
                self.user_id = l
            l += 0
        return self.user_id

    def delete(self):
        del self.rec[self.user_id]
        self.user_id = -1

    @property
    def client_id(self):
        return self.rec[self.user_id]["client"]["client_id"]

    @property
    def client_secret(self):
        return self.rec[self.user_id]["client"]["client_secret"]

    @property
    def entry(self):
        return self.rec[self.user_id]

    def uri_valid(self, uri):
        return uri in self.rec[self.user_id]["client"]["redirects_uris"]

class respond:
    def __init__(self, sl):
        self.sl = sl
        self.sl.respond = self
        self.sl.module.append(self)
        self.sl.log.info("init")
        self.clients = clients(self.sl)
        self.login_process = login_processes(self.sl)
        self.active_user = active_user(self.sl)

    def get(self, req):
        self.sl.log.debug(req.path)
        if req.path == "/.well-known/openid-configuration":
            host = req.headers["X-Host"]
            req.send_result(wellknown(host).encode(), "application/json")
            return True
        elif req.path.startswith("/authorize"):
            get_data = parse_qs(urlparse(req.path).query)
            data = self.clients.get_by_client_id(get_data["client_id"][0])
            if data == None:
                req.send_result("<h1>client unkown</h1>".encode(), "text/html")
                self.sl.log.error("client unkown")
                return True
            self.login_process.add(data, get_data)
            path = self.sl.cfg.cfg["path_app"] + "/www/index_auth.html"
            if os.path.isfile(path):
                f = open(path, "r")
                code = f.read().replace("auth_data", "var auth_id='"+self.login_process.last_id+"';var auth_app='"+self.login_process.last_name+"'")
                req.send_result(code.encode(), "text/html")
                f.close()
                return True
        elif req.path.startswith("/callback"):
            path = self.sl.cfg.cfg["path_base"] + "app/" + self.sl.cfg.cfg["app"] +  "/www/index.html"
            if os.path.isfile(path):
                f = open(path, "rb")
                req.send_result(f.read(), "text/html")
                f.close()
                return True
        elif req.path == "/":
            path = self.sl.cfg.cfg["path_base"] + "app/" + self.sl.cfg.cfg["app"] +  "/www/index.html"
            if os.path.isfile(path):
                f = open(path, "rb")
                req.send_result(f.read(), "text/html")
                f.close()
                return True
        elif req.path == "/js/main.js":
            path = self.sl.cfg.cfg["path_base"] + "app/" + self.sl.cfg.cfg["app"] +  "/www/main.js"
            if os.path.isfile(path):
                f = open(path, "rb")
                req.send_result(f.read(), "application/javascript")
                f.close()
                return True
        elif req.path == "/js/auth.js":
            path = self.sl.cfg.cfg["path_base"] + "app/" + self.sl.cfg.cfg["app"] +  "/www/auth.js"
            if os.path.isfile(path):
                f = open(path, "rb")
                req.send_result(f.read(), "application/javascript")
                f.close()
                return True
        return False

    def post(self, req):
        self.sl.log.debug(req.path)
        if req.path == "/registration":
            if req.headers["X-Forwarded-For"].startswith("192.168.") or \
               req.headers["X-Forwarded-For"] == socket.gethostbyname(self.sl.cfg.cfg["oauth_callback"].split("/")[2]):
                post_data = json.loads(req.rfile.read(int(req.headers['Content-Length'])).decode())
                req.send_result(json.dumps(self.clients.registration(post_data)).encode(), "application/json")
                return True
        elif req.path == "/token":
            ok = True
            content_length = int(req.headers['Content-Length'])
            data = parse_qs(req.rfile.read(content_length).decode())
            if "grant_type" in data and data["grant_type"][0] == "authorization_code":
                if "code" in data:
                    user_id = self.active_user.userid_by_code(data["code"][0])
                if user_id == -1:
                    ret = {"error": "invalid_grant", "error_description": "Request code invalid"}
                    req.send_result(json.dumps(ret).encode(), "application/json", 400)
                    return True
                if self.active_user.client_id != data["client_id"][0]:
                    ret = {"error": "invalid_client", "error_description": "Invalid Client ID"}
                    req.send_result(json.dumps(ret).encode(), "application/json", 400)
                    self.active_user.delete()
                    return True
                if self.active_user.client_secret != data["client_secret"][0]:
                    ret = {"error": "invalid_client", "error_description": "Invalid Client Secret"}
                    req.send_result(json.dumps(ret).encode(), "application/json", 400)
                    self.active_user.delete()
                    return True
                if not self.active_user.uri_valid(data["redirect_uri"][0]):
                    ret = {"error": "invalid_client", "error_description": "Invalid Redirect URI"}
                    req.send_result(json.dumps(ret).encode(), "application/json", 400)
                    self.active_user.delete()
                    return True
                print(self.active_user.entry)
                ret = {"error": "invalid_client", "error_description": "xxx"}
                req.send_result(json.dumps(ret).encode(), "application/json", 400)
                self.active_user.delete()
                return True
            return ok
        elif req.path == "/api":
            token = ""
            ret = {}
            for head in req.headers:
                if head.lower() == "authorization":
                    baerer = req.headers[head].split(" ")
                    if len(baerer) > 1:
                        token = baerer[1]
            
            try:
                content_length = int(req.headers['Content-Length'])
                post_data = json.loads(req.rfile.read(content_length).decode())
            except:
                post_data = {}
            if "action" in post_data and post_data["action"] == "check_code":
                return self.sl.client.get_code(post_data["code"], req)
            else:
                ret["auth"] = "response_type=code&client_id=" + self.sl.client.client_rec.client_id
                ret["auth"] += "&redirect_uri=" + quote_plus(self.sl.cfg.cfg["oauth_callback"]) + "&scope="+quote_plus("userdata")+"&state="
                ret["auth"] = self.sl.client.wellknown.authorization+"?"+ret["auth"]
                req.send_result(json.dumps(ret).encode(), "application/json")
                return True
            print(token)
            print(post_data)
        elif req.path == "/login":
            content_length = int(req.headers['Content-Length'])
            post_data = json.loads(req.rfile.read(content_length).decode())
            process_id = self.login_process.get_processid_by_uuid(post_data['id'])
            is_login = False
            res = False
            if post_data["action"] == "user_name":
                if process_id > -1:
                    self.login_process.user_name = post_data["username"]
                    if self.login_process.user_name == "admin":
                        if  (req.headers["X-Forwarded-For"].startswith("192.168.") or \
                          req.headers["X-Forwarded-For"] == socket.gethostbyname(self.sl.cfg.cfg["oauth_callback"].split("/")[2])):
                            self.login_process.local = True
                    res = True
            elif post_data["action"] == "check_user":
                user_id = self.active_user.userid_by_id(post_data["user"])
                if process_id > -1 and user_id > -1:
                    self.active_user[user_id]["req"] = copy.copy(self.login_process[process_id]["req"])
                else:
                    user_id = -1
                if user_id > -1:
                    url = self.active_user[user_id]["req"]["redirect_uri"][0] + "?code="
                    url += self.active_user[user_id]["code"] + "&state=" + self.active_user[user_id]["req"]["state"][0] 
                    req.send_result(json.dumps({"url":url}).encode(), "appliction/json") 
                else:
                    req.send_result(json.dumps({"url":""}).encode(), "appliction/json")
                return True
            else:
                return False
            if res:
                data = {'step': True}
                if self.login_process.is_login:
                    if self.login_process.redirect_uri_valid:
                        self.login_process.gen_codes()
                        url = self.login_process.redirect_uri + "?code=" + self.login_process.code + "&state=" + self.login_process.state
                        self.active_user.add(self.login_process.entry)
                        req.send_result(json.dumps({"step":-1, "url":url, "user": self.login_process.user_id}).encode(), "appliction/json")
                        return True
                req.send_result(json.dumps({'step': True, 'login': is_login}).encode(), "application/json")
            return res
        return False
