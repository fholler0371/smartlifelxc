#smartlife 4.0
#2021-10

import sys

version = "2021-10"
files_lib = ["smartlife4", "logger4", "server4"]
files_bin = ["oauth/respond", "oauth/reg_self"]

app = "oauth"
base_path = "/opt/smartlife/"
sys.path.append(base_path + "lib")
sys.path.append(base_path + "app")

from smartlife4 import smartlife
import logger4 as logger

from oauth.respond import respond
from oauth.reg_self import reg_self

if __name__ == '__main__':
    cmd = False
    if len(sys.argv) and sys.argv[1] == "cmd":
        cmd = True
    sl = smartlife(base_path, app, cmd, version, secret=True)
    sl.check_version(files_lib, files_bin)
    try:
        respond(sl)
        reg_self(sl)
    except Exception as e:
        logger.error(sl.log, e)
    try:
        sl.run()
    except Exception as e:
        logger.error(sl.log, e)
    try:
        sl.stop()
    except Exception as e:
        logger.error(sl.log, e)
    print("oauth.main", cmd)