#smartlife 4.0
#2021-10

version = "2021-10"
files_lib = ["lib_oauth", "logger4"]
files_bin = []

import lib_oauth
import logger4 as logger

from threading import Timer
class reg_self:
    def __init__(self, sl):
        self.sl = sl
        self.sl.module.append(self)
        self.sl.log.info("init")
        self.tm = None

    def register(self):
        self.sl.log.info("register") 
        try:
            self.sl.client = lib_oauth.client(self.sl)
        except Exception as e:
            logger.error(self.sl.log, e)
            self.sl.running = False

    def start(self):
        self.sl.log.info("start") 
        self.tm = Timer(2, self.register) 
        self.tm.start()
        
    def stop(self):
        self.sl.log.info("stop")
        if self.tm:
            self.tm.cancel()